#!/bin/sh
APP_NAME="MedMonitor"
DEVELOPMENT_TEAM="QKP355AHN6"
PROJECTPATH="$1/platforms/ios/$APP_NAME.xcodeproj"
WORKSPACEPATH="$1/platforms/ios/$APP_NAME.xcworkspace"
XARCHIVEPATH="$1/platforms/ios/$APP_NAME.xcarchive"
APPDISTDIR="/Users/umesh/Projects/PHPProjects/BenesalustechServer/trunk/appdist"
DESTINATIONPATH=$1
SCHEME=$APP_NAME
REMOTEUSER="www"
REMOTEHOST="benesalustech.com"
REMOTEAPPDIR="app.$REMOTEHOST/appdist"
REMOTEARCHIVEDIR="$REMOTEAPPDIR/archive"

if echo "$command" | grep -q "$SOURCE"; then
    CONFIGURATION="Release"
    EXPORTOPTIONSPLIST="$1/ExportOptionsRelease.plist"
else
    CONFIGURATION="Debug"
    EXPORTOPTIONSPLIST="$1/ExportOptionsDebug.plist"
fi

TS=$(date +%s)
echo "Date $TS"

#XCode Archive
xcodebuild -workspace $WORKSPACEPATH -scheme $APP_NAME -configuration $CONFIGURATION clean archive -archivePath $XARCHIVEPATH DEVELOPMENT_TEAM=$DEVELOPMENT_TEAM
#XCode Export to ipa
xcodebuild -exportArchive -archivePath $XARCHIVEPATH  -exportOptionsPlist $EXPORTOPTIONSPLIST -exportPath $DESTINATIONPATH

#Before ftp backup ipa on cloud to archive. Use TS to make archive unique
ssh -T $REMOTEUSER@$REMOTEHOST  "mv $REMOTEAPPDIR/$APP_NAME.ipa $REMOTEARCHIVEDIR/$APP_NAME$TS.ipa"

#FTP to cloud
sftp $REMOTEUSER@$REMOTEHOST -b <<EOF
cd $REMOTEAPPDIR
put $APP_NAME.ipa
EOF
