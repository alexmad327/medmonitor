#!/usr/bin/env node
var fs = require('fs'),
    xml2js = require('xml2js'),
    util = require('util'),
    im = require('imagemagick'),
    exec = require('child_process').exec;

var parser = new xml2js.Parser(),
    xmlBuilder = new xml2js.Builder();

var configFile = 'config.xml';
var preversionSplash = 'resources/preversion/splash.png';
var versionSplash = 'resources/splash.png';

var version = ['0.0.1'];
var versionStr = 'MedMonitor VUnknown';

fs.readFile(configFile, function(err, data) {
    parser.parseString(data, function (err, result) {
        console.log(util.inspect(result.widget.$.version, false, null));
        version = result.widget.$.version;
        versionStr = 'MedMonitor v'+version;
        console.log(util.inspect(version, false, null));
        console.log(util.inspect(versionStr, false, null));
        
        im.convert(['-pointsize', '48', '-fill', '#494949', preversionSplash, '-gravity', 'center', '-annotate', '+0+0', versionStr, versionSplash], function(err, stdout){
            if (err) {
                throw err;
            }
            console.log('stdout:', stdout);
            
            /*
            var child;
            child = exec("ionic cordova resources ios --splash >> tmpp", function (error, stdout, stderr) {
                if (error) {
                    throw error;
                }
                
                console.log('stdout:', stdout);
                console.log('stdout:', stderr);
            });
            */
        });
    });
});
