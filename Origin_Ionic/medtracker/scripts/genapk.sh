#!/bin/sh
command=${CORDOVA_CMDLINE}
APP_NAME="MedMonitor"
PLATFORM="android"
APPDISTDIR="/Users/umesh/Projects/PHPProjects/BenesalustechServer/trunk/appdist"
DESTINATIONPATH=$1
REMOTEUSER="www"
REMOTEHOST="benesalustech.com"
REMOTEAPPDIR="app.$REMOTEHOST/appdist"
REMOTEARCHIVEDIR="$REMOTEAPPDIR/archive"

KEYSTORE="BeneSalusMedMonitor.keystore"
KEYSTOREPASSPHRASE="b3n35alus!"
KEYSTOREALIAS="medmonitor"

ANDROIDSDKVER="26.0.2"
ANDROIDBUILDTOOLSPATH="$HOME/Library/Android/sdk/build-tools/$ANDROIDSDKVER/"

if echo "$command" | grep -q "$SOURCE"; then
    CONFIGURATION="release"
    BUILDFILENAME="android-release-unsigned"
else
    CONFIGURATION="debug"
    BUILDFILENAME="android-debug"
fi

IMAGEPATH="$1/platforms/$PLATFORM/build/outputs/apk/$CONFIGURATION/"
if [ "$CONFIGURATION" = "release" ]; then
    cp $IMAGEPATH/$BUILDFILENAME.apk $DESTINATIONPATH/$BUILDFILENAME.apk
    rm $APP_NAME.apk
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE  $BUILDFILENAME.apk $KEYSTOREALIAS -storepass $KEYSTOREPASSPHRASE
    $ANDROIDBUILDTOOLSPATH/zipalign -v 4 $BUILDFILENAME.apk $APP_NAME.apk
else
    cp $IMAGEPATH/$BUILDFILENAME.apk $DESTINATIONPATH/$APP_NAME.apk
fi

TS=$(date +%s)
echo "Date $TS"
echo $1
echo $IMAGEPATH
echo $CONFIGURATION

ssh -T $REMOTEUSER@$REMOTEHOST  "mv $REMOTEAPPDIR/$APP_NAME.apk $REMOTEARCHIVEDIR/$APP_NAME$TS.apk"
sftp $REMOTEUSER@$REMOTEHOST -b <<EOF
cd $REMOTEAPPDIR
put $APP_NAME.apk
EOF
