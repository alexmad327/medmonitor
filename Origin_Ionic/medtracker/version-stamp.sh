#!/bin/sh
version="0.0.1"
versionStr="\'MedMonitor v$version\'"
ResourceDIR="resources"
PreVersionDIR="$ResourceDIR/preversion"
splashfilename="splash.png"
convert -pointsize 48 -fill '#494949' $PreVersionDIR/$splashfilename -gravity center -annotate +0+0 'MedMonitor v1.1.1' $ResourceDIR/$splashfilename
