angular.module('medmonitor.adjusttimescontroller', ['dragularModule'])
.controller('AdjustTimesCtrl', function($scope, $rootScope, $state, $ionicPopup, updateOnline) {
    getDoseTime = function () {
        var dosetimes = [];
        var newtimes = [];

        $scope.medli.forEach(function(med) {
            if(med.assignedtimes !== undefined) {
                for(var i = 0; i < med.assignedtimes.length; i++) {
                    var newtime = med.assignedtimes[i];
                    if(newtimes.indexOf(newtime) == -1) {
                        newtimes.push(newtime);
                    }
                }
            }
        });

        for(var i = 0; i < newtimes.length; i++) {
            var dosetime = {};
            dosetime.origStr = newtimes[i];
            dosetime.timeMin = doseTimeToMin(newtimes[i]);
            dosetimes.push(dosetime);
        }

        dosetimes.sort(function(a, b) {
            return (a.timeMin - b.timeMin);
        });
        return dosetimes;
    }

    getDefaultDoseTime = function (freq) {
        var eachMedDefTimes = getDefaultTimes(freq+'D');
        var newtimes = adjustedDosageTimes(eachMedDefTimes);
        var dosetimes = [];
        for(var i = 0; i < newtimes.length; i++) {
            var dosetime = {};
            dosetime.origStr = newtimes[i];
            dosetime.timeMin = doseTimeToMin(newtimes[i]);
            dosetimes.push(dosetime);
        }
        return dosetimes;
    }

    $scope.medli = getMedicationList();
    $scope.numDoses = getNumAssignedDoses($scope.medli);
    $scope.dosetimes = getDoseTime($scope.numDoses);
    
    function checkValidTime() {
        var validTime = 0;
        var retVal = false;
        $scope.medli.forEach(function(med) {
            if(med.assignedtimes !== undefined) {
                for(var i = 0; i < med.assignedtimes.length; i++) {
                    for(var j = 0; j < $scope.dosetimes.length; j++) {
                        if(med.assignedtimes[i] == $scope.dosetimes[j].origStr) {
                            med.assignedtimes[i] = doseTimeToStr($scope.dosetimes[j].timeMin);
                            break;
                        }
                    }
                }
                
                validTime += checkValidTimePerMed(med);
            }
        });
        if(validTime > 0) {
            retVal = false;
        }
        else {
            retVal = true;
        }

        return retVal;
    }

    function checkValidTimePerMed(med) {
        var validTime = 0;//Invalid
        var minTimeArray = [1440, 720, 360, 300]; //min
        var minTime = 1439;//min
        for(var i = 1; i < med.assignedtimes.length; i++) {
            delta = doseTimeToMin(med.assignedtimes[i]) - doseTimeToMin(med.assignedtimes[i - 1]);
            if(delta < 0) {
                validTime++;
            }
            else {
                minTime = Math.min(minTime, delta);
            }
        }

        if(minTime > minTimeArray[parseInt(med.freq) - 1]) {
            validTime++;
        }

        return validTime;
    }

    $scope.changeMin= function(idx, deltaMin) {
        var dosetime = parseInt($scope.dosetimes[idx].timeMin) + deltaMin;
        if(dosetime > 1439) {
            dosetime = 1439;
        }
        else if(dosetime < 0) {
            dosetime = 0;
        }

        $scope.dosetimes[idx].timeMin = dosetime;
    }

    $scope.getDosetimeMin= function(idx) {
        var val = '0';

        return val;
    }

    $scope.getDosetimeMax= function(idx) {
        var val = '1439';

        return val;
    }

    $scope.timeStr = function(timeMin) {
        var val = $scope.timeDisplay(doseTimeToStr(timeMin));
        return val;
    }

    $scope.autoAssign = function() {
        $scope.dosetimes = getDefaultDoseTime($scope.numDoses);
    }

    $scope.Reset = function() {
        $scope.dosetimes = getDefaultDoseTime($scope.numDoses);
    }

    $scope.Accept = function() {
        var validTime = checkValidTime(); 

        if(validTime == false) {
            var localmsg = "Check dose times. Time between doses is either too short or too long";
            var titleText = "MedMonitor Adjust Times";
            genericPopupAlert($scope, $ionicPopup, localmsg, titleText);
        }
        else { //true
            var medliObj = {};
            medliObj["meds"] = $scope.medli;
            medliObj["updated_on_server"] = 0;

            var newtimes = [];
            
            $scope.dosetimes.sort(function(a, b) {
                return (a.timeMin - b.timeMin);
            });

            for(var j = 0; j < $scope.dosetimes.length; j++) {
                newtimes.push(doseTimeToStr($scope.dosetimes[j].timeMin));
            }

            window.storage.set('MEDICATION_LIST', medliObj);
            window.storage.set('DEFAULT_BOX_TIMES', newtimes);
            window.storage.set('ADJUSTED_MINS', 0);

            var setDefaultBoxTimeFlag = false;
            updateOnline.updateMedsOnLine(setDefaultBoxTimeFlag, "Adjust Times");
            $state.go('tab.settings');
        }
    }

    $scope.assignColor = function() {
        var clr = "button-balanced";
        /*
        if(checkValidTime() == false) {
            clr = "button-assertive";
        }
        */

        return clr;
    }
});
