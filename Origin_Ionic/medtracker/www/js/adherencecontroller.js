angular.module('medmonitor.adherencecontroller',[])

.controller('AdherenceCtrl', function($scope, $location, $ionicPopup, $rootScope, uiCalendarConfig) {
    adherenceColorLookup = [[{className: 'redbg', color:'red',textColor:'white'},{className: 'greenbg',  color:'green', textColor:'white'}],                           //1 dose/day
                            [{className: 'redbg', color:'red',textColor:'white'},{className: 'yellowbg', color:'yellow',textColor:'black'},{className: 'greenbg',  color:'green', textColor:'white'}],                   //2 doses/day
                            [{className: 'redbg', color:'red',textColor:'white'},{className: 'redbg',    color:'red',   textColor:'white'},{className: 'yellowbg', color:'yellow',textColor:'black'},{className: 'greenbg',  color:'green', textColor:'white'}],            //3 doses/day
                            [{className: 'redbg', color:'red',textColor:'white'},{className: 'redbg',    color:'red',   textColor:'white'},{className: 'yellowbg', color:'yellow',textColor:'black'},{className: 'yellowbg', color:'yellow',textColor:'black'}, {className: 'greenbg', color:'green',textColor:'white'}]]; //4 doses/day

    $scope.boxColor = function(box_time, curDate) {
        var boxcolor = getBoxColor(box_time, formattedDateMMDDYYYY(stringToDate(curDate,'/'))) + 'Box';
        return boxcolor;
    };

    var eventClickHandler = function(calEvent, jsEvent, view) {
        $scope.localscope = calEvent.dosageInfo;
        $scope.curDate = calEvent.start._i;
        var titleText = 'Summary ' + calEvent.start.format('MM-DD-YYYY');
        var myPopup = $ionicPopup.alert({
            scope: $scope,
            templateUrl:'dosageHistory.html', 
            cssClass: "alertpopup-positive",
            title: titleText,
            okType: "button-positive",
        });
    };

    var numDosesTaken = function(dosagesInfo) {
        var numTaken = 0;

        $.each(dosagesInfo, function(doseTime, dosageInfo){
            if(dosageInfo.taken_status == 'TAKEN') {
                numTaken++;
            }
        });
        return numTaken;
    };

    var getDateStr = function(date, separator) {
        var monthStr = ['01','02','03','04','05','06','07','08','09','10',
                        '11','12'];
        var dayStr   = ['01','02','03','04','05','06','07','08','09','10',
                        '11','12','13','14','15','16','17','18','19','20',
                        '21','22','23','24','25','26','27','28','29','30',
                        '31'];

        var year  = date.get('year');
        var month = monthStr[date.get('month')];
        var day   = dayStr[date.get('date')];

        if(separator === undefined)
            separator = "";

        var dateStr = year + separator + month + separator + day;

        return dateStr;

    };

    var getEvent = function(date) {
        return function(event) {
            var dateStr = getDateStr(date);
            return (event.start === dateStr);
        }
    };

    var getEventOnDate = function(date) {
        var events = eventArray.filter(getEvent(date));
        return events[0];
    };

    var eventRender = function(event, element, view) {
        var dosageInfo = event.dosageInfo;
        var numSlots  = event.numSlots;
        var numTaken = numDosesTaken(event.dosageInfo);
        var colors = adherenceColorLookup[numSlots - 1][numTaken];
        //var className = colors.className;
        //var classStr = '.fc-day[data-date="' + getDateStr(event.start,'-') + '"]';
        //to change color of the event with respect to response
        //$('.fc-day[data-date="' + getDateStr(event.start,'-') + '"]').addClass(className);
    };

    var dayRender = function(date, cell) {
        console.log(date, cell);
        /*
        var event = getEventOnDate(date);
        if(event !== undefined) {
            var dosageInfo = event.dosageInfo;
            var numSlots  = event.numSlots;
            var numTaken = numDosesTaken(event.dosageInfo);
            var colors = adherenceColorLookup[numSlots - 1][numTaken];
            var className = colors.color;
            cell.css('background-color',className);
        }
        */
        cell.css('border-radius','5px');
    }
    
    var updateEventData = function(date, cell) {
        var doseTakenInfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
        var eventArray = [];
        
        $.each(doseTakenInfoObj, function(doseDate, dateInfo) {
            var event = {};
            var numSlots  = dateInfo.no_of_slots;
            
            if(numSlots !== undefined) {
                var numTaken = numDosesTaken(dateInfo.dosage_slots);
                var score = numTaken + '/' + numSlots;
                var colors = adherenceColorLookup[numSlots - 1][numTaken];
                var color = colors.color;
                var textColor = colors.textColor;
                var className = colors.className;
                event = {title : score,
                         start : doseDate,
                         color : color,
                         textColor:textColor,
                         allDay : true,
                         dosageInfo:dateInfo.dosage_slots,
                         numSlots : numSlots,
                };
                eventArray.push(event);
            }
        });
        
        var events = {events: eventArray};
        return events;
    }
        
    $rootScope.$on('calendarupdate', function(e, value) {
        console.log('calendarupdate' + value);
        // https://github.com/angular-ui/ui-calendar/issues/276
        // Most of you expect that once you change $scope.events the calendar changes. But of course it doesn't! $scope.eventSources is defined once with the
        // initial contents, the contents of this variable stays the same even if you change $scope.events.
        // When changing
        //      $scope.events[0][0].title = "test123";
        // To make a long story short. What fixed it for me was actually changing $scope.eventSources[0]. That'll trigger a update in full calendar.
        $scope.eventSources[0] = updateEventData();
    });

    $scope.eventSources = [];
    $scope.eventSources[0] = updateEventData();

    $scope.uiConfig = {
        calendar : {
            editable : false,
            height: "auto",
            eventClick: eventClickHandler,
            theme: true,
            header : {
                left : 'prev',
                center : 'title',
                right : 'next'
            },
        }
    };
});
