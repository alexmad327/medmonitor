window.storage = {
    store:localStorage,
    get: function( key ) {
        try {
            return JSON.parse(this.store[key]);
        } catch(e) {};
        return undefined;
    },
    set: function( key, value) {
        try {
            this.store[key] = JSON.stringify(value);
        } catch(e) {};
    },
    removeAll: function() {
        try {
            this.store.clear();

        } catch(e) {};
    },

    loggedIn: function() {
        try {
            for (var key in this.store){
                if(key=='MEDICATION_LIST') {
                    return true;
                    break;
                }
            }

        } catch(e) {};

        return false;
    }
}

window.loadingSpinner = {

    show: function() {
        $('body').waitMe({ effect:'bounce', bg: 'rgba(0,0,0,0.3)', color:'#4682B4', source:'images/img.svg'});
    },

    hide: function() {
        $('body').waitMe('hide');
    }

}

// GetUnique array
Array.prototype.getUnique = function(){
    var u = {}, a = [];
    for(var i = 0, l = this.length; i < l; ++i){
        if(u.hasOwnProperty(this[i])) {
            continue;
        }
        a.push(this[i]);
        u[this[i]] = 1;
    }
    return a;
}

function isEmptyObject(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// Get value for key from object
var getValueForKey = function(obj, key_search) {
    var retObj ;
    for (var key in obj) {
        if ((obj.hasOwnProperty(key)) && (key==key_search)) {
            retObj = obj[key];
        }
    }

    return retObj;

}

// Date utilities
Date.prototype.yyyymmdd = function(separator) {
    var yyyy = this.getFullYear();
    var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based
    var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();

    if(typeof separator == "undefined") {  separator = ""; }

    return "".concat(yyyy).concat(separator).concat(mm).concat(separator).concat(dd);
};

Date.prototype.HHMM = function() {
    var hr = this.getHours();
    var min = this.getMinutes();
    if (hr < 10) {
        hr = "0" + hr;
    } 
    if (min < 10) {
        min = "0" + min;
    } 
   
    return hr + ":" + min;
};

Date.prototype.addDays = function(numDays) {
    this.setDate(this.getDate() + numDays);
}

Date.prototype.mmddyyyy = function() {
    var curr_day = this.getDate();
    var curr_month = this.getMonth();
    var curr_year = this.getFullYear();

    return (parseInt(curr_month+1) + "/" + curr_day + "/" + curr_year) ;
}

var stringToDate = function(str,separator) {
    //works only for dates in format: yyyymmdd
    var year = str.substr(0,4);
    var month = str.substr(4,2);
    var day = str.substr(6,2);

    if(typeof separator == "undefined") {  separator = ""; }

    return "".concat(year).concat(separator).concat(month).concat(separator).concat(day);
}

var meridiemFormat = function(tm) {
    var retval = ""; 
    var spval = tm.split(":");  
    if(spval.length==2) {
        var meridiem = (parseInt(spval[0]) >= 12) ? "PM" : "AM";
        //var tmval = (parseInt(spval[0]) > 12) ? parseInt(spval[0])-12 : parseInt(spval[0]);
        var tmval = ((parseInt(spval[0]) % 12) > 0) ? (parseInt(spval[0]) % 12) : "12";
        retval = tmval + ":" + spval[1] + " " + meridiem;
    }

    return retval; 
}

var todayFormatted = function() {
    var d = new Date();
    return formattedDay(d);
}

var formattedDay = function(d) {
    var curr_day = d.getDate();
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear();
    var hr = d.getHours();
    var min = d.getMinutes();
    var m_names = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    if (min < 10) {
        min = "0" + min;
    }

    var merdm = hr < 12 ? "am" : "pm";
    var hrformat = hr > 12 ? (hr-12) : hr; 


    return (curr_day + " " + m_names[curr_month] + " " + curr_year) + " | " + hrformat + ":" + min + " " + merdm;
}

var dateFormatddMMMyyyy = function(dt) {
    var m_names = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var d = new Date(dt);
    var curr_day = d.getDate();
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear();

    return curr_day + " " + m_names[curr_month] + " " + curr_year; 
}

var formattedDateMMDDYYYY = function(dt) {
    var d = new Date(dt);
    var curr_day = d.getDate();
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear();

    return (parseInt(curr_month+1) + "/" + curr_day + "/" + curr_year) ;
}

var currentTime = function() {
    var d = new Date();
    var hr = d.getHours();
    var min = d.getMinutes();

    if (min < 10) {
        min = "0" + min;
    } 

    return hr + ":" + min;
}

var getAdjustedTime = function(tm, incMins) {
    var adjusted_time = null;
    try {
        var timearr = tm.split(":");
        var hrs = timearr[0];
        var mins = timearr[1];
        var new_mins = parseInt(mins) + parseInt(incMins);
        if(new_mins > 59) {
            hrs = parseInt(hrs) + 1;
            new_mins = new_mins - 60;
        }

        var day = new Date().setHours(hrs,new_mins,"00");

        var adj_hrs = new Date(day).getHours();
        var adj_min = new Date(day).getMinutes();
        adj_min = (adj_min < 10) ? ("0"+adj_min) : adj_min; 
        adjusted_time = adj_hrs + ":" + adj_min;

    } catch(e) {};

    return adjusted_time;
}

var currentFullTime = function() {
    var d = new Date();
    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds();
    if (min < 10) {
        min = "0" + min;
    } 

    if (sec < 10) {
        sec = "0" + sec;
    }

    return hr + ":" + min + ":" + sec;
}

function doseTimeToStr(dosetimeMin) {
    var hrs = Math.floor(dosetimeMin/60);
    var mins = dosetimeMin - hrs*60;

    /*
    if(hrs < 10)
        hrs = '0'+hrs;
    */

    if(mins < 10)
        mins = '0'+mins;
        

    var val = hrs+':'+mins;

    return val;
}

function doseTimeToMin(dosetime) {
    var timearr = dosetime.split(":");
    var hrs = parseInt(timearr[0]);
    var mins = parseInt(timearr[1]);
    var val = hrs*60 + mins;

    return val;
}

function TimeStampToDateConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var rDate = date + ' ' + month + ' ' + year ;
    return rDate;
}

function UnixTimeStampToDateTimeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hours = a.getHours();
    var mins = a.getMinutes();
    var secs = a.getSeconds();
    if(hours < 10)
        hours = '0'+hours;
        
    if(mins < 10)
        mins = '0'+mins;
        
    if(secs < 10)
        secs = '0'+secs;
        
    var rDate = date + ' ' + month + ' ' + year + ' ' + hours + ':' + mins + ':'  + secs;
    return rDate;
}

function UnixTimeStampToMySQLDateTimeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var year = a.getFullYear();
    var month = a.getMonth() + 1;
    var date = a.getDate();
    var hours = a.getHours();
    var mins = a.getMinutes();
    var secs = a.getSeconds();
    if(month < 10)
        month = '0'+month;
        
    if(date < 10)
        date = '0'+date;
        
    if(hours < 10)
        hours = '0'+hours;
        
    if(mins < 10)
        mins = '0'+mins;
        
    if(secs < 10)
        secs = '0'+secs;
        
    var rDate = year + '-' + month + '-' + date + ' ' + hours + ':' + mins + ':'  + secs;
    return rDate;
}

var DateTimeToTimeStampConverter = function(dt, tm) {
    var year = dt.substr(0,4);
    var month = dt.substr(4,2);
    var day = dt.substr(6,2);
    var timearr = tm.split(":");
    var hrs = parseInt(timearr[0]);
    var mins = parseInt(timearr[1]);
    var newDate = new Date();
    newDate.setFullYear(year, month-1, day);
    newDate.setHours(hrs,mins,0);

    //note that this needs to be in milliseconds to work with Charts.js
    var timestamp = newDate.getTime();

    return timestamp;
};

var isSnoozeTime = function(tm) {
    var retValue = false;
    //get adjusted min from storage
    var snz = window.storage.get('ADJUSTED_SNOOZE_MINS');
    var snoozeCount = window.storage.get('USER_SNOOZE_COUNT');

    var snzTimes = [];
    var current_time =  currentFullTime();

    for(var k=0; k < snoozeCount; k++) {
        var newtime = getAdjustedTime(tm, parseInt(snz)*k) + ":00";

        if(newtime == current_time) {
            retValue = true;
            break;    
        }
    }

    return retValue;
}

var adjustedDosageTimesForGivenMinutes = function(timeArr,mins) {
    //get adjusted min from storage
    var adjmin = window.storage.get('ADJUSTED_MINS');

    var adjTimes = [];
    for(var k=0; k<timeArr.length; k++) {

        var atime = getAdjustedTime(timeArr[k],mins);
        adjTimes.push(atime);
    }

    return adjTimes;

}

var adjustedDosageTimes = function(timeArr) {

    //get adjusted min from storage
    var adjmin = window.storage.get('ADJUSTED_MINS');

    var adjTimes = [];
    for(var k=0; k<timeArr.length; k++) {

        var atime = getAdjustedTime(timeArr[k],adjmin);
        adjTimes.push(atime);
    }

    return adjTimes;

}


var getTimeDiff = function(tm) {
    var timeArr = tm.split(":");
    var curTime = new Date();
    var bxstr = "01/01/2016 " + timeArr[0] + ":" + timeArr[1] + ":00";
    var curstr = "01/01/2016 " + curTime.getHours() + ":" + curTime.getMinutes() + ":" + curTime.getSeconds();

    var diff_in_msec = new Date(curstr).getTime() - new Date(bxstr).getTime();
    var diff_sec = Math.floor(diff_in_msec / 1000);

    return diff_sec;
}


var sortTimes = function(tmArr) {

    tmArr.sort(function (a, b) {
        return new Date('1970/01/01 ' + a) - new Date('1970/01/01 ' + b);
    });

    return tmArr;

}

var uniqueArray =  function(array) {
    return $.grep(array, function(el, index) {
        return index == $.inArray(el, array);
    });
}

//base64 image
function convertImgToDataURLviaCanvas(url, callback, outputFormat){
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function(){
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        canvas = null; 
    }
    
    img.onerror = function(){  
        
        callback("");
                            
    };
    
    
    img.src = url;
}

function convertFileToDataURLviaFileReader(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = function() {
        var reader  = new FileReader();
        reader.onloadend = function () {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.send();
}

var saveMedsTakenHistory = function(dataObj) {
    try {
        var newDataObj = jQuery.extend({}, addDosageSlots(dataObj));
        window.storage.set('DOSAGE_TAKEN_INFO', newDataObj);
    } catch(e) {};
}

var saveSchedule = function(dataObj) {
    try {
        window.storage.set('DEFAULT_DOSE_SCHEDULE', dataObj); 
    } catch(e) {};
}

var saveBioMetricsList = function(dataObj) {
    try {
        //Check if info is new
        //TBD Last Update field not present
        window.storage.set('BIOMETRIC_FIELD_INFO', dataObj);
    } catch(e) {};
}

var saveGlobalMedList = function(dataObj) {
    try {
        //Check if info is new
        var localInfoObj = window.storage.get('ALL_MEDICATIONS');
        if(!isEmptyObject(localInfoObj)) {
            var localTS = localInfoObj.lastUpdate;
            if(localTS !== null) {
                if(localTS >= infoObj.dataObj)
                    return;
                //else update global medication list
            }
        }
        window.storage.set('ALL_MEDICATIONS', dataObj); 
    } catch(e) {};
}

var saveList = function(dataObj, dfobj) {
    try {
        var medliObj = window.storage.get('MEDICATION_LIST');
        if(!isEmptyObject(medliObj)) {
            var localTS = medliObj.lastUpdate;
            if(localTS !== null) {
                if(localTS >= dataObj.lastUpdate)
                    return;
                //else update medication list
            }
        }

        window.storage.set('MEDICATION_LIST', dataObj);
        processDosageInfo(dfobj); 
    } catch(e) {};
}

var saveSocialMediaMessages = function(dataObj) {
    try {
        window.storage.set('SOCIAL_MEDIA_MESSAGES', dataObj); 
    } catch(e) {};
}

var getDefaultTimes = function(freqType) {
    var medTimeArr = [];
    //comapre with default times 
    var default_dose_time = window.storage.get('DEFAULT_DOSE_SCHEDULE');

    for (var key in default_dose_time) {
        if ((default_dose_time.hasOwnProperty(key)) && (key==freqType)) {
            medTimeArr = default_dose_time[key];
        }
    }

    return medTimeArr;
}

var compareTime = function(boxTime, timeArr) {
    var flag = false;   
    for(var k=0; k < timeArr.length; k++) { 
        var tmHrs = parseInt(timeArr[k].split(":")); 
        var bxHrs = parseInt(boxTime.split(":")); 

        if((bxHrs == tmHrs) || (bxHrs+1 == tmHrs) || (bxHrs-1 == tmHrs) || (bxHrs+2 == tmHrs) || (bxHrs-2 == tmHrs)) {
            flag = true;
            break;
        }
    }

    return flag;
}

var getBoxes = function(forDate) {
    var medArr = [];
    var freqArr = [];
    var uniqueArr = [];
    var adjTimeArr = [];

    try {
        var doseTakenInfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
        if(forDate === undefined) {
            var dateArr = [];
            dateArr = Object.keys(doseTakenInfoObj);
            dateArr.sort(function(a,b) { return (parseInt(b) - parseInt(a)); });
            forDate = dateArr[0];
        }

        if(!doseTakenInfoObj.hasOwnProperty(forDate)) {
            var medliObj = window.storage.get('MEDICATION_LIST');
            var ml = medliObj.meds;
            medArr = getAssignedDoseTimes(ml);

            if(medArr.length > 0) {
                var adjMin = 0;
                window.storage.set('ADJUSTED_MINS', adjMin);
                window.storage.set('DEFAULT_BOX_TIMES', medArr);
            }
            else {
                $.each(ml, function( index, value ){
                    var fq = value.freq ;
                    freqArr.push(fq);
                });
    
                uniqueArr = uniqueArray(freqArr);
                uniqueArr.sort(function(a, b) {
                    return (parseInt(a) - parseInt(b));
                });
    
                // 1 dose logic
                if(uniqueArr.length > 0) {
                    if(uniqueArr[0]==1 && uniqueArr.length > 1) {
                        uniqueArr.shift();
                    }
                }
    
                var concatFreq = uniqueArr.join('D+') + 'D';
    
                medArr = getDefaultTimes(concatFreq);
                window.storage.set('DEFAULT_BOX_TIMES', medArr);
    
                var tempMedArr =[];
    
                if(adjustBoxTimeRequired()){
                    var adjMin = window.storage.get('ADJUSTED_MINS');
    
                    jQuery.each(medArr, function( ind, tm ){
                        var newAdjTime = getAdjustedTime(tm,adjMin);
                        tempMedArr.push(newAdjTime);
                    });
    
                    medArr = tempMedArr;
                }
            }
        } else {
            //We should discuss this in detail. First thing is adjusted minutes needs to be collected for each day.
            var doseParamObj = getValueForKey(doseTakenInfoObj, forDate);

            if(doseParamObj !== undefined) { //If date object. undefined for MESSAGE object
                var timeObj = getValueForKey(doseParamObj, 'dosage_slots');
                
                if(timeObj !== undefined) { //If date object. undefined for MESSAGE object
                    var adjustedForDefaultBoxArr = [];
                    var tempAdjMin = 0;
                    medArr = Object.keys(timeObj);
        
                    jQuery.each(medArr, function( ind, tm ){
                        var doseObj = getValueForKey(timeObj,tm);
        
                        var adjustedMinutes = getValueForKey(doseObj,'adjusted_min');
                        tempAdjMin = adjustedMinutes;
        
                        if(getValueForKey(doseObj,'taken_status') == "TAKEN"){
                            tempAdjMin = adjustedMinutes;
                        }
        
                        adjustedForDefaultBoxArr.push(getAdjustedTime(tm, -1 * parseInt(adjustedMinutes)));
                    });
        
                    //updating adjusted minutes of latest taken dose.
                    window.storage.set('ADJUSTED_MINS',tempAdjMin);
                    window.storage.set('DEFAULT_BOX_TIMES',adjustedForDefaultBoxArr);
                }
            }
        }
    } catch(e) {};

    return sortTimes(medArr);
}

var getToleranceForBox = function(box_time) {
    //loop medicine object
    var medliObj = window.storage.get('MEDICATION_LIST');
    var medli = medliObj.meds;
    var minTol = 7200; //2 hours

    if(isJson(medli)){
        medli = JSON.parse(medli);
    }

    $.each(medli, function( index, medicine ) {
        var fq = medicine.freq ;
        var eachMedDefTimes = getDefaultTimes(fq+'D');

        //new adjusted times
        var newtimes = [];
        newtimes = adjustedDosageTimes(eachMedDefTimes);

        if(compareTime(box_time, newtimes)) {
            var tol = medicine.tolerance;
            // console.log("minTol", tol, minTol);
            if(tol < minTol) {
                minTol = tol;
            }
        }
    }); 

    return minTol;
}

var getTakenStatus = function(bxtime, dt) {

    var status = "NOT_TAKEN";
    var day = new Date(dt).yyyymmdd();

    if(typeof dt == "undefined") {
        day = new Date().yyyymmdd();
    }

    var doseTakenInfoObj = window.storage.get('DOSAGE_TAKEN_INFO'); 
    var doseParamObj = getValueForKey(doseTakenInfoObj, day);
    if(doseParamObj !== undefined) { //If date object. undefined for MESSAGE object
        var timeObj = getValueForKey(doseParamObj, 'dosage_slots');
        
        if(timeObj !== undefined) { //If date object. undefined for MESSAGE object
            var statusObj = getValueForKey(timeObj,bxtime);
            
            if(statusObj !== undefined)
                status = statusObj["taken_status"];
        }
    }

    return status;

}

var getBoxColor = function(tm, dt) {
    var retColor = 'Gray';

    var tolerance = getToleranceForBox(tm);
    var timeArr = tm.split(":");
    var bxstr = dt + " " + timeArr[0] + ":" + timeArr[1] + ":00";

    var bxTime = new Date(bxstr);

    //get status for box
    var stat = getTakenStatus(tm, dt);
    var diff_in_msec = 0;
    var diff_sec = 0;

    if(stat=='TAKEN') {
        var taken_time = getTakenTime(tm, dt);
        var takenArr = taken_time.split(":");
        var takenstr = dt + " " + takenArr[0] + ":" + takenArr[1] + ":00";
        var takenTime = new Date(takenstr);

        diff_in_msec = bxTime.getTime() - takenTime.getTime();
        diff_sec = Math.floor(diff_in_msec / 1000);

        if(Math.abs(diff_sec) <= tolerance)
            retColor = 'Green';
        else
            retColor = 'Yellow';

        // console.log("Box Color",bxTime, takenTime, taken_time, stat, diff_sec, tm, retColor);
    } else {
        var curstr = formattedDateMMDDYYYY(new Date()) + " " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds();
        var curTime = new Date(curstr);

        diff_in_msec = bxTime.getTime() - curTime.getTime();
        diff_sec = Math.floor(diff_in_msec / 1000);

        if(diff_sec < 0) { //Too late not taken
            retColor = 'Red';
        }
        else if(diff_sec >=0 && (diff_sec <= tolerance)) {//Not taken but still in window
            retColor = 'Yellow';
        }
        else {             // Future
            retColor = 'Gray';
        }

        // console.log("Box Color",bxTime, curTime, stat, diff_sec, tm, retColor);
    }

    return retColor;
}

// set DOSAGE_TAKEN_INFO
var processDosageInfo = function(dObj) {
    //CHECK IF BOX ARR EXISTS
    var boxTimeArray = window.storage.get('BOX_TIMES'); 
    
    if(($.isArray(boxTimeArray)) && (boxTimeArray.length > 0)) {  
         var defTimeArray = window.storage.get('DEFAULT_BOX_TIMES');
         var newBoxTimeArray = adjustedDosageTimes(defTimeArray);
         //update  local store
          window.storage.set('BOX_TIMES',newBoxTimeArray);
    } else {
        boxTimeArray = getBoxes();
        //save to local store
        window.storage.set('BOX_TIMES',boxTimeArray);
    }
    
    var doseInfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
    
    if(typeof doseInfoObj == 'object') {       
        var today = new Date().yyyymmdd();
        
        setMissingDaysInDosageTakenObject(dObj);
        //fn will add adjusted time to the medstaken object in storage
        if(adjustDosageRequired()){
            adjustDosageTakenTimes(dObj); //commented for testing. Needs to decide on this.
        }
    } else {
        dObj.resolve();
    }
}

var adjustDosageRequired = function()
{
    var retVal = false;
    var defTimeArray = window.storage.get('DEFAULT_BOX_TIMES');
    var adjMin = window.storage.get('ADJUSTED_MINS');
    var doseInfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
    var today = new Date().yyyymmdd();
    var doseParamsObj = getValueForKey(doseInfoObj, today);
    if(doseParamsObj !== undefined) { //If date object. undefined for MESSAGE object
        var timeObj = getValueForKey(doseParamsObj, 'dosage_slots'); 
        if(timeObj !== undefined) { //If date object. undefined for MESSAGE object
            var dosageTimeArr = Object.keys(timeObj);
            var currentDosageTime = dosageTimeArr[0];
        
            var defBoxTime = defTimeArray[0];
            var newAdjTime = getAdjustedTime(defBoxTime,adjMin);
         
            if(newAdjTime != currentDosageTime){
                retVal = true;
            }
        }
    }
    
    return retVal;
}

// Check to see if current box time is truly default time +/- adjMin
var adjustBoxTimeRequired = function()
{
    var retVal = false;
    var defTimeArray = window.storage.get('DEFAULT_BOX_TIMES');
    var boxTimeArray = window.storage.get('BOX_TIMES');
    var adjMin = window.storage.get('ADJUSTED_MINS');

    var currentBoxTime = boxTimeArray[0];
    
    var defBoxTime = defTimeArray[0];
    var newAdjTime = getAdjustedTime(defBoxTime,adjMin);
    
    if(newAdjTime != currentBoxTime){
        retVal = true;
    }
    
    return retVal;
}


// adjust times
var adjustDosageTakenTimes = function(dObj){
    var today = new Date().yyyymmdd();
    var doseTakenInfoObj = window.storage.get('DOSAGE_TAKEN_INFO'); 
    var adjMin = window.storage.get('ADJUSTED_MINS'); 
    var adjMin_old = window.storage.get('ADJUSTED_MINS_OLD'); 

    var newAdjMin = parseInt(adjMin) - parseInt(adjMin_old);

    var doseParamsObj = getValueForKey(doseTakenInfoObj, today);

    if(doseParamsObj !== undefined) { //If date object. undefined for MESSAGE object
        var timeObj = getValueForKey(doseParamsObj, 'dosage_slots');  
        if(timeObj !== undefined) { //If date object. undefined for MESSAGE object
            var newObj = {};
        
            for(key in timeObj)  {
                var newkey = getAdjustedTime(key,newAdjMin);
                var innerObj = timeObj[key]; 
                innerObj['adjusted_min'] = adjMin;
                newObj[newkey] = innerObj;
            } 
        
            doseParamsObj['dosage_slots'] = newObj;
        
            doseTakenInfoObj[today] = doseParamsObj;
        
            window.storage.set('DOSAGE_TAKEN_INFO',doseTakenInfoObj);
        }
    }

    dObj.resolve();
}

var getTakenTime = function(bxtime, dt) {
        var taken_time = "";    
        var day = new Date(dt).yyyymmdd();  
        var doseTakenInfoObj = window.storage.get('DOSAGE_TAKEN_INFO'); 
        var doseParamObj = getValueForKey(doseTakenInfoObj, day);
        
        if(doseParamObj !== undefined) { //If date object. undefined for MESSAGE object
            var timeObj = getValueForKey(doseParamObj, 'dosage_slots');
            
            if(timeObj !== undefined) { //If date object. undefined for MESSAGE object
                var statusObj = getValueForKey(timeObj,bxtime);
    
                if(statusObj !== undefined)
                    taken_time = statusObj["taken_time"];
            }
        }

        return taken_time;
    }



//initialize Dosage Taken Info 
var initializeDosageTakenInfo = function(dObj){
    setMissingDaysInDosageTakenObject(dObj);
}


var getBetweenDates = function(start,end){
    var currentDate = new Date(stringToDate(start,'/'));
    var between = [];
    
    while (currentDate <= new Date(stringToDate(end,'/'))) {
        between.push(new Date(currentDate));
        currentDate.setDate(currentDate.getDate() + 1);
    }
    
    return between;
}

var setMissingDaysInDosageTakenObject = function(dObj){
    var doseInfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
    var today = new Date().yyyymmdd();
    var dateArr = [];
    dateArr = Object.keys(doseInfoObj);
    
    if(dateArr.length == 0) {
       addDayInDosageTakenObject(today);
       dObj.resolve();
    } else {
        dateArr.sort(function (a, b) {
            return new Date(stringToDate(a,'/')) - new Date(stringToDate(b,'/'));
        });
        
        var missingDt = getBetweenDates(dateArr[0],today);
        jQuery.each(missingDt, function( ind, dt ){
            var fdate = new Date(dt).yyyymmdd();
            
            if(jQuery.inArray(fdate,dateArr) == -1) {
                addDayInDosageTakenObject(fdate);
            }
            
            if(ind == missingDt.length-1) {
                dObj.resolve();
            }
        });
    }
}

/// date should be in yyyymmdd format
var addDayInDosageTakenObject = function( dt, dObj){
      var boxTimeArray = window.storage.get('BOX_TIMES');
      var boxtimesObj = {};
      var doseParams = {};
      var timeOffset = new Date().getTimezoneOffset();
      var innerObj = {"taken_status": "NOT_TAKEN",
                      "taken_time": "",
                      "updated_time": "",
                      "tzoffset": timeOffset,
                      "updated_on_server":0 ,
                      "adjusted_min": window.storage.get('ADJUSTED_MINS')
                      };
      
      jQuery.each(boxTimeArray, function( ind, tm ){
          innerObj["dosage_slot"] = ind + 1;
          var newObject = jQuery.extend({}, innerObj);
          boxtimesObj[tm] = newObject;
      });
      
      doseParams["dosage_slots"] = boxtimesObj;
      doseParams["no_of_slots"] =  boxTimeArray.length;
      
      var dinfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
      dinfoObj[dt] = doseParams;
      
      window.storage.set('DOSAGE_TAKEN_INFO',dinfoObj);
      
      if(typeof dObj != 'undefined') {
          dObj.resolve();
      }
}

//personal info
window.USERCONFIG = {
    saveInfo: function(infoObj, defferd) {
        try {
            //Check if info is new
            var localInfoObj = window.storage.get('USER_INFO');
            if(!isEmptyObject(localInfoObj)) {
                var localTS = localInfoObj.lastUpdate;
                if(localTS !== null) {
                    if(localTS >= infoObj.lastUpdate)
                        return;
                    //else update user info
                }
            }
            var user_picture = infoObj.user_picture;
            if(user_picture !== null) {
                var imgURL = user_picture + "?" + new Date().getTime();
    
                convertImgToDataURLviaCanvas(imgURL, function(base64ImgSrc) {
                    infoObj["base64_src"] = base64ImgSrc;
                });
            }
            //save adjusted mins and snooze time in local store
            var adjusted_min = infoObj["adjusted_min"];
            var snooze_time = infoObj["snooze_time"];
            
            window.storage.set('ADJUSTED_MINS', adjusted_min);
            window.storage.set('ADJUSTED_SNOOZE_MINS', snooze_time);
            window.storage.set('USER_INFO', infoObj);
            
            if(typeof defferd != 'undefined') {
                defferd.resolve();
            }
        } catch(e) {
            if(typeof defferd != 'undefined') {
                defferd.resolve();
            }
        };
    }
}

var biometricsTobeUpdatedOnServer = function () {
    var bmtObj = window.storage.get('BIOMETRIC_TAKEN_INFO');
    var bmTakenObj = getValueForKey(bmtObj, "biometrics");
    var dateArr = Object.keys(bmTakenObj);

    var newBMTakenObj = {};
    var bmValArr_tobeupdated = [];
    
    jQuery.each(dateArr,function(index,dt) {
        var bmValArr = getValueForKey(bmTakenObj, dt);
                
        var isUpdated = true;
        
         bmValArr_tobeupdated = [];
                
        jQuery.each(bmValArr,function(ind,obj){
            if((typeof obj["updated_on_server"] != "undefined") && (obj["updated_on_server"]==0)) {
                bmValArr_tobeupdated.push(obj);
            }
         });
        
        if(bmValArr_tobeupdated.length > 0) {
            newBMTakenObj[dt] = bmValArr_tobeupdated;
        }
    });
    
    return newBMTakenObj;
}
    
var updateServerTagForBiometric = function(statCode) {
    var bmtObj = window.storage.get('BIOMETRIC_TAKEN_INFO');
    var bmTakenObj = getValueForKey(bmtObj, "biometrics");
    var dateArr = Object.keys(bmTakenObj);
    
    var newBMTakenObj = {};
    
    jQuery.each(dateArr,function(index,dt){
        var bmValArr = getValueForKey(bmTakenObj, dt);
        
        if(Array.isArray(bmValArr)) {
            var bmValArr_updated = [];
            jQuery.each(bmValArr,function(ind,obj){
                obj["updated_on_server"] = statCode;
                bmValArr_updated.push(obj);
            });

            newBMTakenObj[dt] = bmValArr_updated;
        }
    });
    
    var newObj = {};
    newObj["biometrics"] = newBMTakenObj;
    
    window.storage.set('BIOMETRIC_TAKEN_INFO', newObj);
}

var getBiometricTypes = function() {
    //get list froM JSON
    var bmArray = [];
    var bmJSON = window.storage.get('BIOMETRIC_FIELD_INFO');
    
    jQuery.each(bmJSON,function(index,valObj){
        bmArray.push(valObj["biometric_type"]);
    });
    
    return bmArray;
}

var medsTobeUpdatedOnServer = function(){
    var doseTakenObj = window.storage.get('DOSAGE_TAKEN_INFO');
    var dateArr = Object.keys(doseTakenObj);

    var newDoseTakenObj = {};

    jQuery.each(dateArr,function(index,value){
        var doseParamObj = getValueForKey(doseTakenObj, value);
        if(doseParamObj !== undefined) { //If date object. undefined for MESSAGE object
            var doseSlotsObj = getValueForKey(doseParamObj, 'dosage_slots');
            if(doseSlotsObj !== undefined) {//If date object. undefined for MESSAGE object
                var timeArr = Object.keys(doseSlotsObj);
                var isUpdated = true;
        
                jQuery.each(timeArr,function(tmIndex,tm){
                    var doseStatusObj = getValueForKey(doseSlotsObj, tm);
                    var updated_on_server = doseStatusObj["updated_on_server"];
        
                     if(parseInt(updated_on_server) == 0) {
                        isUpdated = false;
                     }
                });
        
                if(!isUpdated) {
                    newDoseTakenObj[value] = doseParamObj;
                }
            }
        }
    });

    return newDoseTakenObj;
}

var updateServerTagForDose = function(tag){
    var doseTakenObj = window.storage.get('DOSAGE_TAKEN_INFO');
    var dateArr = Object.keys(doseTakenObj);
    
    jQuery.each(dateArr,function(index,dtValue){
        var doseParamObj = getValueForKey(doseTakenObj, dtValue);
        if(doseParamObj !== undefined) { //If date object. undefined for MESSAGE object
            var doseSlotsObj = getValueForKey(doseParamObj, 'dosage_slots');
            if(doseSlotsObj !== undefined) { //If date object. undefined for MESSAGE object
                var timeArr = Object.keys(doseSlotsObj);
                var isUpdated = true;
        
                jQuery.each(timeArr,function(tmIndex,tmValue){
                    var doseStatusObj = getValueForKey(doseSlotsObj, tmValue);
                    doseStatusObj["updated_on_server"] = tag;
                    doseSlotsObj[tmValue] = doseStatusObj;
                });
        
                doseParamObj['dosage_slots'] = doseSlotsObj;
                doseTakenObj[dtValue] = doseParamObj;
            }
        }
    });
    
    //put it back in store
    window.storage.set('DOSAGE_TAKEN_INFO', doseTakenObj);
}

var addDosageSlots = function(dataObj){
    var dateArr = Object.keys(dataObj);
    
    var dosePrams = {};
    
    var obj = {};
    
    jQuery.each(dateArr,function(index,value){
        var doseParamObj = getValueForKey(dataObj, value);
        if(doseParamObj !== undefined) { //If date object. undefined for MESSAGE object
            var timeObjLength = doseParamObj['dosage_slots'];
            if(timeObjLength !== undefined) {
                timeObjLength = Object.keys(timeObjLength);
                timeObjLength = timeObjLength.length;
                doseParamObj["no_of_slots"] = timeObjLength;
                dataObj[value] = doseParamObj;
            }
        }
    });
    
    return dataObj;
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function setDefaultBoxTimes() {
    var medArr = [];
    var freqArr = [];
    var uniqueArr = [];

    var medliObj = window.storage.get('MEDICATION_LIST');
    var ml = medliObj.meds;

    jQuery.each(ml, function( index, value ){
        var fq = value.freq ;
        freqArr.push(fq);
    });

    uniqueArr = uniqueArray(freqArr);

    uniqueArr.sort(function(a, b) {
        return parseInt(a) - parseInt(b);
    });

    // 1 dose logic
    if(uniqueArr.length > 0) {

        if(uniqueArr[0]==1 && uniqueArr.length > 1) {

            uniqueArr.shift();
        }
    }

    var concatFreq = uniqueArr.join('D+') + 'D';

    medArr = getDefaultTimes(concatFreq);
    window.storage.set('DEFAULT_BOX_TIMES',medArr);
}

function resetBoxSlots() {
    var defTimeArray = window.storage.get('DEFAULT_BOX_TIMES');
    var newBoxTimeArray = adjustedDosageTimes(defTimeArray);

    //update  local store
    window.storage.set('BOX_TIMES',newBoxTimeArray);
}

function getMedicationList(updated_on_server) {
    var medicationListObj = window.storage.get('MEDICATION_LIST');
    var medicationList = null;
    if(updated_on_server != undefined) {
        if(updated_on_server == 0) {
            if((medicationListObj.updated_on_server === undefined) || (medicationListObj.updated_on_server == 0)) {
                medicationList = medicationListObj.meds;
            }
        }
        else { //updated_on_server == 1
            if(medicationListObj.updated_on_server == 1) {
                medicationList = medicationListObj.meds;
            }
        }
    }
    else { //updated_on_server === undefined
        medicationList = medicationListObj.meds;
    }
        
    if(isJson(medicationList)){
        medicationList = JSON.parse(medicationList);
    }

    return medicationList;
}

function addNewMedicine(medlist) {
    var medliObj = window.storage.get('MEDICATION_LIST');
    var medliArr = medliObj.meds;
    var globalMedList = getGlobalMedList();
    var addList = [];

    //check if med exists
    jQuery.each(medlist, function( desc, strength ){
        var exists = false
        
        jQuery.each(medliArr, function( index, med ){
            var med_desc = med["desc"];
            var med_strength = med["strength"];
        
            if(((desc.toUpperCase()) == (med_desc.toUpperCase()))
             && ((strength.toUpperCase()) == (med_strength.toUpperCase()))) {
                exists = true;
            }
        });
        
        if(exists == false) {
            len = globalMedList.length;
            var breakFlag = false;
            var strengthIdx = -1;
            for(var i = 0; i < len; i++) {
                var med = globalMedList[i];
                if(med.desc.toUpperCase() == desc.toUpperCase()) {
                    len2 = med.doses.length;
                    for(var j = 0; j < len; j++) {
                        if(med.doses[j].strength == strength) {
                            breakFlag = true;
                            strengthIdx = j
                            break;
                        }
                    }
                }
                if(breakFlag) {
                    var newMed = {};
                    newMed.code = med.code;
                    newMed.customMed = false;
                    newMed.desc = desc;
                    newMed.freq = med.doses[strengthIdx].freq;
                    newMed.idx = med.doses[strengthIdx].idx;
                    newMed.image = med.doses[strengthIdx].image;
                    newMed.strength = strength;
                    newMed.tolerance = med.doses[strengthIdx].tolerance;
                    addList.push(newMed);
                    globalMedList.splice(i,1);
                    break;
                }
            }
        }
    });

    if(addList.length > 0) {
        jQuery.each(addList, function( index, medl ){
            medliArr.push(medl); 
        });

        jQuery.each(medliArr, function( index, med ){
            delete med.assignedtimes;
        });

        medliObj["meds"] = medliArr;
        medliObj["updated_on_server"] = 0;
        window.storage.set('MEDICATION_LIST', medliObj);
    }

    return (addList.length > 0);
}

function addCustomMedicine(med) {
    var medliObj = window.storage.get('MEDICATION_LIST');
    var medliArr = medliObj.meds;

    var newMed = {};
    newMed.code = med.desc;
    newMed.customMed = true;
    newMed.desc = med.desc;
    newMed.freq = med.freq;
    newMed.idx = 0;
    newMed.image = null;
    newMed.strength = med.strength;
    newMed.tolerance = 3600;

    medliArr.push(newMed); 

    jQuery.each(medliArr, function( index, med ){
        delete med.assignedtimes;
    });

    medliObj["meds"] = medliArr;
    medliObj["updated_on_server"] = 0;
    window.storage.set('MEDICATION_LIST', medliObj);

    return true;
}

function getGlobalMedList() {
    var allMedsObj = window.storage.get('ALL_MEDICATIONS');
    var allMedsArr = allMedsObj.meds;

    return allMedsArr;
}

function findElementIndex(source, element, defaultLen) {
    var idx = defaultLen;
    if(element === null) {
        return idx;
    }

    var sourceChildren = Array.prototype.slice.call(source.children);
    for(var i = 0; i < sourceChildren.length; i++) {
        if(sourceChildren[i].innerHTML === element.innerHTML){
            idx = i;
            break;
        }
    }

    return idx;
}

function getNumDoses(medli) {
    var numDoses = 0;
    medli.forEach(function(med) {
        var fq = med.freq ;
        numDoses = Math.max(numDoses, fq);
    });
    return numDoses;
}

function getNumAssignedDoses(medli) {
    var numDoses = 0;
    var dosetimeArray = [];
    medli.forEach(function(med) {
        var fq = med.freq ;
        if(med.assignedtimes !== undefined) {
            for(var i = 0; i < med.assignedtimes.length; i++) {
                var newtime = med.assignedtimes[i];
                if(dosetimeArray.indexOf(newtime) == -1) {
                    dosetimeArray.push(newtime);
                }
            }
        }
    });

    numDoses = dosetimeArray.length;
    return numDoses;
}

function getAssignedDoseTimes(medli) {
    var dosetimeArray = [];
    medli.forEach(function(med) {
        var fq = med.freq ;
        if(med.assignedtimes !== undefined) {
            for(var i = 0; i < med.assignedtimes.length; i++) {
                var newtime = med.assignedtimes[i];
                if(dosetimeArray.indexOf(newtime) == -1) {
                    dosetimeArray.push(newtime);
                }
            }
        }
    });
    
    return dosetimeArray;
}

function getMedicinesForBox(box_time) {
    //loop medicine object
    var medliObj = window.storage.get('MEDICATION_LIST');
    var medli = medliObj.meds;

    if(isJson(medli)){
        medli = JSON.parse(medli);
        console.log("JSON Medli", medli);
    }
    else {
        console.log("Object Medli", medli);
    }

    var medinfo_tmp = [];

        var newtimes = [];
    $.each(medli, function( index, medicine ){
        if(medicine.assignedtimes === undefined) {
            var fq = medicine.freq ;
            var eachMedDefTimes = getDefaultTimes(fq+'D');
            
            //new adjusted times
            newtimes = adjustedDosageTimes(eachMedDefTimes);
        }
        else {
            newtimes = medicine.assignedtimes;
        }

        if(compareTime(box_time, newtimes)) {
            var medstr = medicine.desc;
            medinfo_tmp.push(medstr); 
        }
    }); 

    return medinfo_tmp;
}

function getMedBoxInfo(curDisplayDate) {
    var boxArray = getBoxes(curDisplayDate.yyyymmdd());
    var medicationInfo = [];
    var medli = getMedicationList();
    var updateFlag = false;
    $.each(boxArray, function(box_index, box_time){
        var medinfo_tmp = {};
        medinfo_tmp["box_time"] = box_time;

        var taken_time = getTakenTime(box_time, curDisplayDate.yyyymmdd());//get taken time
        medinfo_tmp["taken_time"] = taken_time;

        medinfo_tmp["medicines"] = [];
        medinfo_tmp["boxColor"] = getBoxColor(box_time, formattedDateMMDDYYYY(curDisplayDate)) + 'Box';
        $.each(medli, function( med_index, medicine ){
            var fq = medicine.freq ; 
            //new adjusted times
            var newtimes = [];
            if(medicine.assignedtimes === undefined) {
                var eachMedDefTimes = getDefaultTimes(fq+'D');
                newtimes = adjustedDosageTimes(eachMedDefTimes);
                medicine.assignedtimes = newtimes;
                updateFlag = true;
            }
            else {
                newtimes = medicine.assignedtimes;
            }

            if(medicine.holiday === undefined) {
                medicine.holiday =  {flag:false, startDate:null, endDate:null};
                console.log("Init Holiday");
                updateFlag = true;
            }

            if(compareTime(box_time, newtimes)) {
                if(medicine.image==null) {
                    medicine.image = 'nomedpic.jpg';
                }
                medinfo_tmp["medicines"].push(medicine); 
            }
        });
        medicationInfo.push(medinfo_tmp);
    });
    
    if(updateFlag == true) {
        var medliObj = {};
        medliObj["meds"] = medli;
        window.storage.set('MEDICATION_LIST', medliObj);
    }
    return medicationInfo;
}

function apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage) {
    var localmsg = "";
    if((responseMessage !== undefined) && (responseCode !== undefined)) {
        localmsg +=  responseMessage +  ". Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
    }
    else {
        localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
    }

    var myPopup = $ionicPopup.alert({
        scope: $scope,
        template:localmsg,
        cssClass: "alertpopup-assertive",
        title:  titleText,
        okType: "button-assertive",
    });
    
    myPopup.then(function(res) {
        fsm.deActivate(localmsg);
    });
}

function apiSummaryFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage) {
    var localmsg = "";
    if((responseMessage !== undefined) && (responseCode !== undefined)) {
        localmsg = "Error uploading info to cloud. ";
        switch(responseCode) {
            case responseCodeSUCCESS:
                break;
            case responseCodeINVALIDPHONE:
                localmsg += responseMessage +  ". Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                break;
            case responseCodeINVALIDREG:
                localmsg += responseMessage +  ". DeActivate and Reactivate App. Error Code " + responseCode;
                break;
            case responseCodeINVALIDDATA:
                localmsg += responseMessage +  ". Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                break;
            default:
                localmsg +=  responseMessage +  ". Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                break;
        }
    }
    else {
        localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
    }

    var myPopup = $ionicPopup.alert({
        scope: $scope,
        template:localmsg,
        cssClass: "alertpopup-assertive",
        title:  titleText,
        okType: "button-assertive",
    });
    
    myPopup.then(function(res) {
        fsm.waitAgain(localmsg);
    });
}

function genericPopupAlert($scope, $ionicPopup, localmsg, titleText) {
    var myPopup = $ionicPopup.alert({
        scope: $scope,
        template:localmsg,
        cssClass: "alertpopup-assertive",
        title:  titleText,
        okType: "button-assertive",
    });
    myPopup.then(function(res) {});
}

function initStorage() {
   //cancell all notifications
    cordova.plugins.notification.local.cancelAll();
               
   // set or create storage for dosage taken info
   storage.set('DOSAGE_TAKEN_INFO', {});
               
   // set or create storage for bmfields
   storage.set('BIOMETRIC_FIELD_INFO', {});
    
    // set or create storage for bm taken info
    storage.set('BIOMETRIC_TAKEN_INFO', {});
    
    // set or create storage for ex notificaion info
    storage.set('EX_NOTIFICATION_INFO', {});
    
     // set or create storage for dosage taken info
    storage.set('ALL_MEDICATIONS', {});
    
    //set adjusted mins
    storage.set('ADJUSTED_MINS', 0);
    
    //set adjusted mins
    storage.set('ADJUSTED_MINS_OLD', 0);
    
    //set snooze interval
    storage.set('ADJUSTED_SNOOZE_MINS', gblSnoozeMin); 
    
    //set allowable snooze count
    storage.set('ALLOWED_SNOOZE_COUNT', gblSnoozeCount); 
    
     //set user snooze count
    storage.set('USER_SNOOZE_COUNT', gblSnoozeCount); 
    
   //set default dose times in store
    storage.set('DEFAULT_DOSE_SCHEDULE', {}); 
          
    // set NO_OF_DAYS_DATA_CAN_BE_UPDATED flag
     storage.set('NO_OF_DAYS_DATA_CAN_BE_UPDATED',3);
               
   // set ON_RESUME_REFRESH flag
   storage.set('ON_RESUME_REFRESH', 1);
}

function createMsg(msgType, parentIDX, timestamp, message=null) {
    var newMSG = {  'msgtype':msgType,
                    'parentIDX':parentIDX,
                    'timestamp':timestamp,
                    'message':message,
    }
    
    var unsentSocialMediaMessage = window.storage.get('SOCIAL_MEDIA_MESSAGES');
    if(unsentSocialMediaMessage === undefined) {
        unsentSocialMediaMessage = new array();
    }
    unsentSocialMediaMessage.push(newMSG);
    window.storage.set('SOCIAL_MEDIA_MESSAGES', unsentSocialMediaMessage);
}

function sendUnSentSocialMediaMessage() {
    var unsentSocialMediaMessage = window.storage.get('SOCIAL_MEDIA_MESSAGES');
    if(unsentSocialMediaMessage !== undefined) {
        //Pop from head
        var newMSG = unsentSocialMediaMessage.shift();
        switch(newMSG.msgtype) {
            case SMM_LIKE:
                updateOnline.flagMessageOnline(chat.id, timestamp);
                break;
            case SMM_FLAG:
                updateOnline.flagMessageOnline(chat.id, timestamp);
                break;
            case SMM_ADD:
                updateOnline.addMessageOnline(chat.id, res, timestamp);
                break;
        }
    }
}
