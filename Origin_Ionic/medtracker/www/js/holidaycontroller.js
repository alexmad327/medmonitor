angular.module('medmonitor.holidaycontroller', ['dragularModule'])
.controller('HolidayCtrl', function($scope, $ionicPopup, $rootScope, $state, updateOnline) {
    $scope.medli = getMedicationList();
    $scope.medli.forEach(function(med) {
       if(med.holiday === undefined) {
           console.log("Init Holiday");
           med.holiday =  {flag:false, startDate:null, endDate:null};
       }
       else {
            console.log($scope.medli);
       }
    });

    $scope.minDate =  new Date();
    $scope.maxDate = new Date();
    $scope.maxDate.addDays(100);
    $scope.startDate = null;
    $scope.endDate = null;
 
    $scope.changeAll = function (val) {
        $scope.medli.forEach(function(med) {
            med.holiday.flag = val;
        });
    };
     
    $scope.checkAndEnableHoliday = function () {
        var titleText = "MedMonitor Med Holiday";
        var localmsg = "";
        var holidayFlagCount = 0;
        $scope.medli.forEach(function(med) {
            if(med.holiday.flag == true) {
                holidayFlagCount++;
            }
        });

        if(holidayFlagCount == 0) {
            localmsg = "No meds selected for Medication Holiday";
            genericPopupAlert($scope, $ionicPopup, localmsg, titleText);
            return;
        }

        if(($scope.startDate === null) || ($scope.endDate === null)) {
            localmsg = "Please select valid start and end dates for Medication Holiday";
            genericPopupAlert($scope, $ionicPopup, localmsg, titleText);
            return;
        }

        $scope.medli.forEach(function(med) {
            if(med.holiday.flag == true) {
                med.holiday.startDate = parseInt($scope.startDate.getTime()/1000);
                med.holiday.endDate = parseInt($scope.endDate.getTime()/1000);
            }
            else {
                med.holiday.startDate = null;
                med.holiday.endDate = null;
            }
        });
        
        // Save Medlist with holiday to device store. Later update online
        var medliObj = {};
        medliObj["meds"] = $scope.medli;
        medliObj["updated_on_server"] = 0;
        window.storage.set('MEDICATION_LIST', medliObj);

        var setDefaultBoxTimeFlag = false;
        updateOnline.updateMedsOnLine(setDefaultBoxTimeFlag, "Med Holiday");

        //Return to settings page
        $state.go('tab.settings');
    }
});
