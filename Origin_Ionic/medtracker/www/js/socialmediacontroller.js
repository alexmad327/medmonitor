angular.module('medmonitor.socialmediacontroller', [])
    .controller('SocialMediaCtrl', function($scope, $rootScope, updateOnline) {
        $scope.propertyName = 'to';
        $scope.propertyNameStr = 'Recipient';
        var infoObj = window.storage.get('USER_INFO');

        $scope.profileimage = function(chat) {
            var retStr = "noprofile.png";
            if(chat.to == infoObj.user_name) {
                retStr = "noprofile.png";
            }
            else {
                retStr = "bene_icon.png";
            }

            return retStr;
        }

        $scope.orderList = function() {
            if($scope.propertyName == 'to') {
                $scope.propertyName = 'id';
                $scope.propertyNameStr = 'Date';
            }
            else {
                $scope.propertyName = 'to';
        $scope.propertyNameStr = 'Addressed To';
            }
        };

        var updateScope = function() {
            $scope.chats=window.storage.get('SOCIAL_MEDIA_MESSAGES');
        }
        updateScope();
        $rootScope.$on("updatechats", function(e, value) {
            console.log("updatechats",value);
            updateScope();
        });
        
        $rootScope.$on('checkunsentdata', function(e, value) {
            if(window.storage.loggedIn()) {
                updateOnline.getSocialMessages();

                sendUnSentSocialMediaMessage();
            }
        });
    })
    .controller('ChatDetailCtrl', function($scope, $rootScope, $stateParams, $state, $ionicPopup, $httpParamSerializer, updateOnline) {
        var chats=window.storage.get('SOCIAL_MEDIA_MESSAGES');

        var getChat = function(chatId) {
            var retVal = null
            var arrayLength = chats.length;
            for (var i = 0; i < arrayLength; i++) {
                if(chats[i].id == chatId) {
                    retVal = chats[i];
                    break;
                }
            }

            return retVal;
        }

        var updateScope = function() {
            chats=window.storage.get('SOCIAL_MEDIA_MESSAGES');
            $scope.chat = getChat($stateParams.chatId);
        }

        updateScope();
        $rootScope.$on("updatechats", function(e, value) {
            console.log("updatechats",value);
            updateScope();
        });

        $scope.profileimage = "noprofile.png";

        $scope.like = function(chat) {
            var timestamp = UnixTimeStampToMySQLDateTimeConverter(Math.floor(Date.now() / 1000));
            chat.likes++;
            updateOnline.likeMessageOnline(chat.id, timestamp);
            window.storage.set('SOCIAL_MEDIA_MESSAGES', chats);
            $rootScope.$broadcast("updatechats","like");
            //$state.go('chats');
        };

        $scope.flag = function(chat) {
            var timestamp = UnixTimeStampToMySQLDateTimeConverter(Math.floor(Date.now() / 1000));
            chat.flagged++;
             
            updateOnline.flagMessageOnline(chat.id, timestamp);
            window.storage.set('SOCIAL_MEDIA_MESSAGES', chats);
            $rootScope.$broadcast("updatechats","flag");
        }

        $scope.remove = function(chat) {
            // Chats.remove(chat);
            // $state.go('chats');
        };

        $scope.message = function(chat) {
            var localmsg = "Enter Message";
            var placeHolder ="Message";
            var titleText = chat.subject;
            var myPopup = $ionicPopup.prompt({
                template:localmsg,
                cssClass: "alertpopup-assertive",
                title:  titleText,
                inputType: 'text',
                inputPlaceholder: placeHolder,
                okText: "Post",
                okType: "button-assertive",
            });

            myPopup.then(function(res) {
                if(res !== undefined) {
                    var timestamp = UnixTimeStampToMySQLDateTimeConverter(Math.floor(Date.now() / 1000));
                    var newMsg = {from:"Umesh Iyer", message: res, timestamp: timestamp};
                    chat.messages.push(newMsg);
                    window.storage.set('SOCIAL_MEDIA_MESSAGES', chats);
                    updateOnline.addMessageOnline(chat.id, res, timestamp);
                    $rootScope.$broadcast("updatechats","msg");
                    // $state.go('chats');
                }
            });
        };
    });
