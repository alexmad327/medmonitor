angular.module('medmonitor.summarycontroller', [])

.controller('SummaryCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaLocalNotification, BLE, $timeout, $ionicPopup, $httpParamSerializer, updateMedsTakenStatus, updateBiometricTakenStatus, getUserInfo, updateOnline) {
    $ionicPlatform.ready(function () {
        if(BLE.connectedInfo() == false) {
            BLE.scan();
        }

        $scope.scheduleSingleNotification = function () {
          $cordovaLocalNotification.schedule({
          });
        }
    });

    $scope.date =  new Date();
    $scope.LEFT = -1;
    $scope.RIGHT = 1;
    $scope.device = BLE.pillBox;
    $scope.badges = [];

    $scope.getBMKey = function(bm) {
        var keys = Object.keys(bm);
        return keys[0];
    };

    var bmJSON = window.storage.get('BIOMETRIC_FIELD_INFO');
    if(bmJSON.length == 0) {
        $scope.bmValues = null;
        $scope.biometrics = null;
    }
    else {
            bmJSON.forEach(function(biometric) {
                var bmValue = biometric.biometric_values.values;
                switch(biometric.biometric_values.type) {
                    case "DROPDOWN":
                        biometric.selected = $scope.getBMKey(bmValue[0]);
                        break;
                    case "TEXTBOX":
                    default:
                        break;
                }
            });
        $scope.biometrics = bmJSON;
        $scope.bmValues = {};
    }

    //fn update taken status offline
    var onupdateTakenOfflineFcn = function (event, from, to, msg, bxtime, newtime, ty, dt, mode) { 
        // console.log("Taken Status Offline " + from + " " + to + " " + msg + " Mode " + mode);

        var today = new Date().yyyymmdd();
        var day = today;
        if(dt.length > 0){
            day = dt;
        }

        var doseTakenInfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
        var doseParamsObj = getValueForKey(doseTakenInfoObj, day);
        //If day is missing insert date dose info init'ed tp NOT_TAKEN
        if(doseParamsObj === undefined) {
            addDayInDosageTakenObject(day);
            doseTakenInfoObj = window.storage.get('DOSAGE_TAKEN_INFO');
            doseParamsObj = getValueForKey(doseTakenInfoObj, day);
        }
        var timeObj = getValueForKey(doseParamsObj, 'dosage_slots');

        //get slots
        var no_of_slots = getValueForKey(doseParamsObj, 'no_of_slots');

        var statusObj = getValueForKey(timeObj,bxtime);
        var curtime = currentTime();
        var curstamp = parseInt(new Date().getTime()/1000);

        //get slot no
        var slot_no = statusObj["dosage_slot"] ;

        if(ty=='NOT_MISSED') {
            statusObj["taken_time"] = newtime;

        } else {
            statusObj["taken_time"] = curtime;
        }

        statusObj["taken_status"] = "TAKEN";
        statusObj["updated_time"] = curstamp;
        statusObj["tzoffset"] = new Date().getTimezoneOffset();
        statusObj["updated_on_server"] = 0;
        statusObj["mode"] = mode;

        timeObj[bxtime] = statusObj;
        doseParamsObj['dosage_slots'] = timeObj;
        doseTakenInfoObj[day] = doseParamsObj;
        window.storage.set('DOSAGE_TAKEN_INFO', doseTakenInfoObj);
        $rootScope.$broadcast("calendarupdate","Meds Taken and time update");

        var localmsg = "";
        if(parseInt(today) == parseInt(dt)) {
            localmsg = "Cancel Notifications";
            fsm.cancelNotifications(localmsg, bxtime);
        }
        else {
            localmsg = "Update Meds Taken Online";
            fsm.updateTakenOnline(localmsg);
        }
    }

    var oncancelNotificationsFcn = function (event, from, to, msg, bxtime) {
        console.log("Cancel Notifications " + from + " " + to + " " + msg);
        cancelBoxNotificationsAfterTaken(bxtime, $cordovaLocalNotification, $timeout).then(function(){
            var localmsg = "canelled Notfn. Resched";
            fsm.rescheduleNotifications(localmsg, bxtime);
        });
    }

    var onrescheduleNotificationsFcn = function (event, from, to, msg, bxtime) {
        console.log("ReSchedule Notifications " + from + " " + to + " " + msg);
        scheduleAllNotificationsForBoxTime(bxtime, $cordovaLocalNotification, $timeout, $rootScope, $ionicPopup);
        var localmsg = "Update Meds Taken Online";
        fsm.updateTakenOnline(localmsg);
    }

    var onupdateTakenOnlineFcn = function (event, from, to, msg) {
        console.log("Taken Status Online " + from + " " + to + " " + msg);
        
        var titleText = "MedMonitor Meds Taken"

        var Success = function(responseData) {
            var localmsg = "";
            var responseCode = responseData.ERROR;
            var responseMessage = responseData.MESSAGE;
        
            if(responseCode === undefined)
                responseCode = 0;
        
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";
        
            switch(responseCode) {
                case responseCodeSUCCESS:
                    localmsg = "Update on Server";
                    fsm.updateTakenOnlineTag(localmsg);
                    break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiSummaryFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                    break;
            }
        }

        var Fail = function (httpResponse) {
            apiSummaryFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var medstaken = medsTobeUpdatedOnServer();
        if($.isEmptyObject(medstaken) == false) {
            loadingSpinner.show();
            medstaken = JSON.stringify(medstaken);
    
            var inputData = $httpParamSerializer({data:{phone:phoneval,
                                                        otptoken:gblOtpToken,
                                                        medstaken:medstaken},
                                                  version:$scope.version,
                                                  platform:$scope.platform,
                                                  model:$scope.model,
                                                  regId:$rootScope.regId});
    
            updateMedsTakenStatus.save([], inputData, Success, Fail);
        }
        else {
            fsm.waitAgain("No Meds to be updated online");
        }
    }

    var onupdateTakenOnlineTagFcn = function (event, from, to, msg) {
        console.log("Update Tags " + from + " " + to + " " + msg);
        updateServerTagForDose(1);
        var localmsg = "Sync User Data";
        fsm.syncUserData(localmsg);
    }

    var onsyncUserDataFcn = function (event, from, to, msg) {
        console.log("Sync User Data " + from + " " + to + " " + msg);

        var titleText = "MedMonitor User Info";

        var Success = function(responseData) {
            var localmsg = "";
            var responseCode = responseData.ERROR;
            var responseMessage = responseData.MESSAGE;
        
            if(responseCode === undefined)
                responseCode = 0;
        
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";
        
            switch(responseCode) {
                case responseCodeSUCCESS:
                    localmsg = "Update on Server";
                    fsm.saveUserData(localmsg, responseData);
                    break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiSummaryFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                    break;
            }
        }

        var Fail = function (httpResponse) {
            apiSummaryFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var inputData = $httpParamSerializer({data:{
                                                    phone:phoneval,
                                                    otptoken:gblOtpToken
                                              },
                                              version:$scope.version,
                                              platform:$scope.platform,
                                              model:$scope.model,
                                              regId:$rootScope.regId});
        getUserInfo.save([], inputData, Success, Fail);
    }

    var onsaveUserDataFcn = function (event, from, to, msg, userData) {
        console.log("Sync User Data " + from + " " + to + " " + msg);

        var localmsg = "";
        window.USERCONFIG.saveInfo(userData);

        var latest_stamp = userData["lastUpdate"];
        var userObj = window.storage.get('USER_INFO');
        var current_stamp = userObj["lastUpdate"];
        if(glb24HRClockflag != userData["24HRClockflag"]) {
            glb24HRClockflag = userData["24HRClockflag"];
            $rootScope.$broadcast("medlistupdate","Update 24 time format");
        }
        if(parseInt(latest_stamp) > parseInt(current_stamp)) {   //sync required
            // $.when(syncBiometricFiledData(inputObj), syncUserMedData(inputObj), syncGlobalMedData(inputObj)).then(function(){
            // //Take the user to summary page to realign data after sync happened
            // window.location.href = "summary.html";
            // });
            localmsg = "User Info not in Sync. Further Update Reqd";
        }
        else {
            localmsg = "User Info in Sync. No Further Update";
        }
        fsm.waitAgain(localmsg);
    }

    var onwaitAgainFcn = function (event, from, to, msg) {
        console.log("Wait Again " + from + " " + to + " " + msg);
        loadingSpinner.hide();
        updateScope();
    }

    var diffInDays = function(date1, date2) {
        var time1, time2;
        //Create a Date object pointing to start of day
        var time1 = new Date(date1.toDateString());
        //get time stamp in ms pointing to start of day
        time1 = time1.getTime();

        //Create a Date object pointing to start of day
        var time2 = new Date(date2.toDateString());
        //get time stamp in ms pointing to start of day
        time2 = time2.getTime();

        // ceil for +ve and floor for -ve diff
        diff = (time1-time2)/(86400000);
        diff = Math.sign(diff)*Math.ceil(Math.abs(diff));
        return diff;
    }


    function updateBiometricTakenOffline(bmObjArr) {
        var bmTakenInfoObj = window.storage.get('BIOMETRIC_TAKEN_INFO');
        var bmDaysObj = {};
    
        if(Object.keys(bmTakenInfoObj).length){
            bmDaysObj = getValueForKey(bmTakenInfoObj,"biometrics");
        } 
        
        var today = new Date().yyyymmdd();
        
        if (!bmDaysObj.hasOwnProperty(today)) {
            bmDaysObj[today] =bmObjArr;
        }
        else{
            var tempArr = bmDaysObj[today];
            var concatedArr = tempArr.concat(bmObjArr);
            bmDaysObj[today] = concatedArr;
        }
            
        bmTakenInfoObj["biometrics"] = bmDaysObj;
        window.storage.set('BIOMETRIC_TAKEN_INFO', bmTakenInfoObj);
    }

    function updateBiometricTakenOnline() {
        var Success = function(responseData) {
            var localmsg = "";
            var titleText = "Biometric Info Upload Error";
            var responseCode = responseData.ERROR;
            var responseMessage = responseData.MESSAGE;

            if(responseCode === undefined)
                responseCode = 0;

            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    localmsg = "updateBiometricTakenOnline on Server";
                    updateServerTagForBiometric(1);
                    loadingSpinner.hide();
                    break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiSummaryFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                    break;
            }
        }

        var Fail = function (httpResponse) {
            apiSummaryFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var bmObjFinal = biometricsTobeUpdatedOnServer();
        if($.isEmptyObject(bmObjFinal) == false) {
            loadingSpinner.show();
            bmObjFinal = JSON.stringify(bmObjFinal);
    
            var inputData = $httpParamSerializer({data:{phone:phoneval,
                                                        otptoken:gblOtpToken,
                                                        biometrics:bmObjFinal},
                                                  version:$scope.version,
                                                  platform:$scope.platform,
                                                  model:$scope.model,
                                                  regId:$rootScope.regId});
    
            updateBiometricTakenStatus.save([], inputData, Success, Fail);
        }
        else {          //Cancel
            console.log("No BioMetrics to be updated");
        }
    }

    var updateScope = function() {
        var infoObj = window.storage.get('USER_INFO');
        $scope.name=infoObj.user_name;
        $scope.agency=infoObj.agency_name;
        $scope.doctor=infoObj.doctor_name;
        if($scope.agency.length > 0)
            $scope.agencyStr = $scope.agency;
        else if($scope.doctor.length > 0)
            $scope.agencyStr = $scope.doctor;
        else
            $scope.agencyStr = "BeneSalus Technologies";

        $scope.adherence=infoObj.adherence_percentage;

        $scope.medicationInfo = getMedBoxInfo($scope.date);
    }

    $rootScope.$on('medlistupdate', function(e, value) {
        console.log('medlistupdate' + value);
        updateScope();
    });

    $rootScope.$on('rssi', function(e, value) {
        console.log('rssi', value);
    });

    $rootScope.$on('configupdate', function(e, value) {
        console.log('configupdate', value.Str);
    });

    $rootScope.$on('blemedtaken', function(e, value) {
        console.log('blemedtaken', value);
        var msg = "Update Taken";
        fsm.updateTakenOffline(msg, value.bxtime, value.newtime, 'NOT_MISSED', value.dt, $rootScope.pillbox.macID);
    });

    $rootScope.$on('bleconnected', function(e, value) {
        $scope.$apply();
    });

    $rootScope.$on('bledisconnect', function(e, value) {
        $scope.$apply();
    });

    $rootScope.$on('updatebadge', function(e, value) {
        var badgeObj = window.storage.get('BADGE_INFO');
        var updateFlag = false;
        var deltaTime = glbBadgeLife;//secs
        var checkTime = Math.floor(new Date().getTime()/1000) - deltaTime;
        console.log('updatebadge', value, checkTime);

        if(badgeObj !== undefined) {
            $scope.badges = [];
            for(i = 0; i < badgeObj.length; i++) {
                if(badgeObj[i].rxtime < checkTime) {
                    badgeObj.splice(i, 1);
                    updateFlag = true;
                }
                else {
                    var badgeColor = badgeObj[i].color + "-icon";
                    var badge = {icon:"ion-ribbon-a", color:badgeColor};
                    $scope.badges.push(badge);
                }
            }
        }

        if(updateFlag == true) {
            window.storage.set('BADGE_INFO', badgeObj);
            $scope.$apply();
        }
    });

    $rootScope.$on('checkunsentdata', function(e, value) {
        if(window.storage.loggedIn()) {
            var localmsg = "Update Meds Taken Online";
            console.log('checkunsentdata', value);
    
            fsm.updateTakenOnline(localmsg);
            updateBiometricTakenOnline();
            updateOnline.checkAndUpdateMedsOnLine();
        }
    });

    $scope.showMedInfo = function(idx) {
        var box_time = $scope.timeDisplay($scope.medicationInfo[idx].box_time);
        var box_color = $scope.medicationInfo[idx].boxColor;
        var localscope = $scope.medicationInfo[idx];
        $scope.localscope = localscope;
        $scope.box_color = box_color;
        var myPopup = $ionicPopup.alert({
            scope: $scope,
            templateUrl:'medinfo.html', 
            cssClass: "alertpopup-positive",
            title:  box_time,
            okType: "button-positive",
        });
    };

    $scope.getMedTime = function(idx, event) {
        var box_time = $scope.medicationInfo[idx].box_time;
        var box_color = $scope.medicationInfo[idx].boxColor;
        var templateUrl = 'getdosetimeampm.html';
        var curDate = $scope.date;
        var boxTimeStr = box_time.split(':');
        // curDate.setHours(boxTimeStr[0], boxTimeStr[1]);
        var localscope = {time: curDate};
        $scope.localscope =  localscope;
        $scope.box_color = box_color;
        if(glb24HRClockflag) {
            templateUrl = 'getdosetime.html';
        }
        var myPopup = $ionicPopup.confirm({
            scope: $scope,
            templateUrl:templateUrl,
            cssClass: "confirmpopup-positive",
            title:  $scope.timeDisplay(box_time),
            okText: "Set Taken Time",
            okType: "button-positive",
            cancelType: "button-assertive",
        });
        myPopup.then(function(res) {
            if(res) { //OK
                var dt = curDate.yyyymmdd();
                var newtime = $scope.localscope.time.HHMM();
                var msg = "Update Taken";
                fsm.updateTakenOffline(msg, box_time, newtime, 'NOT_MISSED', dt, 'app');
                // console.log("OK", dt, newtime);
            }
            else {
                // console.log("Cancel", $scope.localscope.time);
            }
        });
        event.stopPropagation();
    };

    $scope.getBioMetrics = function() {
        if($scope.biometrics.length == 0) {
            $scope.bmValues = null;
        }
        else {
            $scope.bmValues = {};
        }

        var myPopup = $ionicPopup.confirm({
            scope: $scope,
            templateUrl:'biometricform.html', 
            cssClass: "confirmpopup-positive",
            title:  "Enter BioMetrics",
            okText: "Update BioMetrics",
            okType: "button-positive",
            cancelType: "button-assertive",
        });
        myPopup.then(function(res) {
            if(res) {       //OK
                var bmObjArr = [];
                $.each($scope.bmValues, function(key, value) {
                    var newObj = {};

                    newObj["biometric_type"] = key;
                    newObj["biometric_values"] = value;
                    newObj["taken_time"] = currentTime();
                    newObj["tzoffset"] = new Date().getTimezoneOffset();
                    newObj["updated_on_server"] = 0;
                
                    bmObjArr.push(newObj);
                });
                updateBiometricTakenOffline(bmObjArr);
                $rootScope.$broadcast("biometricupdate","Updated");
                updateBiometricTakenOnline();
            }
            else {          //Cancel
                console.log("BioMetric Cancel", $scope.biometrics);
            }
        });
    };

    $scope.nextDay = function(dayOffset) {
        $scope.date.setDate($scope.date.getDate() + dayOffset);
        updateScope();
    }

    $scope.centerDateString = function() {
        var dateString = "Not Valid";
        var todayDate = new Date();
        var diff = diffInDays(todayDate, $scope.date);
        switch(diff) {
            case 0:
                dateString = "Today";
                break;
            case 1:
                dateString = "Yesterday";
                break;
            case 2:
                dateString = "Day Before";
                break;
            default:
                dateString = "Not Valid";
        }

        return dateString;
    }

    $scope.checkValidDate = function(offset) {
        var retval = true;
        var todayDate = new Date();
        var diff = diffInDays(todayDate, $scope.date);
        if(offset == $scope.LEFT) {//left arrow true diff = 0, 1
            if((diff > -1) && (diff < 2)) {
                retval = false;
            }
        }
        if(offset == $scope.RIGHT) {//right arrow true diff = 1,2
            if((diff > 0) && (diff < 3)) {
                retval = false;
            }
        }
        // console.log("checkValidDate2", retval);
        return retval;
    }

    $scope.getTakenTime = function(box_time, dt) {
        var taken_time = getTakenTime(box_time, dt);//get taken time
        if(!taken_time)
            taken_time='?';

        return taken_time;
    }

    $scope.greeting = function () {
        return "Welcome";
    }

    var userObj = window.storage.get('USER_INFO');
    console.log("First glb");
    glb24HRClockflag = userObj["24HRClockflag"];
    updateScope();
    console.log("Second update");

    var fsm = StateMachine.create({
        initial: 'waitForUpdate',
        events: [
                { name: 'updateTakenOffline',       from: 'waitForUpdate',                                      to: 'updatedTakenOffline' },
                { name: 'cancelNotifications',      from: 'updatedTakenOffline',                                to: 'cancelledNotifications' },
                { name: 'rescheduleNotifications',  from: 'cancelledNotifications',                             to: 'rescheduledNotifications' },
                { name: 'updateTakenOnline',        from: ['waitForUpdate', 'updatedTakenOffline', 'rescheduledNotifications'],  to: 'updatedTakenOnline' },
                { name: 'updateTakenOnlineTag',     from: 'updatedTakenOnline',                                 to: 'updatedTakenOnlineTag' },
                { name: 'syncUserData',             from: 'updatedTakenOnlineTag',                              to: 'synchedUserData' },
                { name: 'saveUserData',             from: 'synchedUserData',                                    to: 'savedUserData' },
                { name: 'waitAgain',                from: ['updatedTakenOffline', 'cancelledNotifications', 'rescheduledNotifications', 'updatedTakenOnline', 'updatedTakenOnlineTag', 'synchedUserData', 'savedUserData'], to: 'waitForUpdate' }
            ],
        callbacks : {
            onupdateTakenOffline: onupdateTakenOfflineFcn,
            oncancelNotifications: oncancelNotificationsFcn,
            onrescheduleNotifications: onrescheduleNotificationsFcn,
            onupdateTakenOnline: onupdateTakenOnlineFcn,
            onupdateTakenOnlineTag: onupdateTakenOnlineTagFcn,
            onsyncUserData: onsyncUserDataFcn,
            onsaveUserData: onsaveUserDataFcn,
            onwaitAgain: onwaitAgainFcn
        }
    });
})

.directive('pgBar', function() {
    function link(scope, element, attrs) {
        var barColor = "#00FF00";
        if(scope.adherence > 80) {
            barColor = "#00FF00";
        }
        else if(scope.adherence > 40) {
            barColor = "#FFFF00";
        }
        else {
            barColor = "#FF0000";
        }

        var circle = new ProgressBar.Circle('#progress', {
            strokeWidth: 6,
            color: barColor,
            duration: 5000,
            easing: 'easeInOut'
        });
        
        circle.animate(scope.adherence/100);
    }
    return {
        link: link
    };
});
