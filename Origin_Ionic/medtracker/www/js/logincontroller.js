angular.module('medmonitor.logincontroller', [])

.controller('LoginCtrl', function($scope, $state, $rootScope, $httpParamSerializer,
                                  $cordovaLocalNotification, $timeout, $ionicPopup, $ionicPlatform, BLE,
                                  getOTP, getSMSNumbers, getAuthorized, getMedsTakenHistory, getBioMetricHistory, getBioMetricFields, getDDS, getGlbMedList, getUserInfo, getUserMed, getSocialMediaMessages) {
    // State Machine Functions
    var ondeActivateFcn = function(event, from, to, msg) {
        console.log("getDeActivate " + from + " " + to + " " + msg);
        gblOtpToken = '';
        storage.set('USER_PHONE','');
        storage.set('USER_OTPTOKEN', '');
        $scope.loginData.phonenum = '';
        $scope.loginData.otpcode = '';
        window.storage.removeAll();
        loadingSpinner.hide();
    }

    var ongetOTPFcn = function (event, from, to, msg, phoneval) {
        console.log("getOTP " + from + " " + to + " " + msg);

        var titleText = "MedMonitor On Get OTP Code";
        var Success = function(responseData) {
            var localmsg = "";
            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    localmsg = "Get SMS Numbers";
                    gblOtpToken = responseData.OTPTOKEN;
                    //save otp
                    storage.set('USER_OTPTOKEN', gblOtpToken);
                    fsm.getSMSNumbers(localmsg);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        //var Fail = function (responseData, textStatus, jqXHR) {
        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        if(phoneval) {
            //save it to localstorage
            storage.set('USER_PHONE',phoneval);

            //show loading view
            loadingSpinner.show();

            var inputData = $httpParamSerializer({data:{phone:phoneval},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
            getOTP.save([], inputData, Success, Fail);
        }
        else {
            var localmsg ="Please enter valid phone number";
            var myPopup = $ionicPopup.alert({
                scope: $scope,
                template:localmsg,
                cssClass: "alertpopup-positive",
                title:  titleText,
                okType: "button-assertive",
            });
            
            myPopup.then(function(res) {
                fsm.deActivate(localmsg);
            });
        }
    }

    var ongetSMSNumbersFcn = function (event, from, to, msg) {
        console.log("getSMSNumbers " + from + " " + to + " " + msg);

        var titleText = "MedMonitor On Get SMS";

        var Success = function(responseData) {
            var SMSNumberArr="", SMSFormat="";
            var localmsg = "";
            var responseCode = responseData.ERROR;

            if(responseCode === undefined)
                responseCode = 0;
        
            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            loadingSpinner.hide();

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    localmsg = "Registration code sent on your mobile. Please enter below";
                    SMSNumberArr = responseData.Numbers;
                    SMSFormat = responseData.Format;
                    genericPopupAlert($scope, $ionicPopup, localmsg, titleText);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }
        getSMSNumbers.save(Success, Fail);
    }

    var onauthFcn = function (event, from, to, msg, phoneval, otpval) {
        console.log("getSMSNumbers " + from + " " + to + " " + msg);

        var titleText = "MedMonitor OTP Auth";
        
        var Success = function(responseData) {
            var localmsg = "";
            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    localmsg = "Get Med History";
                    fsm.getMedsTakenHistory(localmsg);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        if(phoneval && otpval){
            var inputData = $httpParamSerializer({data:{phone:phoneval, otp:otpval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
            getAuthorized.save([], inputData, Success, Fail);
        }
    }

    var ongetMedsTakenHistoryFcn = function (event, from, to, msg) {
        console.log("getSMSNumbers " + from + " " + to + " " + msg);

        var titleText = "MedMonitor Medication History";

        var Success = function(responseData) {
            var localmsg = "";
            var emptyObj = {};
            var medsTakenObj = {};
            var keys;

            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    keys = Object.keys(responseData);
                    if(keys.length==0) {
                        medsTakenObj = emptyObj;
                    }
                    else if(keys.length==1) {
                        var innerObj = getValueForKey(responseData,keys[0]);
                        var slots = innerObj["dosage_slots"];
                        if(slots.length == 0) {
                            medsTakenObj = emptyObj;
                        } else {
                            medsTakenObj = responseData;
                        }

                    } else {
                        var innerObj = getValueForKey(responseData,keys[0]);
                        var slots = innerObj["dosage_slots"];
                        if(slots.length == 0) {
                            medsTakenObj = emptyObj;
                        } else {
                            medsTakenObj = responseData;
                        }
                    }
                    
                    saveMedsTakenHistory(medsTakenObj);

                    localmsg = "Get BioMetric History";
                    fsm.getBioMetricHistory(localmsg);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var curDay = new Date().yyyymmdd();

        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken, day:curDay, days:30},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getMedsTakenHistory.save([], inputData, Success, Fail);
    }

    var ongetBioMetricHistoryFcn = function (event, from, to, msg) {
        console.log("getBioMetricHistory " + from + " " + to + " " + msg);

        var titleText = "MedMonitor BioMetric History";

        var Success = function(responseData) {
            var localmsg = "";
            var newObj = {};

            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    newObj["biometrics"] = responseData;
                    window.storage.set('BIOMETRIC_TAKEN_INFO', newObj);
                    updateServerTagForBiometric(1);  //add updated_on_server=1 key for all
                    localmsg = "Get BioMetric History from Success";
                    fsm.getBioMetricFields(localmsg);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var curDay = new Date().yyyymmdd();
        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken, day:curDay, days:30},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getBioMetricHistory.save([], inputData, Success, Fail);
    }

    var ongetBioMetricFieldsFcn = function (event, from, to, msg) {
        console.log("getBioMetricFields " + from + " " + to + " " + msg);

        var titleText = "MedMonitor BioMetric Fields";

        var Success = function(responseData) {
            var localmsg = "";

            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    window.storage.set('BIOMETRIC_FIELD_INFO', responseData);
                    localmsg = "Get Default Dose Schedule from Success";
                    fsm.getDefaultDoseSchedule(localmsg);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var curDay = new Date().yyyymmdd();
        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken, day:curDay, days:30},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getBioMetricFields.save([], inputData, Success, Fail);
    }

    var ongetDefaultDoseScheduleFcn = function (event, from, to, msg) {
        console.log("get Default Dose Sch " + from + " " + to + " " + msg);
        var titleText = "MedMonitor Default Dose Sch";

        var Success = function(responseData) {
            var localmsg = "";

            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    saveSchedule(responseData);
                    localmsg = "Get Global MedList from Success";
                    fsm.getGlobalMedList(localmsg);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler(fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var curDay = new Date().yyyymmdd();
        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken, day:curDay, days:30},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getDDS.save([], inputData, Success, Fail);
    }

    var ongetGlobalMedListFcn = function (event, from, to, msg) {
        console.log("get global med list " + from + " " + to + " " + msg);
        
        var titleText = "MedMonitor Global Medication List";

        var Success = function(responseData) {
            var localmsg = "";

            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    if(responseData.meds.length > 0){
                        //save globaal medication data recieved from ajax
                        saveGlobalMedList(responseData);
                        localmsg = "Get User Data";
                        fsm.getUserData(localmsg);
                    }
                    else{
                        localmsg = "No Global MedList for this User. Contact Benesalus Support. Support@benesalustech.com.";
                        var myPopup = $ionicPopup.alert({
                            scope: $scope,
                            template:localmsg,
                            cssClass: "alertpopup-positive",
                            title:  titleText,
                            okType: "button-assertive",
                        });
                        
                        myPopup.then(function(res) {
                            fsm.deActivate(localmsg);
                        });
                    }
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getGlbMedList.save([], inputData, Success, Fail);
    }

    var ongetUserDataFcn = function (event, from, to, msg) {
        console.log("get Default Dose Sch " + from + " " + to + " " + msg);

        var titleText = "MedMonitor User Data List";

        var firstSaveUserData = function(data) {
            var defObj = $.Deferred();
            window.USERCONFIG.saveInfo(data, defObj);
            //return the promise
            return defObj.promise();
        }

        var initSaveUserData = function(data) {
            firstSaveUserData(data).then(function () {
                var localmsg = "Get User Medication Data";
                //fetch user medication data
                fsm.getSocialMessages(localmsg);
            });
        }

        var Success = function(responseData) {
            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    initSaveUserData(responseData);
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getUserInfo.save([], inputData, Success, Fail);
    }

    var ongetSocialMessagesFcn = function (event, from, to, msg) {
        console.log("get Social Messages " + from + " " + to + " " + msg);

        var titleText = "MedMonitor Social Messages";

         var Success = function(responseData) {
             var responseCode = responseData.ERROR;
             
             if(responseCode === undefined) {
                 responseCode = 0;
             }
             
            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

             switch(responseCode) {
                 case responseCodeSUCCESS:
                     responseData = JSON.parse(angular.toJson(responseData));
                     saveSocialMediaMessages(responseData.messages);
                     fsm.getUserMedData(titleText);
                     break;
                 case responseCodeINVALIDPHONE:
                 case responseCodeINVALIDREG:
                 case responseCodeINVALIDDATA:
                 default:
                     apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                     break;
             }
         };
    
         var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
         };

        var phoneval = storage.get('USER_PHONE');
        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getSocialMediaMessages.save([], inputData, Success, Fail);
    }

    var ongetUserMedDataFcn = function (event, from, to, msg) {
        console.log("get User Med Data " + from + " " + to + " " + msg);

        var titleText = "User Medication List";
        
        var salvoMedData = function(data) {
            loadingSpinner.show();
            var defrdObject = $.Deferred();
            
            saveList(data, defrdObject);
            //return the promise
            return defrdObject.promise();
        }


         var initSaveMedData = function(data) {
             salvoMedData(data).then(checkNotificationPermissions).then(function(){
                 loadingSpinner.hide();
                 setupNotifications($cordovaLocalNotification, $timeout, $rootScope, $ionicPopup);

                 $state.go('tab.summary',{});
             });
         }
    
        var Success = function(responseData) {
            var localmsg = "";

            var responseCode = responseData.ERROR;
            if(responseCode === undefined)
                responseCode = 0;

            var responseMessage = responseData.MESSAGE;
            if (responseMessage === undefined)
                responseData.MESSAGE = "Unknown";

            switch(responseCode) {
                case responseCodeSUCCESS:
                    responseData = JSON.parse(angular.toJson(responseData));
                    if(responseData.meds.length > 0){
                        //save medication data recieved from ajax
                        initSaveMedData(responseData);
                    }
                    else{
                        localmsg = "No Data found for this User";
                        var myPopup = $ionicPopup.alert({
                            scope: $scope,
                            template:localmsg,
                            cssClass: "alertpopup-positive",
                            title:  titleText,
                            okType: "button-assertive",
                        });
                        
                        myPopup.then(function(res) {
                            fsm.deActivate(localmsg);
                        });
                    }
                break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                break;
            }
        }

        var Fail = function (httpResponse) {
            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
        }

        var phoneval = storage.get('USER_PHONE');
        var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
        getUserMed.save([], inputData, Success, Fail);
    }

    var fsm = StateMachine.create({
        initial: 'noOTP',
        events: [
            { name: 'getOTP',                   from: ['noOTP', 'gotOTP','gotSMSNumbers'],                                                  to: 'gotOTP' },
            { name: 'getSMSNumbers',            from: 'gotOTP',                                                                             to: 'gotSMSNumbers' },
            { name: 'auth',                     from: ['noOTP', 'gotSMSNumbers'],                                                           to: 'authorized' },
            { name: 'getMedsTakenHistory',      from: 'authorized',                                                                         to: 'gotMedsTakenHistory' },
            { name: 'getBioMetricHistory',      from: 'gotMedsTakenHistory',                                                                to: 'gotBioMetricHistory' },
            { name: 'getBioMetricFields',       from: 'gotBioMetricHistory',                                                                to: 'gotBioMetricFields' },
            { name: 'getDefaultDoseSchedule',   from: 'gotBioMetricFields',                                                                 to: 'gotDefaultDoseSchedule' },
            { name: 'getGlobalMedList',         from: 'gotDefaultDoseSchedule',                                                             to: 'gotGlobalMedList' },
            { name: 'getUserData',              from: 'gotGlobalMedList',                                                                   to: 'gotUserData' },
            { name: 'getSocialMessages',        from: 'gotUserData',                                                                        to: 'gotSocialMessages' },
            { name: 'getUserMedData',           from: 'gotSocialMessages',                                                                        to: 'gotUserMedData' },
            { name: 'deActivate',               from: ['gotOTP','gotSMSNumbers','authorized','getGlobalMedList','getDefaultDoseSchedule'],  to: 'noOTP' }
        ],
        callbacks : {
            ongetOTP:                   ongetOTPFcn,
            ongetSMSNumbers:            ongetSMSNumbersFcn,
            onauth:                     onauthFcn,
            ongetMedsTakenHistory:      ongetMedsTakenHistoryFcn,
            ongetBioMetricHistory:      ongetBioMetricHistoryFcn,
            ongetBioMetricFields:       ongetBioMetricFieldsFcn,
            ongetDefaultDoseSchedule:   ongetDefaultDoseScheduleFcn,
            ongetGlobalMedList:         ongetGlobalMedListFcn,
            ongetUserData:              ongetUserDataFcn,
            ongetSocialMessages:        ongetSocialMessagesFcn,
            ongetUserMedData:           ongetUserMedDataFcn,
            ondeActivate:               ondeActivateFcn
        }
    });

    // Form data for the login modal
    $scope.loginData = {};
    $scope.forms = {};
    $scope.OTP = null;

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        console.log('Changing to tasks');
        window.storage.set('MEDICATION_LIST', 1);
        loadingSpinner.hide();
        $state.go('tab.summary',{});
    }; 

    // Get OTP from cloud
    $scope.getOTP = function() {
        var phoneval = $scope.loginData.phonenum;
        fsm.getOTP("gen OTP", phoneval);
    };

    // Got OTP from cloud. Enable OTP button
    $scope.gotOTP = function() {
        var retval = false;
        if(gblOtpToken.length > 0) {
            retval = true;
        }

        return retval;
    }

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        var phoneval = $scope.loginData.phonenum;
        var otpval = $scope.loginData.otpcode;
        initStorage();

        fsm.auth("authorize", phoneval, otpval);
    };
});
