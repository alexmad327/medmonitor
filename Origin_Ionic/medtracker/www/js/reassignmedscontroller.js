angular.module('medmonitor.reassignmedscontroller', ['dragularModule'])
.controller('ReAssignMedsCtrl', function($scope, $rootScope, $ionicPopup, dragularService, $state, updateOnline) {
    var dragLeft = $("#medsList").children();
    var dragRight = $("#doseTimes").children();
    var dragArray = [dragLeft[0], dragRight[0]];

    function acceptsLR(element, target, source, sibling) {
        var retVal = false;
        var medslen =  $scope.medli.length;
        var doseslen =  $scope.dosetimes.length;
        var idxRightList = -1;;
        var idxLeftList = -1;
        var direct = 'Unk';

        if((target === dragRight[0]) && (source === dragLeft[0])) { //Move from Left to Right
            // direct = 'Move from Left to Right';
            idxLeftList = findElementIndex(source, element, -1);
            idxRightList = findElementIndex(target, sibling, -1);

            var assign = 0;
            if((idxLeftList >= 0) && (idxLeftList < medslen) && (idxRightList >= 0) && (idxRightList < doseslen)) {
                assign = $scope.medli[idxLeftList].assign;

                if(assign > 0) {
                    var med = $scope.medli[idxLeftList];
                    var meds = $scope.dosetimes[idxRightList].meds;

                    var idx = meds.indexOf(med);
                    if(idx == -1)
                        retVal = true;
                }
            }
        }
        else if((target === dragLeft[0]) && (source === dragRight[0])) { //Move from Right to Left
            // direct = 'Move from Right to Left';
            idxRightList = findElementIndex(source, element, -1);
            idxLeftList = findElementIndex(target, sibling, -1);

            if((idxLeftList >= 0) && (idxLeftList < medslen) && (idxRightList >= 0) && (idxRightList < doseslen)) {
                var meds = $scope.dosetimes[idxRightList].meds;
                if (meds.length > 0)
                    retVal = true;
            }
        }
        else {
            retVal = false;
        }

        // console.log("ACC LR", retVal);
        return retVal;
    }

    function copiesLR(element, source) {
        var retVal = false;
        var direct = 'Unk';

        if(source === dragRight[0]) {  //Move from Right to Left
            retVal = true;
            direct = 'Right To Left';
        }
        else if(source === dragLeft[0]) { //Move back from Left to Right
            retVal = true;
            direct = 'Left To Right';
        }

        // console.log("Copy", dragRight[0], dragLeft[0], source, element, direct, retVal);
        return retVal;
    }

    function movesLR(element, container, handle) {
        return true;
    }

    dragularService(dragArray, {
        containersModel: [$scope.medli, $scope.dosetimes],
    accepts: acceptsLR,
    moves: movesLR,
    copy: copiesLR,
    scope: $scope});

    dragularService(".nested", {
        containersModel: [$scope.dosetimes],
        accepts: true,
        moves: true,
        scope: $scope});

    function cancelHandler(event, element, sourcecontainer, sourcemodel, sourceIdx) {
        // console.log("Cancel");
    }

    function dropHandler(event, element, targetContainer, sourceContainer, sourceModel, elementIdx, targetModel, dropIdx) {
        var dosetimes = event.targetScope.dosetimes;
        var medli = event.targetScope.medli;
        var direct = 'Unk';
        if((targetContainer === dragRight[0]) && (sourceContainer === dragLeft[0])) { //Move from Left to Right. Assign time to med
            var assign = medli[elementIdx].assign;
            var med = medli[elementIdx];
            // dosetimes.splice(dropIdx, 1);
            dosetimes[dropIdx].meds.push(med);
            medli[elementIdx].assign = assign - 1;
            direct = 'Move back from Left to Right';
        }
        else if((targetContainer === dragLeft[0]) && (sourceContainer === dragRight[0])) { //Move back from Right to Left. Remove a move from a time
            var medsToDrop = dosetimes[elementIdx].meds;
            // medli.splice(dropIdx, 1);
            for(var i = 0; i < medsToDrop.length; i++) {
                med = medsToDrop[i];
                for(var j = 0; j < medli.length; j++) {
                    if(medli[j].desc === med.desc) {
                        medli[j].assign++;
                        break;
                    }
                }
            }
            targetContainer.removeChild(targetContainer.childNodes[(dropIdx + 1)*2]);

            direct = 'Move from Right to Left';
            medsToDrop.splice(0);
            $scope.$apply();
        }
        console.log("Drop",medli, dosetimes, sourceContainer, targetContainer, dropIdx, direct);
    }

    function myFn(eventName) {
        return function() {
            if(eventName !== 'shadow') {
                console.log(eventName, arguments);
            }
        };
    }

    $scope.$on('dragulardrag', myFn('drag'));
    $scope.$on('dragularrelease', myFn('release'));
    $scope.$on('dragulardragend', myFn('dragEndHandler'));
    $scope.$on('dragulardrop', dropHandler);
    $scope.$on('dragularcancel', cancelHandler);
    $scope.$on('dragularremove', myFn('remove'));
    $scope.$on('dragularshadow', myFn('shadow'));
    $scope.$on('dragularcloned', myFn('cloned'));
    $scope.$on('dragularover', myFn('over'));
    $scope.$on('dragularout', myFn('out'));

    $scope.getDoseTime = function (freq) {
        var eachMedDefTimes = getDefaultTimes(freq+'D');
        var newtimes = adjustedDosageTimes(eachMedDefTimes);
        var dosetimes = [];
        for(var i = 0; i < newtimes.length; i++) {
            var dosetime = {};
            dosetime.time = newtimes[i];
            dosetime.meds = [];
            dosetimes.push(dosetime);
        }
        return dosetimes;
    }

    $scope.getColor = function(assign) {
        retVal = '';
        if(assign === 0){
            retVal = 'assigned';
        }
        // console.log("Color", assign,retVal);

        return retVal;
    }

    $scope.Accept = function() {
        if($scope.checkAssign() == true) {
            var medliObj = {};
            $scope.medli.forEach(function(med) {
                var newtimes = [];
                $scope.dosetimes.forEach(function(dosetime) {
                    dosetime.meds.forEach(function(doseMed) {
                        if(doseMed.desc == med.desc) {
                            newtimes.push(dosetime.time);
                        }
                    });
                });
                med.assignedtimes = newtimes;
            });
            
            //Update default dose times
            var newtimes = getAssignedDoseTimes($scope.medli);
            newtimes.sort(function(a,b) {
                return (doseTimeToMin(a) - doseTimeToMin(b));
            });

            medliObj["meds"] = $scope.medli;
            medliObj["updated_on_server"] = 0;
            window.storage.set('MEDICATION_LIST', medliObj);
            window.storage.set('DEFAULT_BOX_TIMES', newtimes);
            window.storage.set('ADJUSTED_MINS', 0);

            var setDefaultBoxTimeFlag = false;
            updateOnline.updateMedsOnLine(setDefaultBoxTimeFlag, "ReAssign Meds");
            $state.go('tab.settings');
        }
        else {
            var localmsg = "All meds not assigned";
            var titleText = "MedMonitor Assign Meds";
            genericPopupAlert($scope, $ionicPopup, localmsg, titleText);
        }
    }

    $scope.autoAssign = function() {
        var localmsg = "Meds automatically assigned times. Use update time to djust dosetimes or ReAssign to manually assign times";
        var titleText = "MedMonitor Auto Assign Meds";
        var myPopup = $ionicPopup.confirm({
            scope: $scope,
            template:localmsg,
            cssClass: "confirmpopup-positive",
            title:  titleText,
            okText: "Auto Assign",
            okType: "button-positive",
            cancelType: "button-assertive",
        });
        
        myPopup.then(function(res) {
            if(res) { //Auto Assign
                $scope.medli = getMedicationList();
                var numDoses = getNumDoses($scope.medli);
                $scope.medli.forEach(function(med) {
                    var fq = med.freq ;
                    med.assign=fq;
                });
                
                $scope.dosetimes = $scope.getDoseTime(numDoses);
        
                $scope.medli.forEach(function(med) {
                    var fq = med.freq ;
                    var eachMedDefTimes = getDefaultTimes(fq+'D');
        
                    //new adjusted times
                    var newtimes = adjustedDosageTimes(eachMedDefTimes);
                    newtimes.forEach(function(curTime) {
                        $scope.dosetimes.forEach(function(dosetime) {
                            if(curTime == dosetime.time) {
                                dosetime.meds.push(med);
                            }
                        });
                    });
                    med.assign = 0;
                    console.log("Dosetime", med, newtimes, $scope.dosetimes);
                });
            }
            else {//Cancel
            }
        });
    }

    $scope.Reset = function() {
        var localmsg = "Reset and unassign dosetimes";
        var titleText = "MedMonitor Reset Med Assignment";
        var myPopup = $ionicPopup.confirm({
            scope: $scope,
            template:localmsg,
            cssClass: "confirmpopup-positive",
            title:  titleText,
            okText: "Reset",
            okType: "button-assertive",
            cancelType: "button-positive",
        });
        
        myPopup.then(function(res) {
            if(res) { //Reset
                $scope.medli = getMedicationList();
                var numDoses = 4;
                $scope.medli.forEach(function(med) {
                    med.assign=med.freq;
                });
                
                $scope.dosetimes = $scope.getDoseTime(numDoses);
            }
            else {//Cancel
            }
        });
    }

    $scope.assignColor = function() {
        var clr = "button-balanced";
        if($scope.checkAssign() == false) {
            clr = "button-assertive";
        }

        return clr;
    }

    $scope.checkAssign = function() {
        var assignFlag = true;
        for(var i = 0; i < $scope.medli.length; i++) {
            var med = $scope.medli[i];
            if(med.assign != 0) {
                assignFlag = false;
                break;
            }
        }
        
        return assignFlag;
    }

    $scope.medli = getMedicationList();
    $scope.medli.forEach(function(med) {
        med.assign=med.freq;
    });
    $scope.dosetimes = $scope.getDoseTime(4);
});
