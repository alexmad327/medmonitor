var rawRead = function(eventArray, len) {
    var event = eventArray[0]; //len should be 1
    return event;
}

var processString = function(string, len) {
    return string;
}

var processMacID = function(string, len) {
    macID = '';
    len = 6; //8 bytes. last  bytes are 0xff
    var num = string.charCodeAt(0);
    var hexRep = num.toString(16);
    if(num < 16)
        hexRep = '0' + hexRep;

    macID = hexRep;

    for(idx = 1; idx < len; idx++) {
        num = string.charCodeAt(idx);
        hexRep = num.toString(16);
        if(num < 16) {
            hexRep = '0' + hexRep;
        }
        macID += ':' + hexRep;
    }
    return macID;
}

var byte2short = function(byteArray, startIdx) {
    var shortVal = byteArray[startIdx + 1] + byteArray[startIdx +  0]*256;
    return shortVal;
}

var readSystemID = function(data, len) {
    var manuID =  (data[0] + (data[1]  << 8) + (data[2] << 16) + (data[3] << 24) + (data[4] << 32));
    var orgID  =  (data[5] + (data[6]  << 8) + (data[7] << 16)); 
    var sysStr = 'M' + manuID + '. O' + orgID;
    console.log("System ID " + JSON.stringify(data));
    return sysStr;
}

var readTimeArray = function(data, len) {
    var timeStr;
    if(data[len-1] == 0xBE) {
        timeStr = (data[0] + 2000) + //Year
                  '/' + data[1]    + //Month
                  '/' + data[2]    + //Day
                  ' ' + data[3]    + //Hour
                  ':' + data[4]    + //Minute
                  ':' + data[5];     //Second
    }
    else
        timeStr = 'Invalid Str';

    // console.log("readTimeArray " + timeStr);
    return timeStr;
}

var readEventArray = function(data, len) {
    var EventsData = {};
    var GoodFlag;
    console.log("Events", data);
    if(data[len-1] == 0xBE) {
        if((data[6] & 0x80) == 0) {
            GoodFlag = 1;
            EventsData.Str = 'Good Slot';
        }
        else {
            EventsData.Str = 'Wrong Slot';
            GoodFlag = 0;
        }
        
        var slotNum   = data[0]&0x7F;
        var year      = (data[1]&0x7F) + 2000;
        var month     = data[2]&0x7F;
        var day       = data[3]&0x7F;
        var hour      = data[4]&0x7F;
        var min       = data[5]&0x7F;
        var SnoozeCnt = data[6]&0x7F;

        EventsData.Str += data[0]                + //Slot Num
                   ' '  + (data[1] + 2000) + //Year
                   '/'  + data[2]          + //Month
                   '/'  + data[3]          + //Day
                   ' :' + data[4]          + //Hour
                   ':'  + data[5]          + //Minute;
                   ' . Snz#' + SnoozeCnt;    //Snooze Cnt
        
        if(slotNum <= numSlots) {
            EventsData.timeStamp = {};
            var bxTime = getSlotHrMin(slotNum - 1);
            var bxHour = bxTime.hrs; //dose idx Hours
            var bxMin  = bxTime.mins; //dose idx Minutes
            EventsData.timeStamp.bxtime = (bxHour) + ':' + pad(bxMin);
            EventsData.timeStamp.newtime = pad(hour) + ':' + pad(min);
            EventsData.timeStamp.dt = year.toString() + pad(month) + pad(day);
            // updateTakenStatus(bxtime, newtime, 'NOT_MISSED', dt);
        }
        else {
            EventsData.Str += 'invalidSlot!' + slotNum + numSlots;
        }
    }
    else
        EventsData.Str = 'Invalid Str';

    // console.log("Event " + EventStr);
    return EventsData;
}

var readConfigArray = function(data, len) {
    var ConfigData = {};
    ConfigData.Str = '';
    var idx = 0;
    if(data[len-1] == 0xBE) {
        for(idx = 0; idx < numSlots; idx++) {
            ConfigData.Str += 'Slot' + (idx + 1) + ': ' + data[2*idx] + ':' + data[2*idx + 1];
        }
        ConfigData.Str += 'Snz ' + (data[8] & 0x0F);
        ConfigData.Str += 'm/'   + ((data[8] >> 4) & 0x0F);
        ConfigData.Str += '24H ' + data[9];
        ConfigData.Str += 'Lid ' + ((data[10] & 0x0F) * 2);
        ConfigData.Str += 'Tol ' + (((data[10] >> 4) & 0x0F) & 5);
    }
    else
        ConfigData.Str = 'Invalid Str';

    // console.log("Config " + ConfigStr);
    return ConfigData;
}

var readStatusArray = function(data, len) {
    var Status = {};
    Status.Str = '';
    Status.eventCnt = 0;

    if(data[len-1] == 0xBE) {
        var goodCnt, badCnt;
        var selfTest;
        var selfTestStr = '';
        var batteryStatus;
        var batteryStatusStr = '';
        var buttonsStatus;
        var buttonsStatusStr = '';

        goodCnt = byte2short(data, 0);
        badCnt  = byte2short(data, 2);
        Status.eventCnt = goodCnt + badCnt;
        selfTest = data[4];
        batteryStatus = data[5];
        buttonsStatus = data[6];

        console.log(batteryStatus);
        Status.bat = (batteryStatus >> 2)*2;
        batteryStatus = batteryStatus & 0x03;
        Status.charge = batteryStatus;

        if((selfTest & 0x01) > 0)
            selfTestStr += 'RTC: P ';
        else
            selfTestStr += 'RTC: Y ';

        if((selfTest & 0x02) > 0)
            selfTestStr += 'OLED: P ';
        else
            selfTestStr += 'OLED: Y ';

        if((selfTest & 0x04) > 0)
            selfTestStr += 'FGa: P ';
        else
            selfTestStr += 'FGa: Y ';

        if((selfTest & 0x08) > 0)
            selfTestStr += 'LTC: P ';
        else
            selfTestStr += 'LTC: Y ';

        if((selfTest & 0x10) > 0)
            selfTestStr += 'IOx: P ';
        else
            selfTestStr += 'IOx: Y ';

        if((selfTest & 0x20) > 0)
            selfTestStr += 'EEPROM: P ';
        else
            selfTestStr += 'EEPROM: Y ';
        
        switch(batteryStatus) {
            case 0:
                batteryStatusStr = 'NC ';
                break;
            case 1:
                batteryStatusStr = 'WL '; //Wireless charger
                break;
            case 2:
                batteryStatusStr = 'USB '; //USB charger
                break;
            default:
                batteryStatusStr = 'Unk '; //USB charger
                break;
        }
        batteryStatusStr += Status.bat + '% ';

        if((buttonsStatus & 0x01) == 0)
            buttonsStatusStr += 'Slt4: C ';
        else
            buttonsStatusStr += 'Slt4: O ';

        if((buttonsStatus & 0x02) == 0)
            buttonsStatusStr += 'Slt3: C ';
        else
            buttonsStatusStr += 'Slt3: O ';

        if((buttonsStatus & 0x04) == 0)
            buttonsStatusStr += 'Slt2: C ';
        else
            buttonsStatusStr += 'Slt2: O ';

        if((buttonsStatus & 0x08) == 0)
            buttonsStatusStr += 'Slt1: C ';
        else
            buttonsStatusStr += 'Slt1: O ';

        if((buttonsStatus & 0x10) == 0)
            buttonsStatusStr += 'Mode: C ';
        else
            buttonsStatusStr += 'Mode: O ';

        if((buttonsStatus & 0x20) == 0)
            buttonsStatusStr += 'Snz: C ';
        else
            buttonsStatusStr += 'Snz: O ';

        if((buttonsStatus & 0x40) == 0)
            buttonsStatusStr += 'Stat: C ';
        else
            buttonsStatusStr += 'Stat: O ';

        Status.Str = 'Cnt:'   + goodCnt +
                    '/'      + badCnt +
                    'Test:'  + selfTestStr + buttonsStatusStr +
                    'Chg:'   + batteryStatusStr;
                 // 'TBD: '    + data[6];
    }
    else {
        Status.Str = 'Invalid Str';
        Status.eventCnt = 0;
    }

    console.log("Status " + Status.Str, data, buttonsStatus, buttonsStatus&0x01, buttonsStatus&0x02, buttonsStatus&0x04);
    return Status;
}

var readNotificationArray = function(data, len) {
    var notifnStr = '';
    var val = data[0];
    if(data[pBSNotificationCharxLen - 1] == 0xBE) {
        notifnStr += val;
        switch(val) {
            case 1:
            case 2:
            case 3:
            case 4:
                notifnStr += ': Alarm Slot' + val;
                break;
            case 5:
                notifnStr += ': Bat Low';
                break;
            case 6:
                notifnStr += ': Wrong Med';
                break;
            case 7:
                notifnStr += ': Bat Good';
                break;
            default:
                notifnStr += ': Unknown';
                break;
        }
    }
    else
        notifnStr += 'Invalid Str';

    // console.log("notifn " + notifnStr);
    return notifnStr;
}

var readModeArray = function(data, len) {
    var mode = 0xFF;

    if(data[len-1] == 0xBE) {
        mode = data[0];
    }

    return mode;
}

function pad(n) {
    return n < 10 ? "0"+n : n;
}

var formatTime = function(timestamp) {
    var timeVal = new Date(timestamp);
    var optionTime = {hour: "2-digit", minute: "2-digit", second: "2-digit"};
    var timeStr = timeVal.toLocaleTimeString("en-US", optionTime);
    return timeStr;
}

var formatDate = function(timestamp) {
    var timeVal = new Date(timestamp);
    var optionDate = {year: "numeric", month: "short", day:"numeric"};
    var timeStr = timeVal.toLocaleDateString("en-US", optionDate);
    return timeStr;
}

var formatDateTime = function(timestamp) {
    var timeVal = new Date(timestamp);
    var monthStr = ["Jan", "Feb", "Mar", "Apr", "May","Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    // var optionDate = {year: "numeric", month: "short", day:"numeric"};
    // var optionTime = {hour: "2-digit", minute: "2-digit", second: "2-digit"};
    // var timeStr = timeVal.toLocaleDateString("en-US", optionDate);
    // timeStr += "," + timeVal.toLocaleTimeString("en-US", optionTime);
    timeStr = monthStr[timeVal.getMonth()]+" "+pad(timeVal.getDate())+", "+timeVal.getFullYear()+" " + pad(timeVal.getHours()) + ":" + pad(timeVal.getMinutes()) + ":" + pad(timeVal.getSeconds())
    return timeStr;
}

var timeToBuffer = function (hourIn, minIn) {
    var data = new Uint8Array(pBSRTCCharxLen);
    data[pBSRTCCharxLen-1] = 0xBE;
    var d = new Date();

    //Debug setup time to be 13:58 Local time
    /*
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var hour = 12;
    var minutes = 58;
    var seconds = 45;
    */

    //GMT
    /*
    //var year = d.getUTCFullYear();
    //var month = d.getUTCMonth();
    //var day = d.getUTCDate();
    //var hour = d.getUTCHours();
    //var minutes = d.getUTCMinutes();
    //var seconds = d.getUTCSeconds();
    */

    //Local Time
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var hour = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();

    var hourOut, minOut;

    if (typeof(hourIn)==='undefined')
        hourOut = hour;
    else if((hourIn >= 0) && (hourIn <=23)) {
        hourOut = hourIn;
        seconds = 0;
    }
    else
        hourOut = hour;

    if (typeof(minIn)==='undefined')
        minOut = minutes;
    else if((minIn >= 0) && (minIn <=59)) {
        minOut = minIn;
        seconds = 0;
    }
    else
        minOut = minutes;

    data[0] = year - 2000;
    data[1] = month + 1;
    data[2] = day;
    data[3] = hourOut;
    data[4] = minOut;
    data[5] = seconds;

    // console.log("timeToBuffer " + JSON.stringify(data));

    return data;
}

var bufferToTime = function (data) {
    var dateStr = 2000 + data[0] + '/' + data[1] + '/' + data[2] + ' '+ data[3] + ':' + data[4] + ':' + '00';
    // console.log("bufferToTime " + dateStr);
    var eventDate = new Date(dateStr);
    var timestamp = eventDate.getTime();
    return timestamp;
}

var modeConfig = function (mode) {
    var data = [mode, 0xBE];

    console.log("modeCfg " + JSON.stringify(data));
    return data;
}

var getSlotHrMin = function (slot) {
    var bxTime = {hrs:25, mins:0};
    var boxArray = window.storage.get('BOX_TIMES');
    if(slot < boxArray.length) {
        var box_time = boxArray[slot];
        var timearr = box_time.split(":");
        bxTime.hrs = parseInt(timearr[0]);
        bxTime.mins = parseInt(timearr[1]);
    }

    return bxTime;
}

var pillBoxConfig = function (numActiveSlots) {
    var data = new Uint8Array(pBSConfigCharxLen);
    data[pBSConfigCharxLen - 1] = 0xBE;

    for(idx = 0; idx < maxNumSlots; idx++) {
        var bxTime = getSlotHrMin(idx);
        data[2*idx]     = bxTime.hrs; //dose idx Hours
        data[2*idx+ 1]  = bxTime.mins;//dose idx Mins
    }

    var snoozeTime = window.storage.get('ADJUSTED_SNOOZE_MINS');
    if(typeof snoozeTime === undefined) {
        snoozeTime = gblSnoozeMin; //Default Snooze Time min
    }

    var snoozeCount = window.storage.get('USER_SNOOZE_COUNT');
    if(typeof snoozeCount === undefined) {
        snoozeCount = gblSnoozeCount; //Default Snooze Time min
    }

    data[8] = (snoozeTime & 0x0f) | ((snoozeCount & 0x0f) << 4); //Num Snoozes & Snooze Time
    data[9] = glb24HRClockflag;//0x01 24hr, 0x00 12 hr
    //byte10 0:3 LidopenTime in 2 sec increments
    //byte10 4:7 slot tolerance in 5 min increments
    data[10] = (Math.floor(lidOpenTime/2))&0x0f | ((Math.floor(slot_tolerance/5))&0x0f << 4); //Lid Open Time for valid event

    console.log("pBCfg " + JSON.stringify(data));

    return data;
}

var medConfig = function (slotNum) {
    var slotMedInfo = new Uint8Array(pBSSlot1InfoCharxLen);

    slotNum = slotNum - 1; //Convert t0 0-based

    var curMedInfo = medicationInfo[slotNum];
    var numMeds = curMedInfo["numMeds"];
    slotMedInfo[0] = numMeds;

    for(jdx = 0; jdx < numMeds; jdx++) {
        //Med j Name
        var curMedName = curMedInfo["medsName"][jdx];
        var len = curMedName.length;
        var minLen = Math.min(len, maxMedNameLen);
        for(idx = 0; idx < minLen; idx++) {
            slotMedInfo[jdx*maxMedNameLen + 1 + idx] = curMedName.charCodeAt(idx);
        }
        if(minLen < maxMedNameLen) {
            for(idx = minLen; idx < maxMedNameLen; idx++) {
                slotMedInfo[jdx*maxMedNameLen + 1 + idx] = 32; //ASCII Code for ' '
            }
        }
    }

    slotMedInfo[pBSSlot1InfoCharxLen - 1] = 0xBE;//Term Byte
    return slotMedInfo;
}

var getUserInfo = function() {
    var infoObj = window.storage.get('USER_INFO');
    var userName = infoObj.user_name;
    var userNameArray = userName.split(' ');
    var processedName = generateName(userNameArray, pBSUserInfoCharxLen);
    var len = processedName.length;
    var userInfo = new Uint8Array(pBSUserInfoCharxLen);
    
    var minLen = Math.min(len, pBSUserInfoCharxLen-1);

    for(idx = 0; idx < minLen; idx++) {
        userInfo[idx] = processedName.charCodeAt(idx);
    }
    for(idx = minLen; idx < pBSUserInfoCharxLen-1; idx++) {
        userInfo[idx] = 32; //ASCII Code for ' '
    }
    userInfo[pBSUserInfoCharxLen - 1] = 0xBE;
    
    return userInfo;
}

function generateName(nameArray, maxLen) {
    var nameString;
    var len = nameArray.length;
    var tmp;
    switch(len) {
        case 1:
            nameString = nameArray[0];
            tmp = nameArray[0].length;
            if(tmp > maxLen) {
                nameString = nameArray[0].substring(0, maxLen);
            }
            break;
        case 2:
            tmp = nameArray[0].length;
            nameString = nameArray[0];
            if(tmp < maxLen) {
                tmp += nameArray[1].length;

                if(tmp <= maxLen - 1) {
                    nameString += (' ' + nameArray[1]);
                }
                else {
                    tmp = nameArray[1].length;
                    if(tmp <= maxLen - 2) {
                        nameString = (nameArray[0].substring(0, 1) + ' ' + nameArray[1]);
                    }
                    else {
                        nameString = nameArray[0].substring(0, maxLen);
                    }
                }
            }
            else {
                nameString = nameArray[0].substring(0, maxLen);
            }
            break;
        case 3:
            tmp = nameArray[0].length;
            nameString = nameArray[0];
            if(tmp < maxLen) {
                tmp += nameArray[2].length;

                if(tmp <= maxLen - 1) {
                    tmp += nameArray[1].length;

                    if(tmp <= maxLen - 2) {
                        nameString += (' ' + nameArray[1]);
                        nameString += (' ' + nameArray[2]);
                    }
                    else {
                        nameString += (' ' + nameArray[2]);
                    }
                }
                else {
                    tmp = nameArray[2].length;
                    if(tmp <= maxLen - 4) {
                        nameString = (nameArray[0].substring(0, 1) + ' ' + nameArray[1].substring(0, 1) + ' ' + nameArray[2]);
                    }
                    else if(tmp <= maxLen - 2) {
                        nameString = (nameArray[0].substring(0, 1) + ' ' + nameArray[2]);
                    }
                    else {
                        nameString = nameArray[0].substring(0, maxLen);
                    }
                }
            }
            else {
                nameString = nameArray[0].substring(0, maxLen);
            }
            break;
        default:
            nameString = nameArray[0].substring(0, maxLen);
    }
    nameString = nameString.substring(0, maxLen);
    return nameString;
}
