angular.module('medmonitor.controllers', [])

.controller('MainCtrl', function($scope, $rootScope, BLE, $cordovaPushV5, $ionicPopup) {
    gblOtpToken = storage.get('USER_OTPTOKEN');

    var defrdObject = $.Deferred();
    setMissingDaysInDosageTakenObject(defrdObject);

    // Foreground Notfn handler
    function callPopup(notification) {
        console.log("Check Fore Ground", notification);

        $scope.notification = notification;
        var myPopup = $ionicPopup.alert({
            scope: $scope,
            templateUrl:'pushnotfn.html', 
            cssClass: "alertpopup-positive",
            title:  "Push Message",
        });
    }

    // IOS Notification Received Handler
    function handleIOS(notification) {
        handler(notification);
    }

    function handler(notification) {
    }

    // Android Notification Received Handler
    function handleAndroid(notification) {
        handler(notification);
    }


    $rootScope.$on('medlistupdate', function(e, value) {
        console.log('medlistupdate' + value);
        if(BLE.connectedInfo() == true) {
            BLE.updateConfig();
        }
    });

    $rootScope.$on('bleconnected', function(e, value) {
        console.log('connect to ' + value);
        $scope.changeBLEStatusIcon('ion-bluetooth', "blue-icon");
        $scope.changeAudioIcon('ion-volume-high');
        $scope.changeLoadIcon('');
        $scope.changeBatIcon('','');
    });

    $rootScope.$on('bledisconnect', function(e, value) {
        console.log('bledisconnect from ' + value);
        $scope.changeBLEStatusIcon('', "blue-icon");
        $scope.changeAudioIcon('');
        $scope.changeLoadIcon('');
        $scope.changeBatIcon('','');
    });

    $rootScope.$on('blestatechange', function(e, value) {
        console.log('blestatechange to ' + value);
    });

    $rootScope.$on('notificationReceived', function(e, notification) {
        notification.rxtime = parseInt(new Date().getTime()/1000);

        var badge = {};
        for(i=0;i < glbBadgeList.length; i++) {
            var badgeType = glbBadgeList[i];
            if(notification.message.includes("badge") && notification.message.includes(badgeType)){
                badge.color = badgeType;
                badge.rxtime = notification.rxtime;
                break;
            }
        }

        if(badge.color !== undefined) {
            var badgeObj = window.storage.get('BADGE_INFO');
            var badgeFound = false;
            if(badgeObj !== undefined) {
                for(i = 0; i < badgeObj.length; i++) {
                    if(badgeObj[i].color == badge.color) {
                        badgeFound = true;
                        badgeObj[i].rxtime == badge.rxtime;
                        break;
                    }
                }
            }
            else {
                badgeObj = [];
            }
            if(badgeFound == false) {
                badgeObj.push(badge);
            }

            window.storage.set('BADGE_INFO', badgeObj);
            $rootScope.$broadcast("updatebadge", "HW");
        }
        else { //push notification onto message stack if not a badge
            if(notification.additionalData.foreground == true) {
                callPopup(notification); 
            }
    
            if (ionic.Platform.isIOS()) {
                handleIOS(notification);
            }
            else if (ionic.Platform.isAndroid()) {
                handleAndroid(notification);
            }
        }
    });

    $rootScope.timeDisplay = function(tm) {
        if(glb24HRClockflag == 0) {
            return meridiemFormat(tm);
        }
        else return tm;
    }

    $rootScope.legalLinks = function(linkID) {
        var link = kAppPP;
        if(linkID == 1) {
            link = kAppPP;
        }
        else {
            link = kAppTC;
        }
        window.open(link, '_system', 'location=no');
        return false;
    }
});
