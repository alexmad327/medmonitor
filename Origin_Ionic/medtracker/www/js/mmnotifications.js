function cancelAllForeGNotfn($timeout) {
    var timeoutArray = storage.get('FOREG_NOTFN');
    if(timeoutArray !== undefined) {
        for(var i=0; i < timeoutArray.length; i++) {
            var timeoutObj = timeoutArray[i];
            $timeout.cancel(timeoutObj.promise);
        }
        console.log("cancelAll", timeoutArray);
    }
    storage.set('FOREG_NOTFN', []);
}

//Cancel selectef ForeG
function cancelForeGNotifnById(cIdArray, $timeout) {
    var timeoutArray = storage.get('FOREG_NOTFN');
    console.log("cancelById start", timeoutArray, cIdArray);
    if((timeoutArray !== undefined) && (timeoutArray.length > 0)) {
        for(var j=0; j < cIdArray.length; j++) {
            var cancelId = cIdArray[j];
            for(var i=0; i < timeoutArray.length; i++) {
                var timeoutObj = timeoutArray[i];

                //found promise to cancel foreg
                if(timeoutObj.id == cancelId) {
                    $timeout.cancel(timeoutObj.promise);
                    timeoutArray.splice(i, 1);
                    console.log('Cancel ForeG', cancelId);
                    break;
                }
            }
        }
    }
    console.log("cancelById end", timeoutArray);
    //Store update timeoutArray
    storage.set('FOREG_NOTFN', timeoutArray);
}

function setupNotifications($cordovaLocalNotification, $timeout, $rootScope, $ionicPopup) {
    $cordovaLocalNotification.cancelAll();

    cancelAllForeGNotfn($timeout);

    var boxArray = window.storage.get('BOX_TIMES');

    jQuery.each(boxArray, function( box_index, box_time ){ 
        scheduleAllNotificationsForBoxTime(box_time, $cordovaLocalNotification, $timeout, $rootScope, $ionicPopup);
    });
}

function callTimeOut($timeout, newObj, delta, $rootScope, $ionicPopup) {
    var retVal = $timeout(function(){
        foreGNotfn(newObj, $timeout, $rootScope, $ionicPopup);},
        delta);

    return retVal;
}

function foreGNotfn (newObj, $timeout, $rootScope, $ionicPopup) {
    console.log("ForeG Fcn", newObj);
    var timeoutArray = storage.get('FOREG_NOTFN');
    var cancelId = newObj.id;
    for(var i=0; i < timeoutArray.length; i++) {
        var timeoutObj = timeoutArray[i];

        //found promise to cancel foreg
        if(timeoutObj.id == cancelId) {
            timeoutArray.splice(i, 1);
            storage.set('FOREG_NOTFN', timeoutArray);
            console.log('Cancel in ForeG Fcn', cancelId);
            showSnzTaken(newObj, $timeout, $rootScope, $ionicPopup);
            break;
        }
    }
    console.log("ForeG Fcn end", timeoutArray);
}

function timeOutFcn ($timeout, scheduleArray, $rootScope, $ionicPopup) {
    var timeoutArray = storage.get('FOREG_NOTFN');
    if(timeoutArray === undefined)
        timeoutArray = [];

    for(var i=0; i < scheduleArray.length; i++) {
        var newObj = scheduleArray[i];
        var schDate = newObj.at;

        //Alarm may have been schedule days before. So for foreground need to set it up for today
        var alarmTime = new Date();
        alarmTime.setHours(schDate.getHours(), schDate.getMinutes(), 0);

        var notFutureDateFlag = false;
        var delta = (schDate.getFullYear() > alarmTime.getFullYear());
        if(delta > 0) {
            notFutureDateFlag = false;
        }
        else if(delta < 0) {
            notFutureDateFlag = true;
        }
        else {
            delta = (schDate.getMonth() > alarmTime.getMonth());
            if(delta > 0) {
                notFutureDateFlag = false;
            }
            else if(delta < 0) {
                notFutureDateFlag = true;
            }
            else {
                delta = (schDate.getDate() > alarmTime.getDate());
                if(delta > 0) {
                    notFutureDateFlag = false;
                }
                else {
                    notFutureDateFlag = true;
                }
            }
        }

        if(notFutureDateFlag == true) {
            var tmpDate = new Date();
            var tmpTime1 = tmpDate.getTime();
            var tmpTime2 = alarmTime.getTime();
            delta = tmpTime2 - tmpTime1;
            // delta = alarmTime.getTime() - new Date().getTime();
        }
        else { //Future Date Hence no ForeGround Reminder
            delta = -1;
        }

        if(delta > 0) {
            //Call timeout via function to pass newObj as copy not a reference
            var timeoutPromise = callTimeOut($timeout, newObj, delta, $rootScope, $ionicPopup);
            timeoutArray.push({id:newObj.id, promise:timeoutPromise});
        }
    }
    console.log("timeout Fcn end", timeoutArray, scheduleArray);
    storage.set('FOREG_NOTFN', timeoutArray);
}

function createScheduleNotificationsArray(box_time) {
    var scheduleArray = [];

    //get medicinelist
    var medicines = getMedicinesForBox(box_time);
    var medObj = {medicines:medicines, box_time:box_time};

    var title = "MedicationAlert";
    var text = "Time to take " + medicines; 

    var paramArray = getNotificationParamsForBox(box_time);

    for(var i=0; i < paramArray.length; i++) {
        var tempObj = {};
        var pobj = paramArray[i];

        tempObj["id"] = pobj.notification_id;
        tempObj["title"] = title;
        tempObj["text"] = text;
        tempObj["at"] = pobj.schedule_time;
        tempObj["every"] = "day";
        tempObj["data"] = medObj;
        tempObj["sound"] = kNotificationSound;

        scheduleArray.push(tempObj);
    }
    return scheduleArray;
}

function scheduleAllNotificationsForBoxTime(box_time, $cordovaLocalNotification, $timeout, $rootScope, $ionicPopup) {
    var scheduleArray = createScheduleNotificationsArray(box_time);

    //foreground notifications
    timeOutFcn($timeout, scheduleArray, $rootScope, $ionicPopup);

    //background notifications
    $cordovaLocalNotification.schedule(scheduleArray);
}


function getNotificationParamsForBox(box_time) {
    var today = new Date().mmddyyyy();
    var diff;
    var stat = getTakenStatus(box_time, today);
    if(stat=='TAKEN') {// Set diff > 1 to skip
        diff = 1;
    }
    else {
        diff = getTimeDiff(box_time);
    }

    var schedule_time = null;

    var notificationParamArr = [];

    //schedule more alerts
    var snz = window.storage.get('ADJUSTED_SNOOZE_MINS'); 
    var snoozeCount = window.storage.get('USER_SNOOZE_COUNT'); 

    for(var k=0; k < snoozeCount; k++) {
        var paramObj = {}; 
        var snztm = parseInt(snz)*k;  
        var newtime = getAdjustedTime(box_time, snztm);

        var newtimeArr = newtime.split(":"); 

        var tmdate = new Date();
        tmdate.setDate(tmdate.getDate() + 1);
        var notification_id = 0;

        var serial_no = k+notification_id_inc;
        var new_bx_time = formatBoxTime(box_time);
        notification_id = "" + serial_no + new_bx_time + formatSnzTime(snztm);

        schedule_time = new Date();
        schedule_time.setHours(newtimeArr[0],newtimeArr[1],"00");

        if(diff > 0) {// tomorrow
            schedule_time.setDate(schedule_time.getDate() + 1);
        }

        paramObj["schedule_time"] = schedule_time;
        paramObj["notification_id"] = notification_id;

        notificationParamArr.push(paramObj);
    }

    return notificationParamArr;

}

function formatBoxTime(box_time) {

    var box_time_arr = box_time.split(":");
    var part_0 = (parseInt(box_time_arr[0]) < 10) ? "0"+box_time_arr[0] : box_time_arr[0];

    return "" + part_0 + box_time_arr[1];

}

function formatSnzTime(snztm) {
    var formattedStr = null;
    if(parseInt(snztm) < 10) {
        formattedStr = "00" + snztm;
    } else if(parseInt(snztm) < 100) {
        formattedStr = "0" + snztm;
    }

    return formattedStr;

}



function getNotificationParamsForBoxForNextDay(box_time) {

    var schedule_time = null;

    var notificationParamArr = [];

    //schedule more alerts
    var snz = window.storage.get('ADJUSTED_SNOOZE_MINS');
    var snoozeCount = window.storage.get('USER_SNOOZE_COUNT'); 

    for(var k=0; k < snoozeCount; k++) {

        var paramObj = {}; 
        var snztm = parseInt(snz)*k;  
        var newtime = getAdjustedTime(box_time, snztm);

        var newtimeArr = newtime.split(":"); 

        var today = new Date().yyyymmdd();
        var tmdate = new Date();
        tmdate.setDate(tmdate.getDate() + 1);
        var tomorrow = tmdate.yyyymmdd();
        var notification_id = 0;

        //tomoorow
        schedule_time = new Date(new Date(new Date().getTime() + 24 * 60 * 60 * 1000).setHours(newtimeArr[0],newtimeArr[1],"00"));

        var serial_no = k+notification_id_inc;
        var new_bx_time = formatBoxTime(box_time);
        notification_id = "" + serial_no + new_bx_time + formatSnzTime(snztm);

        paramObj["schedule_time"] = schedule_time;
        paramObj["notification_id"] = notification_id;

        notificationParamArr.push(paramObj);

    }

    return notificationParamArr;

}

function getNotfnIDsForBox(box_time) {
    var paramArray = getNotificationParamsForBox(box_time);
    var cIdArray = [];

    for(var i=0; i < paramArray.length; i++) {
        var pobj = paramArray[i];

        cIdArray.push(pobj.notification_id);
    }

    return cIdArray;
}

function cancelBoxNotificationsAfterTaken(box_time, $cordovaLocalNotification, $timeout) { 
    var cbnDfObj = $.Deferred();

    var cIdArray = getNotfnIDsForBox(box_time);
    cancelNotifications(cIdArray, cbnDfObj, $cordovaLocalNotification);
    cancelForeGNotifnById(cIdArray, $timeout);

    return cbnDfObj.promise();
}

function getsnoozeCountForBox(box_time) {
    var boxTimeArray = window.storage.get('BOX_TIMES');
    var snzMins = window.storage.get('ADJUSTED_SNOOZE_MINS');  
    var boxcount = boxTimeArray.length;
    var idx = jQuery.inArray(box_time, boxTimeArray);
    var nextIdx = 0; 
    var nextSlot; 
    var snzCount = 0;
    if(idx > -1) {
        if(idx == boxcount-1) {
            nextIdx = 0;
            nextSlot = boxTimeArray[nextIdx];
        } else {  
            nextIdx = idx + 1;
            nextSlot = boxTimeArray[nextIdx];
        }

        var diff_in_msec = new Date("2016/01/01 " + nextSlot).getTime() - new Date("2016/01/01 " + box_time).getTime();
        var diff_sec = Math.floor(diff_in_msec / 1000);
        var diff_min = Math.floor(diff_sec / 60);
        snzCount = Math.floor(diff_min / parseInt(snzMins));

    } 

    return snzCount;

}

function getSnoozeArray() {

    var snz = window.storage.get('ADJUSTED_SNOOZE_MINS'); 
    var snoozeCount = window.storage.get('USER_SNOOZE_COUNT'); 

    var snzArr = [];

    for(var k=0; k < snoozeCount; k++) {
        var inc = k*parseInt(snz);
        snzArr.push(inc);

    }

    return snzArr;
}

function validCancelIds(cancelIdArr, $cordovaLocalNotification) {
    var validFlagCancelId = []
    for(var i = 0; i < cancelIdArr.length; i++) {
        var cancelId = parseInt(cancelIdArr[i]);
        validFlagCancelId.push($cordovaLocalNotification.isPresent(cancelId));
    };

    return Promise.all(validFlagCancelId);
}

function cancelNotifications(cancelIdArr, d, $cordovaLocalNotification) {
    $cordovaLocalNotification.clearAll();
    validCancelIds(cancelIdArr, $cordovaLocalNotification).then(function(validFlagCancelId) {
        var validCancelIds = [];
        for(var i = 0; i < validFlagCancelId.length; i++) {
            if(validFlagCancelId[i] == true) {
                validCancelIds.push(cancelIdArr[i]);
            }
        }

        if(validCancelIds.length > 0) {
            $cordovaLocalNotification.cancel(validCancelIds).then(function() {
                d.resolve();
            });
        }
        else {
            d.resolve();
        }
    });
}


function checkNotificationPermissions() {

    var dfrdObject = $.Deferred();

    document.addEventListener("deviceready", function(){

        cordova.plugins.notification.local.registerPermission(function (granted) {  dfrdObject.resolve(); });

    });

    return dfrdObject.promise();
}

function showSnzTaken(notification, $cordovaLocalNotification, $rootScope, $ionicPopup) {  
    var timeoutFlag = (typeof($cordovaLocalNotification) === 'function'); //if false Notfn. Notfn is an object timeout a function
    var $timeout = null;
    if(timeoutFlag) {
        $timeout = $cordovaLocalNotification;
    }

    var tmpObj = notification.data;
    if(isJson(tmpObj)) {
        tmpObj = JSON.parse(tmpObj);
    }

    var medlist = tmpObj.medicines;
    var box_time = tmpObj.box_time;

    //check if box time is older than current time
    var box_time_arr = box_time.split(":");
    var box_stamp = parseInt(new Date().setHours(box_time_arr[0],box_time_arr[1],0,0) / 1000);
    var today_stamp = parseInt(new Date().getTime() / 1000);

    if(box_stamp > today_stamp + 5*60) {//Add 5min of Pad as android reminders could come early
        var titleText = "MedMonitor Alert";
        var localmsg = "This is prior day's notification. Please update your medication status from summary screen.";

        var scope = $rootScope.$new();
        var myPopup = $ionicPopup.alert({
            scope: scope,
            template:localmsg,
            cssClass: "alertpopup-assertive",
            title: titleText
        });

        myPopup.then(function(res) {
            $cordovaLocalNotification.clearAll();
        });
    }  else {
        var titleText = "MedMonitor Alert - " + TimeStampToDateConverter(today_stamp) + " " + meridiemFormat(box_time);
        var localmsg = "Did you take medicine: " + medlist + "?";
        var scope = $rootScope.$new();

        var myPopup = $ionicPopup.confirm({
            scope: scope,
            template:localmsg,
            cssClass: "confirmpopup-positive",
            title:  titleText,
            okText: "Taken",
            okType: "button-positive",
            cancelText: "Snooze",
            cancelType: "button-assertive",
        });

        myPopup.then(function(res) {
            if(res) {       //Taken
                if(timeoutFlag) {
                    var cIdArray = getNotfnIDsForBox(box_time);
                    cancelForeGNotifnById(cIdArray, $timeout);
                }
                else {
                    $cordovaLocalNotification.clearAll();
                }

                var today = new Date().yyyymmdd();
                var eventdata = {};
                eventdata.dt = today;
                eventdata.bxtime = box_time;
                eventdata.newtime = new Date().HHMM();
                $rootScope.$broadcast("blemedtaken", eventdata);
            }
            else {          //Snooze
                console.log("Check Snooze 1");
                if(timeoutFlag) {
                    var cIdArray = getNotfnIDsForBox(box_time);
                    cancelForeGNotifnById(cIdArray, $timeout);
                }
                else {
                    $cordovaLocalNotification.clearAll();
                }
            }
        });
    }
}


