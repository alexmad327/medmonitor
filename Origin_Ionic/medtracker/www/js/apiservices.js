angular.module('medmonitor.apiservices', ['ngResource'])

.factory('getOTP', function($resource) {
    return $resource(verifyphone_URL, {}, saveAction);
})

.factory('getSMSNumbers', function($resource) {
    return $resource(getsmsnums_API_URL);
})

.factory('getAuthorized', function($resource) {
    return $resource(auth_URL, {}, saveAction);
})

.factory('getMedsTakenHistory', function($resource) {
    return $resource(medstaken_history_URL, {}, saveAction);
})

.factory('getBioMetricHistory', function($resource) {
    return $resource(user_biometric_data, {}, saveAction);
})

.factory('getBioMetricFields', function($resource) {
    return $resource(user_biometric_fields, {}, saveActionArray);
})

.factory('getDDS', function($resource) {
    return $resource(dose_schedule_URL, {}, saveAction);
})

.factory('getGlbMedList', function($resource) {
    return $resource(medlist_URL, {}, saveAction);
})

.factory('getUserInfo', function($resource) {
    return $resource(user_info_URL, {}, saveAction);
})

.factory('getUserMed', function($resource) {
    return $resource(user_med_URL, {}, saveAction);
})

.factory('updateUserInfo', function($resource) {
    return $resource(update_userinfo_URL, {}, saveAction);
})

.factory('updateMeds', function($resource) {
    return $resource(update_med_URL, {}, saveAction);
})

.factory('updateMedsTakenStatus', function($resource) {
    return $resource(med_taken_URL, {}, saveAction);
})

.factory('updateBiometricTakenStatus', function($resource) {
    return $resource(biometrictaken_URL, {}, saveAction);
})

.factory('getSocialMediaMessages', function($resource) {
    return $resource(getsocialmediamessages_URL, {}, saveAction);
})

.factory('addSocialMediaMessages', function($resource) {
    return $resource(addsocialmediamessages_URL, {}, saveAction);
})

.factory('likeSocialMediaMessages', function($resource) {
    return $resource(likesocialmediamessages_URL, {}, saveAction);
})

.factory('flagSocialMediaMessages', function($resource) {
    return $resource(flagsocialmediamessages_URL, {}, saveAction);
})

.factory('deRegister', function($resource) {
    return $resource(deregister_URL, {}, saveAction);
});
