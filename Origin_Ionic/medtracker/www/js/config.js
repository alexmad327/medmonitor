angular.module('medmonitor.config', [])
.config(function(ChartJsProvider, $ionicConfigProvider, $stateProvider, $urlRouterProvider) {

    ChartJsProvider.setOptions({ colors : [ '#803690', '#46BFBD', '#00ADF9', '#DCDCDC', '#FDB45C', '#949FB1', '#4D5360'] });

    $ionicConfigProvider.tabs.position('bottom');

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'MainCtrl'
  })

  .state('login', {
    url: '/login',
    cache: false,
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

  // Each tab has its own nav history stack:
  .state('tab.summary', {
    url: '/summary',
    views: {
      'tab-summary': {
        templateUrl: 'templates/tab-summary.html',
        controller: 'SummaryCtrl'
      }
    }
  })

  .state('tab.scorecard', {
      url: '/scorecard',
      abstract: true,
      views: {
        'tab-scorecard': {
          templateUrl: 'templates/tab-scorecard.html',
          controller: 'ScorecardCtrl'
        }
      }
    })

  .state('tab.scorecard.biographs', {
      url: '/biographs',
      views: {
        'tab-biographs': {
          templateUrl: 'templates/tab-biographs.html',
          controller: 'BioGraphsCtrl'
        }
      }
  })

  .state('tab.scorecard.adherence', {
      url: '/adherence',
      views: {
        'tab-adherence': {
          templateUrl: 'templates/tab-adherence.html',
          controller: 'AdherenceCtrl'
        }
      }
  })

  .state('tab.socialmedia', {
    url: '/socialmedia',
    views: {
      'tab-socialmedia': {
        templateUrl: 'templates/tab-socialmedia.html',
        controller: 'SocialMediaCtrl'
      }
    }
  })

  .state('tab.chat-detail', {
      url: '/socialmedia/:chatId',
      views: {
          'tab-socialmedia': {
              templateUrl: 'templates/tab-chatdetail.html',
              controller: 'ChatDetailCtrl'
          }
      }
  })

  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })

  .state('tab.reassignmeds', {
    url: '/settings/reassignmeds',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-reassignmeds.html',
        controller: 'ReAssignMedsCtrl'
      }
    }
  })

  .state('tab.adjusttimes', {
    url: '/settings/adjusttimes',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-adjusttimes.html',
        controller: 'AdjustTimesCtrl'
      }
    }
  })

  .state('tab.holiday', {
    url: '/settings/holiday',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-holiday.html',
        controller: 'HolidayCtrl'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  // $urlRouterProvider.otherwise('/login');
  $urlRouterProvider.otherwise(function($injector, $ionicPlatform, $location, BLE) {
      var state = $injector.get('$state');
      
      if(window.storage.loggedIn()) {
          console.log("Going to Summary");
          // alert("Summary");
          var userObj = window.storage.get('USER_INFO');
          glb24HRClockflag = userObj["24HRClockflag"];
          state.go('tab.summary');
      }
      else {
          console.log("Going to Login");
          // alert("Login");
          state.go('login');
      }
      
      // return $location.path();
  });
});

