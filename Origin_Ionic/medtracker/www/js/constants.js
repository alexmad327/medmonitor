//production url
var base_URL = 'https://app.benesalustech.com/api/';

//Send user registration info to API
var reg_URL = '';

//deviceplatform variable
var devicePlatform;

//default snooze time
var defaultSnoozeTime = 15;

//Max Min snooze time
var SNOOZETIME_MAX = 15;
var SNOOZETIME_MIN = 5;

//validatePhone_URL
var verifyphone_URL = base_URL + 'genotp.php';

//get sms numbers
var getsmsnums_API_URL = base_URL + 'getsmsnums.php';

//Send otp for verification
var auth_URL = base_URL + 'verifyotp.php';

//user med list
var user_med_URL = base_URL + 'usermedlist.php';

//user med list
var user_info_URL = base_URL + 'getuserinfo.php';

//Fetch user configurations from API
var medication_data_URL = base_URL + 'medication_data.php';

//Med Taken Update to Onlines api
var med_taken_URL = base_URL + 'medtaken.php';

//Med Taken Update to Onlines api
var medlist_URL = base_URL + 'medlist.php';

//User Info Update - Snooze Time, Adjusttime 
var update_userinfo_URL = base_URL +'userconfigupdate.php';

//Med Update - No more add, edit, delte medicaiton
var update_med_URL = base_URL +'updatemedication.php';

//get default dose schedule
var dose_schedule_URL = base_URL + 'dosageschedule.php';

//get meds taken history
var medstaken_history_URL = base_URL + 'gettodaysmedication.php'; 

//biometrics taken ino
var biometrictaken_URL = base_URL + 'biometrictaken.php';

//get social media messages info
var getsocialmediamessages_URL = base_URL + 'getsocialmediamessages.php';

//get social media messages info
var addsocialmediamessages_URL = base_URL + 'addsocialmediamessages.php';

//get social media messages info
var likesocialmediamessages_URL = base_URL + 'likesocialmediamessages.php';

//get social media messages info
var flagsocialmediamessages_URL = base_URL + 'flagsocialmediamessages.php';

//biometric fields for user.
var user_biometric_fields = base_URL +  'userbiometriclist.php';

//biometric data history
var user_biometric_data = base_URL +  'gettodaysbiometrics.php'

//De-register url
var deregister_URL = base_URL + 'deregister.php'; 

var gblAuthUrlArr = [verifyphone_URL,getsmsnums_API_URL,auth_URL];

var gblOtpToken = '';
var gblSnoozeCount = 3;
var gblSnoozeMin = 5;
var notification_id_inc = 10; //used in notification id generation

var glb24HRClockflag = 0x01; //12HR AM-PM 0x00, 24HR 0x01

var btSIGDevInfoServiceCharx        = '180A';
var btSIGDevSerNum       = '2A25';
var btSIGDevFWRev        = '2A26';
var btSIGDevHWRev        = '2A27';
var btSIGDevSWRev        = '2A28';
var btSIGDevManufacturer = '2A29';
var pBSCharx             = 'DDD0';
var pBSRTCCharx          = 'DDD1';
var pBSConfigCharx       = 'DDD2';
var pBSSlotEventCharx    = 'DDD3';
var pBSSlot1InfoCharx    = 'DDD4';
var pBSSlot2InfoCharx    = 'DDD5';
var pBSSlot3InfoCharx    = 'DDD6';
var pBSSlot4InfoCharx    = 'DDD7';
var pBSModeCharx         = 'DDD8';
var pBSNotificationCharx = 'DDD9';
var pBSStatusCharx       = 'DDDA';
var pBSUserInfoCharx     = 'DDDB';

var pillBoxSlotInfoCharxLen = 32;

var pBSRTCCharxLen          = 7;
var pBSConfigCharxLen       = 12;
var pBSSlotEventCharxLen    = 8;
var pBSSlot1InfoCharxLen    = pillBoxSlotInfoCharxLen;
var pBSSlot2InfoCharxLen    = pillBoxSlotInfoCharxLen;
var pBSSlot3InfoCharxLen    = pillBoxSlotInfoCharxLen;
var pBSSlot4InfoCharxLen    = pillBoxSlotInfoCharxLen;
var pBSModeCharxLen         = 2;
var pBSNotificationCharxLen = 2;
var pBSStatusCharxLen       = 9;
var pBSUserInfoCharxLen     = 16;

var MODE_RESET  = 0xFF; //Vibration/Quiet Mode
var MODE_BUZZER = 0x00; //Vibration/Quiet Mode
var MODE_SILENT = 0x01; //Buzzer Mode
var MODE_SNOOZE = 0x02; //Snooze
var MODE_LOADSTART = 0x03; //Load Start
var MODE_LOADEND = 0x04; //Load End
var MODE_GETEVENT = 0x10; //Get Config: 1=Get Config

var EVENT_SLOT1          = 1;
var EVENT_SLOT2          = 2;
var EVENT_SLOT3          = 3;
var EVENT_SLOT4          = 4;
var EVENT_ALARM_SLOT1    = 5;
var EVENT_ALARM_SLOT2    = 6;
var EVENT_ALARM_SLOT3    = 7;
var EVENT_ALARM_SLOT4    = 8;
var EVENT_ALARM_LOWBAT   = 9;
var EVENT_ALARM_WRONGMED = 10;
var EVENT_ALARM_GOODBAT  = 11;
var STOPSCAN_TIME        = 15000;
var RESCAN_TIME          = 2000;

//Interval to check for unsent data
var CHECKUNSENTINTERVAL = (2*10*1000);

var medicationInfo = [{numMeds: 3, medsName: ['Amox', 'Bmox', 'Cmox']},
                      {numMeds: 3, medsName: ['Dmox', 'Emox', 'Fmox']},
                      {numMeds: 3, medsName: ['Gmox', 'Hmox', 'Imox']},
                      {numMeds: 3, medsName: ['Jmox', 'Kmox', 'Lmox']}];

var numSlots = 4;
var maxNumSlots = 4;
var maxMedNameLen = 10;
var lidOpenTime = 10; //Num secs for lid to kept open for a valid open event
var slot_tolerance = 5; //Num mins in multiples of 5

var statusSampleDur = 20; //num sec for pillbox status update Rate
var configSampleDur = 40; //num sec for pillbox config update Rate
var modeSampleDur = 10; //num sec for RSSI update Rate
var RSSISampleDur = 60; //num sec for RSSI update Rate
var reconnectTimeout = 5; //num sec for ReScan
var kNotificationSound = "";

//api responseCode
var responseCodeSUCCESS = 0;
var responseCodeINVALIDPHONE = 1;
var responseCodeINVALIDREG = 2;
var responseCodeINVALIDDATA = 3;
var glbBadgeLife = 3600;//secs

var glbBadgeList = ["gold","silver","bronze"];
var glbTimeout = 15000; // millisecs
var saveAction =      {save:{method:'POST', headers:{'Content-Type': 'application/x-www-form-urlencoded'}, timeout:glbTimeout}};
var saveActionArray = {save:{method:'POST', headers:{'Content-Type': 'application/x-www-form-urlencoded'}, timeout:glbTimeout, isArray:true}};


//Social Media Message types
var SMM_LIKE = 1;
var SMM_FLAG = 2;
var SMM_ADD = 3;


var kAppPP = "https://app.benesalustech.com/about/appprivacy.php";//privacy policy url
var kAppTC = "https://app.benesalustech.com/about/apptc.php";//terms and conditions url
