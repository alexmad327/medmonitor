angular.module('medmonitor.settingscontroller', [])
.controller('SettingsCtrl', function($scope, $rootScope, $state, $ionicPlatform, $ionicPopup, $ionicHistory, $httpParamSerializer, $timeout, $cordovaLocalNotification, BLE, deRegister, updateUserInfo, updateOnline) {
    $scope.meds = {};
    $scope.meds.custom = {};
    $scope.meds.custom.freq = 2;
    $scope.meds.customFlag = false;
    $scope.medli = getMedicationList();
    $scope.globalMedList = getGlobalMedList();
    
    $scope.snoozetime = {};
    $scope.snoozetime.val = parseInt(window.storage.get('ADJUSTED_SNOOZE_MINS')); 
    $scope.blestatus = BLE.connectedInfo();


    $scope.getDoseTimeStr = function (medicine) {
        var fq = medicine.freq ; 
        //new adjusted times
        var newtimes = [];
        if(medicine.assignedtimes === undefined) {
            var eachMedDefTimes = getDefaultTimes(fq+'D');
            newtimes = adjustedDosageTimes(eachMedDefTimes);
            medicine.assignedtimes = newtimes;
        }
        else {
            newtimes = medicine.assignedtimes;
        }
        var medStr = '';
        for(var i = 0; i < newtimes.length; i++) {
            medStr += newtimes[i];
            if(i == 1)
              medStr += '<br>';
            else
              medStr += ' ';
        }
        return medStr;
    }

    $scope.showCustom = function() {
        $scope.meds.custom = {};
        $scope.meds.custom.freq = "2";
        $scope.meds.customFlag = true;
    }

    $scope.delMedicine = function(idx) {
        var medliObj = {};
        
        jQuery.each($scope.medli, function( index, med ){
            delete med.assignedtimes;
        });

        $scope.medli.splice(idx, 1);

        medliObj["meds"] = $scope.medli;
        medliObj["updated_on_server"] = 0;
        window.storage.set('MEDICATION_LIST', medliObj);
        
        var setDefaultBoxTimeFlag = true;
        updateOnline.updateMedsOnLine(setDefaultBoxTimeFlag, "Del Medicine");
        //updateListOnline();
    };

    $scope.getMedList = function() {
        $scope.meds.list = {};

        $scope.globalMedList.forEach(function(glbMed) {
            $scope.medli.forEach(function(med) {
                if(glbMed.desc.toUpperCase() == med.desc.toUpperCase()) {
                    glbMed.doses.forEach(function(dose) {
                        if(dose.strength.toUpperCase() == med.strength.toUpperCase()) {
                            $scope.meds.list[med.desc]=dose.strength;
                        }
                    });
                }
            })
        });

        var myPopup = $ionicPopup.confirm({
            scope: $scope,
            templateUrl:'addmeds.html', 
            cssClass: 'confirmpopup-positive',
            title:  "Medication List",
            cssClass: 'addmed',
            okText: "Add Med",
            okType: "button-positive",
            cancelType: "button-assertive",
        });

        myPopup.then(function(res) {
            console.log("Glb Meds", res, $scope.meds.customFlag, $scope.globalMedList, $scope.globalMedList[0].doses, $scope.meds.list);
            if(res) {       //OK
                console.log("Custom", $scope.meds.custom);

                var retVal = false;
                if($scope.meds.customFlag == true) {
                    retVal = addCustomMedicine($scope.meds.custom); 
                }
                else {
                    retVal = addNewMedicine($scope.meds.list); 
                }

                if(retVal == true) {
                    $scope.medli = getMedicationList();
                    
                    var setDefaultBoxTimeFlag = true;
                    updateOnline.updateMedsOnLine(setDefaultBoxTimeFlag, "Add Generic");
                    //updateListOnline();
                }
                $scope.meds.customFlag = false;
            }
            else {          //Cancel
                $scope.meds.customFlag = false;
            }
        });
    }

    $scope.deActivate = function() {
        var goToLogin = function() {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
            $state.go('login',{});
        };

        var Success = function(responseData) {
            var responseCode = responseData.ERROR;
            var localmsg = '';
            if(responseCode === undefined)
                responseCode = 0;

            switch(responseCode) {
                case responseCodeSUCCESS:
                    localmsg = "DeActivated";
                    console.log(localmsg);
                    break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    localMsg = "exiting from Success deActivate: " + responseCode;
                    console.log(localmsg);
                    break;
            }

            BLE.disconnect();

            $cordovaLocalNotification.cancelAll();
            cancelAllForeGNotfn($timeout);

            window.storage.removeAll();
            gblOtpToken = '';
            $timeout(goToLogin, 1000);
        };

        var Fail = function (httpResponse) {
            var localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
            var titleText = "DeActivate";
            genericPopupAlert($scope, $ionicPopup, localmsg, titleText);

            BLE.disconnect();
            $timeout(goToLogin, 1000);
        };

        var myPopup = $ionicPopup.confirm({
            scope: $scope,
            template:"<p>Are you sure?<br>All data will be erased from the app.<br>Info saved on the cloud is secure<br>Need to re-register to get started</p>",
            cssClass: "confirmpopup",
            title:  "MedMonitor Deactivate",
            okText: "DeActivate",
            okType: "button-assertive",
            cancelType: "button-positive",
        });

        myPopup.then(function(res) {
            if(res) {       //OK
                var phoneval = storage.get('USER_PHONE');
                var inputData = $httpParamSerializer({data:{phone:phoneval,
                                                            otptoken:gblOtpToken},
                                                      version:$scope.version,
                                                      platform:$scope.platform,
                                                      model:$scope.model,
                                                      regId:$rootScope.regId});
        
                deRegister.save([], inputData, Success, Fail);
                console.log("DeActive Confirmed");
            }
            else {
                console.log("DeActive Cancelled");
            }
        });
    }

    $scope.onSnoozeChange= function() {
        console.log(parseInt($scope.snoozetime.val));
        updateSnoozeTime();
    }

    $scope.changeMin= function(deltaMin) {
        var snoozetime = parseInt($scope.snoozetime.val) + deltaMin;

        if(snoozetime > SNOOZETIME_MAX) {
            snoozetime = SNOOZETIME_MAX;
        }
        else if(snoozetime < SNOOZETIME_MIN) {
            snoozetime = SNOOZETIME_MIN;
        }

        $scope.snoozetime.val = snoozetime;
        updateSnoozeTime();
    }

    var updateSnoozeTime = function() {
        window.storage.set('ADJUSTED_SNOOZE_MINS', $scope.snoozetime.val);
        setupNotifications($cordovaLocalNotification, $timeout, $rootScope, $ionicPopup);

        var phoneval = storage.get('USER_PHONE');
        var titleText = "MedMonitor Update Snooze Time";
        var Success = function(responseData) {
            var responseCode = responseData.ERROR;
            var localmsg = '';
            
            if(responseCode === undefined) {
                responseCode = 0;
            }
            
            switch(responseCode) {
                case responseCodeSUCCESS:
                    localmsg = "Update Snooze Time on Server";
                    console.log(localmsg);
                    break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    localmsg = " Error Uploadng New Snooze Time to Cloud. " + responseData.MESSAGE +  ". Please DeActivate and Reactivate App or Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                    var scope = $rootScope.$new();
                    genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
                    break;
            }
        };
   
        var Fail = function (httpResponse) {
            var localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
            var scope = $rootScope.$new();
            var myPopup = $ionicPopup.alert({
                scope: scope,
                template:localmsg,
                title:  titleText,
                okType: "button-assertive",
            });
            
            genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
        };
    
        var inputData = $httpParamSerializer({data:{phone:phoneval,
                                                    otptoken:gblOtpToken,
                                                    snoozeMin:$scope.snoozetime.val},
                                              version:$rootScope.version,
                                              platform:$rootScope.platform,
                                              model:$rootScope.model,
                                              regId:$rootScope.regId});
        updateUserInfo.save([], inputData, Success, Fail);
        $rootScope.$broadcast("medlistupdate","Update Snooze time");
    }

    $rootScope.$on('bleconnected', function(e, value) {
        $scope.blestatus = BLE.connectedInfo();
    });

    $rootScope.$on('bledisconnect', function(e, value) {
        $scope.blestatus = BLE.connectedInfo();
    });

    $rootScope.$on('medlistupdate', function(e, value) {
        $scope.medli = getMedicationList();
        console.log('Root medlistupdate' + value, $scope.medli);
    });
});
