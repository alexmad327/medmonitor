angular.module('medmonitor.scorecardcontroller', ['chart.js'])

.controller('ScorecardCtrl', function($scope) {
})

.controller('BioGraphsCtrl', function($scope, $rootScope) {
    $rootScope.$on('biometricupdate', function(e, value) {
        console.log('biometricupdate' + value);
        updateScope();
    });

    var updateScope = function () {
        $scope.bmtypes = window.storage.get('BIOMETRIC_FIELD_INFO');
        $scope.seriesIdx = $scope.bmtypes[0].biometric_type;;

        $scope.options = {
            title: {
                display: true,
                text: $scope.seriesIdx,
                fontStyle: 'bold',
                fontSize: 14,
            },
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left',
                    }
                 ],
                xAxes: [
                    {
                        type: 'time',
                        time: {
                            displayFormats: {
                                week: 'MM YY',
                                day: 'MM/DD'
                            },
                            unit: 'day'
                        }
                    }
                ]
            },
            legend: {
                display: true
            }
        };
    
        var bmtObj = window.storage.get('BIOMETRIC_TAKEN_INFO');
        var bmTakenObj = getValueForKey(bmtObj, "biometrics");
        var dateArr = Object.keys(bmTakenObj);
        var bmArray = {};
    
        $scope.bmtypes.forEach(function(bmtype) {
            var labels = [];
            var bmvalues = bmtype.biometric_values.values;

            bmvalues.forEach(function (val) {
                labels.push(Object.keys(val)[0]);
            });

            bmArray[bmtype.biometric_type] = {};
            bmArray[bmtype.biometric_type].labels = labels;
            bmArray[bmtype.biometric_type].data = [labels.length];
            for(var i = 0; i < labels.length; i++) {
                bmArray[bmtype.biometric_type].data[i] = [];
            }
        });
        
        for (var dt in bmTakenObj) {
            if (bmTakenObj.hasOwnProperty(dt)) {
                val = bmTakenObj[dt];
                
                val.forEach(function(bm, idx) {
                    var bmvalues = bm.biometric_values;
                    for (var label in bmvalues) {
                        if (bmvalues.hasOwnProperty(label)) {
                            var idx  = bmArray[bm.biometric_type].labels.indexOf(label);
                            if(idx > -1) {
                                var taken_time = DateTimeToTimeStampConverter(dt, bm.taken_time);
                                var elem = {x:taken_time, y:bmvalues[label]};
                                bmArray[bm.biometric_type].data[idx].push(elem);
                            }
                        }
                    }
                });
            }
        }
    
        $scope.bmArray = bmArray;
        $scope.series = $scope.bmArray[$scope.seriesIdx].labels;
    }

    $scope.onClick = function (points, evt) {
        console.log(points, evt);
    };

    $scope.getButtonColor = function (bmtype) {
        if(bmtype == $scope.seriesIdx) {
            return 'button-balanced';
        }
        else {
            return 'button-positive';
        }
    }

    $scope.selectSeries = function (bmtype) {
        var seriesLabel = ['Series A','Series B'];

        console.log(bmtype);
        $scope.seriesIdx = bmtype;
        $scope.series = $scope.bmArray[bmtype].labels;
        $scope.options.title.text = bmtype;
    }
    
    $scope.labels = [];

    updateScope();
});
