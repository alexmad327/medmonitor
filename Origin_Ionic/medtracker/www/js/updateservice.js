angular.module('medmonitor.updateservice', [])

    .factory('updateOnline', function($rootScope, $cordovaLocalNotification, $ionicPopup, $timeout, $httpParamSerializer, updateMeds, getSocialMediaMessages, addSocialMediaMessages, likeSocialMediaMessages, flagSocialMediaMessages) {
        var titleText = "MedMonitor Update Med List";
        var Success = function(responseData) {
            var responseCode = responseData.ERROR;
            var localmsg = '';
            var medliObj = {};


            if(responseCode === undefined) {
                responseCode = 0;
            }

            switch(responseCode) {
                case responseCodeSUCCESS:
                    localmsg = "Update on Server";
                    medliObj = window.storage.get('MEDICATION_LIST');
                    medliObj.updated_on_server = 1;
                    window.storage.set('MEDICATION_LIST', medliObj);
                    console.log(localmsg);
                    break;
                case responseCodeINVALIDPHONE:
                case responseCodeINVALIDREG:
                case responseCodeINVALIDDATA:
                default:
                    localmsg = " Error Uploadng New Meds List to Cloud. " + responseData.MESSAGE +  ". Please DeActivate and Reactivate App or Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                    var scope = $rootScope.$new();
                    genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
                    break;
            }
        };

        var Fail = function (httpResponse) {
            var localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
            var scope = $rootScope.$new();

            genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
        };

        var self = this;
        self.updateMedsOnLine = function (setDefaultBoxTimeFlag, msg) {
            //Update default dose times
            if(setDefaultBoxTimeFlag) {
                setDefaultBoxTimes();
            }

            resetBoxSlots();

            var today = new Date().yyyymmdd();
            addDayInDosageTakenObject(today);

            self.checkAndUpdateMedsOnLine();

            setupNotifications($cordovaLocalNotification, $timeout, $rootScope, $ionicPopup);
            $rootScope.$broadcast("medlistupdate", msg);
        }

        self.checkAndUpdateMedsOnLine = function () {
            var updated_on_server = 0;
            var medli = getMedicationList(updated_on_server);
            if(medli !== null) {
                var phoneval = storage.get('USER_PHONE');
                var inputData = $httpParamSerializer({data:{phone:phoneval,
                    otptoken:gblOtpToken,
                    meds:medli},
                    version:$rootScope.version,
                    platform:$rootScope.platform,
                    model:$rootScope.model,
                    regId:$rootScope.regId});
                updateMeds.save([], inputData, Success, Fail);
            }
            else {
                console.log("No Med List to be updated");
            }

        }

        self.getSocialMessages = function () {
            var Success = function(responseData) {
                var responseCode = responseData.ERROR;

                if(responseCode === undefined) {
                    responseCode = 0;
                }

                switch(responseCode) {
                    case responseCodeSUCCESS:
                        responseData = JSON.parse(angular.toJson(responseData));
                        saveSocialMediaMessages(responseData.messages);
                        $rootScope.$broadcast("updatechats", "checknewmsgs");
                        break;
                    case responseCodeINVALIDPHONE:
                    case responseCodeINVALIDREG:
                    case responseCodeINVALIDDATA:
                    default:
                        break;
                }
            };

            var Fail = function (httpResponse) {
            };

            var phoneval = storage.get('USER_PHONE');
            var inputData = $httpParamSerializer({data:{phone:phoneval,
                otptoken:gblOtpToken},
                version:$rootScope.version,
                platform:$rootScope.platform,
                model:$rootScope.model,
                regId:$rootScope.regId});
            getSocialMediaMessages.save([], inputData, Success, Fail);
        }

        self.flagMessageOnline = function(parentIDX, timestamp) {
            var titleText = "MedMonitor Flag Message";
            var Success = function(responseData) {
                var responseCode = responseData.ERROR;

                if(responseCode === undefined) {
                    responseCode = 0;
                }

                switch(responseCode) {
                    case responseCodeSUCCESS:
                        break;
                    case responseCodeINVALIDPHONE:
                    case responseCodeINVALIDREG:
                    case responseCodeINVALIDDATA:
                    default:
                        localmsg = " Error flagging message on cloud. " + responseData.MESSAGE +  ". Please DeActivate and Reactivate App or Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                        var scope = $rootScope.$new();
                        genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
                        break;
                }
            };

            var Fail = function (httpResponse) {
                var localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
                var scope = $rootScope.$new();
                createMsg(SMM_FLAG, parentIDX, timestamp);
                genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
            };

            var phoneval = storage.get('USER_PHONE');
            var inputData = $httpParamSerializer({  data:{  phone:phoneval,
                otptoken:gblOtpToken,
                userType:'Patient',
                parentIDX:parentIDX,
                timestamp:timestamp},
                version:$rootScope.version,
                platform:$rootScope.platform,
                model:$rootScope.model,
                regId:$rootScope.regId});
            flagSocialMediaMessages.save([], inputData, Success, Fail);
        }

        self.likeMessageOnline = function(parentIDX, timestamp) {
            var titleText = "MedMonitor Like Message";
            var Success = function(responseData) {
                var responseCode = responseData.ERROR;

                if(responseCode === undefined) {
                    responseCode = 0;
                }

                switch(responseCode) {
                    case responseCodeSUCCESS:
                        break;
                    case responseCodeINVALIDPHONE:
                    case responseCodeINVALIDREG:
                    case responseCodeINVALIDDATA:
                    default:
                        localmsg = " Error flagging message on cloud. " + responseData.MESSAGE +  ". Please DeActivate and Reactivate App or Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                        var scope = $rootScope.$new();
                        genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
                        break;
                }
            };

            var Fail = function (httpResponse) {
                var localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
                var scope = $rootScope.$new();
                createMsg(SMM_LIKE, parentIDX, timestamp);
                genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
            };

            var phoneval = storage.get('USER_PHONE');
            var inputData = $httpParamSerializer({  data:{  phone:phoneval,
                otptoken:gblOtpToken,
                userType:'Patient',
                parentIDX:parentIDX,
                timestamp:timestamp},
                version:$rootScope.version,
                platform:$rootScope.platform,
                model:$rootScope.model,
                regId:$rootScope.regId});
            likeSocialMediaMessages.save([], inputData, Success, Fail);
        }

        self.addMessageOnline = function(parentIDX, message, timestamp) {
            var titleText = "MedMonitor Like Message";
            var Success = function(responseData) {
                var responseCode = responseData.ERROR;

                if(responseCode === undefined) {
                    responseCode = 0;
                }

                switch(responseCode) {
                    case responseCodeSUCCESS:
                        break;
                    case responseCodeINVALIDPHONE:
                    case responseCodeINVALIDREG:
                    case responseCodeINVALIDDATA:
                    default:
                        localmsg = " Error flagging message on cloud. " + responseData.MESSAGE +  ". Please DeActivate and Reactivate App or Contact Benesalus Support. Support@benesalustech.com. Error Code " + responseCode;
                        var scope = $rootScope.$new();
                        genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
                        break;
                }
            };

            var Fail = function (httpResponse) {
                var localmsg = "Network Not Available. Check Cellular and Wifi connection. Try again when network connectivity is good";
                var scope = $rootScope.$new();
                createMsg(SMM_ADD, parentIDX, timestamp, message);
                genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
            };

            var phoneval = storage.get('USER_PHONE');
            var inputData = $httpParamSerializer({  data:{  phone:phoneval,
                otptoken:gblOtpToken,
                userType:'Patient',
                parentIDX:parentIDX,
                message:message,
                timestamp:timestamp},
                version:$rootScope.version,
                platform:$rootScope.platform,
                model:$rootScope.model,
                regId:$rootScope.regId});
            addSocialMediaMessages.save([], inputData, Success, Fail);
        }

        self.getMedAndBioMetricLists = function () {
            var fsm = StateMachine.create({
                initial: 'wait',
                events: [
                    { name: 'getBioMetricFields',       from: 'wait',                                                                               to: 'gotBioMetricFields' },
                    { name: 'getGlobalMedList',         from: 'gotBioMetricFields',                                                                 to: 'gotGlobalMedList' },
                    { name: 'getUserData',              from: 'gotGlobalMedList',                                                                   to: 'gotUserData' },
                    { name: 'getUserMedData',           from: 'gotUserData',                                                                        to: 'gotUserMedData' },
                    { name: 'deActivate',               from: ['gotBioMetricFields','gotGlobalMedList','gotUserData','gotUserMedData'],             to: 'wait' }
                ],
                callbacks : {
                    ongetBioMetricFields:       ongetBioMetricFieldsFcn,
                    ongetGlobalMedList:         ongetGlobalMedListFcn,
                    ongetUserData:              ongetUserDataFcn,
                    ongetUserMedData:           ongetUserMedDataFcn,
                    ondeActivate:               ondeActivateFcn
                }
            });

            var ongetBioMetricFieldsFcn = function (event, from, to, msg) {
                console.log("getBioMetricFields " + from + " " + to + " " + msg);

                var titleText = "MedMonitor BioMetric Fields";

                var Success = function(responseData) {
                    var localmsg = "";

                    var responseCode = responseData.ERROR;
                    if(responseCode === undefined)
                        responseCode = 0;

                    var responseMessage = responseData.MESSAGE;
                    if (responseMessage === undefined)
                        responseData.MESSAGE = "Unknown";

                    switch(responseCode) {
                        case responseCodeSUCCESS:
                            responseData = JSON.parse(angular.toJson(responseData));
                            saveBioMetricsList(responseData);
                            localmsg = "Get Global MedList from Success";
                            fsm.getGlobalMedList(localmsg);
                            break;
                        case responseCodeINVALIDPHONE:
                        case responseCodeINVALIDREG:
                        case responseCodeINVALIDDATA:
                        default:
                            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                            break;
                    }
                }

                var Fail = function (httpResponse) {
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
                }

                var phoneval = storage.get('USER_PHONE');
                var curDay = new Date().yyyymmdd();
                var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken, day:curDay, days:30},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
                getBioMetricFields.save([], inputData, Success, Fail);
            }

            var ongetGlobalMedListFcn = function (event, from, to, msg) {
                console.log("get global med list " + from + " " + to + " " + msg);

                var titleText = "MedMonitor Global Medication List";

                var Success = function(responseData) {
                    var localmsg = "";

                    var responseCode = responseData.ERROR;
                    if(responseCode === undefined)
                        responseCode = 0;

                    var responseMessage = responseData.MESSAGE;
                    if (responseMessage === undefined)
                        responseData.MESSAGE = "Unknown";

                    switch(responseCode) {
                        case responseCodeSUCCESS:
                            responseData = JSON.parse(angular.toJson(responseData));
                            if(responseData.meds.length > 0){
                                //save globaal medication data recieved from ajax
                                saveGlobalMedList(responseData);
                                localmsg = "Get User Data";
                                fsm.getUserData(localmsg);
                            }
                            else{
                                localmsg = "No Global MedList for this User. Contact Benesalus Support. Support@benesalustech.com.";
                                var myPopup = $ionicPopup.alert({
                                    scope: $scope,
                                    template:localmsg,
                                    cssClass: "alertpopup-positive",
                                    title:  titleText,
                                    okType: "button-assertive",
                                });

                                myPopup.then(function(res) {
                                    fsm.deActivate(localmsg);
                                });
                            }
                            break;
                        case responseCodeINVALIDPHONE:
                        case responseCodeINVALIDREG:
                        case responseCodeINVALIDDATA:
                        default:
                            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                            break;
                    }
                }

                var Fail = function (httpResponse) {
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
                }

                var phoneval = storage.get('USER_PHONE');
                var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
                getGlbMedList.save([], inputData, Success, Fail);
            }

            var ondeActivateFcn = function(event, from, to, msg) {
                console.log("goToWait " + from + " " + to + " " + msg);
            }

            var ongetUserDataFcn = function (event, from, to, msg) {
                console.log("get Global Med List " + from + " " + to + " " + msg);

                var titleText = "MedMonitor User Data List";

                var firstSaveUserData = function(data) {
                    var defObj = $.Deferred();
                    window.USERCONFIG.saveInfo(data, defObj);
                    //return the promise
                    return defObj.promise();
                }

                var initSaveUserData = function(data) {
                    firstSaveUserData(data).then(function () {
                        var localmsg = "Get User Medication Data";
                        //fetch user medication data
                        fsm.getUserMedData(localmsg);
                    });
                }

                var Success = function(responseData) {
                    var responseCode = responseData.ERROR;
                    if(responseCode === undefined)
                        responseCode = 0;

                    var responseMessage = responseData.MESSAGE;
                    if (responseMessage === undefined)
                        responseData.MESSAGE = "Unknown";

                    switch(responseCode) {
                        case responseCodeSUCCESS:
                            responseData = JSON.parse(angular.toJson(responseData));
                            initSaveUserData(responseData);
                            break;
                        case responseCodeINVALIDPHONE:
                        case responseCodeINVALIDREG:
                        case responseCodeINVALIDDATA:
                        default:
                            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                            break;
                    }
                }

                var Fail = function (httpResponse) {
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
                }

                var phoneval = storage.get('USER_PHONE');
                var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
                getUserInfo.save([], inputData, Success, Fail);
            }

            var ongetUserMedDataFcn = function (event, from, to, msg) {
                console.log("get User Med Data " + from + " " + to + " " + msg);

                var titleText = "User Medication List";

                var salvoMedData = function(data) {
                    loadingSpinner.show();
                    var defrdObject = $.Deferred();

                    saveList(data, defrdObject);
                    //return the promise
                    return defrdObject.promise();
                }


                var initSaveMedData = function(data) {
                    salvoMedData(data).then(checkNotificationPermissions).then(function(){
                        loadingSpinner.hide();
                        setupNotifications($cordovaLocalNotification, $timeout, $rootScope, $ionicPopup);

                        $state.go('tab.summary',{});
                        //PopUp With asking to check Med Assign
                    });
                }

                var Success = function(responseData) {
                    var localmsg = "";

                    var responseCode = responseData.ERROR;
                    if(responseCode === undefined)
                        responseCode = 0;

                    var responseMessage = responseData.MESSAGE;
                    if (responseMessage === undefined)
                        responseData.MESSAGE = "Unknown";

                    switch(responseCode) {
                        case responseCodeSUCCESS:
                            responseData = JSON.parse(angular.toJson(responseData));
                            if(responseData.meds.length > 0){
                                //save medication data recieved from ajax
                                initSaveMedData(responseData);
                            }
                            else{
                                localmsg = "No Data found for this User";
                                var myPopup = $ionicPopup.alert({
                                    scope: $scope,
                                    template:localmsg,
                                    cssClass: "alertpopup-positive",
                                    title:  titleText,
                                    okType: "button-assertive",
                                });

                                myPopup.then(function(res) {
                                    fsm.deActivate(localmsg);
                                });
                            }
                            break;
                        case responseCodeINVALIDPHONE:
                        case responseCodeINVALIDREG:
                        case responseCodeINVALIDDATA:
                        default:
                            apiLoginFailHandler($scope, $ionicPopup, fsm, titleText, responseCode, responseMessage);
                            break;
                    }
                }

                var Fail = function (httpResponse) {
                    apiLoginFailHandler($scope, $ionicPopup, fsm, titleText);
                }

                var phoneval = storage.get('USER_PHONE');
                var inputData = $httpParamSerializer({data:{phone:phoneval, otptoken:gblOtpToken},version:$scope.version,platform:$scope.platform,model:$scope.model, regId:$rootScope.regId});
                getUserMed.save([], inputData, Success, Fail);
            }
        }

        return self;
    });
