angular.module('medmonitor.bleservices', [])

.factory('BLE', function($rootScope, $ionicPopup) {
    var eventSample = [];

    var serviceDetails = {
        '180A':{name: 'dev info',        type: 'UInt8', len: 0,                       parseFunc: null},
        '2A23':{name: 'System ID',       type: 'UInt8', len: 8,                       parseFunc: readSystemID},
        '2A24':{name: 'Model Num',       type: 'UTF8s', len: 0,                       parseFunc: processString},
        '2A25':{name: 'Serial Num',      type: 'UTF8s', len: 0,                       parseFunc: processMacID},
        '2A26':{name: 'FW Rev Num',      type: 'UTF8s', len: 0,                       parseFunc: processString},
        '2A27':{name: 'HW Rev Num',      type: 'UTF8s', len: 0,                       parseFunc: processString},
        '2A28':{name: 'SW Rev Num',      type: 'UTF8s', len: 0,                       parseFunc: processString},
        '2A29':{name: 'Manu Name',       type: 'UTF8s', len: 0,                       parseFunc: processString},
        'DDD0':{name: 'Pillbox service', type: 'UInt8', len: 0,                       parseFunc: null},
        'DDD1':{name: 'RTC Config',      type: 'UInt8', len: pBSRTCCharxLen,          parseFunc: readTimeArray},
        'DDD2':{name: 'Pillbox Config',  type: 'UInt8', len: pBSConfigCharxLen,       parseFunc: readConfigArray},
        'DDD3':{name: 'Event Info',      type: 'UInt8', len: pBSSlotEventCharxLen,    parseFunc: readEventArray},
        'DDD4':{name: 'Slot1 Info',      type: 'UInt8', len: pBSSlot1InfoCharxLen,    parseFunc: processString},
        'DDD5':{name: 'Slot2 Info',      type: 'UInt8', len: pBSSlot2InfoCharxLen,    parseFunc: processString},
        'DDD6':{name: 'Slot3 Info',      type: 'UInt8', len: pBSSlot3InfoCharxLen,    parseFunc: processString},
        'DDD7':{name: 'Slot4 Info',      type: 'UInt8', len: pBSSlot3InfoCharxLen,    parseFunc: processString},
        'DDD8':{name: 'Buzzer Mode',     type: 'UInt8', len: pBSModeCharxLen,         parseFunc: readModeArray},
        'DDD9':{name: 'Notify Info',     type: 'UInt8', len: pBSNotificationCharxLen, parseFunc: readNotificationArray},
        'DDDA':{name: 'Status Info',     type: 'UInt8', len: pBSStatusCharxLen,       parseFunc: readStatusArray},
        'DDDB':{name: 'User Info',       type: 'UInt8', len: pBSUserInfoCharxLen,     parseFunc: processString},
    };
    
    var fsm = StateMachine.create({
        initial: 'notconnected',
        events: [
            { name: 'stopscan',    from: ['notconnected','scanning', 'periodicFunc'],  to: 'notconnected' },
            { name: 'scan',        from: ['notconnected', 'devicesfound'],  to: 'scanning' },
            { name: 'found',       from: ['scanning', 'devicesfound'],      to: 'devicesfound' },
            { name: 'connect',     from: 'devicesfound',                    to: 'connected' },
            { name: 'readMac',     from: 'connected',                       to: 'foundMac' },
            { name: 'readHWRev',   from: 'foundMac',                        to: 'foundHWRev' },
            { name: 'readFWRev',   from: 'foundHWRev',                      to: 'foundFWRev' },
            { name: 'configRTC',   from: 'foundFWRev',                      to: 'writeRTC' },
            { name: 'configBOX',   from: ['writeRTC', 'periodicFunc'],      to: 'writeConfig' },
            { name: 'configSlot1', from: 'writeConfig',                     to: 'writeCfgSlot1' },
            { name: 'configSlot2', from: 'writeCfgSlot1',                   to: 'writeCfgSlot2' },
            { name: 'configSlot3', from: 'writeCfgSlot2',                   to: 'writeCfgSlot3' },
            { name: 'configSlot4', from: 'writeCfgSlot3',                   to: 'writeCfgSlot4' },
            { name: 'configNotfn', from: 'writeCfgSlot4',                   to: 'writeCfgNotfn' },
            { name: 'cfgUserInfo', from: 'writeCfgNotfn',                   to: 'writeUserInfo' },
            { name: 'cfgPeriodFn', from: 'writeUserInfo',                   to: 'periodicFunc' },
            { name: 'disconnect',  from: ['notconnected', 'scanning', 'devicesfound', 'connected', 'foundMac', 'foundHWRev', 'foundFWRev', 'writeConfig', 'writeRTC', 'writeCfgNotfn', 'writeCfgSlot1', 'writeCfgSlot2', 'writeCfgSlot3', 'writeCfgSlot4', 'writCfnNotfn','writeUserInfo', 'periodicFunc'], to: 'notconnected' }
         ],
        callbacks : {
            onstopscan:    onstopscanFcn,
            ondisconnect:  ondisconnectFcn,
            onscan:        onscanFcn,
            onfound:       onfoundFcn,
            onconnect:     onconnectFcn,
            onreadMac:     onreadMacFcn,
            onreadHWRev:   onreadHWRevFcn,
            onreadFWRev:   onreadFWRevFcn,
            onconfigRTC:   onconfigRTCFcn,
            onconfigBOX:   onconfigBOXFcn,
            onconfigSlot1: onconfigSlot1Fcn,
            onconfigSlot2: onconfigSlot2Fcn,
            onconfigSlot3: onconfigSlot3Fcn,
            onconfigSlot4: onconfigSlot4Fcn,
            onconfigNotfn: onconfigNotfnFcn,
            oncfgUserInfo: oncfgUserInfoFcn,
            oncfgPeriodFn: oncfgPeriodFnFcn,
        }
    });

    // State Machine Functions
    function onstopscanFcn(event, from, to, msg) {
         console.log("Disconnect " + from + " " + to + " " + msg);
         disconnectCleanup();
     }
    
    function ondisconnectFcn(event, from, to, msg) {
         console.log("Disconnect " + from + " " + to + " " + msg);
         disconnectCleanup();
         if(from != 'notconnected') {
             setTimeout(self.scan(), reconnectTimeout*1000);
         }
     }
    
    function onscanFcn(event, from, to, msg) {
        console.log("Starting scan " + from + " " + to + " " + msg);
        smScan();
    }
    
    function onfoundFcn(event, from, to, msg){
        console.log("Found " + from + " " + to + " " + msg);
    }
    
    function onconnectFcn(event, from, to, msg) {
        console.log("Connected " + from + " " + to + " " + msg);
        connectPillBox();
    }
    
    function onreadMacFcn(event, from, to, msg) {
        console.log("Read Mac " + from + " " + to + " " + msg);
        readMac();
    }
    
    function onreadHWRevFcn(event, from, to, msg) {
        console.log("Read HW Rev " + from + " " + to + " " + msg);
        readHWRev();
    }
    
    function onreadFWRevFcn(event, from, to, msg) {
        console.log("Read FW Rev " + from + " " + to + " " + msg);
        readFWRev();
    }
    
    function onconfigRTCFcn(event, from, to, msg) {
        console.log("Config RTC " + from + " " + to + " " + msg);
        writeRTC();
    }
    
    function onconfigBOXFcn(event, from, to, msg) {
        console.log("Config BOX " + from + " " + to + " " + msg);
        updateMedInfo();
        writeConfigBOX();
    }
    
    function onconfigSlot1Fcn(event, from, to, msg) {
        console.log("Config Slot1 " + from + " " + to + " " + msg);
        writeSlot(1);
    }
    
    function onconfigSlot2Fcn(event, from, to, msg) {
        console.log("Config Slot2 " + from + " " + to + " " + msg);
        writeSlot(2);
    }
    
    function onconfigSlot3Fcn(event, from, to, msg) {
        console.log("Config Slot3 " + from + " " + to + " " + msg);
        writeSlot(3);
    }
    
    function onconfigSlot4Fcn(event, from, to, msg) {
        console.log("Config Slot4 " + from + " " + to + " " + msg);
        writeSlot(4);
    }
    
    function onconfigNotfnFcn(event, from, to, msg) {
        console.log("Config Notfn " + from + " " + to + " " + msg);
        writeNotfn();
    }
    
    function oncfgUserInfoFcn(event, from, to, msg) {
        console.log("Config Notfn " + from + " " + to + " " + msg);
        writeUserInfo();
    }

    function oncfgPeriodFnFcn(event, from, to, msg) {
        console.log("Config Notfn " + from + " " + to + " " + msg);
        enablePeriodicFcn();
    }

    var readEventsFunc = function(buffer) {
        var EventsData = readData(buffer, pBSSlotEventCharx);
        console.log("Read Events " + EventsData.Str);
        if(EventsData.timeStamp !== undefined) {
            $rootScope.$broadcast("blemedtaken", EventsData.timeStamp);
        }
    }

    var readMacIDFunc = function(buffer) {
        var defObj = $.Deferred();
        var macID = readData(buffer, btSIGDevSerNum);
        checkMAC(macID, defObj);
        return defObj.promise();
    }

    var checkMAC = function(macID, defObj) {
        //if(macID ==='0f:05:23:f8:e6:a0') {
        var userObj = window.storage.get('USER_INFO');
        var listMAC = userObj["approvedMAC"];

        if(listMAC !== null) {
        
        for(idx = 0; idx < listMAC.length; idx++) {
            var approvedMAC = listMAC[idx];
            if(macID === approvedMAC) {
                defObj.resolve(macID);
                return;
            }
        }
        }
        
        //No approved MAC Found
        var msg = 'Pillbox not authorized. Contact MedMonitor Support';
        defObj.reject(msg);
    }

    var readHWRevFunc = function(buffer) {
        $rootScope.pillbox.HWRev = readData(buffer, btSIGDevHWRev);
    }

    var readFWRevFunc = function(buffer) {
        $rootScope.pillbox.FWRev = readData(buffer, btSIGDevFWRev);
    }

    var readStatusFunc = function(buffer) {
        var StatusData = readData(buffer, pBSStatusCharx);
        $rootScope.batteryUpdate(StatusData.bat, StatusData.charge);
        console.log("Read Status " + StatusData.Str);
        if(StatusData.eventCnt > 0) {
            ble.read(self.pillBox.id, pBSCharx, pBSSlotEventCharx,
                    function(buffer) {
                        readEventsFunc(buffer, pBSSlotEventCharx);
                    },
                    function(error) {
                        var msg = "Read Status failed. Error " + error + ". Disconnected " + self.pillBox.id;
                        fsm.disconnect(msg);
                    });
        }
        self.pillBox.bat = StatusData.bat;
    };
    
    var readStatus= function() {
          ble.read(self.pillBox.id, pBSCharx, pBSStatusCharx,
                  function(buffer) {
                      readStatusFunc(buffer);
                  },
                  function(error) {
                      var msg = "Read Status failed. Error " + error + ". Disconnected " + self.pillBox.id;
                      fsm.disconnect(msg);
                  });
    };

    var readConfig = function() {
          ble.read(self.pillBox.id, pBSCharx, pBSConfigCharx,
                  function(buffer) {
                      checkConfigData(buffer);
                  },
                  function(error) {
                      var msg = "Read Config failed. Error " + error + ". Disconnected " + self.pillBox.id;
                      fsm.disconnect(msg);
                  });
    };

    var readMode = function() {
          ble.read(self.pillBox.id, pBSCharx, pBSModeCharx,
                  function(buffer) {
                      readModeData(buffer);
                  },
                  function(error) {
                      var msg = "Read Mode failed. Error " + error + ". Disconnected " + self.pillBox.id;
                      fsm.disconnect(msg);
                  });
    };

    var readRSSI = function() {
        var device_Id = self.pillBox.id;
        ble.readRSSI(device_Id,
                function(rssi) {
                    console.log("RSSI " + rssi);
                    $rootScope.$broadcast("rssi", rssi);
                },
                function(err) {
                    var msg = 'unable to read RSSI ' + error;
                    fsm.disconnect(msg);
                });
    };

    var periodicTasks = [{func:readStatus, period:statusSampleDur},
                         {func:readConfig, period:configSampleDur},
                         {func:readMode,   period:modeSampleDur},
                         {func:readRSSI,   period:RSSISampleDur}];

    var writeSlot = function(slot) {
        var dataConfig = medConfig(slot);
        var slotCharx;
        var smFunc;
        var smMsgSuccess;

        var smFuncsArray = ["Slot2", "Slot3", "Slot4", "Notfn"];
        var slotCharx = [pBSSlot1InfoCharx, pBSSlot2InfoCharx, pBSSlot3InfoCharx, pBSSlot4InfoCharx];
        var slt_1 = slot - 1;
        var str = "config" + smFuncsArray[slt_1];

        ble.write(self.pillBox.id, pBSCharx, slotCharx[slt_1], dataConfig.buffer,
            function() {
                var msg = "Onconnect Slot" + slot + " Config Pass ";
                //console.log(msg);
                msg += "Write" + str;
                fsm[str](msg);
            },
            function(error) {
                var msg = "Onconnect Slot" + slot + " Config failed. Error " + error + ". Disconnected " + self.pillBox.id;
                fsm.disconnect(msg);
            }
        )
    }

    var onDiscoverDevice = function (device) {
        var dev, len, idx;
        
        fsm.found("Found Another " + device.name);

        if(self.pillBox === null) {
            if(device.name == 'PILLBOX') {
                self.pillBox = device;
                self.pillBox.mac = "Unk";
                window.storage.set('PILL_BOX', self.pillBox);
            }
        }
    };

    var clearPeriodicEvents = function() {
        eventSample.forEach(function(event) {
            clearInterval(event);
        });
        
        eventSample = [];
    }

    var disconnectCleanup = function() {
        clearPeriodicEvents();
        $rootScope.pillbox.macID = 'Unknown';
        $rootScope.pillbox.HWRev = 'Unknown';
        $rootScope.pillbox.FWRev = 'Unknown';

        var name = 'Unk'
        if(self.pillBox !== null) {
            name = self.pillBox.name;
            self.pillBox = null;
            
            $rootScope.$broadcast("bledisconnect", name);
        }
    };
    
    var readMac = function() {
      ble.read(self.pillBox.id, btSIGDevInfoServiceCharx, btSIGDevSerNum,
          function(buffer) {
              var checkMAC = readMacIDFunc(buffer);
              checkMAC.then(function(macID) {
                  $rootScope.pillbox.macID = macID;
                  fsm.readHWRev("Read HW Rev");
              }, function(error) {
                  if (self.pillBox !== null) {
                      var id = self.pillBox.id;
                      ble.disconnect(self.pillBox.id,
                          function() {
                              fsm.disconnect(error);
                          });
                  }
                  else {
                      fsm.disconnect(error);
                  }
              });
          },
          function(error) {
              var msg = "Read Status failed. Error " + error + ". Disconnected " + self.pillBox.id;
              fsm.disconnect(msg);
          });
    };

    var readHWRev = function() {
      ble.read(self.pillBox.id, btSIGDevInfoServiceCharx, btSIGDevHWRev,
          function(buffer) {
              readHWRevFunc(buffer);
              fsm.readFWRev("Read FW Rev");
          },
          function(error) {
              var msg = "Read Status failed. Error " + error + ". Disconnected " + self.pillBox.id;
              fsm.disconnect(msg);
          });
    };

    var readFWRev = function() {
      ble.read(self.pillBox.id, btSIGDevInfoServiceCharx, btSIGDevFWRev,
          function(buffer) {
              readFWRevFunc(buffer);
              fsm.configRTC("Write RTC");
          },
          function(error) {
              var msg = "Read Status failed. Error " + error + ". Disconnected " + self.pillBox.id;
              fsm.disconnect(msg);
          });
    };

    var writeRTC = function() {
        var dataRTC = timeToBuffer();
        ble.write(self.pillBox.id, pBSCharx, pBSRTCCharx, dataRTC.buffer,
            function() {
                var msg = "Onconnect RTC Pass ";
                // console.log(msg);
                msg += "Write Config Box";
                fsm.configBOX(msg);
            },
            function(error) {
                var msg = "writeRTC Failed. Error " + error + ". Disconnected " + self.pillBox.id;
                fsm.disconnect(msg);
            }
        )
    };
    
    var updateMedInfo = function() {
        var curdate = new Date();
        var medlist = getMedBoxInfo(curdate);
        var i = 0;
        for(i = 0; i < medlist.length; i++) {
            curDose = medlist[i].medicines;
            var len = curDose.length;
            medicationInfo[i].numMeds = len;
            medicationInfo[i].medsName = [];
            for(var j = 0; j < len; j++) {
                medicationInfo[i].medsName.push(curDose[j].desc);
            }
        }

        for(; i < maxNumSlots; i++) {
            medicationInfo[i].numMeds = 0;
            medicationInfo[i].medsName = [];
        }
    };

    var writeConfigBOX = function() {
        var dataConfig = pillBoxConfig(numSlots);
        ble.write(self.pillBox.id, pBSCharx, pBSConfigCharx, dataConfig.buffer,
            function() {
                // console.log("Onconnect Config Pass ");
                fsm.configSlot1("Write Slot1");
            },
            function(error) {
                var msg = "write Config Failed. Error " + error + ". Disconnected " + self.pillBox.id;
                fsm.disconnect(msg);
            }
        )
    };

    var writeNotfn = function() {
        ble.startNotification(self.pillBox.id, pBSCharx, pBSNotificationCharx,
            function(buffer) {
                // var msg = "Onconnect Nofication Enable Pass";
                // console.log(msg);
                alertNotification(buffer, pBSNotificationCharx);
            },
            function(error) {
                var msg = "Notfn enable Failed. Error " + error + ". Disconnected " + self.pillBox.id;
                fsm.disconnect(msg);
            }
        );
        var msg = "Write User Info";
        fsm.cfgUserInfo(msg);
    };

    var writeUserInfo = function() {
        var userInfo = getUserInfo();
        ble.write(self.pillBox.id, pBSCharx, pBSUserInfoCharx, userInfo.buffer,
            function() {
                var msg = "Enable Periodic Funcs";
                fsm.cfgPeriodFn(msg);
            },
            function(error) {
                var msg = "write User Info Failed. Error " + error + ". Disconnected " + self.pillBox.id;
                fsm.disconnect(msg);
            }
        )
    }

    var enablePeriodicFcn = function() {
        //Enable period read of RSSI
        $rootScope.$broadcast("bleconnected", "Pillbox");
        periodicTasks.forEach(function(task) {
            var func = task.func;
            var period = task.period;
            func();
            var event = setInterval(func, period*1000);
            eventSample.push(event);
        });
    };

    var readData = function(buffer, charx) {
        var data;
        var charDetails;
        var type;
        var len;
        var parseFunc;
        var parsedEvent = null;
        if(charx in serviceDetails) {
            charDetails = serviceDetails[charx];
            type = charDetails.type;
            len = charDetails.len;
            parseFunc = charDetails.parseFunc;
            switch(type) {
                case 'UTF8s':
                    data = new Uint8Array(buffer);
                    data = String.fromCharCode.apply(null, data);
                    break;
                case 'UInt8':
                    data = new Uint8Array(buffer);
                    break;
                case 'UInt32':
                    data = new Uint32Array(buffer);
                    break;
                default:
                    data = new Uint8Array(buffer);
                break;
            }
            if(parseFunc)
                parsedEvent = parseFunc(data, len);
        }
        else
            parsedEvent = 'Unk';
    
        return parsedEvent;
    };

    var checkConfigData = function(buffer) {
        var ConfigData = readData(buffer, pBSConfigCharx);
        $rootScope.$broadcast("configupdate", ConfigData);
    };

    var readModeData = function(buffer) {
        var mode = readData(buffer, pBSModeCharx);
        $rootScope.modeUpdate(mode);
    };

    var alertNotification = function(buffer, charx) {
        var parsedEvent = readData(buffer, charx);
        var localmsg = "Received Notification " + parsedEvent;
        var titleText = "MedMonitor Pillbox Notfn";
        var scope = $rootScope.$new();
        if(parsedEvent.includes("Alarm Slot")) {
            var myPopup = $ionicPopup.confirm({
                scope: scope,
                template: localmsg,
                cssClass: "confirmpopup",
                title: titleText,
                cancelText: "Cancel",
                okText: "Snooze",
                okType: "button-assertive",
            });
                    
            myPopup.then(function(res) {
                if(res) { //Snooze
                    console.log('Snooze');
                    var modeCfg = modeConfig(MODE_SNOOZE);
                    BLE.write(pBSCharx, pBSModeCharx, modeCfg);
                }
                else { //Canel
                }
            });
        }
        else {
            genericPopupAlert(scope, $ionicPopup, localmsg, titleText);
        }
    };

    var connectPillBox = function() {
        if(self.pillBox !== null) {
            ble.connect(self.pillBox.id,
                function(peripheral) {
                    fsm.readMac("Read Mac");
                },
                function(reason) {
                    if(self.pillBox !== null) {
                        var error = "Disconnected from " + self.pillBox.name + " because " + JSON.stringify(reason);
                        console.log(error);
                        fsm.disconnect(error);
                    }
                    else {
                        var error = "Connection failed: " + reason;
                        console.log(error);
                        fsm.disconnect(error);
                    }
                }
            );
        }
    };

    var smScan = function() {
        /* scan for all devices */
        ble.startScanWithOptions([pBSCharx],
         { reportDuplicates: false },
         onDiscoverDevice,
         function(error) {
            fsm.disconnect(error);
         });

        // stop scan after 5 seconds
        setTimeout(ble.stopScan, 5000,
            function() {
                if(fsm.is('devicesfound') == true) {
                    var msg = "Pillbox Found";
                    fsm.connect(msg);
                    console.log(msg);
                }
                else {
                    var msg = "No devices found";
                    fsm.disconnect(msg);
                    console.log(msg);
                }
            },
            function() {
                var error = "stopScan failed";
                fsm.disconnect(error);
                console.log(error);
            }
        );
    }

    var self = this;
    self.pillBox = null;

    self.connectedInfo = function () {
        var retval = false;
        
        if(fsm.is('periodicFunc') == true) {
            retval = true;
        }
        
        return retval;
    }

    self.scan = function() {
        // disconnect the connected device (hack, device should disconnect when leaving detail page)
        if (self.pillBox !== null) {
            var id = self.pillBox.id;
            ble.disconnect(self.pillBox.id,
                function() {
                    var msg = "Disconnected " + id;
                    fsm.disconnect(msg);
                }
            );
        }
        else {
            var msg = "Scanning ";
            fsm.scan(msg);
        }
    }

    self.updateConfig = function() {
        var msg = "Update config";
        clearPeriodicEvents();
        fsm.configBOX(msg);
    }

    self.changeMode = function(mode) {
        var modeCfg = modeConfig(mode);
        self.write(pBSCharx, pBSModeCharx, modeCfg);
    }

    self.disconnect = function() {
        var msg = "Disconnect and stop scan";
        // disconnect the connected device  and stop scanning as we are about to deActivate app
        if (self.pillBox !== null) {
            var id = self.pillBox.id;
            ble.disconnect(self.pillBox.id,
                function() {
                    var msg = "Stop scan after Disconnected " + id;
                    fsm.stopscan(msg);
                }
            );
        }
        else {
            var msg = "Not connected. But stopscanning";
            fsm.stopscan(msg);
        }
    }

    self.write = function(service, characteristic, value) {
        var len = value.length;
        var data = new Uint8Array(len);

        for(idx = 0; idx < len; idx++) {
            data[idx] = value[idx];
        }

        ble.write(self.pillBox.id, service, characteristic, data.buffer,
                function() {
                    console.log("BLE Write to " + characteristic + " Pass")
                },
                function(err) {
                    console.log("BLE Write Error to " + characteristic + " " + JSON.stringify(err));
                    fsm.disconnect(error);
                });
    }
    
    self.startStateNotifications = function() {
        var success = function(state) {
            $rootScope.$broadcast("blestatechange", state);
        };

        var failure = function(err) {
            console.log("BLE State Change Error " + err);
        };

        ble.startStateNotifications(success, failure);
    }
    
    self.stopStateNotifications = function() {
        var success = function(state) {
            console.log("BLE State Change stop ");
        };

        var failure = function(err) {
            console.log("BLE State Change Error " + err);
        };

        ble.stopStateNotifications(success, failure);
    }

    return self;
});
