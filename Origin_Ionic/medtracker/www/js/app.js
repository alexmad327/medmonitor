// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'medmonitor' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'medmonitor.services' is found in services.js
// 'medmonitor.controllers' is found in controllers.js
angular.module('medmonitor', ['ionic', 'ion-datetime-picker', 'ngCordova', 'ngAnimate', 'chart.js','ui.calendar',
        'medmonitor.config',
        'medmonitor.controllers',
        'medmonitor.logincontroller',
        'medmonitor.summarycontroller',
        'medmonitor.scorecardcontroller',
        'medmonitor.settingscontroller',
        'medmonitor.adherencecontroller',
        'medmonitor.reassignmedscontroller',
        'medmonitor.adjusttimescontroller',
        'medmonitor.holidaycontroller',
        'medmonitor.socialmediacontroller',
        'medmonitor.bleservices',
        'medmonitor.apiservices',
        'medmonitor.updateservice',
        'medmonitor.socialservices'])

.run(function($rootScope, $ionicPlatform, $timeout, $interval, $cordovaLocalNotification, BLE, $ionicPopup, $cordovaAppVersion, $cordovaDevice, $cordovaPushV5) {
    var modeIconChange = function(mode, applyFlag) {
        switch(mode) {
            case MODE_BUZZER:
                $rootScope.changeAudioIcon('ion-volume-high', applyFlag);
                break;
            case MODE_SILENT:
                $rootScope.changeAudioIcon('ion-volume-mute', applyFlag);
                break;
            case MODE_LOADSTART:
                $rootScope.changeLoadIcon('ion-load-a', applyFlag);
                break;
            case MODE_LOADEND:
                $rootScope.changeLoadIcon('', applyFlag);
                break;
        }
    };

    var modeChange = function(mode, applyFlag) {
        console.log('Mode Change to ' + mode);
        if(BLE.connectedInfo() == true) {
            BLE.changeMode(mode);
            modeIconChange(mode, applyFlag);
        }
    };

    var checkUnsentData = function() {
        $rootScope.$broadcast("checkunsentdata", "HW");
        $rootScope.$broadcast("updatebadge", "HW");
    }


    $ionicPlatform.ready(function() {
        setTimeout(function() {navigator.splashscreen.hide();}, 300);

        // Hide the accessory bar by default remove this to show the accessory bar above the keyboard
        // for form inputs
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(true);
        }

        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    
        window.addEventListener("orientationchange", onOrientationChange, false);
        function onOrientationChange() {
            console.log(screen.orientation); // e.g. portrait
        }
    
        screen.lockOrientation('portrait');

        $cordovaAppVersion.getVersionNumber().then(function(version) {
            $rootScope.version = version;
        });

        $rootScope.device = $cordovaDevice.getDevice();
        $rootScope.cordova = $cordovaDevice.getCordova();
        $rootScope.model = $cordovaDevice.getModel();
        $rootScope.platform = $cordovaDevice.getPlatform() + ' ' + ionic.Platform.version();
        $rootScope.uuid = $cordovaDevice.getUUID();
        
        if($rootScope.platform.includes('Android')) {
            //kNotificationSound = 'android.resource://com.benesalustech.medmonitor/raw/sound';
            kNotificationSound = 'file://audio/sound.mp3';
        }
        else {
            kNotificationSound = 'file://audio//sound.caf';
        }

        $interval(checkUnsentData, CHECKUNSENTINTERVAL);
        BLE.startStateNotifications();

        var pushNotfnOptions = {
            android: {
      	},
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            }
        };
    
        // initialize
        $cordovaPushV5.initialize(pushNotfnOptions).then(function() {
            // start listening for new notifications
            $cordovaPushV5.onNotification();

            // start listening for errors
            $cordovaPushV5.onError();

            // register to get registrationId
            var regId = window.storage.get('PUSH_REGID');
            if(regId !== undefined) {
                $rootScope.regId = regId;
                console.log("RegId From Storage", $rootScope.regId);
            }
            else {
                $cordovaPushV5.register().then(function(registrationId) {
                    if (1) { //ionic.Platform.isIOS()
                        $rootScope.regId = registrationId;
                        window.storage.set('PUSH_REGID', registrationId);
                        console.log("iOS",$rootScope.regId);
                    }
                });
            }
        });

        $rootScope.$on('$cordovaLocalNotification:click', function (event, notification, state) {
            notification["state"] = state;
            showSnzTaken(notification, $cordovaLocalNotification, $rootScope, $ionicPopup);
        });
    
    });

    $ionicPlatform.on('pause', function() {
        console.log('pause');
        BLE.stopStateNotifications();
        cancelAllForeGNotfn($timeout);
    });

    $ionicPlatform.on('resume', function() {
        console.log('resume');
        $cordovaLocalNotification.getAll().then(function(notfn) {
        //    alert(notfn.length === 0 ? '- none -' : notfn.join(' ,'));
            console.log(notfn.length === 0 ? '- none -' : 'HW' + notfn.join(' ,'));
        });
        var boxArray = window.storage.get('BOX_TIMES');
        BLE.startStateNotifications();
        jQuery.each(boxArray, function( box_index, box_time ){
            var scheduleArray = createScheduleNotificationsArray(box_time);
            //foreground notifications
            timeOutFcn ($timeout, scheduleArray, $rootScope, $ionicPopup);
        });
        //Check for unsent Data Dose Taken, BM, Medlist
    });
        
    // triggered every time notification received
    $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, notification){
        console.log("Botfn Rx", notification);
        if (1) { // ionic.Platform.isIOS())
            $rootScope.$broadcast("notificationReceived", notification);
        }
        else if (ionic.Platform.isAndroid()) {
            if (notification.event == "registered") {
                $rootScope.regId = notification.regid;
                window.storage.set('PUSH_REGID', $rootScope.regId);
                console.log("Android", $rootScope.regId);
            }
            else if (notification.event == "message") {
                $rootScope.$broadcast("notificationReceived", notification);
            }
        }
    });
  
    // triggered every time error occurs
    $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e){
      // e.message
    });

    $rootScope.ble = {icon: "", color:"blue-icon", audiomode: "", loadmode: ""};
    $rootScope.pillbox = {macID:'Unknown',
                          HWRev:'Unknown',
                          FWRev:'Unknown',
                          bat: {icon: "", color:""},
                          mode: {quietFlag: false, audioMode: 'Buzzer', loadModeFlag: false, loadMode: 'Normal'},
    };
 
    $rootScope.version = "unk";
    $rootScope.regId = "unk";
    $rootScope.imagename="bene_icon.png";

    $rootScope.about = function() {
        var myPopup = $ionicPopup.alert({
            scope: $rootScope,
            templateUrl:'about.html', 
            cssClass: "alertpopup-positive",
            title:  "About MedMonitor",
        });
    }

    $rootScope.batteryUpdate = function(bat, charge) {
        var percentIcon, batIcon;
        var batColor = '';
        if(bat > 90) {
            percentIcon = 'ion-battery-full';
        }
        else if(bat > 60) {
            percentIcon = 'ion-battery-half';
        }
        else if(bat > 25) {
            percentIcon = 'ion-battery-low';
            batColor = 'red-icon';
        }
        else {
            percentIcon = 'ion-battery-empty';
            batColor = 'red-icon';
        }

        switch(charge) {
            case 0:
                batIcon = percentIcon;
                break;
            case 1:
                batIcon = 'ion-battery-charging '; //Wireless charger
                break;
            case 2:
                batIcon = 'ion-battery-charging '; //USB charger
                break;
            default:
                batIcon = percentIcon;
                break;
        }

        $rootScope.changeBatIcon(batIcon, batColor);
    }

    $rootScope.modeUpdate = function(mode) {
        console.log('Mode update',  mode);
        var pillBoxMode = $rootScope.pillbox.mode;
        switch(mode) {
            case MODE_BUZZER:
                if(pillBoxMode.quietFlag == true) {
                    pillBoxMode.quietFlag = false;
                    pillBoxMode.audioMode = 'Buzzer';
                    modeIconChange(mode, true);
                }
                break;
            case MODE_SILENT:
                if(pillBoxMode.quietFlag == false) {
                    pillBoxMode.quietFlag = true;
                    pillBoxMode.audioMode = 'Vibrator';
                    modeIconChange(mode, true);
                }
                break;
            case MODE_LOADSTART:
                if(pillBoxMode.loadModeFlag == false) {
                    pillBoxMode.loadModeFlag = true;
                    pillBoxMode.loadMode = 'Load';
                    modeIconChange(mode, true);
                }
                break;
            case MODE_LOADEND:
                if(pillBoxMode.loadModeFlag == true) {
                    pillBoxMode.loadModeFlag = false;
                    pillBoxMode.loadMode = 'Normal';
                    modeIconChange(mode, true);
                }
                break;
        }
    }
    
    $rootScope.getAudioMode = function() {
        var audioMode, mode;
        var pillBoxMode = $rootScope.pillbox.mode;

        if(pillBoxMode.quietFlag == false) {
            audioMode = "Buzzer";
            mode = MODE_BUZZER;
        }
        else {
            audioMode = "Vibrator";
            mode = MODE_SILENT;
        }

        pillBoxMode.audioMode = audioMode;
        modeChange(mode, false);
    }

    $rootScope.getLoadMode = function() {
        var loadMode, mode;
        var pillBoxMode = $rootScope.pillbox.mode;

        if(pillBoxMode.loadModeFlag == false) {
            loadMode = "Normal";
            mode = MODE_LOADEND;
        }
        else {
            loadMode = "Load";
            mode = MODE_LOADSTART;
        }

        pillBoxMode.loadMode = loadMode;
        modeChange(mode, false);
    }

    $rootScope.changeAudioIcon = function(icon, applyFlag) {
        console.log("Change Icon " + icon);
        $rootScope.ble.audiomode = icon;
        if(applyFlag) {
            $rootScope.$apply();
        }
    }

    $rootScope.changeLoadIcon = function(icon, applyFlag) {
        $rootScope.ble.loadmode = icon;
        if(applyFlag) {
            $rootScope.$apply();
        }
    }

    $rootScope.changeBLEStatusIcon = function(icon, color) {
        $rootScope.ble.icon = icon;
        $rootScope.ble.color = color;
    }

    $rootScope.changeBatIcon = function(batIcon, batColor) {
        var pillBoxBat = $rootScope.pillbox.bat;
        pillBoxBat.icon = batIcon;
        pillBoxBat.color = batColor;
        $rootScope.$apply();
    }

});
