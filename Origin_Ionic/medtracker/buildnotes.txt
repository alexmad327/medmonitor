20180101    iOS Build. When updating UIDD to add or remove devices, need to clear the cache of local provisioning profile. Generating a new profle on developer account is not sufficient.
            XCode does not automatically download the new profile. Follow the steps below
                1. Go to ~/Library/MobileDevice/Provisioning\ Profiles/ and delete all the provisioning profiles from there.
                2. Go to XCode > Preferences > Accounts and select the Apple Id.
                3. Click Download All Profiles. And it will download all the provisioning profiles again.
20810207    Android build used to fail. Based on stackoverflow made the following mods to platforms/android/build.gradle
            - Replaced
              allprojects {
                 repositories {
                     mavenCentral();
                     jcenter()
                 }
              }
              With 
              allprojects {
                 repositories {
                     mavenCentral();
                     jcenter()
                     maven {
                         url "https://maven.google.com"
                     }
                 }
              }
             - Modified platforms/android/project.properties
                Changed target=android-25 to target=android-26
20180228 Android build failed with "Execution failed for task ':transformClassesWithJarMergingForDebug'." messag" message. Fix based on https://github.com/phonegap/phonegap-plugin-barcodescanner/issues/535
             - Modified
                    cordova.system.library.2=com.facebook.android:facebook-android-sdk:4+
                    cordova.system.library.2=com.android.support:support-v4+
               To
                    cordova.system.library.1=com.facebook.android:facebook-android-sdk:4.25.0
                    cordova.system.library.2=com.android.support:support-v4:25.0

             - Modified platforms/android/project.properties
20180323    Build a view from scratch
            - npm install
            - npm instal imagemagick
            - ionic cordova platform remove android
            - ionic cordova platform remove ios 
            - ionic cordova resources android
            - ionic cordova resources ios
            - ionic cordova platform add android
            - ionic cordova platform add ios
            - Added hook to execute before-build shell script to copy build-extras.grade to platform/android
            - Update plugin/cordova-plugin-facebook4/plugin.xml framework src="com.facebook.android:facebook-android-sdk:4.+" to src="com.facebook.android:facebook-android-sdk:4.25.0"
            - Update plugins/de.applant.cordova.plugin.local-notification/plugin.xml framework src="com.android.support:support-v4:+" to src="com.android.support:support-v4:25.0"
            - ionic cordova build android
            - ionic cordova build ios
            - Open project in xcode and fix profile
            - ionic cordova build ios
20180326    Updateed steps to Build a view from scratch. Added couple of steps to fix ios build
            - npm install
            - npm instal imagemagick
            - ionic cordova platform remove android
            - ionic cordova platform remove ios 
            - ionic cordova platform add android
            - ionic cordova platform add ios
            - ionic cordova build ios           //This build will fail due missing resources. However correct splash image with version number will be generated
            - ionic cordova resources android   //Next two steps create the splash and icon resources for both ios and android
            - ionic cordova resources ios
            - ionic cordova build ios           //This build will fail to Export and Archive due to profile issues
            - Open project in xcode and fix profile
            - ionic cordova platform remove ios 
            - ionic cordova platform add ios
            - ionic cordova build ios           //This build should succeed. Not will :)
            - ionic cordova build android       //This build should succeeds. Sometime you may need to remove android add it and then rebuild
20180605    - Updated app to use local version of ionic and npm modules. Update ~/.bash_profile to add local npm_modules to the path.Cordova is still using a global build. Change this later.
            - npm install
            - npm update
            - npm instal imagemagick
            - cordova-ios updated from 4.4.0 to 4.5.4
            - cordova-android updated from 6.2.3 to 6.4.0
            - Cordova-android stores debug and release apk in different folders. Update after_build hooks to cp and upload apk to Server appropriately (genapk.sh)
            - Updated ios after_build hook genipa.sh to handle Archiving and uploading release build (Uploading release build is moot)
            Updated xcode to manually signing the app. This necessicated mods to appstore.plist to include Profile and Certificate Info. Created two plist files ExportOptionsRelease.plist and ExportOptionsDebug.plist that are conditionlly included insread off commenting/uncommenting appstore.plist based on build type
            - Updated confilg.xml to include new resource for APp Store Icon (1024x1024). Supported by cordova-ios 4.5.4
            - Updated Icon to be 1024x1034 from 512x512
            - Conflict in build with plugin-compat lead to removal of plugin-compat. BLE had dependency on this. Hence had to Update bluetooth plugin
            - Update bluetooth (BLE) plugnin to 1.1.9 from 1.1.4. To support iOS10+ requirement added bluetooth plugin as
                ionic cordova plugin add cordova-plugin-ble-central --variable BLUETOOTH_USAGE_DESCRIPTION="Blue tooth peripheral is used to connect to Smart Pillbox"
            - Plugin-compat not needed for Android but updated from 1.1.0 to 1.2.0
            - Removed plugin-console. It is not needed for IOS and removed it from Release build
            - Major updates to app signing. App keystore for Android was lost with Intel XDK going offline. Had to release with a new key and as new app on Play store. To this end had to make the following changes
                - Added  android-packageName="com.benesalustech.android.medmonitor" to have a package name for Android different than the one of iOS
                - Generated a new keystore 
