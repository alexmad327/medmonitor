import {createAppContainer, createStackNavigator} from "react-navigation";
import Registration from "../screens/Registration";
import SettingScreen from "../screens/SettingScreen";
import HomeScreen from "../screens/HomeScreen";
import DailyActivityScreen from "../screens/DailyActivityScreen";
import ManageMedsScreen from "../screens/ManageMedsScreen";
import AdjustDoseTime from "../screens/AdjustDoseTime";
import {DemoScreen} from "../screens/DemoScreen";
import ReAssignMedsScreen from "../screens/ReAssignMedsScreen";
import MedHolidayScreen from "../screens/MedHolidayScreen";
import {MoreScreen} from "../screens/MoreScreen";
import {PillBoxSettings} from "../screens/PillBoxSettings";
import SocialMediaScreen from "../screens/SocialMediaScreen";
import ChatScreen from "../screens/ChatScreen";
import {AdherenceScreen} from "../screens/AdherenceScreen";
import GraphScreen from "../screens/GraphScreen";
import CalendarScreen from "../screens/CalendarScreen";
import {InitialRouteScreen} from "../screens/InitialRouteScreen";

import {colors} from "../constants/Constants";

const MainNavigator = createAppContainer(
    createStackNavigator(
        {
            InitialRouteScreen: {screen: InitialRouteScreen},
            Registration: {screen: Registration},
            SettingScreen: {screen: SettingScreen},
            HomeScreen: {screen: HomeScreen},
            DailyActivityScreen: {screen: DailyActivityScreen},
            ManageMedsScreen: {screen: ManageMedsScreen},
            AdjustDoseTime: {screen: AdjustDoseTime},
            DemoScreen: {screen: DemoScreen},
            ReAssignMedsScreen: {screen: ReAssignMedsScreen},
            MedHolidayScreen: {screen: MedHolidayScreen},
            MoreScreen: {screen: MoreScreen},
            PillBoxSettings: {screen: PillBoxSettings},
            SocialMediaScreen: {screen: SocialMediaScreen},
            ChatScreen: {screen: ChatScreen},
            AdherenceScreen: {screen: AdherenceScreen},
            GraphScreen: {screen: GraphScreen},
            CalendarScreen: {screen: CalendarScreen}
        },
        {
            initialRouteName: "InitialRouteScreen",
            // headerMode:'none'
            navigationOptions: {
                headerTitleStyle: {
                    fontWeight: "bold",
                    color: colors.whiteColor
                },
                headerTintColor: colors.whiteColor,
                headerStyle: {
                    backgroundColor: colors.blueButtonColor
                }
            }
        }
    )
);

export default MainNavigator;
