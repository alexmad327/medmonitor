package com.medmonitor;

import android.os.Bundle; // here
import com.facebook.react.ReactActivity;
// react-native-splash-screen >= 0.3.1
import org.devio.rn.splashscreen.SplashScreen;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import java.util.List;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "MedMonitor";
    }

     @Override
        protected void onCreate(Bundle savedInstanceState) {
            SplashScreen.show(this);  // here
            super.onCreate(savedInstanceState);
            Intent intent = new Intent();

            String manufacturer = android.os.Build.MANUFACTURER;

            switch (manufacturer) {

                    case "xiaomi":
                        intent.setComponent(new ComponentName("com.miui.securitycenter",
                                "com.miui.permcenter.autostart.AutoStartManagementActivity"));
                        break;
                    case "oppo":
                        intent.setComponent(new ComponentName("com.coloros.safecenter",
                                "com.coloros.safecenter.permission.startup.StartupAppListActivity"));

                        break;
                    case "vivo":
                        intent.setComponent(new ComponentName("com.vivo.permissionmanager",
                                "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
                        break;
                }

                List<ResolveInfo> arrayList =  getPackageManager().queryIntentActivities(intent,
                          PackageManager.MATCH_DEFAULT_ONLY);

                    if (arrayList.size() > 0) {
                        startActivity(intent);
                    }
        }
}
