import {combineReducers} from "redux";
import MedicationListReducer from "./reducres/MedicationListReducer";
import UserMedicationListReducer from "./reducres/UserMedicationListReducer";
import SocialMessagesListReducer from "./reducres/SocialMessagesReducer";
import BioMetricReducer from "./reducres/BioMetricReducer";
import TodayMedicationReducer from "./reducres/TodayMedicationReducer"
import TodayBiometricReducer from "./reducres/TodayBiometricReducer";
import userPreferences from "./reducres/userPreference";
import DosageScheduleReducer from "./reducres/DosageScheduleReducer"
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";

const rootReducers = combineReducers({
    medicationListReducer: MedicationListReducer,
    userMedicationListReducer: UserMedicationListReducer,
    socialMessagesReducer: SocialMessagesListReducer,
    bioMetricReducer: BioMetricReducer,
    todayMedicationReducer: TodayMedicationReducer,
    todayBiometricReducer: TodayBiometricReducer,
    userPreferences: userPreferences,
    dosageSchedule: DosageScheduleReducer
});
const Centralstore = createStore(rootReducers, applyMiddleware(thunk));
export default Centralstore;
