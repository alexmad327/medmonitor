import * as actionTypes from "../actions/ActionTypes";

const initialState = {
    todayMedicationListData: null,
    today_error: null,
    today_loading: false
};

const TodayMedicationListReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_TODAY_MEDICATION_LIST_START:
            return {
                ...state,
                today_error: null,
                today_loading: true,
                todayMedicationListData: null
            };

        case actionTypes.FETCH_TODAY_MEDICATION_LIST_SUCCESS:
            return {
                ...state,
                today_error: null,
                today_loading: false,
                todayMedicationListData: action.todayMedicationListData
            };

        case actionTypes.FETCH_TODAY_MEDICATION_LIST_FAIL:
            return {
                ...state,
                today_error: action.error,
                today_loading: false
            };
        default:
            return state;
    }
};
export default TodayMedicationListReducer;
