import * as actionTypes from "../actions/ActionTypes"

const initialState = {
    dosageSchedules: null,
    dosage_error: null,
    dosage_loading: false,
};

const DosageScheduleReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_DOSAGE_SCHEDULE_START:
            return {
                ...state,
                dosage_error: null,
                dosage_loading: true,
                dosageSchedules: null,
            };

        case actionTypes.FETCH_DOSAGE_SCHEDULE_SUCCESS:
            return {
                ...state,
                dosage_error: null,
                dosage_loading: false,
                dosageSchedules: action.dosageSchedules
            };

        case actionTypes.FETCH_DOSAGE_SCHEDULE_FAIL:
            return {
                ...state,
                dosage_error: action.error,
                dosage_loading: false
            };
        default:
            return state;
    }
};
export default DosageScheduleReducer;