import * as actionTypes from "../actions/ActionTypes";

const initialState = {
  socialMessagesListData: null,
  error: null,
  loading: false
};

const SocialMessagesListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_SOCIAL_MESSAGES_LIST_START:
      return {
        ...state,
        error: null,
        loading: true,
        socialMessagesListData: null
      };

    case actionTypes.FETCH_SOCIAL_MESSAGES_LIST_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        socialMessagesListData: action.socialMessagesListData
      };

    case actionTypes.FETCH_SOCIAL_MESSAGES_LIST_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return state;
  }
};
export default SocialMessagesListReducer;
