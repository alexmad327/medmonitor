import * as actionTypes from "../actions/ActionTypes";

const initialState = {
  UsermedicationListData: null,
  error: null,
  loading: false
};

const UserMedicationListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USER_MEDICATION_LIST_START:
      return {
        ...state,
        error: null,
        loading: true,
        UsermedicationListData: null
      };

    case actionTypes.FETCH_USER_MEDICATION_LIST_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        UsermedicationListData: action.UsermedicationListData
      };

    case actionTypes.FETCH_USER_MEDICATION_LIST_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return state;
  }
};
export default UserMedicationListReducer;
