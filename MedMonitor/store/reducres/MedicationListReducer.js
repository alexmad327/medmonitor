import * as actionTypes from "../actions/ActionTypes"

const initialState = {
    medicationListData: null,
    error: null,
    loading: false,
};

const MedicationListReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_MEDICATION_LIST_START:
            return {
                ...state,
                error: null,
                loading: true,
                medicationListData: null,
            };

        case actionTypes.FETCH_MEDICATION_LIST_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                medicationListData: action.medicationListData
            };

        case actionTypes.FETCH_MEDICATION_LIST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false
            };
        default:
            return state;
    }
};
export default MedicationListReducer;