import * as actionTypes from "../actions/ActionTypes";

const initialState = {
  biometricListData: null,
  error: null,
  loading: false
};

const BioMetricReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_BIO_METRIC_LIST_START:
      return {
        ...state,
        error: null,
        loading: true,
        biometricListData: null
      };

    case actionTypes.FETCH_BIO_METRIC_LIST_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        biometricListData: action.biometricListData
      };

    case actionTypes.FETCH_BIO_METRIC_LIST_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return state;
  }
};
export default BioMetricReducer;
