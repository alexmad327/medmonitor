import * as actionTypes from "../actions/ActionTypes";

const initialState = {
    todayBiometricListData: null,
    error: null,
    loading: false
};

const TodayBiometricListReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_TODAY_BIOMETRIC_LIST_START:
            return {
                ...state,
                error: null,
                loading: true,
                todayBiometricListData: null
            };

        case actionTypes.FETCH_TODAY_BIOMETRIC_LIST_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                todayBiometricListData: action.todayBiometricListData
            };

        case actionTypes.FETCH_TODAY_BIOMETRIC_LIST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false
            };
        default:
            return state;
    }
};
export default TodayBiometricListReducer;
