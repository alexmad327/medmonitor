import axios from "axios";

export const fetchSocialMessagesList = (url, method, data) => {
  return axios({
    url: url,
    method: method,
    data: data
  })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      return error;
    });
};
