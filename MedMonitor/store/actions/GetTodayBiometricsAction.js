import axios from "axios";
import * as actionTypes from "./ActionTypes";
import {AsyncStorage, NetInfo} from "react-native";
import {API_BASE_URL} from "../../constants/Constants";

export const fetchTodayBiometricListStart = () => {
    return {
        type: actionTypes.FETCH_TODAY_BIOMETRIC_LIST_START
    };
};

export const fetchTodayBiometricListSuccess = data => {
    return {
        type: actionTypes.FETCH_TODAY_BIOMETRIC_LIST_SUCCESS,
        todayBiometricListData: data
    };
};
export const fetchTodayBiometricListFail = error => {
    return {
        type: actionTypes.FETCH_TODAY_BIOMETRIC_LIST_FAIL,
        error: error
    };
};

export const fetchTodayBiometricList = data => {
    // console.log("TODAY BIOMETRIC LIST DATA : ", data);

    return dispatch => {
        dispatch(fetchTodayBiometricListStart());
        return NetInfo.getConnectionInfo().then(async connectionInfo => {
            if (connectionInfo.type !== "none") {
                const token = await AsyncStorage.getItem("otpToken");
                const phone = await AsyncStorage.getItem("phone");

                let url =
                    API_BASE_URL +
                    `/gettodaysbiometrics.php?version=3.0.0&data={"phone":"${phone.trim()}","otptoken":"${token.trim()}","day":"${data.lastDate}","days":${data.lastDay}}`;
                console.log("URL : " + url);
                return axios
                    .post(url, {
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        }
                    })
                    .then(
                        async res => {
                            dispatch(fetchTodayBiometricListSuccess(res.data));
                            return res.data;
                        },
                        err => {
                            // console.log(JSON.stringify(err));
                            if (err) {
                                dispatch(fetchTodayBiometricListFail(err));
                                return err;
                            } else {
                                dispatch(fetchTodayBiometricListFail("Something Went Wrong"));
                                return err;
                            }
                        }
                    );
            } else {
                dispatch(fetchTodayBiometricListFail("No Internet Connection"));
            }
        });
    };
};
