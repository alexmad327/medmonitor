import axios from "axios";
import * as actionTypes from "./ActionTypes";
import {Platform, NetInfo, AsyncStorage} from "react-native";
import {API_BASE_URL} from "../../constants/Constants";

export const fetchBioMetricListStart = () => {
    return {
        type: actionTypes.FETCH_BIO_METRIC_LIST_START
    };
};

export const fetchBioMetricListSuccess = data => {
    return {
        type: actionTypes.FETCH_BIO_METRIC_LIST_SUCCESS,
        biometricListData: data
    };
};
export const fetchBioMetricListFail = error => {
    return {
        type: actionTypes.FETCH_BIO_METRIC_LIST_FAIL,
        error: error
    };
};

export const fetchBioMetricList = data => {
    console.log("BIO_METRIC LIST DATA : ", data);

    return dispatch => {
        dispatch(fetchBioMetricListStart());
        return NetInfo.getConnectionInfo().then(async connectionInfo => {
            if (connectionInfo.type !== "none") {
                const token = await AsyncStorage.getItem("otpToken");
                const phone = await AsyncStorage.getItem("phone");

                console.log(
                    "Medication List : Phone : " + phone + " : Token : " + token
                );

                // let url = API_BASE_URL + `/medlist.php?data={"phone":"${phone.trim()}","otptoken":"${token.trim()}"}`;
                let url =
                    API_BASE_URL +
                    `/userbiometriclist.php?version=3.0.0&data={"phone":"${phone.trim()}","otptoken":"${token.trim()}"}`;
                return axios
                    .post(url, {
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        }
                    })
                    .then(
                        async res => {
                            // console.log("Medication List Resp : ", JSON.stringify(res.data));
                            dispatch(fetchBioMetricListSuccess(res.data));
                            return res.data;
                        },
                        err => {
                            // console.log(JSON.stringify(err));
                            if (err) {
                                dispatch(fetchBioMetricListFail(err));
                                return err;
                            } else {
                                dispatch(fetchBioMetricListFail("Something Went Wrong"));
                                return err;
                            }
                        }
                    );
            } else {
                dispatch(fetchBioMetricListFail("No Internet Connection"));
            }
        });
    };
};
