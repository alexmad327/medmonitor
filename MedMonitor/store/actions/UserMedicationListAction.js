import axios from "axios";
import * as actionTypes from "./ActionTypes";
import {Platform, NetInfo, AsyncStorage} from "react-native";
import {API_BASE_URL} from "../../constants/Constants";

export const fetchUserMedicationListStart = () => {
    return {
        type: actionTypes.FETCH_USER_MEDICATION_LIST_START
    };
};

export const fetchUserMedicationListSuccess = data => {
    return {
        type: actionTypes.FETCH_USER_MEDICATION_LIST_SUCCESS,
        UsermedicationListData: data
    };
};
export const fetchUserMedicationListFail = error => {
    return {
        type: actionTypes.FETCH_USER_MEDICATION_LIST_FAIL,
        error: error
    };
};

export const fetchUserMedicationList = () => {
    console.log("MEDICATION USER LIST DATA CALLED: ");

    return dispatch => {
        dispatch(fetchUserMedicationListStart());
        return NetInfo.getConnectionInfo().then(async connectionInfo => {
            const RetrivedToken = await AsyncStorage.getItem("otpToken");
            const RetrivedPhone = await AsyncStorage.getItem("phone");

            if (connectionInfo.type !== "none") {
                let url = API_BASE_URL + `/usermedlist.php?version=3.0.0&data={"phone":"${RetrivedPhone.trim()}","otptoken":"${RetrivedToken.trim()}"}`;

                let payload = {
                    data: {
                        phone: `${RetrivedPhone}`,
                        otptoken: `${RetrivedToken}`
                    }
                };

                console.log("TOKENNNN", RetrivedToken);
                console.log("PHONE>>>", RetrivedPhone);
                console.log("DATA >>>>", payload);
                return axios({
                    method: "POST",
                    url: url,
                    //   data: payload
                }).then(
                    async res => {
                        // console.log("Medication User List Resp : ", res.data);
                        dispatch(fetchUserMedicationListSuccess(res.data));
                        return res.data;
                    },
                    err => {
                        console.log("USER MEDICATION LIST ERROR", err);
                        if (err) {
                            dispatch(fetchUserMedicationListFail(err));
                            return err;
                        } else {
                            dispatch(fetchUserMedicationListFail("Something Went Wrong"));
                            return err;
                        }
                    }
                );
            } else {
                dispatch(fetchUserMedicationListFail("No Internet Connection"));
            }
        });
    };
};
