import axios from 'axios';
import * as actionTypes from "./ActionTypes"
import {Platform, NetInfo, AsyncStorage} from "react-native";
import {API_BASE_URL} from "../../constants/Constants";

export const fetchMedicationListStart = () => {
    return {
        type: actionTypes.FETCH_MEDICATION_LIST_START
    };
};

export const fetchMedicationListSuccess = data => {
    return {
        type: actionTypes.FETCH_MEDICATION_LIST_SUCCESS,
        medicationListData: data
    };
};
export const fetchMedicationListFail = error => {
    return {
        type: actionTypes.FETCH_MEDICATION_LIST_FAIL,
        error: error
    };
};

export const fetchMedicationList = data => {
    console.log("MEDICATION LIST DATA : ", data);

    return dispatch => {
        dispatch(fetchMedicationListStart());
        return NetInfo.getConnectionInfo().then(async connectionInfo => {
            if (connectionInfo.type !== "none") {
                const token = await AsyncStorage.getItem("otpToken");
                const phone = await AsyncStorage.getItem("phone");

                console.log("Medication List : Phone : " + phone + " : Token : " + token);

                // let url = API_BASE_URL + `/medlist.php?data={"phone":"${phone.trim()}","otptoken":"${token.trim()}"}`;
                let url = API_BASE_URL + `/medlist.php?version=3.0.0&data={"phone":"${phone.trim()}","otptoken":"${token.trim()}"}`;
                return axios.post(url, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).then(
                    async res => {
                        // console.log("Medication List Resp : ", JSON.stringify(res.data));
                        dispatch(fetchMedicationListSuccess(res.data));
                        return res.data;
                    },
                    err => {
                        // console.log(JSON.stringify(err));
                        if (err) {
                            dispatch(fetchMedicationListFail(err));
                            return err;
                        } else {
                            dispatch(fetchMedicationListFail("Something Went Wrong"));
                            return err;
                        }
                    }
                );
            } else {
                dispatch(fetchMedicationListFail("No Internet Connection"));
            }
        });
    };
};