import axios from "axios";
import * as actionTypes from "./ActionTypes";
import {AsyncStorage, NetInfo} from "react-native";
import {API_BASE_URL} from "../../constants/Constants";

export const fetchDosageScheduleStart = () => {
    return {
        type: actionTypes.FETCH_DOSAGE_SCHEDULE_START
    };
};

export const fetchDosageScheduleSuccess = data => {
    return {
        type: actionTypes.FETCH_DOSAGE_SCHEDULE_SUCCESS,
        dosageSchedules: data
    };
};
export const fetchTodayBiometricListFail = error => {
    return {
        type: actionTypes.FETCH_DOSAGE_SCHEDULE_FAIL,
        error: error
    };
};

export const fetchDosageSchedule = () => {
    return dispatch => {
        dispatch(fetchDosageScheduleStart());
        return NetInfo.getConnectionInfo().then(async connectionInfo => {
            if (connectionInfo.type !== "none") {
                const token = await AsyncStorage.getItem("otpToken");
                const phone = await AsyncStorage.getItem("phone");

                let url =
                    API_BASE_URL +
                    `/dosageschedule.php?version=3.0.0&data={"phone":"${phone.trim()}","otptoken":"${token.trim()}"}`;
                console.log("URL : " + url);
                return axios
                    .post(url, {
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        }
                    })
                    .then(
                        async res => {
                            dispatch(fetchDosageScheduleSuccess(res.data));
                            return res.data;
                        },
                        err => {
                            // console.log(JSON.stringify(err));
                            if (err) {
                                dispatch(fetchTodayBiometricListFail(err));
                                return err;
                            } else {
                                dispatch(fetchTodayBiometricListFail("Something Went Wrong"));
                                return err;
                            }
                        }
                    );
            } else {
                dispatch(fetchTodayBiometricListFail("No Internet Connection"));
            }
        });
    };
};
