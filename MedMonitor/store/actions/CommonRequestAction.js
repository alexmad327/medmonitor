import axios from "axios";
import { API_BASE_URL } from "../../constants/Constants";

export const CommonActions = (url, method, data) => {
  return axios({
    url: API_BASE_URL + url,
    method: method,
    data: data
  })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      return error;
    });
};
