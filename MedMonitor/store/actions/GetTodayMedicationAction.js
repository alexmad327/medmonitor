import axios from "axios";
import * as actionTypes from "./ActionTypes";
import {Platform, NetInfo, AsyncStorage} from "react-native";
import {API_BASE_URL} from "../../constants/Constants";

export const fetchTodayMedicationListStart = () => {
    return {
        type: actionTypes.FETCH_TODAY_MEDICATION_LIST_START
    };
};

export const fetchTodayMedicationListSuccess = data => {
    return {
        type: actionTypes.FETCH_TODAY_MEDICATION_LIST_SUCCESS,
        todayMedicationListData: data
    };
};
export const fetchTodayMedicationListFail = error => {
    return {
        type: actionTypes.FETCH_TODAY_MEDICATION_LIST_FAIL,
        error: error
    };
};

export const fetchTodayMedicationList = data => {
    // console.log("TODAY MEDICATION LIST DATA : ", data);

    return dispatch => {
        dispatch(fetchTodayMedicationListStart());
        return NetInfo.getConnectionInfo().then(async connectionInfo => {
            if (connectionInfo.type !== "none") {
                const token = await AsyncStorage.getItem("otpToken");
                const phone = await AsyncStorage.getItem("phone");

                // let url = API_BASE_URL + `/gettodaysmedication.php?data={"phone":"${phone.trim()}","otptoken":"${token.trim()}"}`;
                let url =
                    API_BASE_URL +
                    `/gettodaysmedication.php?version=3.0.0&data={"phone":"${phone.trim()}","otptoken":"${token.trim()}","day":"${data.lastDate}","days":${data.lastDay}}`;

                // let url = "https://app.benesalustech.com/api/gettodaysmedication.php?data={\"phone\":\"18475550002\",\"otptoken\":\"5c77c43367af2\",\"day\":\"05-05-2019\",\"days\":3}";
                console.log("Get Today Medication URL : " + url);
                return axios
                    .post(url, {
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        }
                    })
                    .then(
                        async res => {
                            console.log("fetchTodayMedicationListSuccess : ", JSON.stringify(res));
                            dispatch(fetchTodayMedicationListSuccess(res.data));
                            return res.data;
                        },
                        err => {
                            // console.log(JSON.stringify(err));
                            if (err) {
                                dispatch(fetchTodayMedicationListFail(err));
                                return err;
                            } else {
                                dispatch(fetchTodayMedicationListFail("Something Went Wrong"));
                                return err;
                            }
                        }
                    );
            } else {
                dispatch(fetchTodayMedicationListFail("No Internet Connection"));
            }
        });
    };
};
