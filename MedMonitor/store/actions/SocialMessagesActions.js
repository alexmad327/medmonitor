import axios from "axios";
import * as actionTypes from "./ActionTypes";
import {Platform, NetInfo, AsyncStorage} from "react-native";
import {API_BASE_URL} from "../../constants/Constants";

export const fetchSocialMessagesListStart = () => {
    return {
        type: actionTypes.FETCH_SOCIAL_MESSAGES_LIST_START
    };
};

export const fetchSocialMessagesListSuccess = data => {
    return {
        type: actionTypes.FETCH_SOCIAL_MESSAGES_LIST_SUCCESS,
        socialMessagesListData: data
    };
};
export const fetchSocialMessagesListFail = error => {
    return {
        type: actionTypes.FETCH_SOCIAL_MESSAGES_LIST_FAIL,
        error: error
    };
};

export const fetchSocialMessagesList = () => {
    console.log("FETCH MESSAGES  CALLED: ");

    return dispatch => {
        dispatch(fetchSocialMessagesListStart());
        return NetInfo.getConnectionInfo().then(async connectionInfo => {
            const RetrivedToken = await AsyncStorage.getItem("otpToken");
            const RetrivedPhone = await AsyncStorage.getItem("phone");
            if (connectionInfo.type !== "none") {
                let url =
                    API_BASE_URL +
                    `/getsocialmediamessages.php?version=3.0.0&data={"phone":"${RetrivedPhone.trim()}","otptoken":"${RetrivedToken.trim()}"}`;

                let payload = {
                    data: {
                        phone: `${RetrivedPhone}`,
                        otptoken: `${RetrivedToken}`
                    }
                };

                console.log("TOKENNNN", RetrivedToken);
                console.log("PHONE>>>", RetrivedPhone);
                console.log("DATA >>>>", payload);
                return axios({
                    method: "POST",
                    url: url
                    //   data: payload
                }).then(
                    async res => {
                        console.log("FETCH  MESSGAES List Resp : ", res.data);
                        dispatch(fetchSocialMessagesListSuccess(res.data));
                        return res.data;
                    },
                    err => {
                        console.log("FETCH MESSGAES ERROR", err);
                        if (err) {
                            dispatch(fetchSocialMessagesListFail(err));
                            return err;
                        } else {
                            dispatch(fetchSocialMessagesListFail("Something Went Wrong"));
                            return err;
                        }
                    }
                );
            } else {
                dispatch(fetchSocialMessagesListFail("No Internet Connection"));
            }
        });
    };
};
