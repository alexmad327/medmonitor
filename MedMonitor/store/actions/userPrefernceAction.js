import {setLocale} from "../../locale/i18n";
import * as actionTypes from "./ActionTypes";
import Centralstore from "../StoreReducer";
// import Store from "../store";
export const setCurrentLanguage = lang => {
    setLocale(lang);
    return {
        type: actionTypes.CHANGE_LANGUAGE,
        payload: lang
    };
};

export const toggleLanguage = (currentLanguage) => {

    // const currentLanguage = Centralstore.getState().userPreferences.language;

    // alert(currentLanguage);
    return dispatch => {
        if (currentLanguage === "en") {
            dispatch(setCurrentLanguage("en"));
        } else {
            dispatch(setCurrentLanguage("sp"));
        }
    };
};
