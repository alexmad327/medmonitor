import React, {Component} from 'react';
import {Button, Container, Header, Title} from 'native-base';
import {Image, Alert} from "react-native";
import {colors} from "../constants/Constants";

export class HeaderView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }

    render() {
        return (
            <Header style={{backgroundColor: colors.blueButtonColor}}>
                <Button transparent onPress={() => {
                    this.props.navigation.goBack();
                }}>
                    <Image source={require('../images/back.png')} style={{height: 25, width: 25}}/>
                </Button>

                <Title style={{
                    flex: 1,
                    textAlign: 'center',
                    alignSelf: 'center',
                    color:colors.whiteColor
                }}>{this.props.name}</Title>
                <Image style={{height: 25, width: 25}}/>
            </Header>
        );
    }
}