import React, {Component} from 'react';
import {Button, Footer, FooterTab, Text} from 'native-base';
import {View, Alert, AsyncStorage} from "react-native";
import {colors} from "../constants/Constants";
import {Image} from "react-native-elements";
import {translate} from "../locale/i18n";

export class FooterView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userMeds: [],
            freq1: [],
            freq2: [],
            freq3: [],
            freq4: []
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View>
                <Footer>
                    <FooterTab style={{backgroundColor: colors.whiteColor}}>
                        <Button onPress={() => {
                            // Alert.alert("Home")
                            navigate("HomeScreen")
                        }} vertical>
                            <Image source={require('../images/home.png')} style={{height: 30, width: 30}}/>
                            <Text style={{color: colors.blackColor, fontSize: 8}}>{translate("footer.home")}</Text>
                        </Button>
                        <Button onPress={() => {
                            // Alert.alert("Daily Activity")
                            navigate("DailyActivityScreen")
                        }} vertical>
                            <Image source={require('../images/reminder.png')} style={{height: 30, width: 30}}/>
                            <Text style={{
                                color: colors.blackColor,
                                fontSize: 8
                            }}>{translate("footer.daily_activity")}</Text>
                        </Button>
                        <Button onPress={() => {
                            // Alert.alert("Manage Meds")
                            navigate("ManageMedsScreen")
                        }} vertical>
                            <Image source={require('../images/pill_single_grey.png')} style={{height: 30, width: 30}}/>
                            <Text
                                style={{color: colors.blackColor, fontSize: 8}}>{translate("footer.manage_meds")}</Text>
                        </Button>
                        <Button onPress={() => {
                            // Alert.alert("More")
                            navigate("MoreScreen")
                        }} vertical>
                            <Image source={require('../images/more.png')} style={{height: 30, width: 30}}/>
                            <Text
                                style={{color: colors.blackColor, fontSize: 8}}>{translate("footer.more")}</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </View>
        );
    }
}