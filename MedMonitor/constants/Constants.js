export const colors = {
    blackColor: "#000000",
    whiteColor: "#FFFFFF",
    blueButtonColor: "#0092D9",
    takenPillGreenColor: "#4cd980", //2BC362
    takenPillYellowColor: "#f4e970",//F4D94C
    notTakenPillColor: "#ff8d75",//ff6854
    greyColor: "#B4B4B4",
    transperantColor: "#00FFFFFF",
    darkBlueColor: "#005E84",
    lightGreyColor: "#d9d9d9"
};

export const headerTitleStyle = {
    color: colors.whiteColor,
    alignContent: "center",
    alignSelf: "center",
    textAlign: "center",
    flex: 1
};

export const LOCALE = "fr";
// export const API_BASE_URL = "https://app.benesalustech.com/api";
export const API_BASE_URL = "https://test.benesalustech.com/api";
// export const API_BASE_URL = "https://test.benesalustech.com/api/";
export const GET_USER_DETAILS_TEMP_URL = "https://test.benesalustech.com/temp/alex.php";
