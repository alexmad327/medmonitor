import ReactNative from "react-native";
import I18n from "react-native-i18n";

// Import all locales
import en from "./en.json";
import fr from "./fr.json";
import sp from "./sp.json";

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
    en,
    fr,
    sp
};

const currentLocale = I18n.currentLocale("sp");

// Is it a RTL language?
export const isRTL =
    currentLocale.indexOf("fr") === 0 || currentLocale.indexOf("ar") === 0;

// Allow RTL alignment in RTL languages
ReactNative.I18nManager.allowRTL(isRTL);

// The method we'll use instead of a regular string
export function translate(name, params = {}) {
    // I18n.locale = locale;

    console.log("Params", params);
    return I18n.t(name, params);
}

export function setLocale(lang) {

    console.log('set locale ', lang);
    I18n.locale = lang;
}

export default I18n;
