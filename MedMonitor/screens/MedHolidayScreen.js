import React, {Component} from "react";
import {
    ActivityIndicator,
    Alert,
    AsyncStorage,
    Platform,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    View
} from "react-native";
import {API_BASE_URL, colors} from "../constants/Constants";
import {Image, Text} from "react-native-elements";
import {Card, CheckBox} from "native-base";
import {FooterView} from "../container/FooterView";
import {HeaderView} from "../container/HeaderView";
import DatePicker from "react-native-datepicker";
import * as UserMedicationListActions from "../store/actions/UserMedicationListAction";
import {connect} from "react-redux";
import axios from "axios";
import I18n, {translate} from "../locale/i18n";
import moment from "moment";

class MedHolidayScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: undefined,
            endDate: undefined,
            updatedMedList: [],
            meds: [],
            fetchingMeds: false,
            temArray: [],
            phone: "",
            otpToken: ""
        };
    }

    updateMedicationList() {
        this.setState({fetchingMeds: true});
        return axios.post(
            API_BASE_URL +
            `/updatemedication.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","meds":${JSON.stringify(
                this.state.updatedMedList
            )}}`,
            {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }
        );
    }

    checkDateValidations(startDate, endDate, isEnable) {
        var start = null;
        var end = null;
        if (startDate != null) {
            start = new Date(startDate);
            start.setHours(0, 0, 0, 0);
        }
        if (endDate != null) {
            end = new Date(endDate);
            end.setHours(0, 0, 0, 0);
        }

        if (!isEnable) {
            if (end != null && start != null && start >= end) {
                if (Platform.OS === 'ios') {
                    setTimeout(() => {
                        Alert.alert("", translate("med_holiday.end_date_must_be_greater"));
                    }, 1000);
                } else
                    Alert.alert("", translate("med_holiday.end_date_must_be_greater"));
                return false;
            } else {
                return true;
            }
        } else {
            if (start == null && end != null) {
                if (Platform.OS === 'ios') {
                    setTimeout(() => {
                        Alert.alert("", translate("med_holiday.start_date_required"));
                    }, 1000);

                } else
                    Alert.alert("", translate("med_holiday.start_date_required"));
                return false;
            } else if (end == null && start != null) {
                if (Platform.OS === 'ios') {
                    setTimeout(() => {
                        Alert.alert("", translate("med_holiday.end_date_required"));
                    }, 1000);
                } else
                    Alert.alert("", translate("med_holiday.end_date_required"));
                return false;
            } else {
                return true;
            }
        }
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount = async () => {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});

        this.props.OnFetchUserMedicationList();
    };

    onSelectAll = (value, placeholder, med) => {
        // console.log("MED", med.desc)
    };
    onCheckboxValueChange = (value, placeholder, med) => {
        // console.log("Value", JSON.stringify(med));
        // Alert.alert("", JSON.stringify(value));
        if (value) {
            // this.setState({temArray:this.state.temArray.concat(med.desc)},()=>{
            //
            //     console.log("MED ARRAT",this.state.temArray)
            // })
            this.setState(
                {
                    [`${placeholder}`]: value,
                    updatedMedList: [
                        ...this.state.updatedMedList,
                        {
                            code: med.code,
                            customMed: med.customMed,
                            desc: med.desc,
                            freq: med.freq,
                            strength: med.strength,
                            tolerance: med.tolerance,
                            idx: med.idx,
                            dosetime1: med.dosetime1,
                            dosetime2: med.dosetime2,
                            dosetime3: med.dosetime3,
                            dosetime4: med.dosetime4,
                            image: med.image,
                            holiday: {flag: false, startDate: null, endDate: null}
                        }
                    ]
                },
                () => {
                    console.log("Update List", this.state.updatedMedList);
                }
            );
        } else {
            this.setState({
                [`${placeholder}`]: undefined,
                updatedMedList: this.state.updatedMedList.filter(d => d.idx !== med.idx)
            });
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.userMedicationListData) {
            this.setState({
                meds: nextProps.userMedicationListData.meds
                // updatedMedList: nextProps.userMedicationListData.meds
            });
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <HeaderView name={translate("med_holiday.med_holiday")} navigation={this.props.navigation}/>
                <ScrollView>
                    <View style={{marginLeft: 20, marginRight: 20, marginTop: 10}}>
                        <Card style={{margin: 20}}>
                            <Text style={styles.selectMedsStyle}>{translate("med_holiday.select_meds")}</Text>

                            {this.props.user_med_list_loading && (
                                <View
                                    style={{
                                        justifyContent: "center",
                                        alignItems: "center",
                                        flexDirection: "row"
                                    }}
                                >
                                    <ActivityIndicator size="small" animating/>
                                    <Text style={{marginLeft: 15, fontWeight: "600", color: colors.blackColor}}>
                                        {translate("med_holiday.fetching_user_meds")}
                                    </Text>
                                </View>
                            )}

                            {!this.props.user_med_list_loading &&
                            this.state.meds.length === 0 && (
                                <View
                                    style={{
                                        alignSelf: "center",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        flexDirection: "column",
                                        flex: 0,
                                        height: 300
                                    }}
                                >
                                    <Image
                                        source={require("../images/pill_blue.png")}
                                        style={{width: 60, height: 60}}
                                    />
                                    <Text style={{marginLeft: 15, fontWeight: "600", color: colors.blackColor}}>
                                        {translate("manage_meds.no_medicine_found")}
                                    </Text>
                                </View>
                            )}

                            {this.state.meds.map((med, index) => {
                                return (
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            marginTop: 10,
                                            paddingLeft: 20
                                        }}
                                    >
                                        <CheckBox
                                            checked={this.state[`${med.idx}`]}
                                            onPress={() => {
                                                this.setState({[med.idx]: !this.state[`${med.idx}`]});
                                                this.onCheckboxValueChange(
                                                    !this.state[`${med.idx}`],
                                                    `${med.code}/${med.strength}`,
                                                    med
                                                );
                                            }}
                                        />
                                        <Text style={{
                                            marginLeft: 12,
                                            marginTop: 2,
                                            color: colors.blackColor
                                        }}> {med.desc}</Text>
                                    </View>
                                );
                            })}

                            <View
                                style={{
                                    flexDirection: "row",
                                    alignContent: "center",
                                    justifyContent: "center"
                                }}
                            >
                                <TouchableOpacity
                                    style={styles.blueButtonStyle}
                                    onPress={() => {
                                        // console.log("Select ALL : " + this.state.meds.length);
                                        this.state.meds.map(async med => {
                                            await this.setState({[med.idx]: true});
                                            await this.onCheckboxValueChange(
                                                true,
                                                `${med.code}/${med.strength}`,
                                                med
                                            );
                                        });
                                    }}
                                >
                                    <Text style={[styles.whiteColorStyle, styles.paddingStyle]}>
                                        {translate("med_holiday.select_all")}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.greyButtonStyle}
                                    onPress={() => {
                                        this.state.meds.map(async (med, index) => {
                                            await this.setState({[med.idx]: false});
                                            await this.onCheckboxValueChange(
                                                false,
                                                `${med.code}/${med.strength}`,
                                                med
                                            );
                                        });
                                    }}
                                >
                                    <Text style={[styles.paddingStyle, styles.blackColorStyle]}>
                                        {translate("med_holiday.clear_all")}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </Card>

                        <View
                            style={{
                                flexDirection: "column",
                                marginTop: 40,
                                marginBottom: 20
                            }}
                        >
                            <View style={{flexDirection: "row"}}>
                                <Text style={{
                                    flex: 0.5,
                                    marginLeft: 5,
                                    color: colors.blackColor
                                }}>{translate("med_holiday.start_date")}</Text>
                                <Text style={{
                                    flex: 0.5,
                                    marginLeft: 5,
                                    color: colors.blackColor
                                }}>{translate("med_holiday.end_date")}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: "row"}}>
                                <DatePicker
                                    style={{flex: 0.5}}
                                    date={this.state.startDate}
                                    minDate={moment().format("YYYY-MM-DD")}
                                    mode="date"
                                    placeholder={translate("med_holiday.select_date")}
                                    format="YYYY-MM-DD"
                                    confirmBtnText={translate("med_holiday.confirm")}
                                    cancelBtnText={translate("manage_meds.cancel")}
                                    iconSource={require("../images/calendar.png")}
                                    onDateChange={date => {
                                        if (this.checkDateValidations(date, this.state.endDate, false)) {
                                            this.setState({startDate: date});
                                        }
                                    }}
                                />
                                <DatePicker
                                    style={{flex: 0.5}}
                                    date={this.state.endDate}
                                    minDate={moment().format("YYYY-MM-DD")}
                                    mode="date"
                                    placeholder={translate("med_holiday.select_date")}
                                    format="YYYY-MM-DD"
                                    confirmBtnText={translate("med_holiday.confirm")}
                                    cancelBtnText={translate("manage_meds.cancel")}
                                    iconSource={require("../images/calendar.png")}
                                    onDateChange={date => {
                                        if (this.checkDateValidations(
                                            this.state.startDate,
                                            date,
                                            false)) {
                                            this.setState({endDate: date});
                                        }
                                    }}
                                />
                                {/*<TouchableWithoutFeedback>
                                    <View style={{
                                        flex: 0.5,
                                        height: 50,
                                        borderRadius: 1,
                                        borderWidth: 1,
                                        borderColor: colors.greyColor,
                                        marginRight: 5,
                                        flexDirection: 'row'
                                    }}>
                                        <Text style={{flex: 1, alignSelf: 'center', paddingLeft: 5}}>
                                        </Text>
                                        <TouchableWithoutFeedback onPress={() => {
                                            console.log("Start Date Clicked.!");

                                        }}>
                                            <Image source={require('../images/calendar.png')}
                                                   style={{
                                                       height: 30,
                                                       width: 30,
                                                       marginTop: 10,
                                                       marginRight: 5
                                                   }}/>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </TouchableWithoutFeedback>

                                <TouchableWithoutFeedback>
                                    <View style={{
                                        flex: 0.5,
                                        height: 50,
                                        borderRadius: 1,
                                        borderWidth: 1,
                                        borderColor: colors.greyColor,
                                        flexDirection: 'row'
                                    }}>
                                        <Text style={{flex: 1, alignSelf: 'center', paddingLeft: 5}}>

                                        </Text>
                                        <TouchableWithoutFeedback onPress={() => {
                                            console.log("End Date Clicked.!");
                                            <DatePicker/>
                                        }}>
                                            <Image source={require('../images/calendar.png')}
                                                   style={{
                                                       height: 30,
                                                       width: 30,
                                                       marginTop: 10,
                                                       marginRight: 5
                                                   }}/>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </TouchableWithoutFeedback>*/}
                            </View>
                        </View>

                        {this.state.fetchingMeds ? (
                            <ActivityIndicator
                                style={{marginTop: 30}}
                                size="large"
                                animating
                            />
                        ) : (
                            <TouchableOpacity
                                style={styles.enableHolidayButton}
                                onPress={() => {
                                    // console.log("Med LIST for UPDATe : " + JSON.stringify(this.state.updatedMedList));
                                    if (this.state.updatedMedList.length > 0) {
                                        if (
                                            this.checkDateValidations(
                                                this.state.startDate,
                                                this.state.endDate,
                                                true
                                            )
                                        ) {
                                            if (
                                                this.state.startDate != null &&
                                                this.state.endDate != null
                                            ) {
                                                this.state.updatedMedList.map((med, index) => {
                                                    med.holiday.flag = true;
                                                    med.holiday.startDate = this.state.startDate;
                                                    med.holiday.endDate = this.state.endDate;
                                                });
                                            }
                                            this.updateMedicationList()
                                                .then(res => {
                                                    this.setState({
                                                        fetchingMeds: false
                                                    });
                                                    if (res.data.ERROR === 0) {
                                                        Alert.alert("", res.data.MESSAGE);
                                                    } else {
                                                        Alert.alert("Error", res.data.MESSAGE);
                                                    }
                                                })
                                                .catch(error => {
                                                    this.setState({fetchingMeds: false});
                                                });
                                        }
                                    } else {
                                        Alert.alert("", translate("med_holiday.select_meds_enable_holidays"));
                                    }
                                }}
                            >
                                <Text
                                    style={{
                                        color: colors.whiteColor,
                                        fontSize: 16,
                                        textAlign: "center",
                                        padding: 10,
                                        alignSelf: "center",
                                        alignItems: "center"
                                    }}
                                >
                                    {/*{" "}*/}
                                    {translate("med_holiday.enable_holidays")}
                                </Text>
                            </TouchableOpacity>
                        )}
                    </View>
                </ScrollView>
                <FooterView isSummary={true} navigation={this.props.navigation}/>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        userMedicationListData:
        state.userMedicationListReducer.UsermedicationListData,
        user_med_list_loading: state.userMedicationListReducer.loading,
        user_med_list_error: state.userMedicationListReducer.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchUserMedicationList: () =>
            dispatch(UserMedicationListActions.fetchUserMedicationList())
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MedHolidayScreen);

const styles = StyleSheet.create({
    selectMedsStyle: {
        backgroundColor: colors.blueButtonColor,
        paddingLeft: 30,
        paddingTop: 10,
        paddingBottom: 10,
        color: colors.whiteColor,
        fontSize: 18
    },
    blueButtonStyle: {
        backgroundColor: colors.blueButtonColor,
        borderRadius: 30,
        color: colors.whiteColor,
        marginBottom: 20,
        marginTop: 20
    },
    greyButtonStyle: {
        backgroundColor: colors.greyColor,
        marginLeft: 10,
        borderRadius: 30,
        color: colors.whiteColor,
        marginBottom: 20,
        marginTop: 20
    },
    paddingStyle: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 30,
        paddingRight: 30
    },
    whiteColorStyle: {
        color: colors.whiteColor
    },
    blackColorStyle: {
        color: colors.blackColor
    },
    enableHolidayButton: {
        backgroundColor: colors.blueButtonColor,
        borderRadius: 30,
        marginBottom: 20,
        marginTop: 30,
        width: 180,
        alignSelf: "center"
    }
});
