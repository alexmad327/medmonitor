import React, {Component} from "react";
import {StyleSheet, TouchableOpacity, View} from "react-native";
import {FooterView} from "../container/FooterView";
import {colors, headerTitleStyle} from "../constants/Constants";
import {Image, Text} from "react-native-elements";
import Icon from "react-native-vector-icons/AntDesign";
import {HeaderView} from "../container/HeaderView";
import I18n, {translate} from "../locale/i18n";

export class MoreScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {logout: false}
    }

    static navigationOptions = {
        header: null,
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <HeaderView name={translate("footer.more")} navigation={this.props.navigation}/>
                <View style={{flex: 1}}>
                    <View style={{
                        flexDirection: 'row',
                        padding: 10,
                        borderWidth: 1,
                        borderColor: colors.greyColor,
                        margin: 20
                    }}>
                        <Text style={{flex: 1, fontSize: 16, color: colors.blackColor}}
                              onPress={() => {
                                  navigate("SocialMediaScreen")
                              }}>
                            {translate("more.message_board")}
                        </Text>
                        <Icon name="right" size={30}/>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        padding: 10,
                        borderWidth: 1,
                        borderColor: colors.greyColor,
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 10
                    }}>
                        <Text style={{flex: 1, fontSize: 16, color: colors.blackColor}}
                              onPress={() => {
                                  navigate("GraphScreen")
                              }}>
                            {translate("more.biometric_graph")}
                        </Text>
                        <Icon name="right" size={30}/>
                    </View>

                    <View style={styles.viewStyle}>
                        <Text style={{flex: 1, fontSize: 16, color: colors.blackColor}}
                              onPress={() => {
                                  navigate("PillBoxSettings")
                              }}>
                            {translate("more.pillbox_medication")}
                        </Text>
                        <Icon name="right" size={30}/>
                    </View>

                    <View style={styles.viewStyle}>
                        <Text style={{flex: 1, fontSize: 16, color: colors.blackColor}}
                              onPress={() => {
                                  navigate("SettingScreen")
                              }}>
                            {translate("more.setting")}
                        </Text>
                        <Icon name="right" size={30}/>
                    </View>
                </View>
                <FooterView isSummary={true}
                            navigation={this.props.navigation}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        flexDirection: 'row',
        padding: 10,
        borderWidth: 1,
        borderColor: colors.greyColor,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10
    }
});