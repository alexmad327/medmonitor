import React, {Component} from "react";
import {
    Alert,
    AppState,
    AsyncStorage,
    FlatList,
    Image,
    Platform,
    PushNotificationIOS,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {API_BASE_URL, colors} from "../constants/Constants";
import {Text} from "react-native-elements";
import {Card, CardItem} from "native-base";
import Icon from "react-native-vector-icons/Feather";
import axios from "axios";
import * as UserMedicationListActions from "../store/actions/UserMedicationListAction";
import {connect} from "react-redux";
import PushNotification from "react-native-push-notification";
import moment from "moment";
import {translate} from "../locale/i18n";

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appState: AppState.currentState,
            data: [
                {
                    id: 1,
                    image: require("../images/dialy_activity.png"),
                    name: translate("home.daily_activity")
                },
                {
                    id: 2,
                    image: require("../images/message_board.png"),
                    name: translate("more.message_board")
                },
                {
                    id: 3,
                    image: require("../images/medication_calendar.png"),
                    name: translate("home.medication_calendar")
                },
                {
                    id: 4,
                    image: require("../images/graph.png"),
                    name: translate("more.biometric_graph")
                },
                {
                    id: 5,
                    image: require("../images/pill_box.png"),
                    name: translate("home.pillbox")
                },
                {
                    id: 6,
                    image: require("../images/manage_meds.png"),
                    name: translate("home.manage_meds")
                }
            ],
            phone: "",
            otpToken: "",
            freq1: [],
            freq2: [],
            freq3: [],
            freq4: [],
            userMeds: [],
            eight: "08:00",
            one: "13:00",
            six: "18:00",
            ten: "22:00",
            freq1Name: [],
            freq2Name: [],
            freq3Name: [],
            freq4Name: [],
            userName: "",
            agencyName: ""
        };
    }

    async getUserDetails() {
        axios
            .post(
                API_BASE_URL +
                `/getuserinfo.php?version=3.0.0&data={"phone":"${this.state.phone.trim()}","otptoken":"${this.state.otpToken.trim()}"}`,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }
            )
            .then(response => {
                console.log("getUserDetails : " + JSON.stringify(response));
                if (response.data.ERROR === 0) {
                    this.setState({userName: response.data.user_name, agencyName: response.data.agency_name});
                    AsyncStorage.setItem(
                        "username",
                        JSON.stringify(response.data.user_name)
                    );
                    AsyncStorage.setItem(
                        "snoozeMin",
                        JSON.stringify(response.data.snooze_time)
                    );
                    AsyncStorage.setItem(
                        "userPicture",
                        JSON.stringify(response.data.user_picture)
                    );
                    AsyncStorage.setItem(
                        "doctorName",
                        JSON.stringify(response.data.doctor_name)
                    );
                    AsyncStorage.setItem(
                        "agencyName",
                        JSON.stringify(response.data.agency_name)
                    );
                }
            })
            .catch((error) => {
                console.log(error);
                Alert.alert("Error : " + JSON.parse(error));
            });
    }

    async componentDidMount() {
        /*await AsyncStorage.setItem("phone", "8475550002", () => {
            console.log("PHONE set!");
        });
        await AsyncStorage.setItem("otpToken", "5c77c43367af2", () => {
            console.log("OTP Token Set.!");
        });*/

        console.log("componentDidMount");
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        console.log("phone", phone);

        await this.setState({phone: phone, otpToken: otp});

        this.getUserDetails();
        this.props.OnFetchUserMedicationList();
        PushNotification.configure({
            onNotification: notification => {
                console.log("NOTIFICATION: ", notification);
            },
            popInitialNotification: true
        });
    }

    async componentWillReceiveProps(nextProps) {
        await AsyncStorage.getItem("eight", (err, value) => {
            if (value != null) this.setState({eight: value});
            // console.log("componentDidMount : Eight : " + value);
        });
        await AsyncStorage.getItem("one", (err, value) => {
            if (value != null) this.setState({one: value});
            // console.log("componentDidMount : One : " + value);
        });
        await AsyncStorage.getItem("six", (err, value) => {
            if (value != null) this.setState({six: value});
            // console.log("componentDidMount : Six : " + value);
        });
        await AsyncStorage.getItem("ten", (err, value) => {
            if (value != null) this.setState({ten: value});
            // console.log("componentDidMount : Ten : " + value);
        });

        console.log("HOME Screen : " + JSON.stringify(nextProps.userMedicationListData));


        if (nextProps.userMedicationListData !== null && nextProps.userMedicationListData.ERROR === 0) {
            await AsyncStorage.setItem(
                "userMeds",
                JSON.stringify(nextProps.userMedicationListData)
            );
            this.setState({userMeds: nextProps.userMedicationListData.meds});

            var today = new Date();
            var eightDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.eight.split(":")[0],
                this.state.eight.split(":")[1],
                0
            );
            if (eightDate < today)
                eightDate = moment(eightDate).add(1, "days").toDate();

            var oneDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.one.split(":")[0],
                this.state.one.split(":")[1],
                0
            );
            if (oneDate < today)
                oneDate = moment(oneDate).add(1, "days").toDate();

            var sixDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.six.split(":")[0],
                this.state.six.split(":")[1],
                0
            );
            if (sixDate < today)
                sixDate = moment(sixDate).add(1, "days").toDate();

            var tenDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.ten.split(":")[0],
                this.state.ten.split(":")[1],
                0
            );
            if (tenDate < today)
                tenDate = moment(tenDate).add(1, "days").toDate();

            //Alert.alert("", " " + eightDate + " : " + oneDate);

            if (Platform.OS === "ios") {
                PushNotificationIOS.cancelAllLocalNotifications();
            } else PushNotification.cancelAllLocalNotifications();

            if (nextProps.userMedicationListData.meds !== null) {
                await nextProps.userMedicationListData.meds.map(med => {
                    if (med.freq === "1") {
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));
                    } else if (med.freq === "2") {
                        if (!this.state.freq2.includes(med.idx))
                            this.setState(prevState => ({
                                freq2: [...prevState.freq2, med.idx],
                                freq2Name: [...prevState.freq2Name, med.desc]
                            }));
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));
                    } else if (med.freq === "3") {
                        if (!this.state.freq3.includes(med.idx))
                            this.setState(prevState => ({
                                freq3: [...prevState.freq3, med.idx],
                                freq3Name: [...prevState.freq3Name, med.desc]
                            }));
                        if (!this.state.freq2.includes(med.idx))
                            this.setState(prevState => ({
                                freq2: [...prevState.freq2, med.idx],
                                freq2Name: [...prevState.freq2Name, med.desc]
                            }));
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));
                    } else if (med.freq === "4") {
                        if (!this.state.freq4.includes(med.idx))
                            this.setState(prevState => ({
                                freq4: [...prevState.freq4, med.idx],
                                freq4Name: [...prevState.freq4Name, med.desc]
                            }));
                        if (!this.state.freq3.includes(med.idx))
                            this.setState(prevState => ({
                                freq3: [...prevState.freq3, med.idx],
                                freq3Name: [...prevState.freq3Name, med.desc]
                            }));
                        if (!this.state.freq2.includes(med.idx))
                            this.setState(prevState => ({
                                freq2: [...prevState.freq2, med.idx],
                                freq2Name: [...prevState.freq2Name, med.desc]
                            }));
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));
                    }
                });

                if (this.state.freq1Name.length > 0) {
                    if (Platform.OS === "ios") {
                        PushNotificationIOS.scheduleLocalNotification({
                            fireDate: eightDate.toISOString(),
                            alertTitle: translate("notification.notification_title"),
                            alertBody: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                            repeatInterval: "week"
                        });
                    } else {
                        PushNotification.localNotificationSchedule({
                            id: 1,
                            priority: "high",
                            title: translate("notification.notification_title"),
                            message: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                            date: eightDate,
                            repeatType: "week"
                        });
                    }
                }

                if (this.state.freq2Name.length > 0 && oneDate) {
                    if (Platform.OS === "ios") {
                        //Alert.alert("One : ",oneDate);
                        PushNotificationIOS.scheduleLocalNotification({
                            fireDate: oneDate.toISOString(),
                            alertTitle: translate("notification.notification_title"),
                            alertBody: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                            repeatInterval: "week"
                        });
                    } else {
                        PushNotification.localNotificationSchedule({
                            id: 2,
                            priority: "high",
                            title: translate("notification.notification_title"),
                            message: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                            date: oneDate,
                            repeatType: "week"
                        });
                    }
                }

                if (this.state.freq3Name.length > 0 && sixDate) {
                    if (Platform.OS === "ios") {
                        //Alert.alert("Sixx : ",sixDate);
                        PushNotificationIOS.scheduleLocalNotification({
                            fireDate: sixDate.toISOString(),
                            alertTitle: translate("notification.notification_title"),
                            alertBody: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                            repeatInterval: "week"
                        });
                    } else {
                        PushNotification.localNotificationSchedule({
                            id: 3,
                            priority: "high",
                            title: translate("notification.notification_title"),
                            message: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                            date: sixDate,
                            repeatType: "week"
                        });
                    }
                }

                if (this.state.freq4Name.length > 0 && tenDate) {
                    if (Platform.OS === "ios") {
                        //Alert.alert("Ten : ",tenDate);
                        PushNotificationIOS.scheduleLocalNotification({
                            fireDate: tenDate.toISOString(),
                            alertTitle: translate("notification.notification_title"),
                            alertBody: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                            repeatInterval: "week"
                        });
                    } else {
                        PushNotification.localNotificationSchedule({
                            id: 4,
                            priority: "high",
                            title: translate("notification.notification_title"),
                            message: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                            date: tenDate,
                            repeatType: "week"
                        });
                    }
                }
            }
        }
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitleStyle: {
                color: colors.whiteColor,
                alignContent: "center",
                alignSelf: "center",
                textAlign: "center",
                flex: 1
            },
            title: translate("home.med_monitor_home"),
            headerLeft: <Icon size={30}/>,
            headerStyle: {
                backgroundColor: colors.blueButtonColor
            },
            headerRight: (
                <View style={{flexDirection: "row", alignContent: "center"}}>
                    <TouchableOpacity
                        onPress={() => {
                            navigation.navigate("SettingScreen");
                        }}
                        style={{paddingRight: 10}}
                    >
                        <Icon size={30} name="settings" color={colors.whiteColor}/>
                        {/*<Image source={require('../images/information.png')} style={{height: 30, width: 30}}/>*/}
                    </TouchableOpacity>
                </View>
            )
        };
    };

    render() {
        return (
            <View style={{flex: 1, margin: 16, flexDirection: 'column'}}>
                <Text
                    style={{
                        color: colors.blueButtonColor,
                        fontSize: 20,
                        textAlign: "center",
                        marginTop: 8
                    }}
                >
                    {`${translate("daily_activity.wlcm")} ${this.state.userName}`}
                    {/*Welcome {this.state.userName}*/}
                </Text>
                <View
                    style={{
                        flexDirection: "row",
                        alignContent: "center",
                        alignSelf: "center",
                        marginBottom: 8
                    }}
                >
                    <Text
                        style={{
                            color: colors.blackColor,
                            fontSize: 16
                        }}
                    >
                        {this.state.agencyName}
                    </Text>
                </View>
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    data={this.state.data}
                    numColumns={2}
                    renderItem={({item}) => (
                        <View style={{height: "50%", width: "50%"}}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    if (item.id === 1) {
                                        this.props.navigation.navigate("DailyActivityScreen", {
                                            userMeds: this.state.userMeds,
                                            freq1: this.state.freq1,
                                            freq2: this.state.freq2,
                                            freq3: this.state.freq3,
                                            freq4: this.state.freq4
                                        });
                                    } else if (item.id === 2) {
                                        this.props.navigation.navigate("SocialMediaScreen");
                                    } else if (item.id === 3) {
                                        this.props.navigation.navigate("CalendarScreen", {
                                            selectedText: "Calendar"
                                        });
                                    } else if (item.id === 4) {
                                        this.props.navigation.navigate("GraphScreen");
                                    } else if (item.id === 5) {
                                        this.props.navigation.navigate("PillBoxSettings");
                                    } else if (item.id === 6) {
                                        this.props.navigation.navigate("ManageMedsScreen", {
                                            freq1: this.state.freq1,
                                            freq2: this.state.freq2,
                                            freq3: this.state.freq3,
                                            freq4: this.state.freq4
                                        });
                                    }
                                }}
                            >
                                <Card>
                                    <CardItem style={{flexDirection: "column"}}>
                                        <Image
                                            style={{height: 100, width: 100, resizeMode: "contain"}}
                                            source={item.image}
                                        />
                                        <Text
                                            numberOfLines={2}
                                            style={{
                                                textAlign: "center",
                                                paddingTop: 8,
                                                paddingBottom: 8,
                                                height: 50,
                                                color: colors.greyColor
                                            }}
                                        >
                                            {item.name}
                                        </Text>
                                    </CardItem>
                                </Card>
                            </TouchableWithoutFeedback>
                        </View>
                    )}
                />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        userMedicationListData:
        state.userMedicationListReducer.UsermedicationListData,
        user_med_list_loading: state.userMedicationListReducer.loading,
        user_med_list_error: state.userMedicationListReducer.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchUserMedicationList: () =>
            dispatch(UserMedicationListActions.fetchUserMedicationList())
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeScreen);
