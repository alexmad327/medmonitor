import React, {Component} from "react";
import {
    FlatList,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    ActivityIndicator,
    Alert,
    AsyncStorage,
    AppState,
    Platform,
    PushNotificationIOS
} from "react-native";
import {API_BASE_URL, colors} from "../constants/Constants";
import BackIcon from "react-native-vector-icons/Ionicons";
import CrossIcon from "react-native-vector-icons/Feather";
import EntypoIcon from "react-native-vector-icons/Entypo";
import {
    Collapse,
    CollapseHeader,
    CollapseBody
} from "accordion-collapse-react-native";
import {
    Card,
    CardItem,
    Input,
    Item,
    ListItem,
    Switch,
    Picker,
    Separator
} from "native-base";
import {Image} from "react-native-elements";
import {FooterView} from "../container/FooterView";
import Dialog, {DialogContent} from "react-native-popup-dialog";
import {HeaderView} from "../container/HeaderView";
import * as medicationListActions from "../store/actions/MedicationListAction";
import * as UserMedicationListActions from "../store/actions/UserMedicationListAction";
import {connect} from "react-redux";
import axios from "axios";
import PushNotification from "react-native-push-notification";
import moment from "moment";
import I18n, {translate} from "../locale/i18n";
import {NavigationEvents} from "react-navigation";
import * as DosageScheduleAction from "../store/actions/DosageScheduleAction";
import * as _ from "lodash";

class ManageMedsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appState: AppState.currentState,
            isExpand: true,
            addMed: false,
            medList: false,
            frequency: 1,
            updatedMedList: [],
            reservedMeds: [],
            meds: [],
            customMedsName: "",
            customMedsStrength: "",
            fetchingMeds: false,
            eight: "08:00",
            one: "13:00",
            six: "18:00",
            ten: "22:00",
            freq1: [],
            freq2: [],
            freq3: [],
            freq4: [],
            freq1Name: [],
            freq2Name: [],
            freq3Name: [],
            freq4Name: [],
            phone: "",
            otpToken: "",
            freqArray: [],
            tolerance1: "",
            tolerance2: "",
            tolerance3: "",
            tolerance4: "",
            apiCall: 1
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount = async () => {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});


        this.props.OnFetchMedicationList({});
        this.setState({apiCall: 1});
        this.props.OnFetchDosageSchedules();

        if (this.props.navigation.state.params)
            this.setState({
                freq1: this.props.navigation.state.params.freq1,
                freq2: this.props.navigation.state.params.freq2,
                freq3: this.props.navigation.state.params.freq3,
                freq4: this.props.navigation.state.params.freq4
            });

        /*AsyncStorage.getItem("eight", (err, value) => {
            if (value != null) this.setState({eight: value});
            console.log("componentDidMount : Eight : " + value);
        });
        AsyncStorage.getItem("one", (err, value) => {
            if (value != null) this.setState({one: value});
            console.log("componentDidMount : One : " + value);
        });
        AsyncStorage.getItem("six", (err, value) => {
            if (value != null) this.setState({six: value});
            console.log("componentDidMount : Six : " + value);
        });
        AsyncStorage.getItem("ten", (err, value) => {
            if (value != null) this.setState({ten: value});
            console.log("componentDidMount : Ten : " + value);
        });*/
    };

    onSwitchValueChange = (value, placeholder, dose, item) => {
        console.log("Value", value);
        if (value) {
            this.setState({
                [`${placeholder}`]: value,
                updatedMedList: [
                    ...this.state.updatedMedList,
                    {
                        code: item.code,
                        customMed: false,
                        desc: item.desc,
                        freq: dose.freq,
                        strength: dose.strength,
                        tolerance: dose.tolerance,
                        idx: dose.idx,
                        dosetime1: null,
                        dosetime2: null,
                        dosetime3: null,
                        dosetime4: null,
                        image: dose.image
                    }
                ]
            });
        } else {
            this.setState({
                [`${placeholder}`]: undefined,
                updatedMedList: this.state.updatedMedList.filter(
                    d => d.idx !== dose.idx
                )
            });
        }
    };
    deleteMedicine = med => {
        let updatedMeds = this.state.meds.filter(d => d.idx !== med.idx);

        Alert.alert(
            `${translate("manage_meds.delete_medicine")}`,
            `${translate("manage_meds.are_you_sure")}`,
            [
                {
                    text: `${translate("manage_meds.yes")}`,
                    onPress: () => {
                        this.setState({[`deleting${med.idx}`]: true});
                        this.DeleteMedicationList(updatedMeds)
                            .then(res => {
                                this.setState(
                                    {
                                        addMed: false,
                                        fetchingMeds: false,
                                        customMedsName: "",
                                        customMedsStrength: "",
                                        [`deleting${med.idx}`]: false,
                                        [`${med.code}/${med.strength}`]: undefined,
                                        meds: res.data.meds,
                                        updatedMedList: res.data.meds,
                                        freq1: [],
                                        freq2: [],
                                        freq3: [],
                                        freq4: [],
                                        freq1Name: [],
                                        freq2Name: [],
                                        freq3Name: [],
                                        freq4Name: []
                                    },
                                    () => {
                                        Alert.alert(
                                            `${translate("manage_meds.deleted")}`,
                                            `${translate("manage_meds.medicine_has_been_deleted")}`
                                        );
                                    }
                                );

                                var today = new Date();
                                var eightDate = new Date(
                                    today.getFullYear(),
                                    today.getMonth(),
                                    today.getDate(),
                                    this.state.eight.split(":")[0],
                                    this.state.eight.split(":")[1],
                                    0
                                );
                                if (eightDate < today)
                                    eightDate = moment(eightDate)
                                        .add(1, "days")
                                        .toDate();
                                var oneDate = new Date(
                                    today.getFullYear(),
                                    today.getMonth(),
                                    today.getDate(),
                                    this.state.one.split(":")[0],
                                    this.state.one.split(":")[1],
                                    0
                                );
                                if (oneDate < today)
                                    oneDate = moment(oneDate)
                                        .add(1, "days")
                                        .toDate();
                                var sixDate = new Date(
                                    today.getFullYear(),
                                    today.getMonth(),
                                    today.getDate(),
                                    this.state.six.split(":")[0],
                                    this.state.six.split(":")[1],
                                    0
                                );
                                if (sixDate < today)
                                    sixDate = moment(sixDate)
                                        .add(1, "days")
                                        .toDate();
                                var tenDate = new Date(
                                    today.getFullYear(),
                                    today.getMonth(),
                                    today.getDate(),
                                    this.state.ten.split(":")[0],
                                    this.state.ten.split(":")[1],
                                    0
                                );
                                if (tenDate < today)
                                    tenDate = moment(tenDate)
                                        .add(1, "days")
                                        .toDate();

                                if (Platform.OS === "ios") {
                                    PushNotificationIOS.cancelAllLocalNotifications();
                                } else PushNotification.cancelAllLocalNotifications();

                                res.data.meds.map(async med => {

                                    await this.setState({freqArray: []});
                                    await res.data.meds.map(med => {
                                        if (this.state.freqArray && !this.state.freqArray.includes(med.freq)) {
                                            this.setState(prevState => ({
                                                freqArray: [...prevState.freqArray, med.freq]
                                            }))
                                        }
                                    });

                                    await this.setState({
                                        freqArray: _.filter(this.state.freqArray, value => {
                                            return value !== "1";
                                        })
                                    });

                                    /*var i = this.state.freqArray.indexOf("1");

                                    if (i > -1) {
                                        this.setState({freqArray: this.state.freqArray.splice(i, 1)});
                                    }*/

                                    this.setState({
                                        freqArray: this.state.freqArray.sort((a, b) => {
                                            return a - b;
                                        })
                                    });

                                    let value = "";

                                    await this.state.freqArray.map(arr => {
                                        if (value !== "") {
                                            value = value + "+" + arr + "D";
                                        } else
                                            value = arr + "D";
                                    });

                                    if (this.state.dosageSchedules != null) {
                                        let array = this.state.dosageSchedules[value];
                                        console.log(" ARRAY IS : " + array);

                                        array.forEach((value, index) => {
                                            if (index === 0) {
                                                this.setState({eight: value})
                                            } else if (index === 1) {
                                                this.setState({one: value})
                                            } else if (index === 2) {
                                                this.setState({six: value})
                                            } else if (index === 3) {
                                                this.setState({ten: value})
                                            }
                                        })
                                    }

                                    if (med.freq === "1") {
                                        if (!this.state.freq1.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq1: [...prevState.freq1, med.idx],
                                                freq1Name: [...prevState.freq1Name, med.desc]
                                            }));
                                    } else if (med.freq === "2") {
                                        if (!this.state.freq2.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq2: [...prevState.freq2, med.idx],
                                                freq2Name: [...prevState.freq2Name, med.desc]
                                            }));
                                        if (!this.state.freq1.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq1: [...prevState.freq1, med.idx],
                                                freq1Name: [...prevState.freq1Name, med.desc]
                                            }));
                                    } else if (med.freq === "3") {
                                        if (!this.state.freq3.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq3: [...prevState.freq3, med.idx],
                                                freq3Name: [...prevState.freq3Name, med.desc]
                                            }));
                                        if (!this.state.freq2.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq2: [...prevState.freq2, med.idx],
                                                freq2Name: [...prevState.freq2Name, med.desc]
                                            }));
                                        if (!this.state.freq1.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq1: [...prevState.freq1, med.idx],
                                                freq1Name: [...prevState.freq1Name, med.desc]
                                            }));
                                    } else if (med.freq === "4") {
                                        if (!this.state.freq4.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq4: [...prevState.freq4, med.idx],
                                                freq4Name: [...prevState.freq4Name, med.desc]
                                            }));
                                        if (!this.state.freq3.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq3: [...prevState.freq3, med.idx],
                                                freq3Name: [...prevState.freq3Name, med.desc]
                                            }));
                                        if (!this.state.freq2.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq2: [...prevState.freq2, med.idx],
                                                freq2Name: [...prevState.freq2Name, med.desc]
                                            }));
                                        if (!this.state.freq1.includes(med.idx))
                                            this.setState(prevState => ({
                                                freq1: [...prevState.freq1, med.idx],
                                                freq1Name: [...prevState.freq1Name, med.desc]
                                            }));
                                    }
                                });

                                if (this.state.freq1Name && this.state.freq1Name.length > 0) {
                                    if (Platform.OS === "ios") {
                                        PushNotificationIOS.scheduleLocalNotification({
                                            fireDate: eightDate.toISOString(),
                                            alertTitle: translate("notification.notification_title"),
                                            alertBody: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                                            repeatInterval: "week"
                                        });
                                    } else {
                                        PushNotification.localNotificationSchedule({
                                            id: 1,
                                            priority: "high",
                                            title: translate("notification.notification_title"),
                                            message: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                                            date: eightDate,
                                            repeatType: "week"
                                        });
                                    }
                                }

                                if (this.state.freq2Name && this.state.freq2Name.length > 0 && oneDate) {
                                    if (Platform.OS === "ios") {
                                        //Alert.alert("One : ",oneDate);
                                        PushNotificationIOS.scheduleLocalNotification({
                                            fireDate: oneDate.toISOString(),
                                            alertTitle: translate("notification.notification_title"),
                                            alertBody: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                                            repeatInterval: "week"
                                        });
                                    } else {
                                        PushNotification.localNotificationSchedule({
                                            id: 2,
                                            priority: "high",
                                            title: translate("notification.notification_title"),
                                            message: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                                            date: oneDate,
                                            repeatType: "week"
                                        });
                                    }
                                }

                                if (this.state.freq3Name && this.state.freq3Name.length > 0 && sixDate) {
                                    if (Platform.OS === "ios") {
                                        //Alert.alert("Sixx : ",sixDate);
                                        PushNotificationIOS.scheduleLocalNotification({
                                            fireDate: sixDate.toISOString(),
                                            alertTitle: translate("notification.notification_title"),
                                            alertBody: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                                            repeatInterval: "week"
                                        });
                                    } else {
                                        PushNotification.localNotificationSchedule({
                                            id: 3,
                                            priority: "high",
                                            title: translate("notification.notification_title"),
                                            message: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                                            date: sixDate,
                                            repeatType: "week"
                                        });
                                    }
                                }

                                if (this.state.freq4Name && this.state.freq4Name.length > 0 && tenDate) {
                                    if (Platform.OS === "ios") {
                                        //Alert.alert("Ten : ",tenDate);
                                        PushNotificationIOS.scheduleLocalNotification({
                                            fireDate: tenDate.toISOString(),
                                            alertTitle: translate("notification.notification_title"),
                                            alertBody: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                                            repeatInterval: "week"
                                        });
                                    } else {
                                        PushNotification.localNotificationSchedule({
                                            id: 4,
                                            priority: "high",
                                            title: translate("notification.notification_title"),
                                            message: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                                            date: tenDate,
                                            repeatType: "week"
                                        });
                                    }
                                }
                                // this.props.OnFetchMedicationList({});
                                // this.props.OnFetchUserMedicationList();
                            })
                            .catch(error => {
                                this.setState({
                                    fetchingMeds: false,
                                    [`deleting${med.idx}`]: false
                                });
                            });
                    }
                },
                {
                    text: translate("manage_meds.cancel"),
                    onPress: () => {
                        this.setState({[`deleting${med.idx}`]: false});
                        console.log("Cancel Pressed");
                    },
                    style: "cancel"
                }
            ],
            {cancelable: true}
        );
    };
    _UpdateMedList = () => {
        if (this.state.customMedsName === "") {
            Alert.alert(translate("manage_meds.error"), translate("manage_meds.enter_medicine_nbame"));
        } else if (this.state.customMedsStrength === "") {
            Alert.alert(translate("manage_meds.error"), translate("manage_meds.enter_mmedicine_strength"));
        } else {
            this.setState({
                updatedMedList: this.state.updatedMedList.push({
                    code: this.state.customMedsName,
                    customMed: true,
                    desc: this.state.customMedsName,
                    freq: this.state.frequency,
                    strength: this.state.customMedsStrength,
                    tolerance: 3600,
                    idx: 0,
                    image: null
                }),
                [`${this.state.customMedsName}/${this.state.customMedsStrength}`]: true
            });

            this.updateMedicationList()
                .then(res => {
                    this.setState({
                        addMed: false,
                        fetchingMeds: false,
                        customMedsName: "",
                        customMedsStrength: ""
                        // meds: [res.data.meds]
                    });
                    // this.props.OnFetchMedicationList({});
                    this.setState({apiCall: 2});
                    this.props.OnFetchUserMedicationList();
                })
                .catch(error => {
                    this.setState({fetchingMeds: false});
                });
        }
    };

    updateMedicationList() {
        this.setState({fetchingMeds: true});
        return axios.post(
            API_BASE_URL +
            `/updatemedication.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","meds":${JSON.stringify(
                this.state.updatedMedList
            )}}`,
            {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }
        );
    }

    DeleteMedicationList(updatedMeds) {
        this.setState({fetchingMeds: true});
        return axios.post(
            API_BASE_URL +
            `/updatemedication.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","meds":${JSON.stringify(
                updatedMeds
            )}}`,
            {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }
        );
    }

    _UpdateMedListNotCustom = () => {
        this.setState({fetchingMeds: true}, () => {
            this.updateMedicationList()
                .then(res => {
                    this.setState({
                        addMed: false,
                        fetchingMeds: false,
                        medList: false,
                        customMedsName: "",
                        customMedsStrength: "",
                        meds: res.data.meds
                    });

                    this.setState({
                        freq1: [],
                        freq2: [],
                        freq3: [],
                        freq4: [],
                        freq1Name: [],
                        freq2Name: [],
                        freq3Name: [],
                        freq4name: []
                    });

                    var today = new Date();
                    var eightDate = new Date(
                        today.getFullYear(),
                        today.getMonth(),
                        today.getDate(),
                        this.state.eight.split(":")[0],
                        this.state.eight.split(":")[1],
                        0
                    );
                    if (eightDate < today)
                        eightDate = moment(eightDate)
                            .add(1, "days")
                            .toDate();
                    var oneDate = new Date(
                        today.getFullYear(),
                        today.getMonth(),
                        today.getDate(),
                        this.state.one.split(":")[0],
                        this.state.one.split(":")[1],
                        0
                    );
                    if (oneDate < today)
                        oneDate = moment(oneDate)
                            .add(1, "days")
                            .toDate();
                    var sixDate = new Date(
                        today.getFullYear(),
                        today.getMonth(),
                        today.getDate(),
                        this.state.six.split(":")[0],
                        this.state.six.split(":")[1],
                        0
                    );
                    if (sixDate < today)
                        sixDate = moment(sixDate)
                            .add(1, "days")
                            .toDate();
                    var tenDate = new Date(
                        today.getFullYear(),
                        today.getMonth(),
                        today.getDate(),
                        this.state.ten.split(":")[0],
                        this.state.ten.split(":")[1],
                        0
                    );
                    if (tenDate < today)
                        tenDate = moment(tenDate)
                            .add(1, "days")
                            .toDate();

                    if (Platform.OS === "ios") {
                        PushNotificationIOS.cancelAllLocalNotifications();
                    } else PushNotification.cancelAllLocalNotifications();

                    res.data.meds.map(async med => {
                        await this.setState({freqArray: []});
                        await res.data.meds.map(med => {
                            if (this.state.freqArray && !this.state.freqArray.includes(med.freq)) {
                                this.setState(prevState => ({
                                    freqArray: [...prevState.freqArray, med.freq]
                                }))
                            }
                        });

                        await this.setState({
                            freqArray: _.filter(this.state.freqArray, value => {
                                return value !== "1";
                            })
                        });

                        /*var i = this.state.freqArray.indexOf("1");

                        if (i > -1) {
                            this.setState({freqArray: this.state.freqArray.splice(i, 1)});
                        }*/

                        this.setState({
                            freqArray: this.state.freqArray.sort((a, b) => {
                                return a - b;
                            })
                        });

                        let value = "";

                        await this.state.freqArray.map(arr => {
                            if (value !== "") {
                                value = value + "+" + arr + "D";
                            } else
                                value = arr + "D";
                        });

                        if (this.state.dosageSchedules) {
                            let array = this.state.dosageSchedules[value];
                            console.log(" ARRAY IS : " + array);

                            array.forEach((value, index) => {
                                if (index === 0) {
                                    this.setState({eight: value})
                                } else if (index === 1) {
                                    this.setState({one: value})
                                } else if (index === 2) {
                                    this.setState({six: value})
                                } else if (index === 3) {
                                    this.setState({ten: value})
                                }
                            })
                        }

                        if (med.freq === "1") {
                            if (!this.state.freq1.includes(med.idx))
                                this.setState(prevState => ({
                                    freq1: [...prevState.freq1, med.idx],
                                    freq1Name: [...prevState.freq1Name, med.desc]
                                }));
                        } else if (med.freq === "2") {
                            if (!this.state.freq2.includes(med.idx))
                                this.setState(prevState => ({
                                    freq2: [...prevState.freq2, med.idx],
                                    freq2Name: [...prevState.freq2Name, med.desc]
                                }));
                            if (!this.state.freq1.includes(med.idx))
                                this.setState(prevState => ({
                                    freq1: [...prevState.freq1, med.idx],
                                    freq1Name: [...prevState.freq1Name, med.desc]
                                }));
                        } else if (med.freq === "3") {
                            if (!this.state.freq3.includes(med.idx))
                                this.setState(prevState => ({
                                    freq3: [...prevState.freq3, med.idx],
                                    freq3Name: [...prevState.freq3Name, med.desc]
                                }));
                            if (!this.state.freq2.includes(med.idx))
                                this.setState(prevState => ({
                                    freq2: [...prevState.freq2, med.idx],
                                    freq2Name: [...prevState.freq2Name, med.desc]
                                }));
                            if (!this.state.freq1.includes(med.idx))
                                this.setState(prevState => ({
                                    freq1: [...prevState.freq1, med.idx],
                                    freq1Name: [...prevState.freq1Name, med.desc]
                                }));
                        } else if (med.freq === "4") {
                            if (!this.state.freq4.includes(med.idx))
                                this.setState(prevState => ({
                                    freq4: [...prevState.freq4, med.idx],
                                    freq4Name: [...prevState.freq4Name, med.desc]
                                }));
                            if (!this.state.freq3.includes(med.idx))
                                this.setState(prevState => ({
                                    freq3: [...prevState.freq3, med.idx],
                                    freq3Name: [...prevState.freq3Name, med.desc]
                                }));
                            if (!this.state.freq2.includes(med.idx))
                                this.setState(prevState => ({
                                    freq2: [...prevState.freq2, med.idx],
                                    freq2Name: [...prevState.freq2Name, med.desc]
                                }));
                            if (!this.state.freq1.includes(med.idx))
                                this.setState(prevState => ({
                                    freq1: [...prevState.freq1, med.idx],
                                    freq1Name: [...prevState.freq1Name, med.desc]
                                }));
                        }
                    });

                    if (this.state.freq1Name && this.state.freq1Name.length > 0) {
                        if (Platform.OS === "ios") {
                            PushNotificationIOS.scheduleLocalNotification({
                                fireDate: eightDate.toISOString(),
                                alertTitle: translate("notification.notification_title"),
                                alertBody: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                                repeatInterval: "week"
                            });
                        } else {
                            PushNotification.localNotificationSchedule({
                                id: 1,
                                priority: "high",
                                title: translate("notification.notification_title"),
                                message: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                                date: eightDate,
                                repeatType: "week"
                            });
                        }
                    }

                    if (this.state.freq2Name && this.state.freq2Name.length > 0 && oneDate) {
                        if (Platform.OS === "ios") {
                            //Alert.alert("One : ",oneDate);
                            PushNotificationIOS.scheduleLocalNotification({
                                fireDate: oneDate.toISOString(),
                                alertTitle: translate("notification.notification_title"),
                                alertBody: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                                repeatInterval: "week"
                            });
                        } else {
                            PushNotification.localNotificationSchedule({
                                id: 2,
                                priority: "high",
                                title: translate("notification.notification_title"),
                                message: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                                date: oneDate,
                                repeatType: "week"
                            });
                        }
                    }

                    if (this.state.freq3Name && this.state.freq3Name.length > 0 && sixDate) {
                        if (Platform.OS === "ios") {
                            //Alert.alert("Sixx : ",sixDate);
                            PushNotificationIOS.scheduleLocalNotification({
                                fireDate: sixDate.toISOString(),
                                alertTitle: translate("notification.notification_title"),
                                alertBody: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                                repeatInterval: "week"
                            });
                        } else {
                            PushNotification.localNotificationSchedule({
                                id: 3,
                                priority: "high",
                                title: translate("notification.notification_title"),
                                message: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                                date: sixDate,
                                repeatType: "week"
                            });
                        }
                    }

                    if (this.state.freq4Name && this.state.freq4Name.length > 0 && tenDate) {
                        if (Platform.OS === "ios") {
                            //Alert.alert("Ten : ",tenDate);
                            PushNotificationIOS.scheduleLocalNotification({
                                fireDate: tenDate.toISOString(),
                                alertTitle: translate("notification.notification_title"),
                                alertBody: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                                repeatInterval: "week"
                            });
                        } else {
                            PushNotification.localNotificationSchedule({
                                id: 4,
                                priority: "high",
                                title: translate("notification.notification_title"),
                                message: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                                date: tenDate,
                                repeatType: "week"
                            });
                        }
                    }
                })
                .catch(error => {
                    this.setState({fetchingMeds: false});
                });
        });
    };

    async componentWillReceiveProps(nextProps) {
        if (nextProps.userMedicationListData && this.state.apiCall === 2) {
            this.setState({
                meds: nextProps.userMedicationListData.meds,
                updatedMedList: nextProps.userMedicationListData.meds
            });
            console.log(" nextProps.userMedicationListData.meds " + this.state.meds);

            var today = new Date();
            var eightDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.eight.split(":")[0],
                this.state.eight.split(":")[1],
                0
            );
            if (eightDate < today)
                eightDate = moment(eightDate)
                    .add(1, "days")
                    .toDate();
            var oneDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.one.split(":")[0],
                this.state.one.split(":")[1],
                0
            );
            if (oneDate < today)
                oneDate = moment(oneDate)
                    .add(1, "days")
                    .toDate();
            var sixDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.six.split(":")[0],
                this.state.six.split(":")[1],
                0
            );
            if (sixDate < today)
                sixDate = moment(sixDate)
                    .add(1, "days")
                    .toDate();
            var tenDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                this.state.ten.split(":")[0],
                this.state.ten.split(":")[1],
                0
            );
            if (tenDate < today)
                tenDate = moment(tenDate)
                    .add(1, "days")
                    .toDate();

            if (Platform.OS === "ios") {
                PushNotificationIOS.cancelAllLocalNotifications();
            } else PushNotification.cancelAllLocalNotifications();

            this.setState({
                freq1: [],
                freq2: [],
                freq3: [],
                freq4: [],
                freq1Name: [],
                freq2Name: [],
                freq3Name: [],
                freq4Name: []
            });

            nextProps.userMedicationListData.meds &&
            nextProps.userMedicationListData.meds.map(med => {
                if (med.freq === "1") {
                    if (!this.state.freq1.includes(med.idx))
                        this.setState(prevState => ({
                            freq1: [...prevState.freq1, med.idx],
                            freq1Name: [...prevState.freq1Name, med.desc]
                        }));
                } else if (med.freq === "2") {
                    if (!this.state.freq2.includes(med.idx))
                        this.setState(prevState => ({
                            freq2: [...prevState.freq2, med.idx],
                            freq2Name: [...prevState.freq2Name, med.desc]
                        }));
                } else if (med.freq === "3") {
                    if (!this.state.freq3.includes(med.idx))
                        this.setState(prevState => ({
                            freq3: [...prevState.freq3, med.idx],
                            freq3Name: [...prevState.freq3Name, med.desc]
                        }));
                } else if (med.freq === "4") {
                    if (!this.state.freq4.includes(med.idx))
                        this.setState(prevState => ({
                            freq4: [...prevState.freq4, med.idx],
                            freq4Name: [...prevState.freq4Name, med.desc]
                        }));
                }
            });

            if (this.state.freq1Name && this.state.freq1Name.length > 0) {
                if (Platform.OS === "ios") {
                    PushNotificationIOS.scheduleLocalNotification({
                        fireDate: eightDate.toISOString(),
                        alertTitle: translate("notification.notification_title"),
                        alertBody: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                        repeatInterval: "week"
                    });
                } else {
                    PushNotification.localNotificationSchedule({
                        id: 1,
                        priority: "high",
                        title: translate("notification.notification_title"),
                        message: translate("notification.time_to_take") + this.state.freq1Name.join(", "),
                        date: eightDate,
                        repeatType: "week"
                    });
                }
            }

            if (this.state.freq2Name && this.state.freq2Name.length > 0 && oneDate) {
                if (Platform.OS === "ios") {
                    //Alert.alert("One : ",oneDate);
                    PushNotificationIOS.scheduleLocalNotification({
                        fireDate: oneDate.toISOString(),
                        alertTitle: translate("notification.notification_title"),
                        alertBody: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                        repeatInterval: "week"
                    });
                } else {
                    PushNotification.localNotificationSchedule({
                        id: 2,
                        priority: "high",
                        title: translate("notification.notification_title"),
                        message: translate("notification.time_to_take") + this.state.freq2Name.join(", "),
                        date: oneDate,
                        repeatType: "week"
                    });
                }
            }

            if (this.state.freq3Name && this.state.freq3Name.length > 0 && sixDate) {
                if (Platform.OS === "ios") {
                    //Alert.alert("Sixx : ",sixDate);
                    PushNotificationIOS.scheduleLocalNotification({
                        fireDate: sixDate.toISOString(),
                        alertTitle: translate("notification.notification_title"),
                        alertBody: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                        repeatInterval: "week"
                    });
                } else {
                    PushNotification.localNotificationSchedule({
                        id: 3,
                        priority: "high",
                        title: translate("notification.notification_title"),
                        message: translate("notification.time_to_take") + this.state.freq3Name.join(", "),
                        date: sixDate,
                        repeatType: "week"
                    });
                }
            }

            if (this.state.freq4Name && this.state.freq4Name.length > 0 && tenDate) {
                if (Platform.OS === "ios") {
                    //Alert.alert("Ten : ",tenDate);
                    PushNotificationIOS.scheduleLocalNotification({
                        fireDate: tenDate.toISOString(),
                        alertTitle: translate("notification.notification_title"),
                        alertBody: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                        repeatInterval: "week"
                    });
                } else {
                    PushNotification.localNotificationSchedule({
                        id: 4,
                        priority: "high",
                        title: translate("notification.notification_title"),
                        message: translate("notification.time_to_take") + this.state.freq4Name.join(", "),
                        date: tenDate,
                        repeatType: "week"
                    });
                }
            }

            AsyncStorage.setItem("freq1", JSON.stringify(this.state.freq1));
            AsyncStorage.setItem("freq2", JSON.stringify(this.state.freq2));
            AsyncStorage.setItem("freq3", JSON.stringify(this.state.freq3));
            AsyncStorage.setItem("freq4", JSON.stringify(this.state.freq4));
            AsyncStorage.setItem("userMeds", JSON.stringify(this.state.meds));

            if (nextProps.userMedicationListData && nextProps.userMedicationListData.meds && nextProps.userMedicationListData.meds.length > 0) {
                await nextProps.userMedicationListData.meds.map(med => {
                    this.setState(prevState => ({
                        freqArray: [...prevState.freqArray, med.freq]
                    }))
                });

                await this.setState({
                    freqArray: _.uniqBy(this.state.freqArray, e => {
                        return e;
                    })
                });

                console.log("Before Sort : " + this.state.freqArray);

                await this.setState({
                    freqArray: _.filter(this.state.freqArray, value => {
                        return value !== "1";
                    })
                });

                /*var i = this.state.freqArray.indexOf("1");

                if (i > -1) {
                    this.setState({freqArray: this.state.freqArray.splice(i - 1, 1)});
                }*/

                console.log("Between Sort : " + this.state.freqArray);

                this.setState({
                    freqArray: this.state.freqArray.sort((a, b) => {
                        return a - b;
                    })
                });

                console.log("After Sort : " + this.state.freqArray);
                let value = "";

                await this.state.freqArray.map(arr => {
                    if (value !== "") {
                        value = value + "+" + arr + "D";
                    } else
                        value = arr + "D";
                });

                console.log("Final Value : " + value);

                if (this.state.dosageSchedules) {
                    let array = this.state.dosageSchedules[value];
                    console.log("Manage Meds ARRAY IS : " + array);

                    await array.forEach((value, index) => {
                        if (index === 0) {
                            this.setState({eight: value});
                            AsyncStorage.setItem("eight", value);
                        } else if (index === 1) {
                            this.setState({one: value});
                            AsyncStorage.setItem("one", value);
                        } else if (index === 2) {
                            this.setState({six: value});
                            AsyncStorage.setItem("six", value);
                        } else if (index === 3) {
                            this.setState({ten: value});
                            AsyncStorage.setItem("ten", value);
                        }
                    })
                }
            }
        }
        if (nextProps.medicationListData) {
            this.setState({reservedMeds: nextProps.medicationListData.meds});
        }

        if (nextProps.dosageSchedules && this.state.apiCall === 1) {
            this.setState({dosageSchedules: nextProps.dosageSchedules});
            console.log("Dosage Schedules : " + JSON.stringify(nextProps.dosageSchedules));
            AsyncStorage.setItem("dosageSchedules", JSON.stringify(nextProps.dosageSchedules));
            this.setState({apiCall: 2});
            this.props.OnFetchUserMedicationList();
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        const {error, loading, medicationListData} = this.props;

        return (
            <View style={{flex: 1}}>
                <NavigationEvents
                    onWillFocus={payload => {

                    }}
                    onDidFocus={payload => {
                        AsyncStorage.getItem("eight", (err, value) => {
                            if (value != null) this.setState({eight: value});
                            console.log("componentDidMount : Eight : " + value);
                        });
                        AsyncStorage.getItem("one", (err, value) => {
                            if (value != null) this.setState({one: value});
                            console.log("componentDidMount : One : " + value);
                        });
                        AsyncStorage.getItem("six", (err, value) => {
                            if (value != null) this.setState({six: value});
                            console.log("componentDidMount : Six : " + value);
                        });
                        AsyncStorage.getItem("ten", (err, value) => {
                            if (value != null) this.setState({ten: value});
                            console.log("componentDidMount : Ten : " + value);
                        });
                    }}
                    onWillBlur={payload => {

                    }}
                    onDidBlur={payload => {

                    }}
                />
                <HeaderView
                    name={translate("manage_meds.manage_medicine")}
                    navigation={this.props.navigation}
                />
                <ScrollView>
                    <View style={{flexDirection: "row", padding: 10, flex: 1}}>
                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem
                                button
                                style={{flexDirection: "column"}}
                                onPress={() => {
                                    this.setState({addMed: false, medList: true});
                                }}
                            >
                                <Image
                                    source={require("../images/pill_blue.png")}
                                    style={{width: 30, height: 30}}
                                />
                                <Text style={{fontSize: 12, textAlign: "center", color: colors.blackColor}}>
                                    {translate("manage_meds.add_meds")}
                                </Text>
                            </CardItem>
                        </Card>

                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem
                                button
                                style={{flexDirection: "column"}}
                                onPress={() => {
                                    navigate("ReAssignMedsScreen");
                                }}
                            >
                                <Image
                                    source={require("../images/reassign.png")}
                                    style={{width: 30, height: 30}}
                                />
                                <Text style={{fontSize: 12, textAlign: "center", color: colors.blackColor}}>
                                    {translate("manage_meds.reassign")}
                                </Text>
                            </CardItem>
                        </Card>

                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem
                                button
                                style={{flexDirection: "column"}}
                                onPress={() => {
                                    navigate("AdjustDoseTime", {
                                        freq1: this.state.freq1,
                                        freq2: this.state.freq2,
                                        freq3: this.state.freq3,
                                        freq4: this.state.freq4
                                    });
                                }}
                            >
                                <Image
                                    source={require("../images/clock.png")}
                                    style={{width: 30, height: 30}}
                                />
                                <Text style={{fontSize: 12, textAlign: "center", color: colors.blackColor}}>
                                    {translate("manage_meds.adjust_time")}
                                </Text>
                            </CardItem>
                        </Card>

                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem
                                button
                                style={{flexDirection: "column"}}
                                onPress={() => {
                                    navigate("MedHolidayScreen");
                                }}
                            >
                                <Image
                                    source={require("../images/vacation.png")}
                                    style={{width: 30, height: 30}}
                                />
                                <Text style={{
                                    fontSize: 12,
                                    color: colors.blackColor
                                }}>{translate("manage_meds.holiday")}</Text>
                            </CardItem>
                        </Card>
                    </View>
                    {/* this.props.user_med_list_loading */}
                    {this.props.user_med_list_loading && (
                        <View
                            style={{
                                justifyContent: "center",
                                alignItems: "center",
                                flexDirection: "row"
                            }}
                        >
                            <ActivityIndicator size="small" animating/>
                            <Text style={{marginLeft: 15, fontWeight: "600", color: colors.blackColor}}>
                                {translate("manage_meds.fetching_meds")}
                            </Text>
                        </View>
                    )}
                    {!this.props.user_med_list_loading && this.state.meds !== undefined && this.state.meds.length === 0 && (
                        <View
                            style={{
                                alignSelf: "center",
                                justifyContent: "center",
                                alignItems: "center",
                                flexDirection: "column",
                                flex: 0,
                                height: 300
                            }}
                        >
                            <Image
                                source={require("../images/pill_blue.png")}
                                style={{width: 60, height: 60}}
                            />
                            <Text style={{marginLeft: 15, fontWeight: "600", color: colors.blackColor}}>
                                {translate("manage_meds.no_medicine_found")}
                            </Text>
                        </View>
                    )}
                    {this.state.meds && this.state.meds.length > 0 && this.state.meds.map((med, index) => {
                        return (
                            <Collapse
                                style={{marginTop: 10}}
                                key={index}
                                isCollapsed={this.state[`isExpand${index}`]}
                                onToggle={isCollapsed =>
                                    this.setState({[`isExpand${index}`]: isCollapsed})
                                }
                            >
                                <CollapseHeader style={{height: 50, alignContent: "center"}}>
                                    <Separator style={{elevation: 3}}>
                                        {this.state[`isExpand${index}`] ? (
                                            <View
                                                style={{
                                                    flex: 1,
                                                    flexDirection: "row",
                                                    alignContent: "center",
                                                    alignItems: "center"
                                                }}
                                            >
                                                <Image
                                                    source={require("../images/pill_blue.png")}
                                                    style={{padding: 10, width: 30, height: 30}}
                                                />
                                                <Text
                                                    style={{
                                                        flex: 1,
                                                        padding: 10,
                                                        fontSize: 20,
                                                        color: colors.blueButtonColor
                                                    }}
                                                >
                                                    {" "}
                                                    {med.desc}
                                                </Text>
                                                <BackIcon
                                                    name={"ios-arrow-down"}
                                                    size={30}
                                                    color={colors.blueButtonColor}
                                                    style={{padding: 10, alignContent: "flex-end"}}
                                                />
                                            </View>
                                        ) : (
                                            <View
                                                style={{
                                                    flex: 1,
                                                    flexDirection: "row",
                                                    alignContent: "center",
                                                    alignItems: "center"
                                                }}
                                            >
                                                <Image
                                                    source={require("../images/pill_grey.png")}
                                                    style={{padding: 10, width: 30, height: 30}}
                                                />
                                                <Text
                                                    style={{
                                                        flex: 1,
                                                        padding: 10,
                                                        fontSize: 20,
                                                        color: colors.greyColor
                                                    }}
                                                >
                                                    {" "}
                                                    {med.desc}
                                                </Text>
                                                <BackIcon
                                                    name={"ios-arrow-up"}
                                                    size={30}
                                                    style={{padding: 10, alignContent: "flex-end"}}
                                                />
                                            </View>
                                        )}
                                    </Separator>
                                </CollapseHeader>
                                <CollapseBody>
                                    <ListItem>
                                        <View style={{flex: 1}}>
                                            <View style={{flexDirection: "row"}}>
                                                <Text>{translate("manage_meds.medication_name")} : </Text>
                                                <Text
                                                    style={{
                                                        color: colors.blackColor
                                                    }}
                                                >
                                                    {med.desc}
                                                </Text>
                                            </View>
                                            <View style={{flexDirection: "row"}}>
                                                <Text>{translate("manage_meds.frequency")} : </Text>
                                                <Text
                                                    style={{
                                                        color: colors.blackColor
                                                    }}
                                                >
                                                    {med.freq}
                                                </Text>
                                            </View>
                                            <View style={{flexDirection: "row"}}>
                                                <Text>{translate("manage_meds.time")} : </Text>
                                                <Text
                                                    style={{
                                                        color: colors.blackColor
                                                    }}
                                                >
                                                    {med.freq === "1"
                                                        ? this.state.eight
                                                        : med.freq === "2"
                                                            ? `${this.state.eight}, ${this.state.one}`
                                                            : med.freq === "3"
                                                                ? `${this.state.eight}, ${this.state.one}, ${
                                                                    this.state.six
                                                                    }`
                                                                : `${this.state.eight}, ${this.state.one}, ${
                                                                    this.state.six
                                                                    }, ${this.state.ten}`}
                                                </Text>
                                            </View>
                                        </View>

                                        <TouchableOpacity onPress={() => this.deleteMedicine(med)}>
                                            {this.state[`deleting${med.idx}`] !== undefined &&
                                            this.state[`deleting${med.idx}`] === true ? (
                                                <ActivityIndicator size="small" animating/>
                                            ) : (
                                                <Image
                                                    source={require("../images/trash.png")}
                                                    style={{
                                                        height: 30,
                                                        width: 30,
                                                        resizeMode: "contain"
                                                    }}
                                                />
                                            )}
                                        </TouchableOpacity>
                                    </ListItem>
                                </CollapseBody>
                            </Collapse>
                        );
                    })}

                    {/*This is Add Custome Medication Dialog*/}
                    <Dialog visible={this.state.addMed}>
                        <DialogContent>
                            <CrossIcon
                                name="x"
                                size={30}
                                color="#ddd"
                                style={{alignSelf: "flex-end", marginTop: 10}}
                                onPress={() => {
                                    this.setState({addMed: false});
                                }}
                            />

                            <Text
                                style={{
                                    fontSize: 22,
                                    color: colors.blueButtonColor,
                                    marginTop: 10,
                                    backgroundColor: colors.whiteColor,
                                    alignSelf: "center"
                                }}
                            >
                                {translate("manage_meds.custom_medicine")}
                            </Text>

                            <Text style={{color: colors.greyColor}}>{translate("manage_meds.medication_name")}</Text>
                            <Item regular style={{height: 50}}>
                                <Input
                                    onChangeText={text => {
                                        this.setState({customMedsName: text});
                                    }}
                                    value={this.state.customMedsName}
                                />
                            </Item>

                            <View
                                style={{
                                    flexDirection: "row",
                                    paddingBottom: 10,
                                    marginTop: 10
                                }}
                            >
                                <View style={{flexDirection: "column"}}>
                                    <Text style={{color: colors.greyColor}}>{translate("manage_meds.strength")}</Text>
                                    <Item regular style={{width: 140, height: 52}}>
                                        <Input
                                            placeholder="250mg"
                                            placeholderTextColor="grey"
                                            onChangeText={text =>
                                                this.setState({customMedsStrength: text})
                                            }
                                            value={this.state.customMedsStrength}
                                        />
                                    </Item>
                                </View>
                                <View style={{flexDirection: "column"}}>
                                    <Text style={{color: colors.greyColor}}>{translate("manage_meds.frequency")}</Text>
                                    <View
                                        style={{
                                            width: 140,
                                            height: 52,
                                            borderWidth: 1,
                                            borderColor: colors.greyColor,
                                            marginLeft: 3
                                        }}
                                    >
                                        <Item picker>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<BackIcon name="ios-arrow-down"/>}
                                                style={{width: undefined, height: 51}}
                                                placeholder="1"
                                                placeholderStyle={{color: "#bfc6ea"}}
                                                placeholderIconColor={colors.blackColor}
                                                selectedValue={this.state.frequency}
                                                onValueChange={this.onFrequencyValueChange.bind(this)}>
                                                <Picker.Item label="1" value="1"/>
                                                <Picker.Item label="2" value="2"/>
                                                <Picker.Item label="3" value="3"/>
                                                <Picker.Item label="4" value="4"/>
                                            </Picker>
                                        </Item>
                                    </View>
                                </View>
                            </View>
                            <TouchableOpacity
                                disabled={this.state.fetchingMeds}
                                onPress={this._UpdateMedList}
                                style={styles.buttonStyle}
                            >
                                {this.state.fetchingMeds ? (
                                    <Text style={styles.textStyle}>{translate("daily_activity.updating")}</Text>
                                ) : (
                                    <Text style={styles.textStyle}>{translate("manage_meds.add_med")}</Text>
                                )}
                            </TouchableOpacity>
                        </DialogContent>
                    </Dialog>

                    {/*This is Medication List Dialog*/}
                    <Dialog visible={this.state.medList} height={500}>
                        <DialogContent>
                            <CrossIcon
                                name="x"
                                size={30}
                                color="#ddd"
                                style={{alignSelf: "flex-end", margin: 10}}
                                onPress={() => {
                                    this.setState({addMed: false, medList: false});
                                }}
                            />

                            <Text
                                style={{
                                    fontSize: 22,
                                    color: colors.blueButtonColor,
                                    backgroundColor: colors.whiteColor,
                                    alignSelf: "center"
                                }}
                            >
                                {translate("manage_meds.medication_list")}
                            </Text>

                            <TouchableWithoutFeedback
                                onPress={() => {
                                    this.setState({addMed: true, medList: false});
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: "row",
                                        height: 40,
                                        backgroundColor: colors.blueButtonColor,
                                        marginTop: 10
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.whiteColor,
                                            fontSize: 16,
                                            padding: 8,
                                            justifyContent: "center"
                                        }}
                                    >
                                        {translate("manage_meds.custom_medicine")}
                                    </Text>
                                    <EntypoIcon
                                        name="circle-with-plus"
                                        size={20}
                                        color={colors.whiteColor}
                                        style={{padding: 8}}
                                    />
                                </View>
                            </TouchableWithoutFeedback>

                            {this.props.loading && (
                                <View
                                    style={[
                                        styles.shadowList,
                                        {height: 300, justifyContent: "center"}
                                    ]}
                                >
                                    <ActivityIndicator size="large" animating/>
                                </View>
                            )}
                            {this.state.reservedMeds && this.state.reservedMeds.length > 0 && (
                                <FlatList
                                    style={{
                                        marginTop: 8,
                                        marginBottom: 10
                                    }}
                                    extraData={this.state}
                                    showsHorizontalScrollIndicator={false}
                                    data={this.state.reservedMeds}
                                    numColumns={1}
                                    renderItem={({item}) => (
                                        <View style={styles.shadowList}>
                                            <Text
                                                style={{
                                                    height: 40,
                                                    width: 279,
                                                    backgroundColor: colors.darkBlueColor,
                                                    fontSize: 16,
                                                    color: colors.whiteColor,
                                                    textAlignVertical: "center",
                                                    paddingLeft: 10
                                                }}
                                            >
                                                {item.desc}
                                            </Text>
                                            <View style={{flexDirection: "column", flex: 1}}>
                                                <View
                                                    style={{
                                                        flexDirection: "row",
                                                        height: 40,
                                                        width: 280
                                                    }}
                                                >
                                                    <Text
                                                        style={styles.listTitletStyle}>{translate("manage_meds.strength")}</Text>
                                                    <Text
                                                        style={styles.listTitletStyle}>{translate("manage_meds.frequency")}</Text>
                                                    <Text
                                                        style={styles.listTitletStyle}>{translate("manage_meds.add")}</Text>
                                                </View>

                                                {item.doses.map(dose => (
                                                    <View key={dose.idx}>
                                                        <View
                                                            style={{
                                                                flexDirection: "row",
                                                                height: 40,
                                                                width: 280
                                                            }}
                                                        >
                                                            <Text style={styles.listValueStyle}>
                                                                {dose.strength}
                                                            </Text>
                                                            <View
                                                                style={{
                                                                    width: 1,
                                                                    backgroundColor: colors.greyColor
                                                                }}
                                                            />
                                                            <Text style={styles.listValueStyle}>
                                                                {dose.freq}
                                                            </Text>
                                                            <View
                                                                style={{
                                                                    width: 1,
                                                                    backgroundColor: colors.greyColor
                                                                }}
                                                            />
                                                            <View
                                                                style={{
                                                                    justifyContent: "center",
                                                                    alignContent: "center",
                                                                    alignItems: "center",
                                                                    flex: 1
                                                                }}
                                                            >
                                                                <Switch
                                                                    onValueChange={value =>
                                                                        this.onSwitchValueChange(
                                                                            value,
                                                                            `${item.code}/${dose.strength}`,
                                                                            dose,
                                                                            item
                                                                        )
                                                                    }
                                                                    value={
                                                                        this.state[
                                                                            `${item.code}/${dose.strength}`
                                                                            ] !== undefined
                                                                            ? true
                                                                            : this.state.updatedMedList.find(
                                                                            d => d.idx == dose.idx
                                                                            ) !== undefined &&
                                                                            this.state.updatedMedList.find(
                                                                                d => d.idx == dose.idx
                                                                            ).idx === dose.idx
                                                                    }
                                                                />
                                                            </View>
                                                        </View>
                                                        <View
                                                            style={{
                                                                height: 1,
                                                                width: 280,
                                                                backgroundColor: colors.greyColor
                                                            }}
                                                        />
                                                    </View>
                                                ))}
                                            </View>
                                        </View>
                                    )}
                                />
                            )}

                            {
                                <TouchableOpacity
                                    onPress={this._UpdateMedListNotCustom}
                                    style={styles.buttonStyle}
                                >
                                    {this.state.fetchingMeds ? (
                                        <Text style={styles.textStyle}>{translate("daily_activity.updating")}</Text>
                                    ) : (
                                        <Text style={styles.textStyle}>{translate("manage_meds.add_med")}</Text>
                                    )}
                                </TouchableOpacity>
                            }
                        </DialogContent>
                    </Dialog>
                </ScrollView>

                <FooterView navigation={this.props.navigation}/>
            </View>
        );
    }

    onFrequencyValueChange(value) {
        this.setState({
            frequency: value
        });
    }
}

const mapStateToProps = state => {
    return {
        loading: state.medicationListReducer.loading,
        error: state.medicationListReducer.error,
        medicationListData: state.medicationListReducer.medicationListData,
        userMedicationListData:
        state.userMedicationListReducer.UsermedicationListData,
        user_med_list_loading: state.userMedicationListReducer.loading,
        user_med_list_error: state.userMedicationListReducer.error,
        dosageSchedules: state.dosageSchedule.dosageSchedules,
        dosage_error: state.dosageSchedule.dosage_error,
        dosage_loading: state.dosageSchedule.dosage_loading,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchMedicationList: data =>
            dispatch(medicationListActions.fetchMedicationList(data)),
        OnFetchUserMedicationList: () =>
            dispatch(UserMedicationListActions.fetchUserMedicationList()),
        OnFetchDosageSchedules: () =>
            dispatch(DosageScheduleAction.fetchDosageSchedule())
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ManageMedsScreen);

const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        width: 160,
        height: 40,
        borderRadius: 30,
        color: colors.whiteColor,
        alignSelf: "center",
        textAlign: "center",
        justifyContent: "center"
    },
    textStyle: {
        color: colors.whiteColor,
        fontSize: 16,
        alignSelf: "center",
        textAlign: "center",
        justifyContent: "center"
    },
    shadowList: {
        borderWidth: 1,
        borderRadius: 1,
        borderColor: "#ddd",
        borderBottomWidth: 0,
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 3,
        width: 280,
        marginTop: 10,
        marginBottom: 10
    },
    listTitletStyle: {
        height: 40,
        width: 100,
        backgroundColor: colors.blueButtonColor,
        color: colors.whiteColor,
        textAlignVertical: "center",
        justifyContent: "center",
        textAlign: "center"
    },
    listValueStyle: {
        height: 40,
        width: 100,
        backgroundColor: colors.whiteColor,
        color: colors.blackColor,
        textAlignVertical: "center",
        justifyContent: "center",
        textAlign: "center"
    }
});
