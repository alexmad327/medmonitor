import React, {Component} from "react";
import {Image, ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {colors, headerTitleStyle} from "../constants/Constants";
import {Card, CardItem, Input, Item, ListItem, Picker, Separator} from 'native-base';
import {FooterView} from "../container/FooterView";
import {Text} from "react-native-elements";
import Icon from "react-native-vector-icons/AntDesign"
import Dialog, {DialogContent} from "react-native-popup-dialog";
import {Collapse, CollapseBody, CollapseHeader} from "accordion-collapse-react-native";
import BackIcon from 'react-native-vector-icons/Ionicons';
import CrossIcon from "react-native-vector-icons/Feather";

export class DemoScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addMedList: false,
            reset: false,
            assign: false,
            selectedDate: new Date(),
            data: [
                {id: 1, title: "Dose 1", time: "08:00"},
                {id: 2, title: "Dose 2", time: "10:00"},
                {id: 3, title: "Dose 3", time: "12:00"}
            ],
            frequency: 5,
            isExpand: true,
        }
    }

    static navigationOptions = {
        headerTitleStyle: headerTitleStyle,
        title: 'Adjust Dosetime',
        headerLeft: (
            <TouchableOpacity onPress={() => {
                navigation.goBack();
            }} style={{padding: 10, marginLeft: 10}}>
                <Image source={require('../images/back.png')} style={{height: 25, width: 25}}/>
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: colors.blueButtonColor,
        },
        headerRight: (
            <View style={{flexDirection: 'row', alignContent: 'center'}}>
                <TouchableOpacity onPress={() => {

                }} style={{paddingRight: 10}}>
                    <Image source={require('../images/information.png')} style={{height: 30, width: 30}}/>
                </TouchableOpacity>
            </View>
        )
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <ScrollView>
                    <View style={{flexDirection: 'row', padding: 10, flex: 1}}>
                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem button style={{flexDirection: 'column'}}
                                      onPress={() => {
                                          this.setState({addMedList: true});
                                      }}>
                                <Image source={require('../images/pill_blue.png')}
                                       style={{width: 30, height: 30}}/>
                                <Text style={{textAlign: 'center'}}>
                                    Add Meds
                                </Text>
                            </CardItem>
                        </Card>

                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem button style={{flexDirection: 'column'}}
                                      onPress={() => {
                                          this.setState({assign: true});
                                      }}>
                                <Image source={require('../images/reassign.png')}
                                       style={{width: 30, height: 30}}/>
                                <Text>
                                    Reassign
                                </Text>
                            </CardItem>
                        </Card>

                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem button style={{flexDirection: 'column'}}
                                      onPress={() => {
                                          navigate("AdjustDoseTime")
                                      }}>
                                <Image source={require('../images/clock.png')} style={{width: 30, height: 30}}/>
                                <Text>
                                    Adjust Time
                                </Text>
                            </CardItem>
                        </Card>

                        <Card style={{flex: 0.25, margin: 10}}>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Image source={require('../images/vacation.png')}
                                       style={{width: 30, height: 30}}/>
                                <Text>
                                    Holiday
                                </Text>
                            </CardItem>
                        </Card>
                    </View>
                    <Collapse
                        isCollapsed={this.state.isExpand}
                        onToggle={(isCollapsed) => this.setState({isExpand: isCollapsed})}>
                        <CollapseHeader style={{height: 50, alignContent: 'center'}}>
                            <Separator style={{elevation: 3}}>
                                {
                                    this.state.isExpand ?
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            alignContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <Image source={require('../images/pill_blue.png')}
                                                   style={{padding: 10, width: 30, height: 30}}/>
                                            <Text style={{
                                                flex: 1,
                                                padding: 10,
                                                fontSize: 20,
                                                color: colors.blueButtonColor
                                            }}> Acetaminophen</Text>
                                            <BackIcon name={"ios-arrow-up"} size={30}
                                                      color={colors.blueButtonColor}
                                                      style={{padding: 10, alignContent: 'flex-end'}}/>
                                        </View>
                                        :
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            alignContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <Image source={require('../images/pill_grey.png')}
                                                   style={{padding: 10, width: 30, height: 30}}/>
                                            <Text style={{
                                                flex: 1,
                                                padding: 10,
                                                fontSize: 20
                                            }}> Acetaminophen</Text>
                                            <BackIcon name={"ios-arrow-down"} size={30}
                                                      style={{padding: 10, alignContent: 'flex-end'}}/>
                                        </View>
                                }
                            </Separator>
                        </CollapseHeader>
                        <CollapseBody>
                            <ListItem>
                                <View style={{flex: 1}}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text>
                                            Medicine Name:
                                        </Text>
                                        <Text style={{
                                            color: colors.blackColor
                                        }}>
                                            {" Acetaminophen. "}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text>
                                            Frequency:
                                        </Text>
                                        <Text style={{
                                            color: colors.blackColor
                                        }}>{" 3 "}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text>
                                            Time:
                                        </Text>
                                        <Text style={{
                                            color: colors.blackColor
                                        }}>{" 8 AM "}</Text>
                                    </View>
                                </View>
                                <Icon name="delete" size={30}/>
                            </ListItem>
                        </CollapseBody>
                    </Collapse>
                    <Collapse style={{marginTop: 12}}>
                        <CollapseHeader style={{height: 50, alignContent: 'center', elevation: 3}}>
                            <Separator style={{elevation: 3}}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    alignContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Image source={require('../images/pill_grey.png')}
                                           style={{padding: 10, width: 30, height: 30}}/>
                                    <Text style={{flex: 1, padding: 10, fontSize: 20}}> Naproxen</Text>
                                    <BackIcon name={"ios-arrow-up"} size={30}
                                              style={{padding: 10}}/>
                                </View>
                            </Separator>
                        </CollapseHeader>
                    </Collapse>
                    <Collapse style={{marginTop: 12}}>
                        <CollapseHeader style={{height: 50, alignContent: 'center', elevation: 3}}>
                            <Separator style={{elevation: 3}}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    alignContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Image source={require('../images/pill_grey.png')}
                                           style={{padding: 10, width: 30, height: 30}}/>
                                    <Text style={{flex: 1, padding: 10, fontSize: 20}}> Cyclobenzaprine</Text>
                                    <BackIcon name={"ios-arrow-up"} size={30}
                                              style={{padding: 10}}/>
                                </View>
                            </Separator>
                        </CollapseHeader>
                    </Collapse>
                    <Collapse style={{marginTop: 12}}>
                        <CollapseHeader style={{height: 50, alignContent: 'center', elevation: 3}}>
                            <Separator style={{elevation: 3}}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    alignContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Image source={require('../images/pill_grey.png')}
                                           style={{padding: 10, width: 30, height: 30}}/>
                                    <Text
                                        style={{flex: 1, padding: 10, fontSize: 20}}> Hydrochlorothiazide</Text>
                                    <BackIcon name={"ios-arrow-up"} size={30}
                                              style={{padding: 10}}/>
                                </View>
                            </Separator>
                        </CollapseHeader>
                    </Collapse>

                    {/*This is Reset Dialog*/}

                    <Dialog visible={this.state.addMedList}>
                        <DialogContent>
                            <CrossIcon name="x" size={30} color="#ddd"
                                       style={{alignSelf: 'flex-end', margin: 10}}
                                       onPress={() => {
                                           this.setState({addMedList: false});
                                       }}/>

                            <Text style={{
                                fontSize: 22,
                                color: colors.blueButtonColor,
                                marginTop: 10,
                                backgroundColor: colors.whiteColor,
                                alignSelf: 'center'
                            }}>
                                Custom Medication
                            </Text>

                            <Text>
                                Mediciation Name
                            </Text>
                            <Item regular style={{height: 50}}>
                                <Input/>
                            </Item>

                            <View style={{flexDirection: 'row', paddingBottom: 10, marginTop: 10}}>
                                <View style={{flexDirection: 'column'}}>
                                    <Text>Strength</Text>
                                    <Item regular style={{width: 140, height: 52}}>
                                        <Input placeholder='250mg'/>
                                    </Item>
                                </View>
                                <View style={{flexDirection: 'column'}}>
                                    <Text>Frequency</Text>
                                    <View style={{
                                        width: 140,
                                        height: 52,
                                        borderWidth: 1,
                                        borderColor: colors.greyColor,
                                        marginLeft: 3
                                    }}>
                                        <Item picker>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<BackIcon name="ios-arrow-down"/>}
                                                style={{width: undefined}}
                                                placeholder="5"
                                                placeholderStyle={{color: "#bfc6ea"}}
                                                placeholderIconColor={colors.blackColor}
                                                selectedValue={this.state.frequency}
                                                onValueChange={this.onValueChange2.bind(this)}
                                            >
                                                <Picker.Item label="1" value="key0"/>
                                                <Picker.Item label="2" value="key1"/>
                                                <Picker.Item label="3" value="key2"/>
                                                <Picker.Item label="4" value="key3"/>
                                                <Picker.Item label="5" value="key4"/>
                                            </Picker>
                                        </Item>
                                    </View>
                                </View>
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({addMedList: false})
                                }}
                                style={styles.buttonStyle}>
                                <Text style={styles.textStyle}>ADD MED</Text>
                            </TouchableOpacity>
                        </DialogContent>
                    </Dialog>

                </ScrollView>

                <FooterView isSummary={true}
                            navigation={this.props.navigation}/>

            </View>
        );
    }

    onValueChange2(value: string) {
        this.setState({
            frequency: value
        });
    }
}

const styles = StyleSheet.create({
    pillTimeStyle: {
        fontSize: 22,
        color: colors.whiteColor,
        textAlign: 'center'
    },
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 30,
        color: colors.whiteColor,
        marginBottom: 20
    },
    textStyle: {
        color: colors.whiteColor,
        fontSize: 20,
        alignSelf: 'center',
        padding: 12
    },
    dateTextStyle: {
        borderWidth: 0,
        borderRadius: 30,
        borderColor: colors.whiteColor,
        borderBottomWidth: 0,
        elevation: 3,
        alignContent: 'center',
        fontSize: 16,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center'
    },
    dialogLabel: {
        fontSize: 15,
        color: colors.blackColor,
        paddingTop: 10,
        paddingBottom: 10
    },
    thumbnailImage: {
        marginTop: -55,
        alignSelf: 'center',
        width: 100,
        height: 100,
        borderRadius: 100 / 2
    },
    listItemStyle: {
        flexDirection: 'row',
        borderRadius: 30,
        elevation: 2,
        alignItems: 'center',
        padding: 5
    },
    plusMinusStyle: {
        padding: 5,
        color: colors.blueButtonColor,
        fontSize: 20
    }
});