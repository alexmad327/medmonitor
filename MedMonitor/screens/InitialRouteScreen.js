import React, {Component} from "react";
import {AsyncStorage, View, Alert} from "react-native";
import {NavigationActions, StackActions} from 'react-navigation';

export class InitialRouteScreen extends Component {

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        await AsyncStorage.getItem("isLoggedIn", (err, result) => {
            if (result) {
                // this.props.navigation.navigate('HomeScreen');
                this.navigateAfterFinish("HomeScreen")
            } else {
                // this.props.navigation.navigate('Registration');
                this.navigateAfterFinish("Registration")
            }
        });
    }

    navigateAfterFinish = (screen) => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: screen})],
        });
        this.props.navigation.dispatch(resetAction);
    };

    render() {
        return (
            <View>

            </View>
        );
    }
}