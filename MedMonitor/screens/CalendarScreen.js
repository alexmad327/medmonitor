import React, {Component} from "react"
import {FlatList, TouchableOpacity, View, BackHandler, AsyncStorage} from "react-native"
import {Calendar} from 'react-native-calendars';
import Icon from "react-native-vector-icons/AntDesign"
import {colors} from "../constants/Constants";
import {Dialog, DialogContent} from "react-native-popup-dialog/src";
import {Text} from "react-native-elements";
import moment from "moment";
import {connect} from "react-redux";
import * as GetTodayMedicationActions from "../store/actions/GetTodayMedicationAction";
import {HeaderView} from "../container/HeaderView";
import I18n, {translate} from "../locale/i18n";
import * as _ from "lodash";

class CalendarScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            date: new Date(),
            keys: [],
            todayMedicationListData: [],
            markedDates: {},
            phone: "",
            otpToken: "",
            lastDay: new Date()
        }
    }

    async componentDidMount() {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");
        this.setState({phone: phone, otpToken: otp});

        var date = new Date();
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        // console.log("Last Day : " + lastDay.getDate());

        this.props.OnFetchTodayMedicationList({
            lastDate: moment(new Date()).format("YYYYMMDD"),
            lastDay: lastDay.getDate()
        });
    }

    async componentWillReceiveProps(nextProps) {
        if (nextProps.todayMedicationListData) {
            // console.log("CALENDAR MEDICATION LIST ====>", nextProps.todayMedicationListData);
            this.setState({todayMedicationListData: nextProps.todayMedicationListData});


            var lastDay = this.state.lastDay;

            const startDate = moment(lastDay).add(-30, 'days').toDate(); //YYYY-MM-DD
            const endDate = lastDay; //YYYY-MM-DD

            const array = await this.getDateArray(startDate, endDate);

            var lastObj = {};

            await _.forEachRight(array, (value) => {
                var key = moment(value).format("YYYYMMDD");
                console.log("Value : " + value + " KEY : " + key);

                if (nextProps.todayMedicationListData[key]) {
                    if (!_.isEmpty(nextProps.todayMedicationListData[key].dosage_slots)) {
                        lastObj = nextProps.todayMedicationListData[key];
                        console.log("Last object has assign value")
                    } else {
                        if (!_.isEmpty(lastObj)) {
                            console.log("Last objet is not empty");

                            Object.keys(lastObj.dosage_slots).map(
                                async k => {
                                    lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                    lastObj.dosage_slots[k].taken_time = "";
                                    lastObj.dosage_slots[k].updated_on_server = 0;
                                    lastObj.dosage_slots[k].updated_time = "";
                                }
                            );

                            nextProps.todayMedicationListData[key] = lastObj;
                        }
                    }
                } else {
                    if (!_.isEmpty(lastObj)) {
                        console.log("Last objet is not empty");

                        Object.keys(lastObj.dosage_slots).map(
                            async k => {
                                lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                lastObj.dosage_slots[k].taken_time = "";
                                lastObj.dosage_slots[k].updated_on_server = 0;
                                lastObj.dosage_slots[k].updated_time = "";
                            }
                        );
                        nextProps.todayMedicationListData[key] = lastObj;
                    }
                }
            });

            await _.forEach(array, (value) => {
                var key = moment(value).format("YYYYMMDD");
                console.log("Value : " + value + " KEY : " + key);

                if (nextProps.todayMedicationListData[key]) {
                    if (!_.isEmpty(nextProps.todayMedicationListData[key].dosage_slots)) {
                        lastObj = nextProps.todayMedicationListData[key];
                        console.log("Last object has assign value")
                    } else {
                        if (!_.isEmpty(lastObj)) {
                            console.log("Last objet is not empty");

                            Object.keys(lastObj.dosage_slots).map(
                                async k => {
                                    lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                    lastObj.dosage_slots[k].taken_time = "";
                                    lastObj.dosage_slots[k].updated_on_server = 0;
                                    lastObj.dosage_slots[k].updated_time = "";
                                }
                            );

                            nextProps.todayMedicationListData[key] = lastObj;
                        }
                    }
                } else {
                    if (!_.isEmpty(lastObj)) {
                        console.log("Last objet is not empty");

                        Object.keys(lastObj.dosage_slots).map(
                            async k => {
                                lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                lastObj.dosage_slots[k].taken_time = "";
                                lastObj.dosage_slots[k].updated_on_server = 0;
                                lastObj.dosage_slots[k].updated_time = "";
                            }
                        );
                        nextProps.todayMedicationListData[key] = lastObj;
                    }
                }
            });

            // console.log("\n\n\n New Generated Array IS : " + JSON.stringify(nextProps.todayMedicationListData));

            Object.keys(nextProps.todayMedicationListData).map((key) => {
                    this.setState({todayMedicationListData: nextProps.todayMedicationListData});
                    // console.log("KEY : " + key + " : " + moment(key).format("YYYY-MM-DD"));
                    var obj = this.state.markedDates;
                    var dd = moment(key).format("YYYY-MM-DD");
                    var newObj = {
                        customStyles: {
                            container: {
                                backgroundColor: colors.whiteColor,
                                borderWidth:
                                    1,
                                borderColor:
                                colors.blueButtonColor
                            },
                            text: {
                                color: 'black',
                                textAlign:
                                    'center',
                                textDecorationLine:
                                    'underline'
                            }
                        }
                    };

                    /*this.setState((state) => {
                        return {markedDates: Object.assign(state.markedDates, newObj)};
                    });*/

                    if (Object.keys(nextProps.todayMedicationListData[key].dosage_slots).length > 0 &&
                        nextProps.todayMedicationListData[key].dosage_slots.constructor === Object) {
                        // console.log("..........." + nextProps.todayMedicationListData[key].dosage_slots);
                        var taken = "";
                        var notTaken = "";
                        var color = "Red";
                        Object.keys(nextProps.todayMedicationListData[key].dosage_slots).map(k => {
                            if (nextProps.todayMedicationListData[key].dosage_slots[k].taken_status === "TAKEN") {
                                taken = "TAKEN"
                            }
                            if (nextProps.todayMedicationListData[key].dosage_slots[k].taken_status === "NOT_TAKEN") {
                                notTaken = "NOT_TAKEN"
                            }
                        });

                        if (!_.isEmpty(taken) && !_.isEmpty(notTaken)) {
                            newObj.customStyles.container.borderColor = colors.takenPillYellowColor;
                        } else if (!_.isEmpty(taken) && _.isEmpty(notTaken)) {
                            newObj.customStyles.container.borderColor = colors.takenPillGreenColor;
                        } else {
                            newObj.customStyles.container.borderColor = colors.notTakenPillColor;
                        }

                        this.setState(prevState => ({
                            markedDates: {
                                ...prevState.markedDates,
                                [dd]: newObj
                            }
                        }));

                        this.setState({
                            [key]: nextProps.todayMedicationListData[key].dosage_slots
                        });
                    }
                }
            );
        }
    }

    getDateArray = (start, end) => {
        var arr = [];
        var dt = new Date(start);
        while (dt <= end) {
            arr.push(new Date(moment(dt)));
            dt.setDate(dt.getDate() + 1);
        }
        return arr;
    };

    static
    navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <HeaderView name={translate("medication_calendar.adherence")} navigation={this.props.navigation}/>
                <View style={{flex: 1, backgroundColor: colors.greyColor}}>

                    <Calendar
                        monthFormat={'MMMM dd, yyyy'}
                        current={this.state.date}
                        onDayPress={(day) => {
                            var lastDay = new Date(day.year, day.month - 1, day.day);
                            var obj = this.state[`${moment(lastDay).format("YYYYMMDD")}`];
                            // console.log("Day Pressed :" + JSON.stringify(day) + " :: " + JSON.stringify(obj));
                            this.setState({visible: true, date: lastDay});
                        }}
                        onDayLongPress={(day) => {
                            console.log('selected day', day);
                        }}
                        onMonthChange={(month) => {
                            console.log('month changed', JSON.stringify(month) + " : " + new Date().getMonth() + " : " +
                                new Date().getFullYear());

                            var lastDay = new Date(month.year, month.month, 0);

                            if (month.month === (new Date().getMonth() + 1) && month.year === new Date().getFullYear()) {
                                lastDay = new Date();
                            } else {
                                lastDay = new Date(month.year, month.month, 0);
                            }

                            this.setState({date: new Date(month.year, month.month - 1, month.day), lastDay: lastDay});
                            this.props.OnFetchTodayMedicationList({
                                lastDate: moment(lastDay).format("YYYYMMDD"),
                                lastDay: lastDay.getDate()
                            });
                        }}
                        hideArrows={false}
                        hideExtraDays={true}
                        disableMonthChange={false}
                        hideDayNames={false}
                        showWeekNumbers={false}
                        onPressArrowLeft={substractMonth => substractMonth()}
                        onPressArrowRight={addMonth => addMonth()}
                        renderArrow={(direction) =>
                            direction === 'left' ? <Icon name="left" size={30}/> :
                                <Icon name="right" size={30}/>
                        }

                        markingType={'custom'}
                        markedDates={this.state.markedDates}
                    />

                    <Dialog visible={this.state.visible} dialogStyle={{margin: 30}}>
                        <DialogContent>
                            <Text style={{
                                fontSize: 18,
                                color: colors.blackColor,
                                backgroundColor: colors.blueButtonColor,
                                alignSelf: 'center',
                                height: 50,
                                width: 300,
                                textAlign: 'center',
                                justifyContent: 'center',
                                paddingTop: 8
                            }}>
                                {translate("medication_calendar.summary") + " " + (moment(this.state.date).format("MM")) + "-" +
                                moment(this.state.date).format("DD-YYYY")}
                            </Text>
                            {
                                this.state[`${moment(this.state.date).format("YYYYMMDD")}`] !== undefined ?
                                    <View style={{height: 200}}>
                                        <FlatList
                                            data={Object.values(this.state[`${moment(this.state.date).format("YYYYMMDD")}`])}
                                            numColumns={2}
                                            renderItem={(item) =>

                                                <View style={{
                                                    margin: 5,
                                                    flex: 0.5,
                                                    borderTopLeftRadius: 20,
                                                    justifyContent: 'center',
                                                    backgroundColor: item.item.taken_status === "TAKEN" ? colors.takenPillGreenColor : colors.notTakenPillColor
                                                }}>
                                                    <Text style={{
                                                        color: colors.blackColor,
                                                        fontSize: 16,
                                                        alignSelf: 'center',
                                                        padding: 8
                                                    }}>
                                                        {Object.keys(this.state[`${moment(this.state.date).format("YYYYMMDD")}`]).find(key => this.state[`${moment(this.state.date).format("YYYYMMDD")}`][key] === item.item)}
                                                    </Text>
                                                    <Text style={{
                                                        backgroundColor: colors.takenPillYellowColor,
                                                        alignSelf: 'center',
                                                        padding: 10,
                                                        borderRadius: 6,
                                                        marginBottom: 8
                                                    }}>
                                                        {item.item.taken_status === 'TAKEN' ? `${translate("daily_activity.taken")}` : `${translate("medication_calendar.missed")}`}
                                                    </Text>
                                                </View>
                                            }
                                        />
                                    </View>
                                    : <Text
                                        style={{
                                            padding: 20,
                                            fontSize: 16,
                                            textAlign: 'center'
                                        }}>
                                        {translate("medication_calendar.no_meds_found")}
                                    </Text>
                            }
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({visible: false})
                                }}
                                style={{
                                    height: 40,
                                    backgroundColor: colors.blueButtonColor,
                                    margin: 10
                                }}>
                                <Text style={{
                                    color: colors.whiteColor,
                                    paddingTop: 10,
                                    alignSelf: 'center',
                                    fontSize: 15
                                }}>{translate("login.ok")}</Text>
                            </TouchableOpacity>
                        </DialogContent>
                    </Dialog>
                </View>
            </View>
        );
    }
}


const mapStateToProps = state => {
    return {
        todayMedicationListData: state.todayMedicationReducer.todayMedicationListData,
        today_loading: state.todayMedicationReducer.today_loading,
        today_error: state.todayMedicationReducer.today_error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchTodayMedicationList: (data) =>
            dispatch(GetTodayMedicationActions.fetchTodayMedicationList(data))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CalendarScreen);