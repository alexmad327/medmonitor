import React, {Component} from "react";
import {
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    View,
    TouchableWithoutFeedback,
    FlatList,
    Alert, AsyncStorage,
    ActivityIndicator

} from "react-native";
import {API_BASE_URL, colors} from "../constants/Constants";
import {Image, Text} from "react-native-elements";
import {FooterView} from "../container/FooterView";
// import {Col, Grid, Row} from "react-native-easy-grid";
import {Card, CheckBox} from "native-base";
import Dialog, {DialogContent} from "react-native-popup-dialog";
import Icon from "react-native-vector-icons/Feather";
import {HeaderView} from "../container/HeaderView";
import * as UserMedicationListActions from "../store/actions/UserMedicationListAction";
import {connect} from "react-redux";
import axios from "axios";
import I18n, {translate} from "../locale/i18n";

class ReAssignMedsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedText: `${translate("re_assign_meds_screen.automatic")}`,
            assign: false,
            reset: false,
            meds: [],
            eightMeds: [],
            oneMeds: [],
            sixMeds: [],
            tenMeds: [],
            medicines: false,
            pressButton: '',
            sum: 0,
            phone: "",
            otpToken: ""
        };
    }

    async componentDidMount() {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});
        this.props.OnFetchUserMedicationList();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userMedicationListData) {
            this.setState({meds: nextProps.userMedicationListData.meds});
            var count = 0;
            nextProps.userMedicationListData.meds.forEach((med, index) => {
                count = count + parseInt(med.freq);
                /*console.log("State  : " + index + " : " + this.state.sum + " : " + parseInt(med.freq));
                this.setState(prevState => (
                    {
                        sum: parseInt(med.freq) + prevState.sum
                    }));*/
                console.log(count + " : " + parseInt(med.freq));
            });

            this.setState({sum: count});
            console.log(" Sum Is : " + this.state.sum);
        }
    }

    static navigationOptions = {
        header: null
    };

    onCheckboxValueChange = (value, placeholder, med) => {
        let stateVar;
        if (this.state.pressButton === 'eight') {
            stateVar = "eightMeds";
        } else if (this.state.pressButton === 'one') {
            stateVar = "oneMeds";
        } else if (this.state.pressButton === 'six') {
            stateVar = "sixMeds";
        } else if (this.state.pressButton === 'ten') {
            stateVar = "tenMeds";
        }
        if (value) {
            this.setState(
                {
                    [`${placeholder}`]: value,
                    [`${stateVar}`]: [
                        ...this.state[`${stateVar}`],
                        {
                            code: med.code,
                            customMed: med.customMed,
                            desc: med.desc,
                            freq: med.freq,
                            strength: med.strength,
                            tolerance: med.tolerance,
                            idx: med.idx,
                            dosetime1: med.dosetime1,
                            dosetime2: med.dosetime2,
                            dosetime3: med.dosetime3,
                            dosetime4: med.dosetime4,
                            image: med.image
                        }
                    ]
                },
                () => {
                    console.log("Update List", this.state[`${stateVar}`]);
                }
            );
        } else {
            this.setState({
                [`${placeholder}`]: undefined
            });
            if (this.state.pressButton === 'eight') {
                this.setState({eightMeds: this.state.eightMeds.filter(d => d.idx !== med.idx)});
            } else if (this.state.pressButton === 'one') {
                this.setState({oneMeds: this.state.oneMeds.filter(d => d.idx !== med.idx)});
            } else if (this.state.pressButton === 'six') {
                this.setState({sixMeds: this.state.sixMeds.filter(d => d.idx !== med.idx)});
            } else if (this.state.pressButton === 'ten') {
                this.setState({tenMeds: this.state.tenMeds.filter(d => d.idx !== med.idx)});
            }
        }
    };


    checkFreqOfMed = (id) => {
        let count = 0;
        if (this.state.eightMeds.filter((value) => {
            return value.idx === id;
        }).length > 0) {
            count++;
        }
        if (this.state.oneMeds.filter((value) => {
            return value.idx === id;
        }).length > 0) {
            count++;
        }
        if (this.state.sixMeds.filter((value) => {
            return value.idx === id;
        }).length > 0) {
            count++;
        }
        if (this.state.tenMeds.filter((value) => {
            return value.idx === id;
        }).length > 0) {
            count++;
        }

        return count;
    };

    updateMedicationList() {
        return axios.post(
            API_BASE_URL +
            `/updatemedication.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","meds":${JSON.stringify(
                this.state.meds
            )}}`,
            {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }
        );
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <HeaderView
                    name={translate("re_assign_meds_screen.re_assign_meds")}
                    navigation={this.props.navigation}
                />
                <ScrollView>
                    <View style={{flexDirection: "column"}}>
                        <View
                            style={{height: 50, flexDirection: "row", alignItems: "center"}}
                        >
                            <Text
                                style={{
                                    flex: 0.33,
                                    paddingTop: 15,
                                    paddingBottom: 15,
                                    textAlign: "center",
                                    fontSize: 16,
                                    backgroundColor:
                                        this.state.selectedText === "Automatic"
                                            ? colors.darkBlueColor
                                            : colors.whiteColor,
                                    color:
                                        this.state.selectedText === "Automatic"
                                            ? colors.whiteColor
                                            : colors.blackColor
                                }}
                                onPress={() => {
                                    this.setState({selectedText: "Automatic"});
                                }}
                            >
                                {translate("re_assign_meds_screen.automatic")}
                            </Text>
                            <View
                                style={{
                                    width: 1,
                                    backgroundColor: colors.greyColor,
                                    height: 50
                                }}
                            />
                            <Text
                                style={{
                                    flex: 0.33,
                                    textAlign: "center",
                                    paddingTop: 15,
                                    paddingBottom: 15,
                                    fontSize: 16,
                                    backgroundColor:
                                        this.state.selectedText === "Reset"
                                            ? colors.darkBlueColor
                                            : colors.whiteColor,
                                    color:
                                        this.state.selectedText === "Reset"
                                            ? colors.whiteColor
                                            : colors.blackColor
                                }}
                                onPress={() => {
                                    this.setState({selectedText: "Reset", reset: true});
                                }}
                            >
                                {translate("re_assign_meds_screen.reset")}
                            </Text>
                            <View
                                style={{
                                    width: 1,
                                    backgroundColor: colors.greyColor,
                                    height: 50
                                }}
                            />
                            <Text
                                style={{
                                    flex: 0.34,
                                    textAlign: "center",
                                    paddingTop: 15,
                                    paddingBottom: 15,
                                    fontSize: 16,
                                    backgroundColor:
                                        this.state.selectedText === "Accept"
                                            ? colors.darkBlueColor
                                            : colors.whiteColor,
                                    color:
                                        this.state.selectedText === "Accept"
                                            ? colors.whiteColor
                                            : colors.blackColor
                                }}
                                onPress={() => {
                                    this.setState({selectedText: "Automatic", assign: true});
                                }}
                            >
                                {translate("re_assign_meds_screen.accept")}
                            </Text>
                        </View>
                    </View>
                    <View style={{height: 1, backgroundColor: colors.greyColor}}/>

                    <View style={{margin: 20, flexDirection: 'column'}}>
                        <Card>
                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.medicine_frequency")}
                                    </Text>
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.whiteColor}}/>
                                <View style={{flex: 0.4}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.default_times")}
                                    </Text>
                                </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    {
                                        this.state.eightMeds.length > 0 ?
                                            this.state.eightMeds.map(med => {
                                                return (
                                                    <View style={{flexDirection: 'row', flex: 1}}>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.8
                                                        }}>
                                                            {med.desc}
                                                        </Text>
                                                        <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.2
                                                        }}>
                                                            {med.freq}
                                                        </Text>
                                                    </View>
                                                )
                                            })
                                            :
                                            <Text style={{textAlign: 'center'}}>
                                                {translate("re_assign_meds_screen.no_meds_assigned")}
                                            </Text>
                                    }
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.setState({pressButton: 'eight', medicines: true});
                                }}>
                                    <View style={{flex: 0.4, justifyContent: 'center'}}>
                                        <Text
                                            style={{
                                                backgroundColor: colors.whiteColor,
                                                padding: 10,
                                                color: colors.blackColor,
                                                fontSize: 16,
                                                textAlign: 'center'
                                            }}>
                                            08:00 AM
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                        </Card>

                        <Card>
                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.medicine_frequency")}
                                    </Text>
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.whiteColor}}/>
                                <View style={{flex: 0.4}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.default_times")}
                                    </Text>
                                </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    {
                                        this.state.oneMeds.length > 0 ?
                                            this.state.oneMeds.map(med => {
                                                return (
                                                    <View style={{flexDirection: 'row', flex: 1}}>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.8
                                                        }}>
                                                            {med.desc}
                                                        </Text>
                                                        <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.2
                                                        }}>
                                                            {med.freq}
                                                        </Text>
                                                    </View>
                                                )
                                            })
                                            :
                                            <Text style={{textAlign: 'center'}}>
                                                {translate("re_assign_meds_screen.no_meds_assigned")}
                                            </Text>
                                    }
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.setState({pressButton: 'one', medicines: true});
                                }}>
                                    <View style={{flex: 0.4, justifyContent: 'center'}}>
                                        <Text
                                            style={{
                                                backgroundColor: colors.whiteColor,
                                                padding: 10,
                                                color: colors.blackColor,
                                                fontSize: 16,
                                                textAlign: 'center'
                                            }}
                                        >
                                            01:00 PM
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                        </Card>

                        <Card>
                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.medicine_frequency")}
                                    </Text>
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.whiteColor}}/>
                                <View style={{flex: 0.4}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.default_times")}
                                    </Text>
                                </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    {
                                        this.state.sixMeds.length > 0 ?
                                            this.state.sixMeds.map(med => {
                                                return (
                                                    <View style={{flexDirection: 'row', flex: 1}}>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.8
                                                        }}>
                                                            {med.desc}
                                                        </Text>
                                                        <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.2
                                                        }}>
                                                            {med.freq}
                                                        </Text>
                                                    </View>
                                                )
                                            })
                                            :
                                            <Text style={{textAlign: 'center'}}>
                                                {translate("re_assign_meds_screen.no_meds_assigned")}
                                            </Text>
                                    }
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.setState({pressButton: 'six', medicines: true});
                                }}>
                                    <View style={{flex: 0.4, justifyContent: 'center'}}>
                                        <Text
                                            style={{
                                                backgroundColor: colors.whiteColor,
                                                padding: 10,
                                                color: colors.blackColor,
                                                fontSize: 16,
                                                textAlign: 'center'
                                            }}
                                        >
                                            06:00 PM
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                        </Card>

                        <Card>
                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.medicine_frequency")}
                                    </Text>
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.whiteColor}}/>
                                <View style={{flex: 0.4}}>
                                    <Text
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        {translate("re_assign_meds_screen.default_times")}
                                    </Text>
                                </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{flex: 0.6}}>
                                    {
                                        this.state.tenMeds.length > 0 ?
                                            this.state.tenMeds.map(med => {
                                                return (
                                                    <View style={{flexDirection: 'row', flex: 1}}>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.8
                                                        }}>
                                                            {med.desc}
                                                        </Text>
                                                        <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                                        <Text style={{
                                                            padding: 10,
                                                            color: colors.blackColor,
                                                            fontSize: 16,
                                                            flex: 0.2
                                                        }}>
                                                            {med.freq}
                                                        </Text>
                                                    </View>
                                                )
                                            })
                                            :
                                            <Text style={{textAlign: 'center'}}>
                                                {translate("re_assign_meds_screen.no_meds_assigned")}
                                            </Text>
                                    }
                                </View>
                                <View style={{flex: 0.002, backgroundColor: colors.greyColor}}/>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.setState({pressButton: 'ten', medicines: true});
                                }}>
                                    <View style={{flex: 0.4, justifyContent: 'center'}}>
                                        <Text
                                            style={{
                                                backgroundColor: colors.whiteColor,
                                                padding: 10,
                                                color: colors.blackColor,
                                                fontSize: 16,
                                                textAlign: 'center'
                                            }}
                                        >
                                            10:00 PM
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                        </Card>

                        {/*<Card>
                            <View style={{flex: 100, flexDirection: 'row'}}>
                                <View style={{flex: 60}}>
                                    <Grid>
                                        <Row style={{height: 40}}>
                                            <Col size={59.9}>
                                                <Text
                                                    style={{
                                                        backgroundColor: colors.blueButtonColor,
                                                        padding: 10,
                                                        color: colors.whiteColor,
                                                        fontSize: 16
                                                    }}
                                                >
                                                    Medicine Frequency
                                                </Text>
                                            </Col>
                                            <Col size={0.1}>
                                                <View style={{backgroundColor: colors.whiteColor}}/>
                                            </Col>
                                        </Row>

                                        {this.state.eightMeds.length > 0 ? this.state.eightMeds.map(med => {
                                                return (
                                                    <Row
                                                        style={{
                                                            borderBottomColor: colors.greyColor,
                                                            borderBottomWidth: 1,
                                                            height: 40
                                                        }}>
                                                        <Col size={50}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.desc}
                                                            </Text>
                                                        </Col>
                                                        <Col size={9.9}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.freq}
                                                            </Text>
                                                        </Col>
                                                        <Col
                                                            size={0.1}
                                                            style={{backgroundColor: colors.greyColor}}
                                                        />
                                                    </Row>
                                                );
                                            }) :
                                            <View style={{
                                                justifyContent: 'center',
                                                alignSelf: 'center',
                                                alignItems: 'center',
                                                alignContent: 'center',
                                                height: 50
                                            }}>
                                                <Text style={{textAlign: 'center'}}>
                                                    No Meds are assigned yet
                                                </Text>
                                            </View>}
                                    </Grid>
                                </View>

                                <View style={{flex: 40, flexDirection: 'column', height: 200}}>
                                    <Text
                                        style={{
                                            height: 40,
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        Default Times
                                    </Text>
                                    <Text
                                        style={{
                                            padding: 8,
                                            color: colors.blackColor,
                                            fontSize: 16
                                        }}>
                                        08:00 AM
                                    </Text>
                                </View>
                            </View>
                        </Card>

                        <Card>
                            <View style={{flex: 100, flexDirection: 'row'}}>
                                <View style={{flex: 60}}>
                                    <Grid>
                                        <Row style={{height: 40}}>
                                            <Col size={59.9}>
                                                <Text
                                                    style={{
                                                        backgroundColor: colors.blueButtonColor,
                                                        padding: 10,
                                                        color: colors.whiteColor,
                                                        fontSize: 16
                                                    }}
                                                >
                                                    Medicine Frequency
                                                </Text>
                                            </Col>
                                            <Col size={0.1}>
                                                <View style={{backgroundColor: colors.whiteColor}}/>
                                            </Col>
                                        </Row>

                                        {this.state.oneMeds.length > 0 ? this.state.oneMeds.map(med => {
                                                return (
                                                    <Row
                                                        style={{
                                                            borderBottomColor: colors.greyColor,
                                                            borderBottomWidth: 1,
                                                            height: 40
                                                        }}
                                                    >
                                                        <Col size={50}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.desc}
                                                            </Text>
                                                        </Col>
                                                        <Col size={9.9}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.freq}
                                                            </Text>
                                                        </Col>
                                                        <Col
                                                            size={0.1}
                                                            style={{backgroundColor: colors.greyColor}}
                                                        />
                                                    </Row>
                                                );
                                            }) :
                                            <View style={{justifyContent: 'center'}}>
                                                <Text style={{textAlign: 'center'}}>
                                                    No Meds are assigned yet
                                                </Text>
                                            </View>}
                                    </Grid>
                                </View>

                                <View style={{flex: 40, flexDirection: 'column', height: 200}}>
                                    <Text
                                        style={{
                                            height: 40,
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        Default Times
                                    </Text>
                                    <Text
                                        style={{
                                            padding: 8,
                                            color: colors.blackColor,
                                            fontSize: 16,
                                            textAlign: 'center',
                                            alignSelf: 'center'
                                        }}
                                    >
                                        01:00 PM
                                    </Text>
                                </View>
                            </View>
                        </Card>

                        <Card>
                            <View style={{flex: 100, flexDirection: 'row'}}>
                                <View style={{flex: 60}}>
                                    <Grid>
                                        <Row style={{height: 40}}>
                                            <Col size={59.9}>
                                                <Text
                                                    style={{
                                                        backgroundColor: colors.blueButtonColor,
                                                        padding: 10,
                                                        color: colors.whiteColor,
                                                        fontSize: 16
                                                    }}
                                                >
                                                    Medicine Frequency
                                                </Text>
                                            </Col>
                                            <Col size={0.1}>
                                                <View style={{backgroundColor: colors.whiteColor}}/>
                                            </Col>
                                        </Row>

                                        {this.state.oneMeds.length > 0 ? this.state.oneMeds.map(med => {
                                                return (
                                                    <Row
                                                        style={{
                                                            borderBottomColor: colors.greyColor,
                                                            borderBottomWidth: 1,
                                                            height: 40
                                                        }}
                                                    >
                                                        <Col size={50}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.desc}
                                                            </Text>
                                                        </Col>
                                                        <Col size={9.9}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.freq}
                                                            </Text>
                                                        </Col>
                                                        <Col
                                                            size={0.1}
                                                            style={{backgroundColor: colors.greyColor}}
                                                        />
                                                    </Row>
                                                );
                                            }) :
                                            <View style={{justifyContent: 'center'}}>
                                                <Text style={{textAlign: 'center'}}>
                                                    No Meds are assigned yet
                                                </Text>
                                            </View>}
                                    </Grid>
                                </View>

                                <View style={{flex: 40, flexDirection: 'column', height: 200}}>
                                    <Text
                                        style={{
                                            height: 40,
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        Default Times
                                    </Text>
                                    <Text
                                        style={{
                                            padding: 8,
                                            color: colors.blackColor,
                                            fontSize: 16,
                                            textAlign: 'center',
                                            alignSelf: 'center'
                                        }}
                                    >
                                        06:00 PM
                                    </Text>
                                </View>
                            </View>
                        </Card>

                        <Card>
                            <View style={{flex: 100, flexDirection: 'row'}}>
                                <View style={{flex: 60}}>
                                    <Grid>
                                        <Row style={{height: 40}}>
                                            <Col size={59.9}>
                                                <Text
                                                    style={{
                                                        backgroundColor: colors.blueButtonColor,
                                                        padding: 10,
                                                        color: colors.whiteColor,
                                                        fontSize: 16
                                                    }}
                                                >
                                                    Medicine Frequency
                                                </Text>
                                            </Col>
                                            <Col size={0.1}>
                                                <View style={{backgroundColor: colors.whiteColor}}/>
                                            </Col>
                                        </Row>

                                        {this.state.oneMeds.length > 0 ? this.state.oneMeds.map(med => {
                                                return (
                                                    <Row
                                                        style={{
                                                            borderBottomColor: colors.greyColor,
                                                            borderBottomWidth: 1,
                                                            height: 40
                                                        }}
                                                    >
                                                        <Col size={50}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.desc}
                                                            </Text>
                                                        </Col>
                                                        <Col size={9.9}>
                                                            <Text
                                                                style={{
                                                                    padding: 10,
                                                                    color: colors.blackColor,
                                                                    fontSize: 16
                                                                }}
                                                            >
                                                                {med.freq}
                                                            </Text>
                                                        </Col>
                                                        <Col
                                                            size={0.1}
                                                            style={{backgroundColor: colors.greyColor}}
                                                        />
                                                    </Row>
                                                );
                                            }) :
                                            <View style={{justifyContent: 'center'}}>
                                                <Text style={{textAlign: 'center'}}>
                                                    No Meds are assigned yet
                                                </Text>
                                            </View>}
                                    </Grid>
                                </View>

                                <View style={{flex: 40, flexDirection: 'column', height: 200}}>
                                    <Text
                                        style={{
                                            height: 40,
                                            backgroundColor: colors.blueButtonColor,
                                            padding: 10,
                                            color: colors.whiteColor,
                                            fontSize: 16
                                        }}
                                    >
                                        Default Times
                                    </Text>
                                    <Text
                                        style={{
                                            padding: 8,
                                            color: colors.blackColor,
                                            fontSize: 16,
                                            textAlign: 'center',
                                            alignSelf: 'center'
                                        }}
                                    >
                                        10:00 PM
                                    </Text>
                                </View>
                            </View>
                        </Card>*/}
                    </View>

                    {/*This is Accept Dialog*/}
                    <View>
                        <Dialog visible={this.state.assign}>
                            <DialogContent>
                                <Icon
                                    name="x"
                                    size={30}
                                    color={colors.greyColor}
                                    style={{alignSelf: "flex-end", marginTop: 10}}
                                    onPress={() => {
                                        this.setState({assign: false});
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: 22,
                                        color: colors.blueButtonColor,
                                        marginTop: 10,
                                        backgroundColor: colors.whiteColor,
                                        alignSelf: "center"
                                    }}
                                >
                                    {translate("re_assign_meds_screen.med_monitor_assign_meds")}
                                </Text>

                                <Text style={{alignSelf: "center", padding: 10, color: colors.blackColor}}>
                                    {
                                        (this.state.sum === (this.state.eightMeds.length +
                                            this.state.oneMeds.length + this.state.sixMeds.length + this.state.tenMeds.length))
                                            ?
                                            `${translate("re_assign_meds_screen.all_meds_assigned")}`
                                            :
                                            `${translate("re_assign_meds_screen.all_meds_not_assigned")}`
                                    }

                                </Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        if (this.state.sum === (this.state.eightMeds.length +
                                            this.state.oneMeds.length + this.state.sixMeds.length + this.state.tenMeds.length)) {
                                            this.updateMedicationList();
                                            this.props.navigation.goBack();
                                        }
                                        this.setState({assign: false});
                                    }}
                                    style={styles.buttonStyle}
                                >
                                    <Text style={styles.textStyle}>{translate("login.ok")}</Text>
                                </TouchableOpacity>
                            </DialogContent>
                        </Dialog>
                    </View>

                    {/*This is Reset Dialog*/}
                    <View>
                        <Dialog visible={this.state.reset}>
                            <DialogContent>
                                <Icon name="x" size={30} color={colors.greyColor}
                                      style={{alignSelf: 'flex-end', marginTop: 10}}
                                      onPress={() => {
                                          this.setState({reset: false});
                                      }}/>
                                <Text style={{
                                    fontSize: 22,
                                    color: colors.blueButtonColor,
                                    marginTop: 10,
                                    backgroundColor: colors.whiteColor,
                                    alignSelf: 'center'
                                }}>
                                    {translate("re_assign_meds_screen.med_monitor_reset_med")}
                                </Text>

                                <Text style={{alignSelf: 'center', padding: 10, color: colors.blackColor}}>
                                    {translate("re_assign_meds_screen.reset_medicine_dose")}
                                </Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.state.meds.map(med => {
                                            if (this.state[`eight/${med.idx}`]) {
                                                this.setState({[`eight/${med.idx}`]: false});
                                            }
                                            if (this.state[`one/${med.idx}`]) {
                                                this.setState({[`one/${med.idx}`]: false});
                                            }
                                            if (this.state[`six/${med.idx}`]) {
                                                this.setState({[`six/${med.idx}`]: false});
                                            }
                                            if (this.state[`ten/${med.idx}`]) {
                                                this.setState({[`ten/${med.idx}`]: false});
                                            }
                                        });
                                        this.setState({
                                            eightMeds: [],
                                            oneMeds: [],
                                            sixMeds: [],
                                            tenMeds: [],
                                            reset: false,
                                            selectedText: "Automatic"
                                        });
                                    }}
                                    style={styles.buttonStyle}>
                                    <Text style={styles.textStyle}>{translate("re_assign_meds_screen.reset")}</Text>
                                </TouchableOpacity>
                            </DialogContent>
                        </Dialog>
                    </View>

                    {/*This Dialog to show all medicine list  to assign*/}
                    <View>
                        <Dialog visible={this.state.medicines} width={300} height={500}>
                            <DialogContent>
                                <View style={{flexDirection: 'column'}}>
                                    <Icon
                                        name="x"
                                        size={30}
                                        color={colors.greyColor}
                                        style={{alignSelf: "flex-end", marginTop: 10}}
                                        onPress={() => {
                                            this.setState({medicines: false});
                                        }}
                                    />
                                    <Text style={{
                                        fontSize: 22,
                                        color: colors.blueButtonColor,
                                        marginTop: 10,
                                        backgroundColor: colors.whiteColor,
                                        alignSelf: "center"
                                    }}>
                                        {translate("re_assign_meds_screen.select_medicine")}
                                    </Text>

                                    {
                                        this.props.user_med_list_loading &&
                                        <ActivityIndicator size={"large"}/>
                                    }

                                    {
                                        (!this.props.user_med_list_loading && this.state.meds.length === 0) &&
                                        <View
                                            style={{
                                                alignSelf: "center",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                flexDirection: "column",
                                                flex: 0,
                                                height: 300
                                            }}
                                        >
                                            <Image
                                                source={require("../images/pill_blue.png")}
                                                style={{width: 60, height: 60}}
                                            />
                                            <Text style={{
                                                marginLeft: 15,
                                                fontWeight: "600",
                                                color: colors.blackColor
                                            }}>
                                                {translate("manage_meds.no_medicine_found")}
                                            </Text>
                                        </View>
                                    }

                                    {
                                        (!this.props.user_med_list_loading && this.state.meds.length > 0)
                                        &&
                                        <FlatList style={{
                                            marginTop: 8,
                                            marginBottom: 20,
                                            height: 300
                                        }}
                                                  numColumns={1}
                                                  extraData={this.state}
                                                  data={this.state.meds}
                                                  renderItem={(item) =>
                                                      (
                                                          <View style={{flexDirection: 'column'}}>
                                                              <View
                                                                  style={{flexDirection: 'row', padding: 5}}>
                                                                  <CheckBox
                                                                      checked={this.state[`${this.state.pressButton}/${item.item.idx}`]}
                                                                      color={this.checkFreqOfMed(item.item.idx) > 0 ?
                                                                          (this.state[`${this.state.pressButton}Meds`].filter((value) => {
                                                                              return value.idx === item.item.idx;
                                                                          }).length > 0) ? colors.takenPillGreenColor
                                                                              :
                                                                              ((this.checkFreqOfMed(item.item.idx) === parseInt(item.item.freq))
                                                                                  ?
                                                                                  colors.takenPillGreenColor : colors.takenPillYellowColor)
                                                                          :
                                                                          colors.blackColor
                                                                      }
                                                                      onPress={() => {
                                                                          if ((this.state[`${this.state.pressButton}Meds`].filter((value) => {
                                                                                  return value.idx === item.item.idx;
                                                                              }).length > 0) ||
                                                                              this.checkFreqOfMed(item.item.idx) < item.item.freq) {
                                                                              this.setState({
                                                                                  [`${this.state.pressButton}/${item.item.idx}`]:
                                                                                      !this.state[`${this.state.pressButton}/${item.item.idx}`]
                                                                              });
                                                                              this.onCheckboxValueChange(
                                                                                  !this.state[`${this.state.pressButton}/${item.item.idx}`],
                                                                                  `${item.item.code}/${item.item.strength}`,
                                                                                  item.item
                                                                              );
                                                                          }
                                                                      }}
                                                                  />
                                                                  <Text style={{
                                                                      marginLeft: 12,
                                                                      marginTop: 2,
                                                                      color: this.checkFreqOfMed(item.item.idx) > 0 ?
                                                                          (this.state[`${this.state.pressButton}Meds`].filter((value) => {
                                                                              return value.idx === item.item.idx;
                                                                          }).length > 0) ? colors.takenPillGreenColor
                                                                              :
                                                                              ((this.checkFreqOfMed(item.item.idx) === parseInt(item.item.freq))
                                                                                  ?
                                                                                  colors.takenPillGreenColor : colors.takenPillYellowColor)
                                                                          :
                                                                          colors.blackColor
                                                                  }}> {item.item.desc}</Text>
                                                              </View>
                                                          </View>
                                                      )
                                                  }/>

                                    }

                                    <TouchableOpacity style={styles.buttonStyle}
                                                      onPress={() => {
                                                          this.setState({medicines: false});
                                                      }}>
                                        <Text style={styles.textStyle}>
                                            {translate("re_assign_meds_screen.done")}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </DialogContent>
                        </Dialog>
                    </View>
                </ScrollView>
                <FooterView isSummary={true} navigation={this.props.navigation}/>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        userMedicationListData:
        state.userMedicationListReducer.UsermedicationListData,
        user_med_list_loading: state.userMedicationListReducer.loading,
        user_med_list_error: state.userMedicationListReducer.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchUserMedicationList: () =>
            dispatch(UserMedicationListActions.fetchUserMedicationList())
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReAssignMedsScreen);

const styles = StyleSheet.create({
    listItemStyle: {
        flexDirection: "row",
        borderRadius: 30,
        elevation: 2,
        alignItems: "center",
        padding: 5
    },
    plusMinusStyle: {
        padding: 3,
        color: colors.blueButtonColor,
        fontSize: 20
    },
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 30,
        color: colors.whiteColor,
        marginBottom: 20,
        marginTop: 20
    },
    textStyle: {
        color: colors.whiteColor,
        fontSize: 20,
        alignSelf: "center",
        padding: 12
    }
});
