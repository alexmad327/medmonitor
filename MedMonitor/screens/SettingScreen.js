import React, {Component} from "react";
import {
    ActivityIndicator,
    AsyncStorage,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    Platform,
    Linking
} from "react-native";
import {Item, Picker} from "native-base";
import {WebView} from "react-native-webview";
import {API_BASE_URL, colors} from "../constants/Constants";
import Icon from "react-native-vector-icons/AntDesign";
import {Image} from "react-native-elements";
import Dialog, {DialogContent} from "react-native-popup-dialog";
import {HeaderView} from "../container/HeaderView";
import axios from "axios";
import {NavigationActions, StackActions} from "react-navigation";
import DeviceInfo from "react-native-device-info";
import AndroidOpenSettings from "react-native-android-open-settings";
import PushNotification from "react-native-push-notification";
import I18n, {translate} from "../locale/i18n";
import * as userPrefernceAction from "../store/actions/userPrefernceAction";
import {connect} from "react-redux";
import BackIcon from "react-native-vector-icons/Ionicons";

class SettingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logout: false,
            phone: "",
            otpToken: "",
            loading: false,
            privacyAndTerms: false,
            title: "",
            url: "",
            language: this.props.currentLanguage
        };
    }

    static navigationOptions = {
        header: null
    };

    async componentDidMount() {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});
    }

    deactivateUser() {
        this.setState({loading: true});
        axios
            .post(
                API_BASE_URL +
                `/deregister.php?version=3.0.0&data={"phone":"${this.state.phone.trim()}","otptoken":"${this.state.otpToken.trim()}"}`,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }
            )
            .then(async response => {
                // console.log("deactivateUser : " + JSON.stringify(response));
                if (response.data.ERROR === 0) {
                    this.setState({loading: false, logout: false});

                    await AsyncStorage.getAllKeys((error, keys) => {
                        keys.map(key => {
                            // console.log("Key : " + key);
                            AsyncStorage.removeItem(key);
                        });
                    });

                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({routeName: "Registration"})]
                    });
                    this.props.navigation.dispatch(resetAction);
                    PushNotification.cancelAllLocalNotifications();
                }
            })
            .catch(function (error) {
                console.log(error);
                this.setState({loading: false, logout: false});
            });
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1, flexDirection: "column"}}>
                <HeaderView name={translate("settings.setting")} navigation={this.props.navigation}/>
                <View style={{flexDirection: "row", padding: 10}}>
                    {/*<MainIcon name="mail" size={30}/>*/}
                    <View style={{
                        height: 52,
                        marginLeft: 3
                    }}>

                        <Picker
                            iosIcon={<BackIcon name="ios-arrow-down"/>}
                            style={{width: Platform.OS === 'ios' ? undefined : 150, height: 51}}
                            selectedValue={this.state.language}
                            placeholder="Select Language"
                            mode={"dialog"}
                            onValueChange={(itemValue, itemIndex) => {
                                this.setState({language: itemValue});
                                this.props.ToggleLangage(itemValue);
                                setTimeout(() => {
                                    alert("Language changes successfully.")
                                }, Platform.OS === 'ios' ? 1000 : 0);
                            }}>
                            <Picker.Item label="🇺🇸 English" value="en"/>
                            <Picker.Item label="🇪🇸 Spanish" value="sp"/>
                            <Picker.Item label="🇨🇳 Chinese" value="cn"/>
                        </Picker>

                    </View>
                </View>
                <View style={{flexDirection: "row", padding: 10}}>
                    <Image
                        source={require("../images/app.png")}
                        style={{width: 30, height: 30}}
                    />
                    {/*<Icon name="sound" size={30}/>*/}
                    <Text style={styles.textStyle}>
                        {`${translate("login.app_version")} ${DeviceInfo.getVersion()}`}
                    </Text>
                </View>
                <View style={{flexDirection: "row", padding: 10}}>
                    <Image
                        source={require("../images/sound.png")}
                        style={{width: 30, height: 30, resizeMode: "contain"}}
                    />
                    {/*<Icon name="sound" size={30}/>*/}
                    <Text
                        style={styles.textStyle}
                        onPress={() => {
                            if (Platform.OS === "ios") {
                                Linking.openURL('app-settings:');
                            } else {
                                // AndroidOpenSettings.generalSettings();
                                AndroidOpenSettings.appDetailsSettings()
                            }
                        }}
                    >
                        {translate("settings.sound_settings")}
                    </Text>
                </View>

                <View style={{flexDirection: "row", padding: 10}}>
                    <Image
                        source={require("../images/privacy.png")}
                        style={{width: 30, height: 30, resizeMode: "contain"}}
                    />
                    {/*<LockIcon name="lock" size={30}/>*/}
                    <Text
                        style={styles.textStyle}
                        onPress={() => {
                            this.setState({
                                privacyAndTerms: true,
                                url: "https://app.benesalustech.com/about/appprivacy.php",
                                title: translate("privacy_terms.pricacy_policy_title")
                            });
                        }}
                    >
                        {translate("settings.privacy_policy_and")}
                    </Text>
                    <Text
                        style={styles.termsConditionsStyle}
                        onPress={() => {
                            this.setState({
                                privacyAndTerms: true,
                                url: "https://app.benesalustech.com/about/apptc.php",
                                title: translate("privacy_terms.terms_title")
                            });
                        }}
                    >
                        {translate("settings.terms_conditn")}
                    </Text>
                </View>

                <View style={{flexDirection: "row", padding: 10}}>
                    <Image
                        source={require("../images/envelope.png")}
                        style={{width: 30, height: 30, resizeMode: "contain"}}
                    />
                    {/*<MainIcon name="mail" size={30}/>*/}
                    <Text style={styles.textStyle}>
                        {`${translate("settings.email_support")} - support@benesalustech.com`}

                        {/*{"Email Support - support@benesalustech.com"}*/}
                    </Text>
                </View>

                <TouchableWithoutFeedback
                    onPress={() => {
                        this.setState({logout: true});
                    }}
                >
                    <View style={{flexDirection: "row", padding: 10}}>
                        <Image
                            source={require("../images/logout.png")}
                            style={{width: 30, height: 30, resizeMode: "contain"}}
                        />
                        {/*<LockIcon name="lock" size={30}/>*/}
                        <Text style={styles.textStyle}>{translate("settings.logout")}</Text>
                    </View>
                </TouchableWithoutFeedback>

                {/*This is Logout Dialog*/}
                <View>
                    <Dialog visible={this.state.logout}>
                        <DialogContent>
                            <Icon
                                name="close"
                                size={30}
                                color={colors.greyColor}
                                style={{alignSelf: "flex-end", marginTop: 10}}
                                onPress={() => {
                                    this.setState({logout: false});
                                }}
                            />
                            <Text
                                style={{
                                    fontSize: 22,
                                    color: colors.blueButtonColor,
                                    marginTop: 10,
                                    backgroundColor: colors.whiteColor,
                                    alignSelf: "center",
                                    paddingLeft: 20,
                                    paddingRight: 20
                                }}
                            >
                                {translate("settings.med_monitor_logout")}
                            </Text>

                            <Text
                                style={{
                                    alignSelf: "center",
                                    padding: 10,
                                    color: "red",
                                    fontSize: 20
                                }}
                            >
                                {translate("manage_meds.are_you_sure")}
                            </Text>

                            <View
                                style={{
                                    flexDirection: "row",
                                    justifyContent: "center"
                                }}
                            >
                                <Image
                                    source={require("../images/tick_green.png")}
                                    style={{width: 40, height: 40, resizeMode: "contain"}}
                                />
                                <Image
                                    source={require("../images/red_cross.png")}
                                    style={{
                                        width: 40,
                                        height: 40,
                                        marginLeft: 10,
                                        resizeMode: "contain"
                                    }}
                                />
                            </View>

                            <Text
                                style={{textAlign: "center", fontSize: 16, marginTop: 10}}
                            >
                                {translate("settings.all_data_will_be_erased")}
                            </Text>

                            <Text style={{textAlign: "center", fontSize: 16}}>
                                {translate("settings.info_save_on_cloud")}
                            </Text>

                            <Text style={{textAlign: "center", fontSize: 16}}>
                                {translate("settings.need_to_re_register")}
                            </Text>

                            {!this.state.loading ? (
                                <TouchableOpacity
                                    onPress={() => {
                                        this.deactivateUser();
                                    }}
                                    style={styles.buttonStyle}
                                >
                                    <Text style={styles.dialogTextStyle}>{translate("settings.logout")}</Text>
                                </TouchableOpacity>
                            ) : (
                                <ActivityIndicator size="large" animating/>
                            )}
                        </DialogContent>
                    </Dialog>
                </View>

                {/*This is Privacy policy / Terms & conditions Dialog*/}
                <View>
                    <Dialog
                        visible={this.state.privacyAndTerms}
                        dialogStyle={{
                            marginTop: 50,
                            marginBottom: 50,
                            marginLeft: 20,
                            marginRight: 20
                        }}
                    >
                        <DialogContent>
                            <Icon
                                name="close"
                                size={30}
                                color={colors.greyColor}
                                style={{alignSelf: "flex-end", marginTop: 10}}
                                onPress={() => {
                                    this.setState({privacyAndTerms: false});
                                }}
                            />
                            <Text
                                style={{
                                    fontSize: 18,
                                    color: colors.blueButtonColor,
                                    marginTop: 10,
                                    backgroundColor: colors.whiteColor,
                                    alignSelf: "center",
                                    paddingLeft: 20,
                                    paddingRight: 20
                                }}
                            >
                                {this.state.title}
                            </Text>
                            <WebView
                                style={{marginTop: 8}}
                                source={{uri: this.state.url}}
                            />
                        </DialogContent>
                    </Dialog>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    console.log("STATTE", state);
    return {
        currentLanguage: state.userPreferences.language
    };
};
const mapDispatchToProps = dispatch => {
    return {
        ToggleLangage: (currentLanguage) => dispatch(userPrefernceAction.toggleLanguage(currentLanguage))
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingScreen);

const styles = StyleSheet.create({
    textStyle: {
        color: colors.blackColor,
        fontSize: 16,
        marginLeft: 16,
        alignContent: "center",
        padding: 5
    },
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 30,
        color: colors.whiteColor,
        marginTop: 20
    },
    dialogTextStyle: {
        color: colors.whiteColor,
        fontSize: 15,
        alignSelf: "center",
        padding: 12
    },
    termsConditionsStyle: {
        color: colors.blackColor,
        fontSize: 16,
        alignContent: "center",
        padding: 5
    }
});
