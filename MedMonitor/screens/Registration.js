import React, {Component} from "react";
import {
    ActivityIndicator,
    Alert,
    AsyncStorage,
    Keyboard,
    NetInfo,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import {API_BASE_URL, colors} from "../constants/Constants";
import Dialog, {DialogContent} from "react-native-popup-dialog";
import {Picker} from "native-base";
import axios from "axios";
import {translate} from "../locale/i18n";
import * as userPrefernceAction from "../store/actions/userPrefernceAction";
import {connect} from "react-redux";
import {WebView} from "react-native-webview";
import DeviceInfo from "react-native-device-info";
import {NavigationActions, StackActions} from "react-navigation";

class Registration extends Component {
    static navigationOptions = {
        header: null,
        phoneNo: "",
        smsCode: "",
        otpToken: ""
    };

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            textInput: true,
            verificationCode: false,
            loading: false,
            button: translate("login.get_registration_code"),
            privacyAndTerms: false,
            title: "",
            url: "",
            currentLanguage: this.props.currentLanguage,
            language: this.props.currentLanguage
        };
    }

    generateOTP() {
        this.setState({loading: true});
        axios
            .post(
                API_BASE_URL +
                `/genotp.php?version=3.0.0&data={"phone":"${this.state.phoneNo.trim()}"}`,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }
            )
            .then(async response => {
                console.log(response);
                let phone = this.state.phoneNo;
                /*AsyncStorage.multiSet(
                    [["otpToken", response.data.OTPTOKEN], ["phone", this.state.phoneNo]],
                    () => {
                        console.log("Data Set.!");
                    }
                );*/
                console.log("Rewsponse : " + JSON.stringify(response));
                if (response.data.ERROR === 0) {
                    await AsyncStorage.setItem("phone", phone, () => {
                        console.log("PHONE set!");
                    });
                    await AsyncStorage.setItem("otpToken", response.data.OTPTOKEN, () => {
                        console.log("OTP Token Set.!");
                    });
                    /*await AsyncStorage.setItem("phone", "8475550002", () => {
                        console.log("PHONE set!");
                    });
                    await AsyncStorage.setItem("otpToken", "5c77c43367af2", () => {
                        console.log("OTP Token Set.!");
                    });*/
                    /*AsyncStorage.multiSet(
                        [["otpToken", response.data.OTPTOKEN], ["phone", response.data.PhoneNum]],
                        () => {
                            console.log("Data Set.!");
                        }
                    );*/

                    this.setState({
                        visible: true,
                        loading: false,
                        textInput: true,
                        verificationCode: false,
                        // otpToken: "5c77c43367af2",
                        // phoneNo: "8475550002"
                        otpToken: response.data.OTPTOKEN,
                        phoneNo: phone
                    });
                }

                if (response.data.ERROR === 1) {
                    this.setState({loading: false});
                    `1`;
                    Alert.alert("Error", response.data.MESSAGE);
                }
            })
            .catch(function (error) {
                console.log(error);

                this.setState({loading: false});
                Alert.alert("Error : " + JSON.parse(error));
            });
    }

    checkValidation() {
        if (this.state.phoneNo !== undefined) {
            NetInfo.getConnectionInfo().then(connectionInfo => {
                if (connectionInfo.type !== "none") {
                    this.generateOTP();
                } else {
                    Alert.alert("", translate("internet.no_internet"));
                }
            });
        } else {
            Alert.alert("", translate("login.enter_valid_phone_number"));
        }
    }

    verifyOTP(navigate) {
        this.setState({loading: true});
        axios
            .post(
                API_BASE_URL +
                `/verifyotp.php?version=3.0.0&data={"phone":"${this.state.phoneNo.trim()}","otp":"${
                    this.state.smsCode
                    }","otptoken":"${this.state.otpToken.trim()}"}`,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }
            )
            .then(async (response) => {
                this.setState({loading: false});
                console.log(" Verify OTP response IS : ", JSON.stringify(response));
                if (response.data.ERROR === 0) {
                    await AsyncStorage.setItem("otpToken", response.data.TOKEN, () => {
                        console.log("OTP Token Set.!");
                    });
                    await AsyncStorage.setItem("isLoggedIn", JSON.stringify(true));
                    // navigate("HomeScreen");
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({routeName: "HomeScreen"})],
                    });
                    this.props.navigation.dispatch(resetAction);
                } else {
                    Alert.alert("", response.data.MESSAGE);
                }
            })
            .catch(function (error) {
                this.setState({loading: false});
                console.log(error);
            });
    }

    componentWillReceiveProps(nextProps) {
        console.log("NEXT PROPS", nextProps);
    }

    render() {
        const {navigate} = this.props.navigation;
        const {ToggleLangage, currentLanguage} = this.props;
        console.log("THIS", this.props);
        return (
            <View style={{flex: 1}}>
                <ScrollView keyboardShouldPersistTaps={"always"}>
                    <View
                        style={{
                            flex: 1,
                            marginLeft: 16,
                            marginRight: 16,
                            flexDirection: "column",
                            alignContent: "center",
                            marginTop: 20
                        }}
                    >
                        <Text style={styles.welcomeStyle}>
                            {translate("login.welcome")}
                        </Text>
                        <Text
                            style={{
                                color: colors.blueButtonColor,
                                alignSelf: "flex-end",
                                marginRight: Platform.OS === 'ios' ? 40 : 30
                            }}
                        >
                            {/*{translate("login.app_version")}*/}
                            {`${translate("login.app_version")} ${DeviceInfo.getVersion()}`}
                        </Text>

                        <View style={styles.phoneNumberStyle}>
                            <Icon
                                name="phone"
                                size={20}
                                color={colors.blackColor}
                                style={{alignSelf: "center", marginLeft: 10}}
                            />
                            <TextInput
                                returnKeyType="done"
                                keyboardType="phone-pad"
                                style={{
                                    paddingLeft: 10, width: "100%",
                                    shadowColor: '#FFFFFF',
                                    shadowOffset: {width: 0, height: 0},
                                    shadowOpacity: 0,
                                    shadowRadius: 0
                                }}
                                placeholder="123456789"
                                underlineColorAndroid="transparent"
                                value={this.state.phoneNo}
                                onChangeText={phoneNo => this.setState({phoneNo: phoneNo})}
                            />
                        </View>
                        {this.state.verificationCode ? (
                            <View>
                                <View style={styles.phoneNumberStyle}>
                                    <Icon
                                        size={30}
                                        color={colors.blackColor}
                                        style={{alignSelf: "center", marginLeft: 10}}
                                    />
                                    {/*<TextInput
                                        returnKeyType="done"
                                        keyboardType="number-pad"
                                        style={{paddingLeft: 10}}
                                        placeholder={translate("login.register_placeholder")}
                                        underlineColorAndroid="transparent"
                                        onChangeText={text => this.setState({smsCode: text})}
                                        value={this.state.smsCode}
                                    />*/}

                                    <TextInput
                                        returnKeyType="done"
                                        keyboardType="number-pad"
                                        style={{
                                            paddingLeft: 10, width: "100%",
                                            shadowColor: '#FFFFFF',
                                            shadowOffset: {width: 0, height: 0},
                                            shadowOpacity: 0,
                                            shadowRadius: 0
                                        }}
                                        placeholder={translate("login.register_placeholder")}
                                        underlineColorAndroid="transparent"
                                        value={this.state.smsCode}
                                        onChangeText={(text) => {
                                            this.setState({smsCode: text})
                                        }}/>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "column",
                                        marginTop: 10,
                                        marginLeft: 30,
                                        marginRight: 30,
                                        alignContent: "space-around"
                                    }}
                                >
                                    <Text style={{textAlign: "center"}}>
                                        {translate("login.reg_code_message")}
                                    </Text>
                                    <Text
                                        style={{
                                            color: colors.blueButtonColor,
                                            textAlign: "center"
                                        }}
                                    >
                                        {translate("login.resend")}
                                    </Text>
                                </View>
                            </View>
                        ) : null}
                        <Text
                            style={{
                                margin: 20,

                                alignContent: "center",
                                fontSize: 15,
                                textAlign: "center"
                            }}>
                            {}
                            {translate("login.agree_msg")}
                            <Text style={{textDecorationLine: "underline"}}
                                  onPress={() => {
                                      this.setState({
                                          privacyAndTerms: true,
                                          url: "https://app.benesalustech.com/about/appprivacy.php",
                                          title: `${translate("privacy_terms.pricacy_policy_title")}`
                                      })
                                  }}>
                                {" "}
                                {translate("login.privacy")}
                            </Text>{" "}
                            {translate("login.and")}{" "}
                            <Text style={{textDecorationLine: "underline"}} onPress={() => {
                                this.setState({
                                    privacyAndTerms: true,
                                    url: "https://app.benesalustech.com/about/apptc.php",
                                    title: `${translate("privacy_terms.terms_title")}`
                                })
                            }}>
                                {translate("login.tc")}
                            </Text>
                        </Text>
                        {!this.state.loading ? (
                            <TouchableOpacity
                                onPress={() => {
                                    Keyboard.dismiss();
                                    // Alert.alert(translate("login.get_registration_code") + this.state.button === translate("login.get_registration_code"));
                                    if (!this.state.verificationCode) {
                                        this.checkValidation();
                                    } else {
                                        // navigate('HomeScreen')
                                        if (this.state.smsCode !== undefined) {
                                            NetInfo.getConnectionInfo().then(connectionInfo => {
                                                if (connectionInfo.type !== "none") {
                                                    this.verifyOTP(navigate);
                                                } else {
                                                    Alert.alert("", translate("internet.no_internet"));
                                                    this.setState({verificationCode: false});
                                                }
                                            });
                                        } else {
                                            Alert.alert("", translate("login.enter_valid_sms_code"));
                                        }

                                    }
                                }}
                                style={styles.buttonStyle}
                            >
                                <Text style={[styles.textStyle, {fontSize: 15}]}>
                                    {/*{" "}*/}
                                    {!this.state.verificationCode ?
                                        translate("login.get_registration_code")
                                        :
                                        translate("login.register")}
                                </Text>
                            </TouchableOpacity>
                        ) : (
                            <ActivityIndicator size="large" animating/>
                        )}
                        <View style={styles.container}>
                            <Dialog
                                width={300}
                                style={{
                                    overlayBackgroundColor: colors.whiteColor,
                                    overlayOpacity: 0,
                                    marginLeft: 10,
                                    marginRight: 10,
                                    hasOverlay: false,
                                    opacity: 1
                                }}
                                visible={this.state.visible}
                                onTouchOutside={() => {
                                    this.setState({visible: false});
                                    this.setState({textInput: true});
                                    this.setState({verificationCode: false});
                                }}
                            >
                                <DialogContent>
                                    <Icon
                                        name="x"
                                        size={30}
                                        color="#ddd"
                                        style={{alignSelf: "flex-end", margin: 10}}
                                        onPress={() => {
                                            this.setState({
                                                visible: false,
                                                textInput: true,
                                                verificationCode: false
                                            });
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            color: colors.blueButtonColor,
                                            marginTop: 10,
                                            backgroundColor: colors.whiteColor,
                                            alignSelf: "center"
                                        }}
                                    >
                                        {translate("login.sms_code")}
                                    </Text>
                                    <Text
                                        style={{
                                            fontSize: 16,
                                            textAlign: "center",
                                            marginTop: 20
                                        }}
                                    >
                                        {translate("login.reg_code_sent")}
                                    </Text>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                visible: false,
                                                verificationCode: true,
                                                textInput: true,
                                                button: translate("login.register")
                                            });
                                        }}
                                        style={[styles.okbuttonStyle, {justifyContent: 'center'}]}>
                                        <Text style={{
                                            color: colors.whiteColor,
                                            fontSize: 15,
                                            alignSelf: "center"
                                        }}>
                                            {translate("login.ok")}
                                        </Text>
                                    </TouchableOpacity>
                                </DialogContent>
                            </Dialog>
                        </View>
                        {/*This is Privacy policy / Terms & conditions Dialog*/}
                        <View>
                            <Dialog visible={this.state.privacyAndTerms}
                                    dialogStyle={{marginTop: 50, marginBottom: 50, marginLeft: 20, marginRight: 20}}>
                                <DialogContent>
                                    <Icon name="x" size={30} color={colors.greyColor}
                                          style={{alignSelf: 'flex-end', marginTop: 10}}
                                          onPress={() => {
                                              this.setState({privacyAndTerms: false});
                                          }}/>
                                    <Text style={{
                                        fontSize: 18,
                                        color: colors.blueButtonColor,
                                        marginTop: 10,
                                        backgroundColor: colors.whiteColor,
                                        alignSelf: 'center',
                                        paddingLeft: 20,
                                        paddingRight: 20
                                    }}>
                                        {this.state.title}
                                    </Text>
                                    <WebView style={{marginTop: 8}}
                                             source={{uri: this.state.url}}/>
                                </DialogContent>
                            </Dialog>
                        </View>
                    </View>
                </ScrollView>
                <View
                    style={{
                        height: 50,
                        backgroundColor: colors.whiteColor,
                        bottom: 0,
                        marginTop: 10,
                        flexDirection: 'row'
                    }}
                >

                    <Text style={styles.emailText}>
                        {translate("login.email", {
                            email: "support@benesalustech.com"
                        })}
                    </Text>

                    <TouchableOpacity>
                        <View style={styles.popup}>
                            <Picker
                                style={{width: Platform.OS === 'ios' ? undefined : 150, height: 51}}
                                selectedValue={this.state.language}
                                placeholder="Select Language"
                                mode={"dialog"}
                                onValueChange={(itemValue, itemIndex) => {
                                    this.setState({language: itemValue});
                                    this.props.ToggleLangage(itemValue)
                                }}>
                                <Picker.Item label="🇺🇸 English" value="en"/>
                                <Picker.Item label="🇪🇸 Spanish" value="sp"/>
                                <Picker.Item label="🇨🇳 Chinese" value="cn"/>
                            </Picker>

                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    console.log("STATTE", state);
    return {
        currentLanguage: state.userPreferences.language
    };
};
const mapDispatchToProps = dispatch => {
    return {
        ToggleLangage: (currentLanguage) => dispatch(userPrefernceAction.toggleLanguage(currentLanguage))
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Registration);

const styles = StyleSheet.create({
    popup: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 20,
        marginBottom: 5,
        marginLeft: 10
    },
    welcomeStyle: {
        color: colors.blackColor,
        fontSize: 25,
        alignContent: "center",
        alignSelf: "center",
        marginTop: 50
    },
    phoneNumberStyle: {
        flexDirection: "row",
        marginLeft: 30,
        marginRight: 30,
        alignContent: "center",
        marginTop: 30,
        borderWidth: Platform.OS === 'ios' ? 3 : 1,
        borderRadius: 2,
        borderColor: '#ddd',
        elevation: 2,
        height: 50
    },
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 30,
        color: colors.whiteColor
    },
    okbuttonStyle: {
        backgroundColor: colors.blueButtonColor,
        height: 40,
        width: 100,
        alignSelf: "center",
        borderRadius: 30,
        color: colors.whiteColor,
        marginTop: 20
    },
    textStyle: {
        color: colors.whiteColor,
        fontSize: 15,
        alignSelf: "center",
        padding: 12
    },
    emailText: {
        marginLeft: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: colors.whiteColor,
        color: colors.blackColor,
        textAlign: "center"
    },
    iconStyle: {
        margin: 20
    },
    dialogTextStyle: {
        fontSize: 15,
        textAlign: "center",
        padding: 10
    },
    container: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.whiteColor,
        marginLeft: 10,
        marginRight: 10
    }
});
