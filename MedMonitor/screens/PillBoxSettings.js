import React, {Component} from "react";
import {
    ActivityIndicator,
    Alert,
    AsyncStorage,
    ScrollView,
    Slider,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {Card, Switch} from "native-base";
import {FooterView} from "../container/FooterView";
import {API_BASE_URL, colors} from "../constants/Constants";
import {HeaderView} from "../container/HeaderView";
import axios from "axios";
import {translate} from "../locale/i18n";

export class PillBoxSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            settings: true,
            sliderValue: 5,
            loading: false,
            audio: true,
            load: false,
            phone: "",
            otpToken: ""
        };
    }

    async componentDidMount() {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});
        AsyncStorage.getItem("snoozeMin", (error, result) => {
            //Alert.alert("Result : " + result + " : " + parseInt(JSON.parse(result)));
            if (result)
                this.setState({sliderValue: parseInt(JSON.parse(result))});
        });
    }

    static navigationOptions = {
        header: null,
    };

    async updateUserConfigs() {
        this.setState({loading: true});
        let phone = await AsyncStorage.getItem("phone");
        let otpToken = await AsyncStorage.getItem("otpToken");

        // let phone = "18475550002";
        // let otpToken = "5c77c43367af2";

        return axios
            .post(
                API_BASE_URL + `/userconfigupdate.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","snoozeMin":${this.state.sliderValue}}`,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }
            );
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <HeaderView name={translate("pillbox.configure")} navigation={this.props.navigation}/>
                <ScrollView style={{flex: 1}}>
                    <Text style={{paddingLeft: 20, paddingTop: 20, fontSize: 20, color: colors.blackColor}}>
                        {translate("pillbox.pillbox_setting")}
                    </Text>
                    {
                        this.state.settings ?
                            <View style={{margin: 20}}>
                                <Card>
                                    <Text style={{
                                        color: colors.whiteColor, backgroundColor: colors.blueButtonColor,
                                        paddingLeft: 20, paddingTop: 10, paddingBottom: 20, fontSize: 18
                                    }}>
                                        {translate("pillbox.not_connected")}
                                    </Text>
                                    <Text style={{paddingLeft: 20, paddingTop: 10, paddingBottom: 40, fontSize: 18}}>
                                        {translate("pillbox.num_approved_box")}
                                    </Text>
                                </Card>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({settings: false})
                                    }}
                                    style={styles.buttonStyle}>
                                    <Text style={styles.textStyle}>{translate("pillbox.pair_new")}</Text>
                                </TouchableOpacity>
                            </View>
                            : <View style={{margin: 20}}>
                                <Card>
                                    <Text style={{
                                        color: colors.whiteColor, backgroundColor: colors.blueButtonColor,
                                        paddingLeft: 20, paddingTop: 10, paddingBottom: 20, fontSize: 18
                                    }}>
                                        {`${translate("pillbox.snooze")} ${this.state.sliderValue} ${translate("pillbox.min")}`}
                                        {/*Snooze {this.state.sliderValue} Min*/}
                                    </Text>
                                    <Slider
                                        value={this.state.sliderValue}
                                        step={1}
                                        minimumValue={0}
                                        maximumValue={60}
                                        minimumTrackTintColor={colors.blueButtonColor}
                                        maximumTrackTintColor={colors.blueButtonColor}
                                        thumbTintColor={colors.blueButtonColor}
                                        onValueChange={(ChangedValue) => this.setState({sliderValue: ChangedValue})}
                                        style={{width: '100%', paddingLeft: 20, paddingTop: 10, paddingBottom: 40}}
                                    />
                                    <Text style={{
                                        color: colors.whiteColor, backgroundColor: colors.blueButtonColor,
                                        paddingLeft: 20, paddingTop: 10, paddingBottom: 20, fontSize: 16
                                    }}>
                                        {translate("pillbox.pillbox_ab")}
                                    </Text>
                                    <View style={{flexDirection: 'row', padding: 20}}>
                                        <Text style={{flex: 1}}>
                                            {translate("pillbox.audia_mode_buzzer")}
                                        </Text>
                                        <Switch
                                            value={this.state.audio}
                                            onValueChange={(value) => {
                                                this.setState({audio: value})
                                            }}/>
                                    </View>

                                    <View style={{flexDirection: 'row', padding: 20}}>
                                        <Text style={{flex: 1}}>
                                            {translate("pillbox.load_normal_mode")}
                                        </Text>
                                        <Switch value={this.state.load}
                                                onValueChange={(value) => {
                                                    this.setState({load: value})
                                                }}/>
                                    </View>
                                </Card>

                                {!this.state.loading ? (
                                    <TouchableOpacity
                                        onPress={() => {
                                            // this.setState({settings: false})
                                            this.updateUserConfigs()
                                                .then(response => {
                                                    this.setState({loading: false});
                                                    console.log(response);
                                                    if (response.data.ERROR === 2) {
                                                        this.setState({loading: false});
                                                        Alert.alert("Error", response.data.MESSAGE);
                                                    }

                                                    if (response.data.ERROR === 0) {
                                                        Alert.alert("", response.data.MESSAGE);
                                                        AsyncStorage.setItem(
                                                            "snoozeMin", this.state.sliderValue.toString(),
                                                            () => {
                                                                console.log("Snooze Min : " + this.state.sliderValue.toString());
                                                            }
                                                        );
                                                    }
                                                })
                                                .catch(function (error) {
                                                    console.log(error);
                                                    this.setState({loading: false});
                                                    Alert.alert("Error : " + JSON.parse(error));
                                                });
                                        }}
                                        style={styles.buttonStyle}>
                                        <Text style={styles.textStyle}>{translate("pillbox.snooze")}</Text>
                                    </TouchableOpacity>
                                ) : (
                                    <ActivityIndicator style={{marginTop: 20}} size="large" animating/>
                                )}
                            </View>
                    }
                </ScrollView>
                < FooterView isSummary={true}
                             navigation={this.props.navigation}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        width: 160,
        height: 50,
        borderRadius: 30,
        color: colors.whiteColor,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    textStyle: {
        color: colors.whiteColor,
        fontSize: 20,
        alignSelf: 'center',
        padding: 12
    }
});