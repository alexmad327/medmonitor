import React, {Component} from "react";
import {
    ActivityIndicator,
    Alert,
    AsyncStorage,
    FlatList,
    Image,
    ImageBackground,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {colors} from "../constants/Constants";
import CalendarStrip from "react-native-calendar-strip";
import moment from "moment";
import {Card, CardItem, Input, Item, Picker} from "native-base";
import {FooterView} from "../container/FooterView";
import {Text} from "react-native-elements";
import Icon from "react-native-vector-icons/AntDesign";
import Dialog, {DialogContent} from "react-native-popup-dialog";
import * as GetBiometricTypesListActions from "../store/actions/GetBiometricTypes";
import * as GetTodayMedicationActions from "../store/actions/GetTodayMedicationAction";
import {CommonActions} from "../store/actions/CommonRequestAction";
import {connect} from "react-redux";
import DateTimePicker from "react-native-modal-datetime-picker";
import * as UserMedicationListActions from "../store/actions/UserMedicationListAction";
import {NavigationEvents} from "react-navigation";
import I18n, {translate} from "../locale/i18n";
import * as DosageScheduleAction from "../store/actions/DosageScheduleAction";
import * as _ from "lodash";
import Ionicons from "react-native-vector-icons/Ionicons"

class DailyActivityScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            selectedDate: moment().toDate(),
            selectedDay: `${translate("daily_activity.today")}`,
            dateFormat: "",
            payload: [],
            data: [],
            selected2: "Post Meal",
            bioMetricTypes: [],
            adding_bio_metric: false,
            valuesArray: [],
            pickerArray: [],
            errors: {},
            blood_sugar_value: "",
            userName: "",
            doctorName: "",
            agencyName: "",
            userPicture: "",
            todayMedicationListData: {},
            showtime: false,
            currentday: [],
            yesterday: [],
            dayBefore: [],
            freq1: [],
            freq2: [],
            freq3: [],
            freq4: [],
            showMedicineList: false,
            selectedCelltime: {},
            userMeds: [],
            pillDoseTime: moment(new Date()).format("HH:mm"),
            isDateTimePickerVisible: false,
            meds: [],
            timeLoader: false,
            phone: "",
            otpToken: "",
            box: "Red Box",
            dosageSchedules: {},
            freqArray: [],
            tolerance1: "",
            tolerance2: "",
            tolerance3: "",
            tolerance4: "",
            eight: "",
            one: "",
            six: "",
            ten: "",
            eightDate1: new Date(),
            eightDate2: new Date(),
            oneDate1: new Date(),
            oneDate2: new Date(),
            sixDate1: new Date(),
            sixDate2: new Date(),
            tenDate1: new Date(),
            tenDate2: new Date(),
            eightDate: new Date(),
            oneDate: new Date(),
            sixDate: new Date(),
            tenDate: new Date(),
            apiCall: 1,
            loading: false,
            lastDayObject: {},
            maxFreq: 0
        };
    }

    async componentDidMount() {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});

        this.setState({
            dateFormat:
                translate("daily_activity.today") + ", " +
                new Date().getDate() +
                " " +
                moment(new Date()).format("MMMM")
        });
        AsyncStorage.getItem("username", (error, result) =>
            this.setState({userName: JSON.parse(result)})
        );
        AsyncStorage.getItem("doctorName", (error, result) =>
            this.setState({doctorName: JSON.parse(result)})
        );
        AsyncStorage.getItem("agencyName", (error, result) =>
            this.setState({agencyName: JSON.parse(result)})
        );
        AsyncStorage.getItem("userPicture", (error, result) =>
            this.setState({userPicture: JSON.parse(result)})
        );

        this.setState({apiCall: 1, loading: true});
        this.props.OnFetchDosageSchedules();

        this.props.OnFetchBioMetricTypeList();

        /*this.props.OnFetchTodayMedicationList({
            lastDate: moment(new Date()).format("YYYYMMDD"),
            lastDay: 3
        });*/
    }

    async componentWillReceiveProps(nextProps) {
        if (nextProps.dosageSchedules != null && this.state.apiCall === 1) {
            console.log("DOsage Shcedules : " + JSON.stringify(nextProps.dosageSchedules));
            this.setState({dosageSchedules: nextProps.dosageSchedules, apiCall: 2});
            this.props.OnFetchUserMedicationList();
            AsyncStorage.setItem("dosageSchedules", JSON.stringify(nextProps.dosageSchedules))
        }

        let today = new Date();
        let yesterday = moment(new Date())
            .subtract(1, "days")
            .toDate();
        let dayBefor = moment(new Date())
            .subtract(2, "days")
            .toDate();
        if (nextProps.biometricListData) {
            this.setState({bioMetricTypes: nextProps.biometricListData});
        }

        if (nextProps.todayMedicationListData && this.state.apiCall === 3) {
            this.setState({loading: false});
            // nextProps.todayMedicationListData = this.state.todayMedicationListData;
            console.log("Dashboard todayMedicationListData : " + JSON.stringify(nextProps.todayMedicationListData));
            this.setState({
                todayMedicationListData: nextProps.todayMedicationListData
            });

            await Object.keys(nextProps.todayMedicationListData).map(async d => {
                let length = this.state.maxFreq;

                if (length === 0) {
                    length = Object.keys(
                        nextProps.todayMedicationListData[d].dosage_slots
                    ).length;
                }

                if (Object.keys(
                    nextProps.todayMedicationListData[d].dosage_slots
                ).length < length) {
                    length = Object.keys(
                        nextProps.todayMedicationListData[d].dosage_slots
                    ).length;
                }

                if (length > 0) {
                    await this.setState({lastDayObject: nextProps.todayMedicationListData[d]});
                    console.log("Last Object is : D : " + d + " : " + JSON.stringify(this.state.lastDayObject))
                    // alert(JSON.stringify(this.state.lastDayObject))
                }

                /*let o = nextProps.todayMedicationListData[d];
                if (!_.values(o).some(x => x !== undefined)) {
                    // this.setState({lastDayObject: nextProps.todayMedicationListData[d]});
                    console.log("lastDayObject Is : " + JSON.stringify(nextProps.todayMedicationListData[d]));
                } // true
*/
                console.log(" In D Key Loop");

                Object.keys(nextProps.todayMedicationListData[d].dosage_slots).map(
                    async k => {
                        console.log(" In K Key Loop");
                        let pillTime = "";
                        let boxColor = "";
                        var takenDate = new Date(
                            today.getFullYear(),
                            today.getMonth(),
                            today.getDate(),
                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time.split(":")[0],
                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time.split(":")[1],
                            0
                        );
                        if (nextProps.todayMedicationListData[d].dosage_slots[k]
                            .dosage_slot === "1") {
                            pillTime = this.state.eight;
                            if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time) && nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time.indexOf(":") > -1) {
                                if (takenDate >= this.state.eightDate1 && takenDate <= this.state.eightDate2) {
                                    boxColor = "Green";
                                } else if (takenDate < this.state.eightDate1 || takenDate > this.state.eightDate2) {
                                    boxColor = "Yellow";
                                } else {
                                    boxColor = "Red";
                                }
                            } else {
                                boxColor = "Red";
                            }
                            /*if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.eightDate > this.state.eightDate1 && this.state.eightDate < this.state.eightDate2)) {
                                boxColor = "Green";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.eightDate > this.state.eightDate1)) {
                                boxColor = "Yellow";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.eightDate < this.state.eightDate1)) {
                                boxColor = "Grey";
                            } else {
                                boxColor = "Red";
                            }*/
                        } else if (nextProps.todayMedicationListData[d].dosage_slots[k]
                            .dosage_slot === "2") {
                            pillTime = this.state.one;
                            if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time) && nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time.indexOf(":") > -1) {
                                if (takenDate >= this.state.oneDate1 && takenDate <= this.state.oneDate2) {
                                    boxColor = "Green";
                                } else if (takenDate < this.state.oneDate1 || takenDate > this.state.oneDate2) {
                                    boxColor = "Yellow";
                                } else {
                                    boxColor = "Red";
                                }
                            } else {
                                boxColor = "Red";
                            }
                            /*if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.oneDate > this.state.oneDate1 && this.state.oneDate < this.state.oneDate2)) {
                                boxColor = "Green";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.oneDate > this.state.oneDate1)) {
                                boxColor = "Yellow";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.oneDate < this.state.oneDate1)) {
                                boxColor = "Grey";
                            } else {
                                boxColor = "Red";
                            }*/
                        } else if (nextProps.todayMedicationListData[d].dosage_slots[k]
                            .dosage_slot === "3") {
                            pillTime = this.state.six;
                            if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time) && nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time.indexOf(":") > -1) {
                                if (takenDate >= this.state.sixDate1 && takenDate <= this.state.sixDate2) {
                                    boxColor = "Green";
                                } else if (takenDate < this.state.sixDate1 || takenDate > this.state.sixDate2) {
                                    boxColor = "Yellow";
                                } else {
                                    boxColor = "Red";
                                }
                            } else {
                                boxColor = "Red";
                            }
                            /*if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.sixDate > this.state.sixDate1 && this.state.sixDate < this.state.sixDate2)) {
                                boxColor = "Green";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.sixDate > this.state.sixDate1)) {
                                boxColor = "Yellow";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.sixDate < this.state.sixDate1)) {
                                boxColor = "Grey";
                            } else {
                                boxColor = "Red";
                            }*/
                        } else if (nextProps.todayMedicationListData[d].dosage_slots[k]
                            .dosage_slot === "4") {
                            pillTime = this.state.ten;
                            if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time) && nextProps.todayMedicationListData[d].dosage_slots[k]
                                .taken_time.indexOf(":") > -1) {
                                if (takenDate >= this.state.tenDate1 && takenDate <= this.state.tenDate2) {
                                    boxColor = "Green";
                                } else if (takenDate < this.state.tenDate1 || takenDate > this.state.tenDate2) {
                                    boxColor = "Yellow";
                                } else {
                                    boxColor = "Red";
                                }
                            } else {
                                boxColor = "Red";
                            }
                            /*if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.tenDate > this.state.tenDate1 && this.state.tenDate < this.state.tenDate2)) {
                                boxColor = "Green";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.tenDate > this.state.tenDate1)) {
                                boxColor = "Yellow";
                            } else if (!_.isEmpty(nextProps.todayMedicationListData[d].dosage_slots[k]
                                    .taken_time) &&
                                (this.state.tenDate < this.state.tenDate1)) {
                                boxColor = "Grey";
                            } else {
                                boxColor = "Red";
                            }*/
                        }

                        console.log("Color : ", boxColor, " PillTime : ", pillTime);

                        if (_.isEmpty(pillTime))
                            pillTime = k;

                        if (d === moment(new Date()).format("YYYYMMDD")) {
                            console.log("In Current Day : " + length);
                            // alert(this.state.currentday.length + "Length : " + length);
                            if (this.state.currentday.length < length) {
                                var date = new Date(
                                    today.getFullYear(),
                                    today.getMonth(),
                                    today.getDate(),
                                    k.split(":")[0],
                                    k.split(":")[1],
                                    0
                                );
                                let status =
                                    nextProps.todayMedicationListData[d].dosage_slots[k]
                                        .taken_status;

                                if (
                                    today.getHours() < date.getHours() ||
                                    (today.getHours() === date.getHours() &&
                                        today.getMinutes() < date.getMinutes())
                                ) {
                                    status = `${translate("daily_activity.future")}`;
                                    boxColor = "Grey";
                                }

                                this.setState(prevState => ({
                                    currentday: [
                                        ...prevState.currentday,
                                        {
                                            pillTime: pillTime,
                                            status: status,
                                            takenTime:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .taken_time,
                                            dosageSlot:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .dosage_slot,
                                            boxColor: boxColor
                                        }
                                    ]
                                }));
                            }
                        } else if (d === moment(yesterday).format("YYYYMMDD")) {
                            if (this.state.yesterday.length < length) {
                                console.log("In YESTERDAY  Day");
                                this.setState(prevState => ({
                                    yesterday: [
                                        ...prevState.yesterday,
                                        {
                                            pillTime: pillTime,
                                            status:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .taken_status,
                                            takenTime:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .taken_time,
                                            dosageSlot:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .dosage_slot,
                                            boxColor: boxColor
                                        }
                                    ]
                                }));
                            }
                        } else if (d === moment(dayBefor).format("YYYYMMDD")) {
                            if (this.state.dayBefore.length < length) {
                                console.log("In Day Before");
                                this.setState(prevState => ({
                                    dayBefore: [
                                        ...prevState.dayBefore,
                                        {
                                            pillTime: pillTime,
                                            status:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .taken_status,
                                            takenTime:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .taken_time,
                                            dosageSlot:
                                            nextProps.todayMedicationListData[d].dosage_slots[k]
                                                .dosage_slot,
                                            boxColor: boxColor
                                        }
                                    ]
                                }));
                            }
                        }
                    }
                );
            });

            const startDate = moment().add(-30, 'days'); //YYYY-MM-DD
            const endDate = moment().toDate(); //YYYY-MM-DD

            const array = await this.getDateArray(startDate, endDate);

            console.log("Dates Array IS : " + JSON.stringify(array));

            var lastObj = {};

            _.forEachRight(array, (value) => {
                var key = moment(value).format("YYYYMMDD");
                console.log("Value : " + value + " KEY : " + key);

                if (nextProps.todayMedicationListData[key]) {
                    if (!_.isEmpty(nextProps.todayMedicationListData[key].dosage_slots)) {
                        lastObj = nextProps.todayMedicationListData[key];
                        console.log("Last object has assign value")
                    } else {
                        if (!_.isEmpty(lastObj)) {
                            console.log("Last objet is not empty");

                            Object.keys(lastObj.dosage_slots).map(
                                async k => {
                                    lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                    lastObj.dosage_slots[k].taken_time = "";
                                    lastObj.dosage_slots[k].updated_on_server = 0;
                                    lastObj.dosage_slots[k].updated_time = "";
                                }
                            );

                            nextProps.todayMedicationListData[key] = lastObj;
                        }
                    }
                } else {
                    if (!_.isEmpty(lastObj)) {
                        console.log("Last objet is not empty");

                        Object.keys(lastObj.dosage_slots).map(
                            async k => {
                                lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                lastObj.dosage_slots[k].taken_time = "";
                                lastObj.dosage_slots[k].updated_on_server = 0;
                                lastObj.dosage_slots[k].updated_time = "";
                            }
                        );
                        nextProps.todayMedicationListData[key] = lastObj;
                    }
                }
            });

            _.forEach(array, (value) => {
                var key = moment(value).format("YYYYMMDD");
                console.log("Value : " + value + " KEY : " + key);

                if (nextProps.todayMedicationListData[key]) {
                    if (!_.isEmpty(nextProps.todayMedicationListData[key].dosage_slots)) {
                        lastObj = nextProps.todayMedicationListData[key];
                        console.log("Last object has assign value")
                    } else {
                        if (!_.isEmpty(lastObj)) {
                            console.log("Last objet is not empty");

                            Object.keys(lastObj.dosage_slots).map(
                                async k => {
                                    lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                    lastObj.dosage_slots[k].taken_time = "";
                                    lastObj.dosage_slots[k].updated_on_server = 0;
                                    lastObj.dosage_slots[k].updated_time = "";
                                }
                            );

                            nextProps.todayMedicationListData[key] = lastObj;
                        }
                    }
                } else {
                    if (!_.isEmpty(lastObj)) {
                        console.log("Last objet is not empty");

                        Object.keys(lastObj.dosage_slots).map(
                            async k => {
                                lastObj.dosage_slots[k].taken_status = "NOT_TAKEN";
                                lastObj.dosage_slots[k].taken_time = "";
                                lastObj.dosage_slots[k].updated_on_server = 0;
                                lastObj.dosage_slots[k].updated_time = "";
                            }
                        );
                        nextProps.todayMedicationListData[key] = lastObj;
                    }
                }
            });


            /*for (var j = array.length - 1; j >= 0; j--) {
                if (nextProps.todayMedicationListData[moment(array[j]).format("YYYYMMDD")]) {
                    if (!_.isEmpty(nextProps.todayMedicationListData[moment(array[j]).format("YYYYMMDD")].dosage_slots)) {
                        lastObj = nextProps.todayMedicationListData[moment(array[j]).format("YYYYMMDD")];
                    } else {
                        if (!_.isEmpty(lastObj)) {
                            nextProps.todayMedicationListData[moment(array[j]).format("YYYYMMDD")] = lastObj;
                        }
                    }
                }
            }*/

            if (_.isEmpty(this.state.lastDayObject)) {
                this.setDefaultDosageSlots();
            } else {
                // alert("last object is not empty" + JSON.stringify(this.state.lastDayObject));
                if (this.state.currentday.length === 0) {
                    if (this.state.lastDayObject) {
                        Object.keys(this.state.lastDayObject.dosage_slots).map(
                            async k => {
                                let pillTime = "";
                                let boxColor = "";
                                if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "1") {
                                    pillTime = this.state.eight;
                                    boxColor = "Red";
                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "2") {
                                    pillTime = this.state.one;
                                    boxColor = "Red";

                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "3") {
                                    pillTime = this.state.six;
                                    boxColor = "Red";
                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "4") {
                                    pillTime = this.state.ten;
                                    boxColor = "Red";
                                }

                                console.log("Color : ", boxColor, " PillTime : ", pillTime);

                                var date = new Date(
                                    today.getFullYear(),
                                    today.getMonth(),
                                    today.getDate(),
                                    k.split(":")[0],
                                    k.split(":")[1],
                                    0
                                );
                                let status = "NOT_TAKEN";

                                if (
                                    today.getHours() < date.getHours() ||
                                    (today.getHours() === date.getHours() &&
                                        today.getMinutes() < date.getMinutes())
                                ) {
                                    status = `${translate("daily_activity.future")}`;
                                    boxColor = "Grey";
                                }

                                // alert("maxLength : " + this.state.maxLength);
                                this.setState(prevState => ({
                                    currentday: [
                                        ...prevState.currentday,
                                        {
                                            pillTime: k,
                                            status: status,
                                            takenTime: "",
                                            dosageSlot: this.state.lastDayObject.dosage_slots[k].dosage_slot,
                                            boxColor: boxColor
                                        }
                                    ]
                                }));
                            }
                        );
                    }
                }
                if (this.state.yesterday.length === 0) {
                    if (this.state.lastDayObject) {
                        Object.keys(this.state.lastDayObject.dosage_slots).map(
                            async k => {
                                let pillTime = "";
                                let boxColor = "";
                                if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "1") {
                                    pillTime = this.state.eight;
                                    boxColor = "Red";
                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "2") {
                                    pillTime = this.state.one;
                                    boxColor = "Red";

                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "3") {
                                    pillTime = this.state.six;
                                    boxColor = "Red";
                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "4") {
                                    pillTime = this.state.ten;
                                    boxColor = "Red";
                                }

                                console.log("Color : ", boxColor, " PillTime : ", k);

                                let status = "NOT_TAKEN";

                                this.setState(prevState => ({
                                    yesterday: [
                                        ...prevState.yesterday,
                                        {
                                            pillTime: k,
                                            status: status,
                                            takenTime: "",
                                            dosageSlot: this.state.lastDayObject.dosage_slots[k].dosage_slot,
                                            boxColor: boxColor
                                        }
                                    ]
                                }));
                            }
                        );
                    }
                }
                if (this.state.dayBefore.length === 0) {
                    if (this.state.lastDayObject) {
                        Object.keys(this.state.lastDayObject.dosage_slots).map(
                            async k => {
                                let pillTime = "";
                                let boxColor = "";
                                if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "1") {
                                    pillTime = this.state.eight;
                                    boxColor = "Red";
                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "2") {
                                    pillTime = this.state.one;
                                    boxColor = "Red";

                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "3") {
                                    pillTime = this.state.six;
                                    boxColor = "Red";
                                } else if (this.state.lastDayObject.dosage_slots[k]
                                    .dosage_slot === "4") {
                                    pillTime = this.state.ten;
                                    boxColor = "Red";
                                }
                                console.log("Color : ", boxColor, " PillTime : ", pillTime);
                                let status = "NOT_TAKEN";
                                this.setState(prevState => ({
                                    dayBefore: [
                                        ...prevState.dayBefore,
                                        {
                                            pillTime: k,
                                            status: status,
                                            takenTime: "",
                                            dosageSlot: this.state.lastDayObject.dosage_slots[k].dosage_slot,
                                            boxColor: boxColor
                                        }
                                    ]
                                }));
                            }
                        );
                    }
                }
            }
        }

        if (nextProps.userMedicationListData && this.state.apiCall === 2) {
            console.log("Dashboard userMedicationListData : " + JSON.stringify(nextProps.userMedicationListData));
            this.setState({
                meds: nextProps.userMedicationListData.meds
            });

            var tolerance1, tolerance2, tolerance3, tolerance4;
            if (nextProps.userMedicationListData.meds.length > 0) {
                var maxFreq = 0;
                await nextProps.userMedicationListData.meds.map(med => {
                    if (med.freq === "1") {
                        tolerance1 = med.tolerance;
                        if (maxFreq < Number(med.freq))
                            maxFreq = 1;
                    } else if (med.freq === "2") {
                        tolerance2 = med.tolerance;
                        if (maxFreq < Number(med.freq))
                            maxFreq = 2;
                    } else if (med.freq === "3") {
                        tolerance3 = med.tolerance;
                        if (maxFreq < Number(med.freq))
                            maxFreq = 3;
                    } else if (med.freq === "4") {
                        tolerance4 = med.tolerance;
                        if (maxFreq < Number(med.freq))
                            maxFreq = 4;
                    }

                    if (!tolerance1 && tolerance2) {
                        tolerance1 = tolerance2;
                    } else if (!tolerance2 && tolerance1) {
                        tolerance2 = tolerance1;
                    } else if (!tolerance3 && tolerance2) {
                        tolerance3 = tolerance2;
                    } else if (tolerance3 && !tolerance2) {
                        tolerance2 = tolerance3;
                    } else if (!tolerance3 && tolerance1) {
                        tolerance3 = tolerance1;
                    } else if (tolerance3 && !tolerance1) {
                        tolerance1 = tolerance3;
                    } else if (!tolerance1 && tolerance4) {
                        tolerance1 = tolerance4;
                    } else if (!tolerance2 && tolerance4) {
                        tolerance2 = tolerance4;
                    } else if (!tolerance3 && tolerance4) {
                        tolerance3 = tolerance4;
                    } else if (!tolerance4 && tolerance1) {
                        tolerance4 = tolerance1;
                    } else if (!tolerance4 && tolerance2) {
                        tolerance4 = tolerance2;
                    } else if (!tolerance4 && tolerance3) {
                        tolerance4 = tolerance3;
                    }

                    this.setState(prevState => ({
                        freqArray: [...prevState.freqArray, med.freq]
                    }))
                });

                await this.setState({
                    maxFreq: maxFreq,
                    freqArray: _.uniqBy(this.state.freqArray, e => {
                        return e;
                    })
                });

                console.log("Before Sort : " + this.state.freqArray);

                await this.setState({
                    freqArray: _.filter(this.state.freqArray, value => {
                        return value !== "1";
                    })
                });

                /* var i = this.state.freqArray.indexOf("1");

                 if (i > -1) {
                     this.setState({freqArray: this.state.freqArray.splice(i, 1)});
                 }*/

                console.log("Between Sort : " + this.state.freqArray);

                this.setState({
                    freqArray: this.state.freqArray.sort((a, b) => {
                        return a - b;
                    })
                });

                console.log("After Sort : " + this.state.freqArray);
                let value = "";

                await this.state.freqArray.map(arr => {
                    if (value !== "") {
                        value = value + "+" + arr + "D";
                    } else
                        value = arr + "D";
                });

                console.log("Final Value : " + value);

                if (this.state.dosageSchedules) {
                    let array = this.state.dosageSchedules[value];
                    console.log(" ARRAY IS : " + array + " : " + tolerance1 + " : " + tolerance2 + " : " + tolerance3
                        + " : " + tolerance4);
                    var minutes;
                    await array.forEach((value, index) => {
                        if (index === 0) {
                            this.setState({eight: value});
                            var eigthDate = new Date(
                                today.getFullYear(),
                                today.getMonth(),
                                today.getDate(),
                                value.split(":")[0],
                                value.split(":")[1],
                                0
                            );

                            minutes = tolerance1 / 60;
                            var eight1 = moment(eigthDate).subtract(minutes, 'minutes').toDate();
                            var eight2 = moment(eigthDate).add(minutes, 'minutes').toDate();

                            this.setState({eightDate1: eight1, eightDate2: eight2});
                            console.log("Eight 1 : " + eight1 + " : Eight 2 : " + eight2 + " : Minutes : " + minutes);

                        } else if (index === 1) {
                            this.setState({one: value});
                            var oneDate = new Date(
                                today.getFullYear(),
                                today.getMonth(),
                                today.getDate(),
                                value.split(":")[0],
                                value.split(":")[1],
                                0
                            );
                            minutes = tolerance2 / 60;
                            var one1 = moment(oneDate).subtract(minutes, 'minutes').toDate();
                            var one2 = moment(oneDate).add(minutes, 'minutes').toDate();

                            this.setState({oneDate1: one1, oneDate2: one2});
                            console.log("One 1 : " + one1 + " : One 2 : " + one2 + " : Minutes : " + minutes);
                        } else if (index === 2) {
                            this.setState({six: value});
                            var sixDate = new Date(
                                today.getFullYear(),
                                today.getMonth(),
                                today.getDate(),
                                value.split(":")[0],
                                value.split(":")[1],
                                0
                            );
                            minutes = tolerance3 / 60;
                            var six1 = moment(sixDate).subtract(minutes, 'minutes').toDate();
                            var six2 = moment(sixDate).add(minutes, 'minutes').toDate();

                            this.setState({sixDate1: six1, sixDate2: six2});
                            console.log("Six 1 : " + six1 + " : Six 2 : " + six2 + " : Minutes : " + minutes);
                        } else if (index === 3) {
                            this.setState({ten: value});
                            var tenDate = new Date(
                                today.getFullYear(),
                                today.getMonth(),
                                today.getDate(),
                                value.split(":")[0],
                                value.split(":")[1],
                                0
                            );
                            minutes = tolerance4 / 60;
                            var ten1 = moment(tenDate).subtract(minutes, 'minutes').toDate();
                            var ten2 = moment(tenDate).add(minutes, 'minutes').toDate();

                            this.setState({tenDate1: ten1, tenDate2: ten2});
                            console.log("Ten 1 : " + ten1 + " : Ten 2 : " + ten2 + " : Minutes : " + minutes);
                        }
                    })
                }

                await nextProps.userMedicationListData.meds.map(async med => {
                    var minutes = Math.floor(med.tolerance / 60);

                    if (med.freq === "1") {
                        // this.setState({tolerance1: med.tolerance});
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));

                    } else if (med.freq === "2") {
                        // this.setState({tolerance2: med.tolerance});
                        if (!this.state.freq2.includes(med.idx))
                            this.setState(prevState => ({
                                freq2: [...prevState.freq2, med.idx],
                                freq2Name: [...prevState.freq2Name, med.desc]
                            }));
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));
                    } else if (med.freq === "3") {
                        // this.setState({tolerance3: med.tolerance});
                        if (!this.state.freq3.includes(med.idx))
                            this.setState(prevState => ({
                                freq3: [...prevState.freq3, med.idx],
                                freq3Name: [...prevState.freq3Name, med.desc]
                            }));
                        if (!this.state.freq2.includes(med.idx))
                            this.setState(prevState => ({
                                freq2: [...prevState.freq2, med.idx],
                                freq2Name: [...prevState.freq2Name, med.desc]
                            }));
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));
                    } else if (med.freq === "4") {
                        // this.setState({tolerance4: med.tolerance});
                        if (!this.state.freq4.includes(med.idx))
                            this.setState(prevState => ({
                                freq4: [...prevState.freq4, med.idx],
                                freq4Name: [...prevState.freq4Name, med.desc]
                            }));
                        if (!this.state.freq3.includes(med.idx))
                            this.setState(prevState => ({
                                freq3: [...prevState.freq3, med.idx],
                                freq3Name: [...prevState.freq3Name, med.desc]
                            }));
                        if (!this.state.freq2.includes(med.idx))
                            this.setState(prevState => ({
                                freq2: [...prevState.freq2, med.idx],
                                freq2Name: [...prevState.freq2Name, med.desc]
                            }));
                        if (!this.state.freq1.includes(med.idx))
                            this.setState(prevState => ({
                                freq1: [...prevState.freq1, med.idx],
                                freq1Name: [...prevState.freq1Name, med.desc]
                            }));
                    }
                });
            }

            this.setState({apiCall: 3});
            this.props.OnFetchTodayMedicationList({
                lastDate: moment(new Date()).format("YYYYMMDD"),
                lastDay: 30
            });
        }
    }

    getDateArray = (start, end) => {
        var arr = [];
        var dt = new Date(start);
        while (dt <= end) {
            arr.push(new Date(moment(dt)));
            dt.setDate(dt.getDate() + 1);
        }
        return arr;
    };

    setDefaultDosageSlots = () => {
        var size = 1;
        if (this.state.meds) {
            if (this.state.eight) {
                size = 2;
            }
            if (this.state.one) {
                size = 3;
            }
            if (this.state.six) {
                size = 4;
            }
            if (this.state.ten) {
                size = 5;
            }
        }
        for (var i = 1; i < size; i++) {

            var pillTime = "";
            if (i === 1)
                pillTime = this.state.eight;
            else if (i === 2)
                pillTime = this.state.one;
            else if (i === 3)
                pillTime = this.state.six;
            else if (i === 4)
                pillTime = this.state.ten;

            console.log(" IIII :: " + i + " : Pill Time : " + pillTime);

            this.setState(prevState => ({
                dayBefore: [
                    ...prevState.dayBefore,
                    {
                        pillTime: pillTime,
                        status: "NOT_TAKEN",
                        takenTime: "",
                        dosageSlot: i,
                        boxColor: "Red"
                    }
                ]
            }));

            this.setState(prevState => ({
                yesterday: [
                    ...prevState.yesterday,
                    {
                        pillTime: pillTime,
                        status: "NOT_TAKEN",
                        takenTime: "",
                        dosageSlot: i,
                        boxColor: "Red"
                    }
                ]
            }));

            var today = new Date();
            var date = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                pillTime.split(":")[0],
                pillTime.split(":")[1],
                0
            );
            let status = "NOT_TAKEN";
            var boxColor = "Red";

            if (
                today.getHours() < date.getHours() ||
                (today.getHours() === date.getHours() &&
                    today.getMinutes() < date.getMinutes())
            ) {
                status = `${translate("daily_activity.future")}`;
                boxColor = "Grey";
            }

            this.setState(prevState => ({
                currentday: [
                    ...prevState.currentday,
                    {
                        pillTime: pillTime,
                        status: status,
                        takenTime: "",
                        dosageSlot: i,
                        boxColor: boxColor
                    }
                ]
            }));
        }
    };

    validateTextBox = async (text, type, lable) => {
    debugger;
        let d = lable[0].split("/");
        console.log("DD", d);
        console.log("Validation", /^\d+$/.test(text));

        if (text !== "") {
            if (/^\d+$/.test(text)) {
                console.log("In 1111");
                this.setState(
                    {
                        [`${lable[0]}/error`]: null,
                        [lable[0]]: text
                    },
                    () => {
                        let errors = Object.assign({}, this.state.errors);
                        delete errors[`${lable[0]}/error`];
                        this.setState({
                            errors: errors
                        });
                        /*console.log(
                            "In 1111 Set state callback.." + JSON.stringify(errors)
                        );*/
                    }
                );
            } else {
                // console.log("In 222");
                this.setState(
                    {
                        [`${lable[0]}/error`]: `${d[0]} ${translate("daily_activity.must_have_numeric")}`,
                        [lable[0]]: text
                    },
                    () => {
                        this.setState({
                            errors: Object.assign(this.state.errors, {
                                [`${lable[0]}/error`]: this.state[`${lable[0]}/error`]
                            })
                        });
                    }
                );
            }
        } else {
            console.log("In 333");
            let error = Object.assign({}, this.state.errors);
            delete error[`${lable[0]}/error`];

            console.log("Error",
                error);
            this.setState({
                [`${lable[0]}/error`]: translate("daily_activity.enter_value").toLowerCase(),
                errors: error,
                [lable[0]]: text
            });
        }
    };
    updateBioMetrics = async () => {
        // console.log("MY STATE======>", this.state.pickerArray);
        let textBoxArray = [];
        let pickerArray = [];
        this.setState({adding_bio_metric: true});
        let flag = 0;
        Object.keys(this.state).map((d, index) => {
            if (d.includes("/")) {
                console.log(
                    "validation check",
                    /^\d+$/.test(this.state[d]),
                    this.state[d]
                );
                let type = d.split("/");

                if (/^\d+$/.test(this.state[d])) {
                    flag = 0;
                    if (type[2] !== undefined) {
                        pickerArray.push({
                            biometric_type: type[1],
                            biometric_values: {
                                [this.state.selected2]: this.state[d]
                            },
                            taken_time: "16:13",
                            tzoffset: -330,
                            updated_on_server: 0
                        });
                    } else if (!d.includes("picker")) {
                        textBoxArray.push({
                            biometric_type: type[1],
                            biometric_values: {
                                [type[0]]: this.state[d],
                                taken_time: "16:13",
                                tzoffset: -330,
                                updated_on_server: 0
                            }
                        });
                    }
                } else {
                    flag = 1;
                }
            }
        });

        // console.log("TextBox", textBoxArray);
        // console.log("PickerArray", pickerArray);
        const keys = [];
        const result = [];
        for (let i = 0; i < textBoxArray.length; ++i) {
            const index = keys.indexOf(textBoxArray[i]["biometric_type"]);
            console.log("INDEX ====", index);
            console.log("keys ====", keys);
            if (index === -1) {
                console.log("data[i]", textBoxArray[i]);
                result.push(textBoxArray[i]);
                keys.push(textBoxArray[i]["biometric_type"]);
            } else {
                result[index]["biometric_values"] = Object.assign(
                    {},
                    result[index]["biometric_values"],
                    textBoxArray[i]["biometric_values"]
                );
            }
        }

        if (Object.keys(this.state.errors).length === 0) {
            let payload = result.concat(pickerArray);
            console.log("Pay load", payload);
            var request = `{"${moment(new Date()).format(
                "YYYYMMDD"
            )}" : ${JSON.stringify(payload)}}}`;

            let url = `/biometrictaken.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","biometrics":${request}`;

            if (payload.length > 0) {
                this.setState({payload: payload});

                console.log("URL", url);
                let method = "POST";

                CommonActions(url, method).then(res => {
                    console.log("Res", res);
                    this.setState({adding_bio_metric: false}, () => {
                        if (res.ERROR === 0) {
                            Alert.alert("Success", res.MESSAGE);
                            Object.keys(this.state).map(d => {
                                if (d.includes("/")) {
                                    this.setState({[d]: "", visible: false});
                                }
                            });
                        }
                    });
                });
            } else {
                this.setState({adding_bio_metric: false});
                Alert.alert("Error", translate("daily_activity.enter_value"));
            }
        } else {
        }
    };
    renderTextBox = biometric => {
        return (
            <View>
                <Text style={styles.dialogLabel}>{biometric.biometric_type}</Text>
                <View style={{flexDirection: "row", paddingBottom: 10}}>
                    {biometric.biometric_values.values.map(d => {
                        return (
                            <View style={{flex: 1}}>
                                <Item regular style={{width: 140, height: 50}}>
                                    <Input
                                        returnKeyType="done"
                                        keyboardType="number-pad"
                                        value={
                                            this.state[
                                                `${Object.keys(d)[0]}/${biometric.biometric_type}`
                                                ]
                                        }
                                        onChangeText={event =>
                                            this.validateTextBox(event, biometric.biometric_type, [
                                                `${Object.keys(d)[0]}/${biometric.biometric_type}`
                                            ])
                                        }
                                        placeholderTextColor="lightgrey"
                                        placeholder={Object.keys(d)[0]}
                                    />
                                </Item>
                                {Object.keys(this.state.errors).length > 0 && (
                                    <Text style={{color: "red"}}>
                                        {
                                            this.state[
                                                `${Object.keys(d)[0]}/${biometric.biometric_type}/error`
                                                ]
                                        }
                                    </Text>
                                )}
                            </View>
                        );
                    })}
                </View>
            </View>
        );
    };
    renderPicker = biometric => {
        return (
            <View>
                <Text style={styles.dialogLabel}>{biometric.biometric_type}</Text>
                <View style={{flexDirection: "row", paddingBottom: 10}}>
                    <View
                        style={{
                            width: 140,
                            height: 52,
                            borderWidth: 1,
                            borderColor: colors.greyColor
                        }}
                    >
                        <Item picker>
                            <Input
                                returnKeyType='done'
                                placeholder="Sugar Level"
                                keyboardType="number-pad"
                                value={
                                    this.state[
                                        `${"blood_sugar_value"}/${biometric.biometric_type}/picker`
                                        ]
                                }
                                // onChangeText={text =>
                                //   this.setState({
                                //     [`${"blood_sugar_value"}/${
                                //       biometric.biometric_type
                                //     }/picker`]: text
                                //   })
                                onChangeText={event =>
                                    this.validateTextBox(event, biometric.biometric_type, [
                                        `${"blood_sugar_value"}/${biometric.biometric_type}/picker`
                                    ])
                                }
                            />
                        </Item>
                    </View>

                    <View
                        style={{
                            width: 140,
                            height: 52,
                            borderWidth: 1,
                            borderColor: colors.greyColor,
                            marginLeft: 2
                        }}
                    >
                        <Item picker>

                            {/* {biometric.biometric_values.values.map((d, index) => {
                                    return (
                                        // <Picker.Item
                                        //     label={Object.keys(d)[0]}
                                        //     value={Object.keys(d)[0]}
                                        // />
                                        <Text>{Object.keys(d)[0]}</Text>
                                    );
                                })} */}
                            <Picker
                                mode="dialog"
                                iosIcon={<Ionicons name="ios-arrow-down"/>}
                                style={{width: undefined}}
                                // placeholder="Pre Meal"
                                placeholderStyle={{color: "#bfc6ea"}}
                                placeholderIconColor={colors.blackColor}
                                selectedValue={this.state.selected2}
                                // enabled={
                                //     this.state[
                                //         `${"blood_sugar_value"}/${biometric.biometric_type}/picker`
                                //         ] !== undefined
                                // }
                                onValueChange={value =>
                                    this.onValueChange2(value, biometric.biometric_type)
                                }
                            >
                                {biometric.biometric_values.values.map((d, index) => {
                                    return (
                                        <Picker.Item
                                            label={Object.keys(d)[0]}
                                            value={Object.keys(d)[0]}
                                        />
                                    );
                                })}
                            </Picker>
                        </Item>
                    </View>
                </View>
                {Object.keys(this.state.errors).length > 0 && (
                    <Text style={{color: "red"}}>
                        {
                            this.state[
                                `${`blood_sugar_value`}/${
                                    biometric.biometric_type
                                }/picker/error`
                                ]
                        }
                    </Text>
                )}
            </View>
        );
    };

    onValueChange2(value, type) {
        console.log("Picked Value", value);
        this.setState({
            selected2: value,
            pickerArray: [
                {
                    biometric_type: type,
                    biometric_values: {
                        [value]: this.state.blood_sugar_value
                    }
                }
            ]
        });
    }

    updateMedsTakenTime = async () => {
        var newDate = moment(new Date(
            new Date().getFullYear(),
            new Date().getMonth(),
            new Date().getDate(),
            this.state.pillDoseTime.split(":")[0],
            this.state.pillDoseTime.split(":")[1],
            0
        )).format("HH:mm");

        var date = new Date(
            new Date().getFullYear(),
            new Date().getMonth(),
            new Date().getDate(),
            newDate.split(":")[0],
            newDate.split(":")[1],
            0
        );

        let data;
        let boxColor = "";
        Object.keys(this.state.todayMedicationListData).map(d => {
            if (d === moment(this.state.selectedDate).format("YYYYMMDD")) {
                Object.keys(this.state.todayMedicationListData[d].dosage_slots).map(
                    k => {
                        if (this.state.selectedCelltime.time === k) {
                            this.state.todayMedicationListData[d].dosage_slots[
                                k
                                ].taken_status = "TAKEN";
                            this.state.todayMedicationListData[d].dosage_slots[
                                k
                                ].taken_time = this.state.pillDoseTime;
                            this.state.todayMedicationListData[d].dosage_slots[
                                k
                                ].updated_time = this.state.pillDoseTime;

                            if (this.state.todayMedicationListData[d].dosage_slots[k]
                                .dosage_slot === "1") {
                                /*if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.eightDate1 && date < this.state.eightDate2)) {
                                    boxColor = "Green";
                                    alert(" 1: " + boxColor + " : " + date + " : " + this.state.eightDate1 + " : " + this.state.eightDate2)
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.eightDate1)) {
                                    boxColor = "Yellow";
                                    alert(" 2: " + boxColor + " : " + date + " : " + this.state.eightDate1 + " : " + this.state.eightDate2)
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date < this.state.eightDate1)) {
                                    boxColor = "Grey";
                                    alert(" 3: " + boxColor + " : " + date + " : " + this.state.eightDate1 + " : " + this.state.eightDate2)
                                } else {
                                    boxColor = "Red";
                                    alert(" 4: " + boxColor + " : " + date + " : " + this.state.eightDate1 + " : " + this.state.eightDate2)
                                }*/
                                // alert(this.state.eightDate1 + " :: " + this.state.eightDate2);

                                if (!_.isEmpty(this.state.pillDoseTime)) {

                                    if (date >= this.state.eightDate1 && date <= this.state.eightDate2) {
                                        boxColor = "Green";
                                    } else if (date < this.state.eightDate1 || date > this.state.eightDate2) {
                                        boxColor = "Yellow";
                                    } else {
                                        boxColor = "Red";
                                    }
                                } else {
                                    boxColor = "Red";
                                }
                            } else if (this.state.todayMedicationListData[d].dosage_slots[k]
                                .dosage_slot === "2") {
                                /*if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.oneDate1 && date < this.state.oneDate2)) {
                                    boxColor = "Green";
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.oneDate1)) {
                                    boxColor = "Yellow";
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date < this.state.oneDate1)) {
                                    boxColor = "Grey";
                                } else {
                                    boxColor = "Red";
                                }*/

                                // alert(this.state.oneDate1 + " :: " + this.state.oneDate2);

                                if (!_.isEmpty(this.state.pillDoseTime)) {
                                    if (date >= this.state.oneDate1 && date <= this.state.oneDate2) {
                                        boxColor = "Green";
                                    } else if (date < this.state.oneDate1 || date > this.state.oneDate2) {
                                        boxColor = "Yellow";
                                    } else {
                                        boxColor = "Red";
                                    }
                                } else {
                                    boxColor = "Red";
                                }
                            } else if (this.state.todayMedicationListData[d].dosage_slots[k]
                                .dosage_slot === "3") {
                                /*if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.sixDate1 && date < this.state.sixDate2)) {
                                    boxColor = "Green";
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.sixDate1)) {
                                    boxColor = "Yellow";
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date < this.state.sixDate1)) {
                                    boxColor = "Grey";
                                } else {
                                    boxColor = "Red";
                                }*/
                                // alert(this.state.sixDate1 + " :: " + this.state.sixDate2);
                                if (!_.isEmpty(this.state.pillDoseTime)) {
                                    if (date >= this.state.sixDate1 && date <= this.state.sixDate2) {
                                        boxColor = "Green";
                                    } else if (date < this.state.sixDate1 || date > this.state.sixDate2) {
                                        boxColor = "Yellow";
                                    } else {
                                        boxColor = "Red";
                                    }
                                } else {
                                    boxColor = "Red";
                                }
                            } else if (this.state.todayMedicationListData[d].dosage_slots[k]
                                .dosage_slot === "4") {
                                /*if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.tenDate1 && date < this.state.tenDate2)) {
                                    boxColor = "Green";
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date > this.state.tenDate1)) {
                                    boxColor = "Yellow";
                                } else if (!_.isEmpty(this.state.todayMedicationListData[d].dosage_slots[k]
                                        .taken_time) &&
                                    (date < this.state.tenDate1)) {
                                    boxColor = "Grey";
                                } else {
                                    boxColor = "Red";
                                }*/
                                // alert(this.state.tenDate1 + " :: " + this.state.tenDate2);
                                if (!_.isEmpty(this.state.pillDoseTime)) {
                                    if (date >= this.state.tenDate1 && date <= this.state.tenDate2) {
                                        boxColor = "Green";
                                    } else if (date < this.state.tenDate1 || date > this.state.tenDate2) {
                                        boxColor = "Yellow";
                                    } else {
                                        boxColor = "Red";
                                    }
                                } else {
                                    boxColor = "Red";
                                }
                            }
                            this.state.todayMedicationListData[d].dosage_slots[
                                k
                                ].boxColor = boxColor;
                        }
                    }
                );
                data = JSON.stringify(this.state.todayMedicationListData[d]);
                console.log(" CHANGED DATA Is  :::::::: " + data);
            }
        });

        let url = `/medtaken.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","medstaken":{"${moment(
            this.state.selectedDate
        ).format("YYYYMMDD")}":${data}}}`;

        console.log("MedTaken URL : ", url);
        let method = "POST";

        CommonActions(url, method).then(res => {
            console.log("Res", res);
            // Alert.alert("Success", res.MESSAGE);
            this.setState({showtime: false, timeLoader: false});
            if (res.ERROR === 0) {

                if (this.state.selectedDay === translate("daily_activity.today")) {
                    this.state.currentday.map(day => {
                        if (day.pillTime === this.state.selectedCelltime.time) {
                            day.takenTime = moment(this.state.pillDoseTime, [
                                "h:mm a"
                            ]).format("HH:mm");
                            day.status = "TAKEN";
                            day.boxColor = boxColor;
                        }
                    });
                    this.setState({currentday: this.state.currentday});
                    // console.log(" CUrrent : " + JSON.stringify(this.state.currentday));
                } else if (this.state.selectedDay === translate("daily_activity.yesterday")) {
                    this.state.yesterday.map(day => {
                        if (day.pillTime === this.state.selectedCelltime.time) {
                            day.takenTime = moment(this.state.pillDoseTime, [
                                "h:mm a"
                            ]).format("HH:mm");
                            day.status = "TAKEN";
                            day.boxColor = boxColor;
                        }
                    });
                    this.setState({yesterday: this.state.yesterday});
                    // console.log(" YEsterday : " + JSON.stringify(this.state.yesterday));
                } else if (this.state.selectedDay === translate("daily_activity.day_before")) {
                    this.state.dayBefore.map(day => {
                        if (day.pillTime === this.state.selectedCelltime.time) {
                            day.takenTime = moment(this.state.pillDoseTime, [
                                "h:mm a"
                            ]).format("HH:mm");
                            day.status = "TAKEN";
                            day.boxColor = boxColor;
                        }
                    });
                    this.setState({dayBefore: this.state.dayBefore});
                    // console.log(" DAy Before : " + JSON.stringify(this.state.dayBefore));
                }
            }
        });
    };

    static navigationOptions = {
        header: null
    };

    renderSeparator = () => (
        <View style={{flex: 1, backgroundColor: colors.greyColor, margin: 20}}/>
    );

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={{flex: 1}}>
                <NavigationEvents
                    onWillFocus={payload => {
                        console.log("will focus", payload);
                    }}
                    onDidFocus={payload => {
                        console.log("did focus", payload);
                        this.setState({apiCall: 1, loading: true});
                        this.setState({
                            freq1: [],
                            freq2: [],
                            freq3: [],
                            freq4: [],
                            freq1Name: [],
                            freq2Name: [],
                            freq3Name: [],
                            freq4Name: []
                        });

                        /*if (this.state.dosageSchedules !== undefined && this.state.dosageSchedules !== {}) {
                            this.setState({apiCall: 2});
                            this.props.OnFetchUserMedicationList();
                        } else {*/
                        this.props.OnFetchDosageSchedules();
                        // }
                        // this.props.OnFetchBioMetricTypeList();
                    }}
                    onWillBlur={payload => {
                        console.log("will blur", payload);
                    }}
                    onDidBlur={payload => {
                        console.log("did blur", payload);
                    }}
                />
                <ScrollView>
                    <View style={{flexDirection: "column"}}>
                        <ImageBackground
                            source={require("../images/bg_daily_activity.jpg")}
                            style={{height: 90}}
                        >
                            <Text
                                style={{
                                    color: colors.whiteColor,
                                    fontSize: 25,
                                    textAlign: "center",
                                    padding: 10,
                                    marginTop: 20,
                                    fontWeight: "bold"
                                }}
                            >
                                {translate("daily_activity.dashboard")}
                            </Text>
                        </ImageBackground>
                        {/* <Text
                            style={{
                                color: colors.blueButtonColor,
                                fontSize: 20,
                                textAlign: "center",
                                marginTop: 8
                            }}
                        >
                            {`${translate("daily_activity.wlcm")} ${this.state.userName}`}
                            Welcome {this.state.userName}
                        </Text>
                        <View
                            style={{
                                flexDirection: "row",
                                alignContent: "center",
                                alignSelf: "center",
                                marginBottom: 8
                            }}
                        >
                            <Text
                                style={{
                                    color: colors.blackColor,
                                    fontSize: 16
                                }}
                            >
                                {this.state.agencyName}
                            </Text>
                            <Text
                                style={{
                                    color: colors.greyColor,
                                    fontSize: 14
                                }}
                            >
                                (MA Phycology)
                            </Text>
                        </View>*/}
                        <Card
                            style={{
                                marginLeft: 20,
                                marginRight: 20
                            }}
                        >
                            <CardItem>
                                <View style={{flex: 1, flexDirection: "column", height: 100}}>
                                    <CalendarStrip
                                        calendarAnimation={{type: "sequence", duration: 30}}
                                        daySelectionAnimation={{
                                            type: "border",
                                            duration: 200,
                                            borderWidth: 1,
                                            borderHighlightColor: colors.whiteColor
                                        }}
                                        style={{paddingBottom: 10, flex: 1}}
                                        calendarColor={colors.whiteColor}
                                        calendarHeaderStyle={{
                                            color: colors.whiteColor,
                                            fontSize: 0
                                        }}
                                        dateNumberStyle={{color: colors.blackColor}}
                                        dateNameStyle={{color: colors.blackColor}}
                                        highlightDateNumberStyle={{color: colors.blueButtonColor}}
                                        highlightDateNameStyle={{color: colors.blueButtonColor}}
                                        disabledDateNameStyle={{color: colors.blackColor}}
                                        disabledDateNumberStyle={{color: colors.blackColor}}
                                        datesWhitelist={[
                                            {
                                                start: new Date(
                                                    new Date().getFullYear(),
                                                    new Date().getMonth(),
                                                    new Date().getDate() - 2
                                                ),
                                                end: new Date()
                                            }
                                        ]}
                                        iconLeft={require("../images/back.png")}
                                        iconRight={require("../images/back.png")}
                                        iconStyle={{height: 0, width: 0}}
                                        onDateSelected={date => {
                                            var calendarDate = new Date(date);
                                            var day =
                                                this.convertDate(date) +
                                                ", " +
                                                calendarDate.getDate() +
                                                " " +
                                                moment(calendarDate).format("MMMM");
                                            this.setState({selectedDate: date, dateFormat: day});
                                            console.log(
                                                "Select : " + moment(date).format("YYYYMMDD")
                                            );
                                        }}
                                        selectedDate={this.state.selectedDate}
                                    />
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            alignContent: "center",
                                            alignItems: "center",
                                            alignSelf: "center"
                                        }}
                                    >
                                        <Icon
                                            color={
                                                this.state.selectedDay === `${translate("daily_activity.day_before")}`
                                                    ? colors.greyColor
                                                    : colors.blackColor
                                            }
                                            name="left"
                                            size={20}
                                            style={{
                                                alignContent: "center",
                                                alignSelf: "center",
                                                padding: 10
                                            }}
                                            onPress={() => {
                                                var date = new Date();
                                                var firstDay = new Date(
                                                    date.getFullYear(),
                                                    date.getMonth(),
                                                    1
                                                );
                                                var lastDay = new Date(
                                                    date.getFullYear(),
                                                    date.getMonth() + 1,
                                                    0
                                                );

                                                var select = moment(this.state.selectedDate).toDate();

                                                var convertDate;

                                                // console.log("FIRST DAY : " + firstDay + " : LAST DAY  " + lastDay);

                                                if (this.state.selectedDay !== `${translate("daily_activity.day_before")}`) {
                                                    if (
                                                        select.getFullYear() === firstDay.getFullYear() &&
                                                        select.getMonth() === firstDay.getMonth() &&
                                                        select.getDate() === firstDay.getDate()
                                                    ) {
                                                        convertDate = new Date(); // current date
                                                        convertDate.setDate(1); // going to 1st of the month
                                                        convertDate.setHours(-1);
                                                        /*console.log(
                                                            " THIS IS First Day on Month..!" + convertDate
                                                        );*/
                                                    } else if (
                                                        select.getFullYear() === lastDay.getFullYear() &&
                                                        select.getMonth() === lastDay.getMonth() &&
                                                        select.getDate() === lastDay.getDate()
                                                    ) {
                                                        convertDate = new Date(
                                                            date.getFullYear(),
                                                            date.getMonth() + 1,
                                                            1
                                                        );
                                                        /*console.log(
                                                            " THIS IS Last Day on Month..!" + convertDate
                                                        );*/
                                                    } else {
                                                        // console.log(" THIS IS between Day on Month..!");
                                                        convertDate = moment(this.state.selectedDate)
                                                            .subtract(1, "days")
                                                            .toDate();
                                                    }

                                                    this.setState({
                                                        selectedDate: convertDate
                                                    });

                                                    var day =
                                                        this.convertDate(convertDate) +
                                                        ", " +
                                                        convertDate.getDate() +
                                                        " " +
                                                        moment(convertDate).format("MMMM");
                                                    this.setState({dateFormat: day});
                                                }
                                            }}
                                        />
                                        <Text style={styles.dateTextStyle}>
                                            {this.state.dateFormat}
                                        </Text>
                                        <Icon
                                            color={
                                                this.state.selectedDay === `${translate("daily_activity.today")}`
                                                    ? colors.greyColor
                                                    : colors.blackColor
                                            }
                                            name="right"
                                            size={20}
                                            style={{
                                                alignContent: "center",
                                                alignSelf: "center",
                                                padding: 10
                                            }}
                                            onPress={() => {
                                                if (this.state.selectedDay !== `${translate("daily_activity.today")}`) {
                                                    var date = new Date();
                                                    var firstDay = new Date(
                                                        date.getFullYear(),
                                                        date.getMonth(),
                                                        1
                                                    );
                                                    var lastDay = new Date(
                                                        date.getFullYear(),
                                                        date.getMonth() + 1,
                                                        0
                                                    );

                                                    var select = moment(this.state.selectedDate).toDate();

                                                    var convertDate;

                                                    if (
                                                        select.getFullYear() === lastDay.getFullYear() &&
                                                        select.getMonth() === lastDay.getMonth() &&
                                                        select.getDate() === lastDay.getDate()
                                                    ) {
                                                        convertDate = new Date(
                                                            date.getFullYear(),
                                                            date.getMonth() + 1,
                                                            1
                                                        );
                                                        // console.log(" THIS IS NEXT Day on Month..!" + convertDate);
                                                    } else {
                                                        // console.log(" THIS IS between Day on Month..!");
                                                        convertDate = moment(this.state.selectedDate)
                                                            .add(1, "days")
                                                            .toDate();
                                                    }

                                                    this.setState({
                                                        selectedDate: convertDate
                                                    });
                                                    var day =
                                                        this.convertDate(convertDate) +
                                                        ", " +
                                                        convertDate.getDate() +
                                                        " " +
                                                        moment(convertDate).format("MMMM");
                                                    this.setState({dateFormat: day});
                                                }
                                            }}
                                        />
                                    </View>
                                </View>
                            </CardItem>
                        </Card>
                    </View>

                    {this.state.loading && (
                        <View
                            style={{
                                justifyContent: "center",
                                alignItems: "center",
                                flexDirection: "row",
                                padding: 30
                            }}
                        >
                            <ActivityIndicator size="small" animating/>
                            <Text style={{marginLeft: 15, fontWeight: "600", color: colors.blackColor}}>
                                {translate("manage_meds.fetching_meds")}
                            </Text>
                        </View>
                    )}
                    {(this.state.selectedDay === `${translate("daily_activity.today")}` &&
                        this.state.currentday.length > 0) ||
                    (this.state.selectedDay === `${translate("daily_activity.yesterday")}` &&
                        this.state.yesterday.length > 0) ||
                    (this.state.selectedDay === `${translate("daily_activity.day_before")}` &&
                        this.state.dayBefore.length > 0) ? (
                        <FlatList
                            style={{marginLeft: 20, marginRight: 20, alignSelf: 'center'}}
                            showsHorizontalScrollIndicator={false}
                            data={
                                this.state.selectedDay === `${translate("daily_activity.today")}`
                                    ? this.state.currentday
                                    : this.state.selectedDay === `${translate("daily_activity.yesterday")}`
                                    ? this.state.yesterday
                                    : this.state.dayBefore
                            }
                            numColumns={2}
                            renderItem={({item}) => (
                                <TouchableWithoutFeedback
                                    onPress={() => {
                                        this.state[`freq${item.dosageSlot}`] && this.state[`freq${item.dosageSlot}`].length > 0 &&
                                        this.setState({
                                            selectedCelltime: {
                                                time: item.pillTime,
                                                slots: item.dosageSlot
                                            },
                                            showMedicineList: true
                                        });
                                    }}
                                >
                                    <View
                                        style={{
                                            width: 150,
                                            backgroundColor:
                                                item.status === `${translate("daily_activity.future")}`
                                                    ? colors.greyColor :
                                                    item.boxColor === "Green"
                                                        ? colors.takenPillGreenColor
                                                        : item.boxColor === "Yellow"
                                                        ? colors.takenPillYellowColor
                                                        : item.boxColor = "Red"
                                                            ? colors.notTakenPillColor
                                                            : item.boxColor === "Grey"
                                                                ? colors.greyColor
                                                                : colors.notTakenPillColor,
                                            flexDirection: "column",
                                            margin: 5,
                                            padding: 5,
                                            alignContent: "center",
                                            justifyContent: "center",
                                            alignItems: 'center',
                                            borderRadius: 5
                                        }}
                                    >
                                        {
                                            this.state[`freq${item.dosageSlot}`] && this.state[`freq${item.dosageSlot}`].length > 0 ? (
                                                <View
                                                    style={{
                                                        flexDirection: "row",
                                                        alignContent: "center",
                                                        alignSelf: "center"
                                                    }}
                                                >
                                                    {this.state[`freq${item.dosageSlot}`].map(() => {
                                                        return (
                                                            <View>
                                                                <Image
                                                                    source={require("../images/pill.png")}
                                                                    style={{height: 12, width: 12}}
                                                                />
                                                            </View>
                                                        );
                                                    })}
                                                </View>
                                            ) : (
                                                <View
                                                    style={{
                                                        flexDirection: "row",
                                                        alignContent: "center",
                                                        alignSelf: "center"
                                                    }}
                                                >
                                                    <View>
                                                        <Image style={{height: 12, width: 12}}/>
                                                    </View>
                                                </View>
                                            )
                                        }

                                        <Text
                                            style={{
                                                textAlign: "center",
                                                paddingTop: 8,
                                                paddingBottom: 8,
                                                color: colors.whiteColor,
                                                fontSize: 18
                                            }}
                                        >
                                            {item.pillTime}
                                        </Text>
                                        <Text
                                            style={{
                                                borderColor: colors.whiteColor,
                                                borderWidth: 1,
                                                borderRadius: 5,
                                                textAlign: "center",
                                                padding: 8,
                                                color: colors.whiteColor,
                                                fontSize: 15,
                                                backgroundColor: colors.takenPillYellowColor,
                                                alignContent: "center"
                                            }}
                                            onPress={() => {
                                                /*if (
                                                    item.status !== `${translate("daily_activity.future")}` &&
                                                    item.status !== "TAKEN"
                                                )*/
                                                /*if (item.status === "TAKEN") {
                                                    this.setState({box: "Green Box"});
                                                } else {
                                                    this.setState({box: "Red Box"});
                                                }*/

                                                if (item.boxColor === "Red") {
                                                    this.setState({box: "Red Box"});
                                                } else if (item.boxColor === "Yellow") {
                                                    this.setState({box: "Yellow Box"});
                                                } else if (item.boxColor === "Grey") {
                                                    this.setState({box: "Grey Box"});
                                                } else if (item.boxColor === "Green") {
                                                    this.setState({box: "Green Box"});
                                                }

                                                if (item.status !== `${translate("daily_activity.future")}`) {
                                                    this.setState({
                                                        selectedCelltime: {
                                                            time: item.pillTime,
                                                            slots: item.dosageSlot
                                                        },
                                                        showtime: true
                                                    });
                                                }
                                            }}
                                        >
                                            {item.status === "TAKEN"
                                                ? `${translate("daily_activity.taken")} ${item.takenTime}`
                                                : item.status === `${translate("daily_activity.future")}`
                                                    ? `${translate("daily_activity.not_taken")}`
                                                    : `${translate("daily_activity.taken")}?`}
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            )}
                        />
                    ) : (
                        !this.state.loading && !this.props.today_loading && (
                            <View>
                                <Text style={{textAlign: "center", padding: 30, color: colors.blackColor}}>
                                    {translate("daily_activity.not_meds_record")}
                                </Text>
                            </View>
                        )
                    )}

                    <TouchableOpacity
                        onPress={() => {
                            this.setState({visible: true});
                        }}
                        style={styles.buttonStyle}
                    >
                        <Text style={styles.textStyle}> {translate("daily_activity.biometrics")} </Text>
                    </TouchableOpacity>

                    <View>
                        <Dialog visible={this.state.visible}>
                            {!this.props.loading ? (
                                <DialogContent>
                                    <Icon
                                        name="close"
                                        size={30}
                                        color={colors.greyColor}
                                        style={{alignSelf: "flex-end", marginTop: 10}}
                                        onPress={() => {
                                            this.setState({visible: false});
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            color: colors.blueButtonColor,
                                            marginTop: 10,
                                            backgroundColor: colors.whiteColor,
                                            alignSelf: "center"
                                        }}
                                    >
                                        {translate("daily_activity.enter_bio_metrics")}
                                    </Text>

                                    {this.state.bioMetricTypes && this.state.bioMetricTypes.length > 0 && this.state.bioMetricTypes.map(biometric => {
                                        if (biometric.biometric_values.type === "TEXTBOX") {
                                            return this.renderTextBox(biometric);
                                        }

                                        if (biometric.biometric_values.type === "DROPDOWN") {
                                            return this.renderPicker(biometric);
                                        }
                                    })}
                                    {/* <Text>{Object.keys(this.state.errors).length}</Text> */}
                                    <TouchableOpacity
                                        disabled={Object.keys(this.state.errors).length !== 0}
                                        onPress={this.updateBioMetrics}
                                        style={styles.buttonStyle}
                                    >
                                        {this.state.adding_bio_metric ? (
                                            <Text style={[styles.textStyle, {fontSize: 15}]}>
                                                {translate("daily_activity.updating")}
                                            </Text>
                                        ) : (
                                            <Text style={[styles.textStyle, {fontSize: 15}]}>
                                                {translate("daily_activity.update_biometrics")}
                                            </Text>
                                        )}
                                    </TouchableOpacity>
                                </DialogContent>
                            ) : (
                                <DialogContent>
                                    <View
                                        style={{
                                            flex: 0,
                                            height: 80,
                                            width: 80,
                                            justifyContent: "center",
                                            flexDirection: "column"
                                        }}
                                    >
                                        <Icon
                                            name="close"
                                            size={30}
                                            color={colors.greyColor}
                                            style={{alignSelf: "flex-end", marginTop: 10}}
                                            onPress={() => {
                                                this.setState({visible: false});
                                            }}
                                        />
                                        <ActivityIndicator size="large"/>
                                    </View>
                                </DialogContent>
                            )}
                        </Dialog>
                    </View>

                    <View>
                        <Dialog visible={this.state.showtime} width={300}>
                            <DialogContent>
                                <Icon
                                    name="close"
                                    size={30}
                                    color={colors.greyColor}
                                    style={{alignSelf: "flex-end", marginTop: 10}}
                                    onPress={() => {
                                        this.setState({showtime: false});
                                    }}
                                />

                                <View
                                    style={{
                                        backgroundColor: colors.blueButtonColor,
                                        marginBottom: 10,
                                        marginTop: 5
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.whiteColor,
                                            fontSize: 16,
                                            alignSelf: "center",
                                            padding: 12
                                        }}
                                    >
                                        {this.state.selectedCelltime.time}
                                    </Text>
                                </View>

                                <Text
                                    style={{
                                        color: colors.blackColor,
                                        fontSize: 20,
                                        fontWeight: "bold"
                                    }}
                                >
                                    {this.state.box}
                                    {/*{translate("daily_activity.red_box")}*/}
                                </Text>

                                <Text
                                    style={styles.timeComponentTextStyle}
                                    onPress={() => {
                                        this.setState({isDateTimePickerVisible: true});
                                    }}
                                >
                                    {this.state.pillDoseTime}
                                </Text>

                                {this.state.timeLoader ? (
                                    <ActivityIndicator size={"large"} style={{marginTop: 8}}/>
                                ) : (
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({showtime: false});
                                        }}
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            color: colors.whiteColor,
                                            marginTop: 8
                                        }}
                                    >
                                        <Text
                                            style={[styles.textStyle, {fontSize: 18}]}
                                            onPress={() => {
                                                this.setState({timeLoader: true});
                                                return this.updateMedsTakenTime();
                                            }}
                                        >
                                            {translate("daily_activity.set_taken_time")}
                                        </Text>
                                    </TouchableOpacity>
                                )}
                            </DialogContent>
                        </Dialog>
                    </View>

                    <View>
                        <Dialog
                            visible={this.state.showMedicineList}
                            height={400}
                            width={300}
                        >
                            <DialogContent style={{flex: 1}}>
                                <View style={{flex: 1, flexDirection: "column"}}>
                                    <Icon
                                        name="close"
                                        size={30}
                                        color={colors.greyColor}
                                        style={{alignSelf: "flex-end", marginTop: 10}}
                                        onPress={() => {
                                            this.setState({showMedicineList: false});
                                        }}
                                    />

                                    <View
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            marginBottom: 10,
                                            marginTop: 5
                                        }}
                                    >
                                        <Text
                                            style={{
                                                color: colors.whiteColor,
                                                fontSize: 18,
                                                alignSelf: "center",
                                                padding: 12
                                            }}
                                        >
                                            {this.state.selectedCelltime.time}
                                        </Text>
                                    </View>

                                    {/*<FlatList
                                        style={{margin: 20}}
                                        data={this.state.userMeds}
                                        ItemSeparatorComponent={this.renderSeparator()}
                                        renderItem={(item) => {
                                            console.log(" ITEMMM : " + item);
                                            return (
                                                item.freq === this.state.selectedCelltime.slots && (
                                                    <View
                                                        style={{
                                                            flex: 1,
                                                            flexDirection: "row",
                                                            margin: 5,
                                                            padding: 5,
                                                            alignContent: "center",
                                                            justifyContent: "center"
                                                        }}>
                                                        <View style={{
                                                            height: 40,
                                                            width: 40,
                                                            alignSelf: "center",
                                                            borderRadius: 40 / 2,
                                                            borderColor: colors.greyColor,
                                                            borderWidth: 1,
                                                            backgroundColor: colors.lightGreyColor
                                                        }}>
                                                            <Image
                                                                style={{
                                                                    height: 30,
                                                                    width: 30,
                                                                    alignSelf: 'center'
                                                                }}
                                                                source={require("../images/pill_grey.png")}
                                                            />
                                                        </View>

                                                        <View
                                                            style={{
                                                                flexDirection: "column",
                                                                alignContent: "center",
                                                                alignSelf: "center",
                                                                marginLeft: 10
                                                            }}
                                                        >
                                                            <Text
                                                                style={{color: colors.blackColor, fontSize: 15}}
                                                            >
                                                                {item.desc}
                                                            </Text>
                                                            <Text
                                                                style={{
                                                                    color: colors.greyColor,
                                                                    fontSize: 12,
                                                                    padding: 5
                                                                }}
                                                            >
                                                                {item.strength}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                )
                                            );
                                        }}/>*/}
                                    <ScrollView style={{flex: 0.5}}>
                                        {this.state.showMedicineList &&
                                        this.state.meds !== undefined &&
                                        this.state.meds.map(med => {
                                            return (
                                                parseInt(med.freq) >= parseInt(this.state.selectedCelltime.slots) && (
                                                    <View
                                                        style={{
                                                            flex: 1,
                                                            flexDirection: "row",
                                                            margin: 5,
                                                            padding: 5
                                                        }}
                                                    >
                                                        <View
                                                            style={{
                                                                height: 40,
                                                                width: 40,
                                                                alignSelf: "center",
                                                                borderRadius: 40 / 2,
                                                                borderColor: colors.greyColor,
                                                                borderWidth: 1,
                                                                backgroundColor: colors.lightGreyColor
                                                            }}
                                                        >
                                                            <Image
                                                                style={{
                                                                    height: 30,
                                                                    width: 30,
                                                                    alignSelf: "center"
                                                                }}
                                                                source={require("../images/pill_grey.png")}
                                                            />
                                                        </View>

                                                        <View
                                                            style={{
                                                                flexDirection: "column",
                                                                alignContent: "center",
                                                                alignSelf: "center",
                                                                marginLeft: 10
                                                            }}
                                                        >
                                                            <Text
                                                                style={{
                                                                    color: colors.blackColor,
                                                                    fontSize: 15
                                                                }}
                                                            >
                                                                {med.desc}
                                                            </Text>
                                                            <Text
                                                                style={{
                                                                    color: colors.greyColor,
                                                                    fontSize: 12,
                                                                    padding: 5
                                                                }}
                                                            >
                                                                {med.strength}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                )
                                            );
                                        })}
                                    </ScrollView>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({showMedicineList: false});
                                        }}
                                        style={{
                                            backgroundColor: colors.blueButtonColor,
                                            color: colors.whiteColor,
                                            marginTop: 8
                                        }}
                                    >
                                        <Text style={[styles.textStyle, {fontSize: 18}]}>{translate("login.ok")}</Text>
                                    </TouchableOpacity>
                                </View>
                            </DialogContent>
                        </Dialog>
                    </View>

                    <View>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={date => {
                                console.log("TImeeee : " + date);
                                this.setState({
                                    isDateTimePickerVisible: false,
                                    pillDoseTime: moment(date).format("HH:mm")
                                });
                            }}
                            onCancel={() => {
                                this.setState({isDateTimePickerVisible: false});
                            }}
                            mode={"time"}
                        />
                    </View>
                </ScrollView>

                <FooterView
                    navigation={this.props.navigation}
                    style={{marginTop: 10}}
                />
            </View>
        );
    }

    convertDate(oldDate) {
        let today = new Date();
        let date = new Date(oldDate);
        let tomorrow = moment(new Date())
            .add(1, "days")
            .toDate();
        let yesterday = moment(new Date())
            .subtract(1, "days")
            .toDate();
        let dayBefor = moment(new Date())
            .subtract(2, "days")
            .toDate();
        if (
            date.getFullYear() === today.getFullYear() &&
            date.getMonth() === today.getMonth() &&
            date.getDate() === today.getDate()
        ) {
            this.setState({selectedDay: `${translate("daily_activity.today")}`});
            return `${translate("daily_activity.today")}`;
        } else if (
            date.getFullYear() === yesterday.getFullYear() &&
            date.getMonth() === yesterday.getMonth() &&
            date.getDate() === yesterday.getDate()
        ) {
            this.setState({selectedDay: `${translate("daily_activity.yesterday")}`});
            return `${translate("daily_activity.yesterday")}`;
        } else if (
            date.getFullYear() === tomorrow.getFullYear() &&
            date.getMonth() === tomorrow.getMonth() &&
            date.getDate() === tomorrow.getDate()
        ) {
            this.setState({selectedDay: `${translate("daily_activity.tomorrow")}`});
            return `${translate("daily_activity.tomorrow")}`;
        } else if (
            date.getFullYear() === dayBefor.getFullYear() &&
            date.getMonth() === dayBefor.getMonth() &&
            date.getDate() === dayBefor.getDate()
        ) {
            this.setState({selectedDay: `${translate("daily_activity.day_before")}`});
            return `${translate("daily_activity.day_before")}`;
        } else {
            return moment(date).format("dddd");
        }
    }
}

const mapStateToProps = state => {
    return {
        biometricListData: state.bioMetricReducer.biometricListData,
        loading: state.bioMetricReducer.loading,
        error: state.bioMetricReducer.error,
        todayMedicationListData:
        state.todayMedicationReducer.todayMedicationListData,
        today_loading: state.todayMedicationReducer.today_loading,
        today_error: state.todayMedicationReducer.today_error,
        userMedicationListData:
        state.userMedicationListReducer.UsermedicationListData,
        user_med_list_loading: state.userMedicationListReducer.loading,
        user_med_list_error: state.userMedicationListReducer.error,
        dosageSchedules: state.dosageSchedule.dosageSchedules,
        dosage_error: state.dosageSchedule.dosage_error,
        dosage_loading: state.dosageSchedule.dosage_loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchBioMetricTypeList: () =>
            dispatch(GetBiometricTypesListActions.fetchBioMetricList()),
        OnFetchTodayMedicationList: data =>
            dispatch(GetTodayMedicationActions.fetchTodayMedicationList(data)),
        OnFetchUserMedicationList: () =>
            dispatch(UserMedicationListActions.fetchUserMedicationList()),
        OnFetchDosageSchedules: () =>
            dispatch(DosageScheduleAction.fetchDosageSchedule())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DailyActivityScreen);

const styles = StyleSheet.create({
    pillTimeStyle: {
        fontSize: 22,
        color: colors.whiteColor,
        textAlign: "center"
    },
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 30,
        color: colors.whiteColor,
        marginTop: 8
    },
    textStyle: {
        color: colors.whiteColor,
        fontSize: 20,
        alignSelf: "center",
        padding: 12
    },
    dateTextStyle: {
        alignContent: "center",
        fontSize: 16,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: "center",
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#ddd',
        borderBottomWidth: 1,
        elevation: 3,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.5,
        shadowRadius: 2,
        color: colors.blackColor
    },
    dialogLabel: {
        fontSize: 15,
        color: colors.blackColor,
        paddingTop: 10,
        paddingBottom: 10
    },
    thumbnailImage: {
        marginTop: -40,
        alignSelf: "center",
        width: 80,
        height: 80,
        borderRadius: 80 / 2
    },
    timeComponentTextStyle: {
        borderRadius: 0,
        borderWidth: 1,
        borderColor: colors.greyColor,
        color: colors.blackColor,
        backgroundColor: colors.whiteColor,
        padding: 10,
        fontSize: 15,
        marginTop: 10
    }
});
