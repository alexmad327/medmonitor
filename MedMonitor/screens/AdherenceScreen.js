import React, {Component} from "react";
import {View} from "react-native";
import {CalendarScreen} from "./CalendarScreen";
import {Text} from "react-native-elements";
import {colors} from "../constants/Constants";
import {FooterView} from "../container/FooterView";
import {HeaderView} from "../container/HeaderView";

export class AdherenceScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedText: "Calendar"
        }
    }

    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <HeaderView name="Adherence" navigation={this.props.navigation}/>
                <View style={{height: 50, flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={{
                        flex: 1,
                        paddingTop: 15,
                        paddingBottom: 15,
                        textAlign: 'center',
                        fontSize: 18,
                        backgroundColor: this.state.selectedText === "Calendar" ? colors.darkBlueColor : colors.whiteColor,
                        color: this.state.selectedText === "Calendar" ? colors.whiteColor : colors.blackColor
                    }}
                          onPress={() => {
                              this.setState({selectedText: "Calendar"});
                          }}>
                        Calendar
                    </Text>
                    {/*<View style={{width: 1, backgroundColor: colors.greyColor, height: 50}}/>
                    <Text style={{
                        flex: 0.5,
                        textAlign: 'center',
                        paddingTop: 15,
                        paddingBottom: 15,
                        fontSize: 16,
                        backgroundColor: this.state.selectedText === "Graph" ? colors.darkBlueColor : colors.whiteColor,
                        color: this.state.selectedText === "Graph" ? colors.whiteColor : colors.blackColor
                    }}
                          onPress={() => {
                              this.setState({selectedText: "Graph"});
                          }}>
                        Graph
                    </Text>*/}
                </View>
                <View style={{flex: 1}}>
                    <CalendarScreen navigation={this.props.navigation}/>
                </View>
                <FooterView isSummary={true}
                            navigation={this.props.navigation}/>
            </View>
        );
    }
}