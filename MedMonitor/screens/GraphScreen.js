import React, {Component} from "react"
import {Text, View, Alert, AsyncStorage} from "react-native"
import {LineChart} from 'react-native-chart-kit'
import {Dimensions} from 'react-native'
import {colors} from "../constants/Constants";
import {HeaderView} from "../container/HeaderView";
import * as GetTodayBiometricsActions from "../store/actions/GetTodayBiometricsAction";
import {connect} from "react-redux";
import moment from "moment";
import I18n, {translate} from "../locale/i18n";

class GraphScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedText: `${translate("biometric_graph.blood_pressure")}`,
            x: [],
            systolic: [],
            diastolic: [],
            weight: [],
            preMeal: [],
            postMeal: [],
            fasting: [],
            todayBiometricListData: {},
            loading: false
        }
    }

    componentDidMount() {
        var date = new Date();
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        this.props.OnFetchTodayBioMetricList({
            lastDate: moment(date).format("YYYYMMDD"),
            lastDay: lastDay.getDate()
        });
    }

    async componentWillReceiveProps(nextProps) {
        if (nextProps.todayBiometricListData !== null) {
            // console.log("GRAPHS : " + JSON.stringify(nextProps.todayBiometricListData));
            this.setState({todayBiometricListData: nextProps.todayBiometricListData});
            await Object.keys(nextProps.todayBiometricListData).map(key => {
                this.setState(prevState => ({
                    x: [
                        ...prevState.x,
                        `${key.substring(6, 8)}/${key.substring(4, 6)}`
                    ]
                }));
                nextProps.todayBiometricListData[key].map(data => {
                    if (data.biometric_type === "Blood Pressure") {
                        Object.keys(data.biometric_values).map(key => {
                            if (key === "Systolic") {
                                this.setState(prevState => ({
                                    systolic: [
                                        ...prevState.systolic,
                                        data.biometric_values[key]
                                    ]
                                }));
                            } else {
                                this.setState(prevState => ({
                                    diastolic: [
                                        ...prevState.diastolic,
                                        data.biometric_values[key]
                                    ]
                                }));
                            }
                        });
                    } else if (data.biometric_type === "Blood Sugar") {
                        Object.keys(data.biometric_values).map(key => {
                            if (key === "Post Meal") {
                                this.setState(prevState => ({
                                    postMeal: [
                                        ...prevState.postMeal,
                                        data.biometric_values[key]
                                    ]
                                }));
                            } else if (key === "Fasting") {
                                this.setState(prevState => ({
                                    fasting: [
                                        ...prevState.fasting,
                                        data.biometric_values[key]
                                    ]
                                }));
                            } else {
                                this.setState(prevState => ({
                                    preMeal: [
                                        ...prevState.preMeal,
                                        data.biometric_values[key]
                                    ]
                                }));
                            }
                        });
                    } else {
                        Object.keys(data.biometric_values).map(key => {
                            this.setState(prevState => ({
                                weight: [
                                    ...prevState.weight,
                                    data.biometric_values[key]
                                ]
                            }));
                        });
                    }
                });
            });
        }
    }

    static navigationOptions = {
        header: null,
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <HeaderView name={translate("biometric_graph.graph")} navigation={this.props.navigation}/>
                <View>
                    <Text style={{fontSize: 20, textAlign: 'center', padding: 10}}>
                        {this.state.selectedText}
                    </Text>
                    {
                        this.state.selectedText === `${translate("biometric_graph.blood_pressure")}` ?
                            <View style={{flexDirection: 'row', margin: 10}}>
                                <View
                                    style={{
                                        flex: 0.6,
                                        flexDirection: 'row',
                                        alignSelf: 'flex-end',
                                        alignContent: 'flex-end',
                                        justifyContent: 'center'
                                    }}>
                                    <Text style={{
                                        width: 20,
                                        height: 20,
                                        borderRadius: 10,
                                        backgroundColor: colors.blueButtonColor,
                                        marginRight: 5
                                    }}/>
                                    <Text>
                                        {translate("biometric_graph.systolic")}
                                    </Text>
                                </View>
                                <View style={{
                                    flex: 0.4,
                                    flexDirection: 'row',
                                    alignSelf: 'flex-start',
                                    alignContent: 'flex-end'
                                }}>
                                    <Text style={{
                                        width: 20,
                                        height: 20,
                                        borderRadius: 10,
                                        backgroundColor: colors.takenPillGreenColor,
                                        marginRight: 5
                                    }}/>
                                    <Text>
                                        {translate("biometric_graph.diastolic")}
                                    </Text>
                                </View>
                            </View>
                            :
                            this.state.selectedText === "Blood Sugar" ?
                                <View style={{flexDirection: 'row', margin: 10}}>
                                    <View style={{
                                        flex: 0.35,
                                        flexDirection: 'row',
                                        alignSelf: 'flex-start',
                                        alignContent: 'flex-end'
                                    }}>
                                        <Text style={{
                                            width: 20,
                                            height: 20,
                                            borderRadius: 10,
                                            backgroundColor: colors.takenPillGreenColor,
                                            marginRight: 5
                                        }}/>
                                        <Text>
                                            {translate("biometric_graph.post_meal")}
                                        </Text>
                                    </View>
                                    <View style={{
                                        flex: 0.35,
                                        flexDirection: 'row',
                                        alignSelf: 'flex-start',
                                        alignContent: 'flex-end'
                                    }}>
                                        <Text style={{
                                            width: 20,
                                            height: 20,
                                            borderRadius: 10,
                                            backgroundColor: colors.notTakenPillColor,
                                            marginRight: 5
                                        }}/>
                                        <Text>
                                            {translate("biometric_graph.fasting")}
                                        </Text>
                                    </View>
                                    <View
                                        style={{
                                            flex: 0.3,
                                            flexDirection: 'row',
                                            alignSelf: 'flex-end',
                                            alignContent: 'flex-end',
                                            justifyContent: 'center'
                                        }}>
                                        <Text style={{
                                            width: 20,
                                            height: 20,
                                            borderRadius: 10,
                                            backgroundColor: colors.blueButtonColor,
                                            marginRight: 5
                                        }}/>
                                        <Text>
                                            {translate("biometric_graph.other")}
                                        </Text>
                                    </View>
                                </View>
                                :
                                <View style={{flexDirection: 'row', margin: 10, alignContent: 'center'}}>
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        alignSelf: 'center',
                                        alignContent: 'center',
                                        justifyContent: 'center'
                                    }}>
                                        <Text style={{
                                            width: 20,
                                            height: 20,
                                            borderRadius: 10,
                                            backgroundColor: colors.blueButtonColor,
                                            marginRight: 5
                                        }}/>
                                        <Text>
                                            {translate("biometric_graph.weight")}
                                        </Text>
                                    </View>
                                </View>
                    }
                    <LineChart
                        data={{
                            labels: this.state.x,
                            datasets: this.state.selectedText === `${translate("biometric_graph.blood_pressure")}` ? [{
                                data: this.state.systolic,
                                color: (opacity = 1) => `rgba(0, 146, 217, ${opacity})`, // optional
                                strokeWidth: 2 // optional
                            }, {
                                data: this.state.diastolic,
                                color: (opacity = 1) => `rgba(43, 195, 98, ${opacity})`, // optional
                                strokeWidth: 2 // optional
                            }] : this.state.selectedText === `${translate("biometric_graph.blooad_sugar")}` ?
                                [{
                                    data: this.state.preMeal,
                                    color: (opacity = 1) => `rgba(255, 104, 84, ${opacity})`, // optional
                                    strokeWidth: 2 // optional
                                }, {
                                    data: this.state.postMeal,
                                    color: (opacity = 1) => `rgba(43, 195, 98, ${opacity})`, // optional
                                    strokeWidth: 2 // optional
                                }, {
                                    data: this.state.fasting,
                                    color: (opacity = 1) => `rgba(0, 146, 217, ${opacity})`, // optional
                                    strokeWidth: 2 // optional
                                }] : [{
                                    data: this.state.weight,
                                    color: (opacity = 1) => `rgba(0, 146, 217, ${opacity})`, // optional
                                    strokeWidth: 2 // optional
                                }]
                        }}
                        width={Dimensions.get('window').width} // from react-native
                        height={220}
                        bezier
                        chartConfig={{
                            backgroundColor: colors.whiteColor,
                            backgroundGradientFrom: colors.whiteColor,
                            backgroundGradientTo: colors.whiteColor,
                            decimalPlaces: 0, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                            style: {
                                borderRadius: 16
                            }
                        }}
                        withDots={true}
                        withShadow={false}
                        withInnerLines={false}
                        withOuterLines={false}
                        onDataPointClick={({value, dataset, getColor}) => {

                        }}
                        style={{
                            marginVertical: 8,
                            borderRadius: 16,
                            marginTop: 10
                        }}
                    />
                    <View style={{flexDirection: 'row', margin: 10, alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{
                            width: 100,
                            borderRadius: 20,
                            borderWidth: 2,
                            backgroundColor: this.state.selectedText === `${translate("biometric_graph.blood_pressure")}` ? colors.blueButtonColor : colors.whiteColor,
                            borderColor: colors.blueButtonColor,
                            marginRight: 10
                        }}>
                            <Text
                                style={{
                                    padding: 10,
                                    color: this.state.selectedText === `${translate("biometric_graph.blood_pressure")}` ? colors.whiteColor : colors.blackColor,
                                    textAlign: 'center',
                                    marginRight: 8
                                }} onPress={() => {
                                this.setState({selectedText: `${translate("biometric_graph.blood_pressure")}`})
                            }}>
                                {translate("biometric_graph.blood_pressure")}
                            </Text>
                        </View>

                        <View style={{
                            width: 100,
                            borderRadius: 20,
                            borderWidth: 2,
                            backgroundColor: this.state.selectedText === `${translate("biometric_graph.seizure_activity")}` ? colors.blueButtonColor : colors.whiteColor,
                            borderColor: colors.blueButtonColor,
                            marginRight: 10
                        }}>
                            <Text style={{
                                padding: 10,
                                color: this.state.selectedText === `${translate("biometric_graph.seizure_activity")}` ? colors.whiteColor : colors.blackColor,
                                textAlign: 'center',
                                marginRight: 8
                            }} onPress={() => {
                                this.setState({selectedText: `${translate("biometric_graph.seizure_activity")}`})
                            }}>
                                {translate("biometric_graph.seizure_activity")}
                            </Text>
                        </View>

                        <View style={{
                            width: 100,
                            borderRadius: 20,
                            borderWidth: 2,
                            backgroundColor: this.state.selectedText === `${translate("biometric_graph.blooad_sugar")}` ? colors.blueButtonColor : colors.whiteColor,
                            borderColor: colors.blueButtonColor
                        }}>
                            <Text style={{
                                padding: 10,
                                color: this.state.selectedText === `${translate("biometric_graph.blooad_sugar")}` ? colors.whiteColor : colors.blackColor,
                                textAlign: 'center'
                            }} onPress={() => {
                                this.setState({selectedText: `${translate("biometric_graph.blooad_sugar")}`})
                            }}>
                                {translate("biometric_graph.blooad_sugar")}
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        todayBiometricListData: state.todayBiometricReducer.todayBiometricListData,
        loading: state.todayBiometricReducer.loading,
        error: state.todayBiometricReducer.error
    };
};

const mapDispatchToProps = dispatch => ({
    OnFetchTodayBioMetricList: (data) =>
        dispatch(GetTodayBiometricsActions.fetchTodayBiometricList(data))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GraphScreen);