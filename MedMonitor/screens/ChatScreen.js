import React, {Component} from "react";
import {
    FlatList,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions, AsyncStorage
} from "react-native";
import {FooterView} from "../container/FooterView";
import {colors, headerTitleStyle} from "../constants/Constants";
import {SearchBar} from "react-native-elements";
import moment from "moment";
import {sortBy} from "lodash";
import {CommonActions} from "../store/actions/CommonRequestAction";
import * as SocialMessagesListActions from "../store/actions/SocialMessagesActions";
import I18n, {translate} from "../locale/i18n";
import {connect} from "react-redux";
import {reverse} from "lodash";

const {height, width} = Dimensions.get("screen");

class ChatScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.navigation.state.params.messages,
            count: 5,
            message: "",
            viewHeight: 0,
            phone: "",
            otpToken: ""
        };
    }

    async componentDidMount() {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});
    }

    componentWillReceiveProps(nextProps) {
        console.log("YESSSSS", nextProps);
    }

    sendMessages = msg => {
        let data = {
            phone: `${this.state.phone}`,
            otptoken: `${this.state.otpToken}`,
            userType: "Patient",
            parentIDX: this.props.navigation.state.params.id,
            message: this.state.message,
            timestamp: moment().format("YYYY-MM-DD hh:mm:ss")
        };

        let url = `/addsocialmediamessages.php?version=3.0.0&data=${JSON.stringify(data)}`;
        // console.log("gggg", url);

        let method = "POST";

        let messagePayLoad = {
            from: this.props.navigation.state.params.name,
            message: this.state.message,
            timestamp: moment().format("YYYY-MM-DD hh:mm:ss")
        };
        if (this.state.message !== "") {
            //   console.log("Messages=============", this.state.data);
            CommonActions(url, method, {}).then(res => {
                this.props.OnFetchSocialMessagesList();
            });
            this.setState(
                {
                    data: [...this.state.data, messagePayLoad]
                },
                () => {
                    this.setState({message: ""});
                }
            );
        }
    };
    static navigationOptions = ({navigation}) => {
        return {
            headerTitleStyle: headerTitleStyle,
            headerTitle: (
                <View style={{flex: 1, alignItems: "center"}}>
                    <Text
                        style={{
                            fontSize: 18,
                            color: colors.whiteColor,
                            fontWeight: "bold"
                        }}
                    >{`${navigation.state.params.name}`}</Text>
                    <Text
                        style={{
                            fontSize: 14,
                            color: colors.whiteColor
                        }}
                    >
                        {navigation.state.params.subject}
                    </Text>
                </View>
            ),
            headerLeft: (
                <TouchableOpacity
                    onPress={() => {
                        navigation.goBack();
                    }}
                    style={{padding: 10, marginLeft: 10}}
                >
                    <Image
                        source={require("../images/back.png")}
                        style={{height: 25, width: 25}}
                    />
                </TouchableOpacity>
            ),
            headerStyle: {
                backgroundColor: colors.blueButtonColor
            },
            headerRight: (
                <View style={{flexDirection: "row", alignContent: "center"}}/>
            )
        };
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <FlatList
                        ref="flatList"
                        onContentSizeChange={() => this.refs.flatList.scrollToEnd({animated: true})}
                        onLayout={() => this.refs.flatList.scrollToEnd({animated: true})}
                        // ref={ref => {
                        //   this.flatListRef = ref;
                        // }}
                        // onContentSizeChange={() => this.refs.flatListRef.scrollToEnd()}
                        // style={{ flexDirection: "column-reverse" ,flex:1}}
                        // inverted
                        data={this.state.data}
                        renderItem={({item}) => (
                            <View
                                key={item.id}
                                style={{
                                    margin: 10,
                                    paddingRight: 40,
                                    marginLeft: 20,
                                    borderRadius: 10,
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    paddingLeft: 10,
                                    borderWidth: 1,
                                    borderColor: "#ddd",
                                    borderBottomWidth: 0,
                                    elevation: 1,
                                    alignSelf: "flex-start",
                                    marginRight: 70
                                    //   height:100
                                }}>
                                {
                                    <View style={{}}>
                                        <Text style={{fontWeight: "600"}}>{item.from}</Text>
                                        <Text>{item.message}</Text>
                                        <Text
                                            style={{
                                                fontSize: 8
                                            }}
                                        >
                                            {item.timestamp}
                                        </Text>
                                    </View>

                                    // : (
                                    //   <View
                                    //     style={{
                                    //       paddingLeft: 40,
                                    //       alignSelf: "flex-end",
                                    //       backgroundColor: colors.darkBlueColor,
                                    //       marginRight: 20,
                                    //       borderRadius: 10,
                                    //       paddingTop: 10,
                                    //       paddingBottom: 10,
                                    //       paddingRight: 10,
                                    //       marginLeft: 70
                                    //     }}
                                    //   >
                                    //     <Text style={{ color: colors.whiteColor }}>
                                    //       {item.message}
                                    //     </Text>
                                    //     <Text
                                    //       style={{
                                    //         color: colors.whiteColor,
                                    //         fontSize: 8,
                                    //         textAlign: "right"
                                    //       }}
                                    //     >
                                    //       {item.time}
                                    //     </Text>
                                    //   </View>
                                    // )
                                }
                            </View>
                        )}
                    />
                </View>

                <View style={{flexDirection: "row", marginRight: 10}}>
                    <TextInput
                        placeholder={translate("social_media.type_message")}
                        style={{flex: 1, paddingLeft: 20}}
                        onChangeText={text => this.setState({message: text})}
                        value={this.state.message}
                    />
                    <View
                        style={{
                            backgroundColor: colors.blueButtonColor,
                            height: 40,
                            width: 40,
                            borderRadius: 40 / 2,
                            justifyContent: "center",
                            alignContent: "center"
                        }}
                    >
                        <TouchableOpacity onPress={this.sendMessages}>
                            <Image
                                source={require("../images/chat.png")}
                                style={{height: 25, width: 25, alignSelf: "center"}}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <FooterView navigation={this.props.navigation}/>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.socialMessagesReducer.loading,
        error: state.socialMessagesReducer.error,
        socialMessagesListData: state.socialMessagesReducer.socialMessagesListData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchSocialMessagesList: () =>
            dispatch(SocialMessagesListActions.fetchSocialMessagesList())
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatScreen);
