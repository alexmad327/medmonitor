import React, {Component} from "react";
import {ActivityIndicator, AsyncStorage, ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {API_BASE_URL, colors} from "../constants/Constants";
import {Text} from "react-native-elements";
import {FooterView} from "../container/FooterView";
import Icon from "react-native-vector-icons/Feather";
import Dialog, {DialogContent} from "react-native-popup-dialog";
import {HeaderView} from "../container/HeaderView";
import moment from "moment";
import * as UserMedicationListActions from "../store/actions/UserMedicationListAction";
import {connect} from "react-redux";
import {NavigationEvents} from "react-navigation";
import {translate} from "../locale/i18n";
import axios from "axios";

class AdjustDoseTime extends Component {

    constructor(props) {
        super(props);
        this.state = {
            reset: false,
            assign: false,
            eight: "0",
            one: "1",
            six: "2",
            ten: "3",
            freq1: [],
            freq2: [],
            freq3: [],
            freq4: [],
            user_med_list_loading: false,
            eightDate: undefined,
            oneDate: undefined,
            sixDate: undefined,
            tenDate: undefined,
            dateValidation: false,
            meds: []
        }
    }

    async componentDidMount() {
        /*this.setState({
            freq1: this.props.navigation.state.params.freq1,
            freq2: this.props.navigation.state.params.freq2,
            freq3: this.props.navigation.state.params.freq3,
            freq4: this.props.navigation.state.params.freq4
        });*/

        await AsyncStorage.getAllKeys((err, result) => {
            console.log("Result IS : " + JSON.stringify(result));
        });
        /*await AsyncStorage.getItem("eight", (err, value) => {
            if (value != null)
                this.setState({eight: value});
        });
        await AsyncStorage.getItem("one", (err, value) => {
            if (value != null)
                this.setState({one: value});
        });
        await AsyncStorage.getItem("six", (err, value) => {
            if (value != null)
                this.setState({six: value});
        });
        await AsyncStorage.getItem("ten", (err, value) => {
            if (value != null)
                this.setState({ten: value});
        });*/

        var today = new Date();
        /*this.setState({eightDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.eight.split(":")[0], this.state.eight.split(":")[1], 0)});
        this.setState({oneDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.one.split(":")[0], this.state.one.split(":")[1], 0)});
        this.setState({sixDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.six.split(":")[0], this.state.six.split(":")[1], 0)});
        this.setState({tenDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.ten.split(":")[0], this.state.ten.split(":")[1], 0)});
*/
        /* AsyncStorage.getItem("freq1", (err, result) => {
             this.setState({freq1: JSON.parse(result)});
             console.log("FREQ1 Result : " + JSON.parse(result));
         });
         AsyncStorage.getItem("freq2", (err, result) => {
             this.setState({freq2: JSON.parse(result)});
             console.log("FREQ2 Result : " + JSON.parse(result));
         });
         AsyncStorage.getItem("freq3", (err, result) => {
             this.setState({freq3: JSON.parse(result)});
             console.log("FREQ3 Result : " + JSON.parse(result));
         });
         AsyncStorage.getItem("freq4", (err, result) => {
             this.setState({freq4: JSON.parse(result)});
             console.log("FREQ4 Result : " + JSON.parse(result));
         });*/
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userMedicationListData) {
            console.log(" nextProps.userMedicationListData.meds " + this.state.meds);

            this.setState({meds: nextProps.userMedicationListData.meds});

            nextProps.userMedicationListData.meds &&
            nextProps.userMedicationListData.meds.map(async med => {
                var today = new Date();
                if (med.freq === "1") {
                    if (!this.state.freq1.includes(med.idx))
                        this.setState(prevState => ({
                            freq1: [...prevState.freq1, med.idx]
                        }));

                    await AsyncStorage.getItem("eight", (err, value) => {
                        if (value != null)
                            this.setState({eight: value});
                    });

                    this.setState({eightDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.eight.split(":")[0], this.state.eight.split(":")[1], 0)});

                } else if (med.freq === "2") {
                    if (!this.state.freq2.includes(med.idx))
                        this.setState(prevState => ({
                            freq2: [...prevState.freq2, med.idx]
                        }));
                    await AsyncStorage.getItem("one", (err, value) => {
                        if (value != null)
                            this.setState({one: value});
                    });
                    this.setState({oneDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.one.split(":")[0], this.state.one.split(":")[1], 0)});

                } else if (med.freq === "3") {
                    if (!this.state.freq3.includes(med.idx))
                        this.setState(prevState => ({
                            freq3: [...prevState.freq3, med.idx]
                        }));
                    await AsyncStorage.getItem("six", (err, value) => {
                        if (value != null)
                            this.setState({six: value});
                    });
                    this.setState({sixDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.six.split(":")[0], this.state.six.split(":")[1], 0)});

                } else if (med.freq === "4") {
                    if (!this.state.freq4.includes(med.idx))
                        this.setState(prevState => ({
                            freq4: [...prevState.freq4, med.idx]
                        }));

                    await AsyncStorage.getItem("ten", (err, value) => {
                        if (value != null)
                            this.setState({ten: value});
                    });
                    this.setState({tenDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state.ten.split(":")[0], this.state.ten.split(":")[1], 0)});
                }
            });
        }
    }

    static navigationOptions = {
        header: null,
    };

    checkDoseTimeValidations = () => {
        if (this.state.tenDate) {
            return (this.state.eightDate < this.state.oneDate) && (this.state.oneDate < this.state.sixDate)
                && (this.state.sixDate < this.state.tenDate) &&
                (this.state.eight !== this.state.one !== this.state.six !== this.state.ten);
        } else if (this.state.sixDate) {
            return (this.state.eightDate < this.state.oneDate) && (this.state.oneDate < this.state.sixDate) &&
                (this.state.eight !== this.state.one !== this.state.six);
        } else if (this.state.oneDate) {
            return (this.state.eightDate < this.state.oneDate) && (this.state.eight !== this.state.one);
        } else {
            return (this.state.eightDate && this.state.eight);
        }
        /*return (this.state.eightDate && this.state.oneDate && (this.state.eightDate < this.state.oneDate)) &&
            (this.state.oneDate && this.state.sixDate && (this.state.oneDate < this.state.sixDate))
            && (this.state.sixDate && this.state.tenDate && (this.state.sixDate < this.state.tenDate)) &&
            (this.state.eight !== this.state.one !== this.state.six !== this.state.ten);*/

    };

    subtractTime = (name) => {
        var today = new Date();
        var date = new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state[name].split(":")[0], this.state[name].split(":")[1], 0);
        console.log("Old Date : " + date);
        if (!(this.state[name].split(":")[0] === "0" && this.state[name].split(":")[1] === "10")) {
            if (this.state[name].split(":")[0] === "23" && this.state[name].split(":")[1] === "59")
                date = moment(date).subtract(9, 'minutes').toDate();
            else
                date = moment(date).subtract(10, 'minutes').toDate();

            this.setState({[`${name}Date`]: date});

            console.log("New Date : " + date);
            this.setState({[name]: `${date.getHours()}:${date.getMinutes() === 0 ? '00' : date.getMinutes()}`});
        }
    };

    addTime = (name) => {
        var today = new Date();
        var date = new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.state[name].split(":")[0], this.state[name].split(":")[1], 0);
        console.log("Old Date : " + date);
        if (!(this.state[name].split(":")[0] === "23" && this.state[name].split(":")[1] === "59")) {
            if (this.state[name].split(":")[0] === "23" && this.state[name].split(":")[1] === "50")
                date = moment(date).add(9, 'minutes').toDate();
            else
                date = moment(date).add(10, 'minutes').toDate();
            // date = moment(date).add(10, 'minutes').toDate();

            this.setState({[`${name}Date`]: date});

            console.log("New Date : " + date);
            this.setState({[name]: `${date.getHours()}:${date.getMinutes() === 0 ? '00' : date.getMinutes()}`});
        }
    };

    updateMedsWithDoseTimes = async () => {
        console.log(" Meds before Update : " + JSON.stringify(this.state.meds));

        await this.state.meds.map(med => {
            if (med.freq === "4") {
                med.dosetime1 = this.state.eight;
                med.dosetime2 = this.state.one;
                med.dosetime3 = this.state.six;
                med.dosetime4 = this.state.ten;

                med.assignedtimes = [this.state.eight, this.state.one, this.state.six, this.state.ten];
            } else if (med.freq === "3") {
                med.dosetime1 = this.state.eight;
                med.dosetime2 = this.state.one;
                med.dosetime3 = this.state.six;

                med.assignedtimes = [this.state.eight, this.state.one, this.state.six];
            } else if (med.freq === "2") {
                med.dosetime1 = this.state.eight;
                med.dosetime2 = this.state.one;

                med.assignedtimes = [this.state.eight, this.state.one];
            } else if (med.freq === "1") {
                med.dosetime1 = this.state.eight;

                med.assignedtimes = [this.state.eight];
            }
            med.assign = 0;
        });

        console.log(" Updated Meds Are : " + JSON.stringify(this.state.meds));

        return axios.post(
            API_BASE_URL +
            `/updatemedication.php?version=3.0.0&data={"phone":"${this.state.phone}","otptoken":"${this.state.otpToken}","meds":${JSON.stringify(
                this.state.meds
            )}}`,
            {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }
        );
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <HeaderView name={translate("adjust_dosetime.adjust_dosetime")} navigation={this.props.navigation}/>
                <ScrollView>
                    <View style={{flexDirection: 'column'}}>
                        <NavigationEvents
                            onWillFocus={payload => {
                                console.log("will focus", payload);
                            }}
                            onDidFocus={payload => {
                                console.log("did focus", payload);
                                this.setState({
                                    freq1: [],
                                    freq2: [],
                                    freq3: [],
                                    freq4: []
                                });
                                this.props.OnFetchUserMedicationList();
                            }}
                            onWillBlur={payload => {
                                console.log("will blur", payload);
                                // this.props.OnFetchUserMedicationList();
                            }}
                            onDidBlur={payload => {
                                console.log("did blur", payload);
                            }}
                        />
                        <View style={{height: 50, flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{flex: 0.5, textAlign: 'center', color: colors.blackColor, padding: 10}}
                                  onPress={() => {
                                      this.setState({reset: true});
                                  }}>
                                {translate("re_assign_meds_screen.reset")}
                            </Text>
                            <View style={{width: 1, backgroundColor: colors.greyColor, height: 50}}/>
                            <Text style={{flex: 0.5, textAlign: 'center', color: colors.blackColor, padding: 10}}
                                  onPress={() => {
                                      this.setState({assign: true});
                                  }}>
                                {translate("re_assign_meds_screen.accept")}
                            </Text>
                        </View>
                    </View>
                    <View style={{height: 1, backgroundColor: colors.greyColor, marginBottom: 10}}/>
                    {/* <FlatList
                        style={{margin: 20}}
                        showsHorizontalScrollIndicator={false}
                        data={this.state.data}
                        numColumns={1}
                        renderItem={({item}) => (
                            <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Text
                                        style={{
                                            color: colors.blackColor,
                                            flex: 1,
                                            padding: 10,
                                            fontSize: 16
                                        }}>{item.title}</Text>
                                    <View style={styles.listItemStyle}>
                                        <Text style={styles.plusMinusStyle} onPress={() => {

                                        }}> - </Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={{fontSize: 16}}>{item.time}</Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.plusMinusStyle} onPress={() => {

                                        }}> + </Text>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        height: 1,
                                        backgroundColor: colors.greyColor,
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}/>
                            </View>
                        )}/>*/}

                    {this.props.user_med_list_loading && (
                        <View
                            style={{
                                justifyContent: "center",
                                alignItems: "center",
                                flexDirection: "row"
                            }}
                        >
                            <ActivityIndicator size="small" animating/>
                            <Text style={{marginLeft: 15, fontWeight: "600", color: colors.blackColor}}>
                                {translate("adjust_dosetime.fetching_dose")}
                            </Text>
                        </View>
                    )}

                    {
                        (this.state.freq1.length > 0 || this.state.freq2.length > 0 || this.state.freq3.length > 0 || this.state.freq4.length > 0) ?
                            <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Text
                                        style={{
                                            color: colors.blackColor,
                                            flex: 1,
                                            padding: 10,
                                            fontSize: 16
                                        }}>{translate("adjust_dosetime.dose1")}</Text>
                                    <View style={styles.listItemStyle}>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.subtractTime('eight');
                                        }}> - </Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.timeStyle}>{this.state.eight}</Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.addTime('eight');
                                        }}> + </Text>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        height: 1,
                                        backgroundColor: colors.greyColor,
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}/>
                            </View>
                            : null
                    }
                    {
                        (this.state.freq2.length > 0 || this.state.freq3.length > 0 || this.state.freq4.length > 0) ?
                            <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Text
                                        style={{
                                            color: colors.blackColor,
                                            flex: 1,
                                            padding: 10,
                                            fontSize: 16
                                        }}>{translate("adjust_dosetime.dose2")}</Text>
                                    <View style={styles.listItemStyle}>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.subtractTime('one');
                                        }}> - </Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.timeStyle}>{this.state.one}</Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.addTime('one');
                                        }}> + </Text>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        height: 1,
                                        backgroundColor: colors.greyColor,
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}/>
                            </View>
                            : null
                    }

                    {
                        (this.state.freq3.length > 0 || this.state.freq4.length > 0) ?
                            <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Text
                                        style={{
                                            color: colors.blackColor,
                                            flex: 1,
                                            padding: 10,
                                            fontSize: 16
                                        }}>{translate("adjust_dosetime.dose3")}</Text>
                                    <View style={styles.listItemStyle}>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.subtractTime('six');
                                        }}> - </Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.timeStyle}>{this.state.six}</Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.addTime('six');
                                        }}> + </Text>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        height: 1,
                                        backgroundColor: colors.greyColor,
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}/>
                            </View> : null
                    }

                    {
                        this.state.freq4.length > 0 ?
                            <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Text
                                        style={{
                                            color: colors.blackColor,
                                            flex: 1,
                                            padding: 10,
                                            fontSize: 16
                                        }}>{translate("adjust_dosetime.dose4")}</Text>
                                    <View style={styles.listItemStyle}>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.subtractTime('ten');
                                        }}> - </Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.timeStyle}>{this.state.ten}</Text>
                                        <View style={{width: 10, backgroundColor: colors.greyColor}}/>
                                        <Text style={styles.plusMinusStyle} onPress={async () => {
                                            await this.addTime('ten');
                                        }}> + </Text>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        height: 1,
                                        backgroundColor: colors.greyColor,
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}/>
                            </View> : null
                    }

                    {/*This is Reset Dialog*/}
                    <View>
                        <Dialog visible={this.state.reset}>
                            <DialogContent>
                                <Icon name="x" size={30} color={colors.greyColor}
                                      style={{alignSelf: 'flex-end', marginTop: 10}}
                                      onPress={() => {
                                          this.setState({reset: false});
                                      }}/>
                                <Text style={{
                                    fontSize: 22,
                                    color: colors.blueButtonColor,
                                    marginTop: 10,
                                    backgroundColor: colors.whiteColor,
                                    alignSelf: 'center'
                                }}>
                                    {translate("adjust_dosetime.med_monitor_reset_meds")}
                                </Text>

                                <Text style={{alignSelf: 'center', padding: 10, color: colors.blackColor}}>
                                    {translate("adjust_dosetime.reset_and_unassigned_dose")}
                                </Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({eight: "08:00", one: "13:00", six: "18:00", ten: "22:00"});

                                        AsyncStorage.setItem("eight", "08:00");
                                        AsyncStorage.setItem("one", "13:00");
                                        AsyncStorage.setItem("six", "18:00");
                                        AsyncStorage.setItem("ten", "22:00");

                                        var today = new Date();
                                        this.setState({eightDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), "08", "00", 0)});
                                        this.setState({oneDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), "13", "00", 0)});
                                        this.setState({sixDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), "18", "00", 0)});
                                        this.setState({tenDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), "22", "00", 0)});

                                        this.setState({reset: false});
                                    }}
                                    style={styles.buttonStyle}>
                                    <Text style={styles.textStyle}>{translate("re_assign_meds_screen.reset")}</Text>
                                </TouchableOpacity>
                            </DialogContent>
                        </Dialog>
                    </View>

                    {/*This is Assign Dialog*/}
                    <View>
                        <Dialog visible={this.state.assign}>
                            <DialogContent>
                                <Icon name="x" size={30} color={colors.greyColor}
                                      style={{alignSelf: 'flex-end', marginTop: 10}}
                                      onPress={() => {
                                          this.setState({assign: false});
                                      }}/>
                                <Text style={{
                                    fontSize: 22,
                                    color: colors.blueButtonColor,
                                    marginTop: 10,
                                    backgroundColor: colors.whiteColor,
                                    alignSelf: 'center'
                                }}>
                                    {translate("re_assign_meds_screen.med_monitor_assign_meds")}
                                </Text>

                                <Text style={{alignSelf: 'center', padding: 10, color: colors.blackColor}}>
                                    {translate("re_assign_meds_screen.all_meds_assigned")}
                                </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({assign: false});
                                        if (this.checkDoseTimeValidations()) {
                                            if (this.state.eight)
                                                AsyncStorage.setItem("eight", this.state.eight);
                                            if (this.state.one)
                                                AsyncStorage.setItem("one", this.state.one);
                                            if (this.state.six)
                                                AsyncStorage.setItem("six", this.state.six);
                                            if (this.state.ten)
                                                AsyncStorage.setItem("ten", this.state.ten);

                                            this.updateMedsWithDoseTimes().then(response => {

                                            }).catch(error => {

                                            });

                                            this.props.navigation.goBack();
                                        } else {
                                            this.setState({dateValidation: true});
                                        }
                                    }}
                                    style={styles.buttonStyle}>
                                    <Text style={styles.textStyle}>{translate("login.ok")}</Text>
                                </TouchableOpacity>
                            </DialogContent>
                        </Dialog>
                    </View>

                    {/*This is date validation dialog*/}
                    <View>
                        <Dialog visible={this.state.dateValidation} width={300}>
                            <DialogContent>
                                <Icon
                                    name="x"
                                    size={30}
                                    color={colors.greyColor}
                                    style={{alignSelf: "flex-end", marginTop: 10}}
                                    onPress={() => {
                                        this.setState({dateValidation: false});
                                    }}
                                />

                                <View
                                    style={{
                                        backgroundColor: colors.blueButtonColor,
                                        marginBottom: 10,
                                        marginTop: 5
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.whiteColor,
                                            fontSize: 16,
                                            alignSelf: "center",
                                            padding: 12
                                        }}
                                    >
                                        {translate("adjust_dosetime.med_monitor_adjust_times")}
                                    </Text>
                                </View>


                                <Text style={{padding: 12}}>
                                    {translate("adjust_dosetime.times_validation_message")}
                                </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({dateValidation: false});
                                    }}
                                    style={{
                                        backgroundColor: colors.blueButtonColor,
                                        color: colors.whiteColor,
                                        marginTop: 8
                                    }}
                                >
                                    <Text
                                        style={[styles.textStyle, {fontSize: 18}]}
                                        onPress={() => {
                                            this.setState({dateValidation: false});
                                        }}
                                    >
                                        {translate("login.ok")}
                                    </Text>
                                </TouchableOpacity>
                            </DialogContent>
                        </Dialog>
                    </View>
                </ScrollView>

                <FooterView isSummary={true}
                            navigation={this.props.navigation}/>

            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        userMedicationListData:
        state.userMedicationListReducer.UsermedicationListData,
        user_med_list_loading: state.userMedicationListReducer.loading,
        user_med_list_error: state.userMedicationListReducer.error
    };
};
const styles = StyleSheet.create({
    listItemStyle: {
        flexDirection: 'row',
        borderRadius: 30,
        elevation: 3,
        alignItems: 'center',
        padding: 5,
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 1,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.4,
        shadowRadius: 2
    },
    plusMinusStyle: {
        padding: 3,
        color: colors.blueButtonColor,
        fontSize: 20
    },
    buttonStyle: {
        backgroundColor: colors.blueButtonColor,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 30,
        color: colors.whiteColor,
        marginBottom: 20,
        marginTop: 20
    },
    textStyle: {
        color: colors.whiteColor,
        fontSize: 20,
        alignSelf: 'center',
        padding: 12
    },
    timeStyle: {
        width: 50, fontSize: 16, textAlign: 'center', color: colors.blackColor
    }
});
const mapDispatchToProps = dispatch => {
    return {
        OnFetchUserMedicationList: () =>
            dispatch(UserMedicationListActions.fetchUserMedicationList())
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdjustDoseTime);
