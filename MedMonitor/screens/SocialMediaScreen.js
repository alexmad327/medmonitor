import React, {Component} from "react";
import {
    FlatList,
    Image,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View,
    ActivityIndicator, AsyncStorage
} from "react-native";
import {FooterView} from "../container/FooterView";
import {colors, headerTitleStyle} from "../constants/Constants";
import Icon from "react-native-vector-icons/FontAwesome";
import {HeaderView} from "../container/HeaderView";
import * as SocialMessagesListActions from "../store/actions/SocialMessagesActions";
import {connect} from "react-redux";
import moment from "moment";
import {CommonActions} from "../store/actions/CommonRequestAction";
import I18n, {translate} from "../locale/i18n";

class SocialMediaScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reset: false,
            assign: false,
            messages: [],
            loadingMessage: `${translate("social_media.fetching_messages")}`,
            liking: false,
            sortTitle: `${translate("social_media.message_sort_by_address")}`,
            sortMessages: [],
            phone: "",
            otpToken: ""
        };
    }

    async componentDidMount() {
        let phone = await AsyncStorage.getItem("phone");
        let otp = await AsyncStorage.getItem("otpToken");

        this.setState({phone: phone, otpToken: otp});
        this.props.OnFetchSocialMessagesList();
    }

    componentWillReceiveProps(nextProps) {
        console.log("Next Props Messages", nextProps.socialMessagesListData);

        if (nextProps.socialMessagesListData !== null) {
            this.setState({
                messages: nextProps.socialMessagesListData.messages,
                sortMessages: nextProps.socialMessagesListData.messages
            });

            if (this.state.sortTitle === `${translate("social_media.message_sort_by_date")}`) {
                this.sortMessagesByName();
            } else {
                this.sortMessagesById();
            }
        }
    }

    async sortMessagesByName() {
        await this.state.sortMessages.sort((a, b) => {
            var nameA = a.to.toLowerCase(), nameB = b.to.toLowerCase();
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });
        this.setState({messages: this.state.sortMessages})
    }

    async sortMessagesById() {

        await this.state.sortMessages.sort((a, b) => {
            if (a.id < b.id) //sort string ascending
                return -1;
            if (a.id > b.id)
                return 1;
            return 0;
        });
        this.setState({messages: this.state.sortMessages});
    }

    LikeMessage = item => {
        console.log("Item", item.id);
        this.setState(
            {
                [`liking${item.id}`]: true,
                loadingMessage: "Liking"
            },
            () => {
                let data = {
                    phone: `${this.state.phone}`,
                    otptoken: `${this.state.otpToken}`,
                    userType: "Patient",
                    parentIDX: item.id,
                    timestamp: moment().format("YYYY-MM-DD hh:mm:ss")
                };

                let url = `/likesocialmediamessages.php?version=3.0.0&data=${JSON.stringify(data)}`;
                let method = "POST";

                CommonActions(url, method).then(res => {
                    console.log("YESSSSSSSSSS", res);
                    this.props.OnFetchSocialMessagesList();

                    this.setState({
                        [`liking${item.id}`]: false
                        // loadingMessage: "Fetching Messages"
                    });
                });
            }
        );
    };
    static navigationOptions = {
        header: null
    };

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={{flex: 1}}>
                <HeaderView name={translate("social_media.social_media")} navigation={this.props.navigation}/>
                {/* <View
          style={{
            flexDirection: "row",
            borderRadius: 20,
            margin: 10,
            backgroundColor: colors.lightGreyColor,
            paddingLeft: 10,
            paddingRight: 10
          }}
        >
          <TextInput style={{ flex: 1 }} placeholder="Search" />
          <Icon
            name="search"
            color={colors.greyColor}
            size={20}
            style={{ alignSelf: "center" }}
          />
        </View> */}
                <View style={{flex: 1}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color: colors.blackColor, fontSize: 22, margin: 20, flex: 1}}>
                            {" "}
                            {this.state.sortTitle}
                        </Text>
                        <TouchableOpacity
                            style={{width: 30, height: 30, alignSelf: 'center', marginRight: 30}}
                            onPress={() => {
                                if (this.state.sortTitle === `${translate("social_media.message_sort_by_date")}`) {
                                    this.setState({sortTitle: `${translate("social_media.message_sort_by_address")}`});
                                    this.sortMessagesByName();
                                } else {
                                    this.setState({sortTitle: `${translate("social_media.message_sort_by_date")}`});
                                    this.sortMessagesById();
                                }
                            }}>
                            <Image style={{width: 30, height: 30, alignSelf: 'center'}}
                                   source={require('../images/app.png')}/>
                        </TouchableOpacity>
                    </View>

                    <View
                        style={{
                            height: 1,
                            backgroundColor: colors.blackColor,
                            marginLeft: 20,
                            marginRight: 20
                        }}
                    />
                    {this.props.loading && (
                        <View
                            style={{
                                alignItems: "center",
                                justifyContent: "center",
                                height: 300,
                                flexDirection: "column"
                            }}
                        >
                            <ActivityIndicator size="large" animating/>
                            <Text style={{fontWeight: "600"}}>
                                {this.state.loadingMessage + "...."}
                            </Text>
                        </View>
                    )}
                    <FlatList
                        extraData={this.state}
                        data={this.state.messages}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({item}) => (
                            <View>
                                <View style={{flexDirection: "column", marginLeft: 15, marginRight: 15, marginTop: 15}}>
                                    <View style={{flexDirection: "row"}}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                navigate("ChatScreen", {
                                                    name: item.to,
                                                    messages: item.messages,
                                                    subject: item.subject,
                                                    id: item.id,
                                                    getAllmessage: this.props.OnFetchSocialMessagesList(),
                                                    image:
                                                        "https://homepages.cae.wisc.edu/~ece533/images/zelda.png"
                                                });
                                            }}>
                                            <View style={{flexDirection: "row"}}>
                                                <Image
                                                    source={require("../images/launcher.png")}
                                                    style={{
                                                        height: 50,
                                                        width: 50,
                                                        borderRadius: 50 / 2,
                                                        borderWidth: 0,
                                                        borderColor: colors.greyColor
                                                    }}
                                                />
                                                <View style={{
                                                    flexDirection: "column",
                                                    marginLeft: 10,
                                                    justifyContent: 'center'
                                                }}>
                                                    <Text style={{color: colors.blackColor, fontSize: 16}}>
                                                        {item.to}
                                                    </Text>
                                                    <Text style={{fontSize: 14}}>{item.subject}</Text>
                                                    {/* <Text style={{ fontSize: 12 }}>{item.time}</Text> */}
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                        <View
                                            style={{
                                                width: "40%",
                                                justifyContent: "flex-end",
                                                flexDirection: "row"
                                            }}
                                        >
                                            {item.subject.indexOf("gold") !== -1 ? (
                                                <Image
                                                    source={require("../images/gold_social.png")}
                                                    style={{
                                                        height: 25,
                                                        width: 25,
                                                        marginTop: 20,
                                                        marginLeft: 5
                                                    }}
                                                />
                                            ) : (
                                                <Image
                                                    source={require("../images/silver_social.png")}
                                                    style={{
                                                        height: 25,
                                                        width: 25,
                                                        marginTop: 20,
                                                        marginLeft: 5
                                                    }}
                                                />
                                            )}
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => this.LikeMessage(item)}
                                                >
                                                    <Image
                                                        source={require("../images/thums_up.png")}
                                                        style={{
                                                            height: 25,
                                                            width: 25,
                                                            marginTop: 20,
                                                            marginLeft: 5
                                                        }}
                                                    />
                                                    <View
                                                        style={{
                                                            marginTop: -30,
                                                            marginLeft: 22,
                                                            height: 20,
                                                            width: 20,
                                                            borderRadius: 20 / 2,
                                                            backgroundColor: "red"
                                                        }}
                                                    >
                                                        {!this.state[`liking${item.id}`] !== undefined &&
                                                        this.state[`liking${item.id}`] === true ? (
                                                            <ActivityIndicator color="white" size="small"/>
                                                        ) : (
                                                            <Text
                                                                style={{
                                                                    textAlign: "center",
                                                                    color: colors.whiteColor,
                                                                    fontSize: 10
                                                                }}
                                                            >
                                                                {item.likes}
                                                            </Text>
                                                        )}
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                    <View
                                        style={{
                                            height: 1,
                                            backgroundColor: colors.greyColor,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                            </View>
                        )}
                    />
                </View>
                <FooterView isSummary={true} navigation={this.props.navigation}/>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.socialMessagesReducer.loading,
        error: state.socialMessagesReducer.error,
        socialMessagesListData: state.socialMessagesReducer.socialMessagesListData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        OnFetchSocialMessagesList: () =>
            dispatch(SocialMessagesListActions.fetchSocialMessagesList())
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SocialMediaScreen);
