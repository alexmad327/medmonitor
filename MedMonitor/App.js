import React, {Component} from 'react';
import {View} from 'react-native';
import MainNavigator from "./navigator/MainNavigator";
import SplashScreen from "react-native-splash-screen";
import {Provider} from 'react-redux';
import Centralstore from './store/StoreReducer';


export default class App extends Component {
    componentDidMount() {
        SplashScreen.hide();
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Provider store={Centralstore}>
                    <MainNavigator/>
                </Provider>
            </View>
        );
    }
}
