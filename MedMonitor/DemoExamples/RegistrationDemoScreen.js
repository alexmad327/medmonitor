import React, {Component} from 'react';
import {Image, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Card, CardItem, Container, Content} from 'native-base';

export class RegistrationDemoScreen extends Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        // const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1, margin: 10}}>
                <Container>
                    <Content>
                        <Card>
                            <CardItem header bordered>
                                <View style={{flex: 1}}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image
                                            style={{flex: 2}}
                                            source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
                                        />
                                        <View style={{flex: 8, flexDirection: 'column', marginLeft: 10}}>
                                            <Text style={{color: '#000000'}}>
                                                Benesalus Technology LLC
                                            </Text>
                                            <Text style={{color: '#000000', fontWeight: 'bold'}}>
                                                MedTracker
                                            </Text>
                                            <Text style={{color: '#000000', fontWeight: 'bold'}}>
                                                2.0.10
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem bordered>
                                <View style={{flex: 1}}>
                                    <Text>Phone Number</Text>
                                    <TextInput placeholder="9999999999"/>
                                    <View
                                        style={{
                                            flex: 1,
                                            borderBottomColor: '#dddcd9',
                                            borderBottomWidth: 1,
                                            marginTop: 10,
                                        }}
                                    />
                                    <TouchableOpacity
                                        style={{
                                            flex: 1, backgroundColor: '#ead23f', marginTop: 20, marginBottom: 10,
                                            marginLeft: 20, marginRight: 20, borderRadius: 5
                                        }}>
                                        <Text style={styles.textStyle}> Get Registration Code</Text>
                                    </TouchableOpacity>
                                    <View
                                        style={{
                                            flex: 1,
                                            borderBottomColor: '#dddcd9',
                                            borderBottomWidth: 1,
                                            marginTop: 10,
                                        }}
                                    />
                                </View>
                            </CardItem>
                            <CardItem bordered>
                                <Text style={{fontStyle: 'italic', color: '#000000', fontSize: 16}}>
                                    <Text>By clicking on registry you are agreeing to our </Text>
                                    <Text style={{color: 'blue', textDecorationLine: 'underline'}}>Privacy
                                        Policy. </Text>
                                    <Text>and </Text>
                                    <Text style={{color: 'blue', textDecorationLine: 'underline'}}>Terms &
                                        Conditions</Text>
                                </Text>
                            </CardItem>
                            <CardItem bordered>
                                <Text style={{color: '#000000'}}>
                                    Help: support@benesalustech.com
                                </Text>
                            </CardItem>
                        </Card>
                    </Content>
                </Container>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textStyle: {
        color: '#FFFFFF',
        fontSize: 15,
        alignSelf: 'center',
        padding: 12
    }
});
